require('dotenv').config();

let dbUrl;
switch (process.env.NODE_ENV) {
  case 'demo':
    dbUrl = process.env.DBURL_DEMO;
    break;
  case 'prod':
    dbUrl = process.env.DBURL_PROD;
    break;
  case 'staging':
    dbUrl = process.env.DBURL_STAGING;
    break;
  case 'localhost':
    dbUrl = 'mongodb://localhost/app';
    break;
  case 'test':
    dbUrl = 'mongodb://localhost/app_test';
    break;
  default:
    dbUrl = 'mongodb://localhost/app';
}

let hostName;
switch (process.env.NODE_ENV) {
  case 'demo':
    hostName = process.env.HOST_NAME;
    break;
  case 'localhost':
    hostName = process.env.BOT_TOKEN;
    break;
  case 'test':
    hostName = 'localhost:8010';
    break;
  case 'prod':
    hostName = process.env.HOST_NAME;
    break;
  case 'staging':
    hostName = process.env.HOST_NAME;
    break;
  default:
    hostName = 'localhost:8000';
}

let bot_token;
switch (process.env.NODE_ENV) {
  case 'prod':
    bot_token = '1065579794:AAGlBS45DfYuS6OE1yhgn9sIlcnnoBH-OFs';
    break;
  case 'staging':
    bot_token = '1097553166:AAG775X6tRemvJdlwR8OM22MjHflYuBkoQU';
    break;
  case 'demo':
    bot_token = '992164376:AAGnvA1FytQC9xVh9bWY8_19taPaxFlq8Pw';
    break;
  case 'test':
    bot_token = '917917047:AAHCwzpdP3Uc5lhUohhaOl6HrhVlvzlIqWI';
    break;
  case 'localhost':
    bot_token = process.env.BOT_TOKEN;
    break;
  default:
    bot_token = '917917047:AAHCwzpdP3Uc5lhUohhaOl6HrhVlvzlIqWI';
}

module.exports = {
  hostName,
  debug: process.env.DEBUG === 'true',

  dbUrl,
  mongoOptions: {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  },
  telegram: {
    bot_token
  },

  hrmHost: process.env.HRM_HOST_NAME,
  hrmSecretKey: process.env.SECRET_KEY_ATTRACTOR_REPORTING_BOT,
  hrmPayload: process.env.SECRET_ATTRACTOR_REPORTING_BOT_PAYLOAD,

  smtp: {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: false,
    auth: {
      user: process.env.SMTP_LOGIN,
      pass: process.env.SMTP_PASSWORD
    }
  }
};
