const Scene = require('telegraf/scenes/base');
const {Markup} = require("telegraf");
const logger = require('../../../logger');
const {
  isUserExist,
  isActive,
  isUserExistWithEmail,
  isUserExistWithPhone,
  getHrmUserByPhone,
  getHrmUserByEmail,
  createUser,
  getChooseAuthenticateBtns,
} = require('./startHelpers');
const {
  sendConfirmationCodeToEmail,
  generateConfirmationCode,
  deleteUserByTelegramUserId
} = require('../utils/helpers');
const messages = require('./startMessages');
const sceneIds = require('../sceneIds');


const start = new Scene(sceneIds.START);


start.enter(async ctx => {
  logger.debug(ctx, 'User in start scene!');

  if (await isUserExist(ctx.from.id) && await isActive(ctx.from.id)){
    await ctx.scene.enter(sceneIds.PROJECTS)
  } else {
    await ctx.reply(messages.CHOOSE_AUTH_WITH, getChooseAuthenticateBtns())
  }
});


start.on('message', async ctx => {
  const text = ctx.message.text;

  switch (text) {
    case '/start':
      await ctx.reply('_', Markup.removeKeyboard(true).extra());
      await ctx.scene.reenter();
      break;


    case messages.button.EXIT:
      await deleteUserByTelegramUserId(ctx.from.id);
      await ctx.reply(messages.SUCCESS_EXITED, Markup.removeKeyboard(true).extra());
      await ctx.scene.leave();
      break;


    case messages.button.AUTH_WITH_EMAIL:
      await ctx.reply(messages.ENTER_YOUR_EMAIL);
      break;


    case undefined:
      if (ctx.message.contact){
        const phoneNumber = ctx.message.contact.phone_number;
        if (await isUserExistWithPhone(phoneNumber)){
          await ctx.reply(messages.USER_WITH_THIS_NUMBER_ALREADY_EXIST);
          return
        }

        const hrmUserByPhone = await getHrmUserByPhone(phoneNumber);
        if (hrmUserByPhone){
          await createUser({
            ...hrmUserByPhone,
            telegramUserId: ctx.from.id,
            telegramChatId: ctx.chat.id,
            active: true
          });
          logger.debug(ctx, 'New registered user.');
          await ctx.reply(messages.WELCOME, Markup.removeKeyboard(true).extra());
          await ctx.scene.enter(sceneIds.PROJECTS)
        } else {
          await ctx.reply(messages.CANT_FIND_ACTIVE_USER_BY_NUMBER)
        }
      }

      break;


    default:
      const email = ctx.message.text;
      if (await isUserExistWithEmail(email)){
        await ctx.reply(messages.USER_WITH_THIS_EMAIL_ALREADY_EXIST);
        return
      }
      logger.debug(ctx, `User entered email address: ${email}`);
      const hrmUser = await getHrmUserByEmail(email);
      if (hrmUser){
        logger.debug(ctx, `User found by email address: ${hrmUser.email}`);
        await createUser({
          ...hrmUser,
          telegramUserId: ctx.from.id,
          telegramChatId: ctx.chat.id,
          active: false
        });
        const confirmCode = generateConfirmationCode();
        try{
          await sendConfirmationCodeToEmail(email, confirmCode);
          await ctx.reply(messages.WE_SENT_MAIL_TO_YOUR_EMAIL);
          await ctx.scene.enter(sceneIds.CONFIRM_EMAIL, {email: email, confirmCode: confirmCode});
          logger.debug(ctx, `A letter with the code was sent to the mail: ${hrmUser.email}`);
        } catch (e) {
          await deleteUserByTelegramUserId(ctx.from.id);
          await ctx.reply(messages.ERROR_WHEN_SEMD_EMAIL);
          logger.error(ctx, 'Error when send email.', e)
        }
      } else {
        await ctx.reply(messages.CANT_FIND_ACTIVE_USER_BY_EMAIL)
      }
  }
});

module.exports = start;
