const {Markup} = require("telegraf");
const axios = require('axios');
const jwt = require('jsonwebtoken');

const logger = require('../../../logger');
const config = require('../../../config');
const User = require('../../../models/User');
const messages = require('./startMessages');


function getChooseAuthenticateBtns() {
  return Markup.keyboard(
    [
      [{
        text: messages.button.SEND_MY_PHONE,
        request_contact: true
      }],
      [{
        text: messages.button.AUTH_WITH_EMAIL
      }],
      [{
        text: messages.button.EXIT
      }]
    ]
  ).resize().extra()
}

async function createUser(userData) {
  try {
    const user = await User(userData);
    await user.save();
    logger.debug(undefined, `Created a new user for the bot. Name: ${userData.name}, and mail: ${userData.email}`);
    return user
  } catch (e) {
    logger.error(undefined, 'New user not created.', e);
  }
}

async function isUserExist(telegramUserId) {
  let payload = {'payload': config.hrmPayload};
  let token = jwt.sign(payload, config.hrmSecretKey, {algorithm: 'HS256'});

  try {
    const user = await User.findOne({telegramUserId});
    if (user.phoneNumber !== null && user.phoneNumber !== '') {
      const hrmPhoneUser = await axios.get(config.hrmHost + `/api/v2/attractor-reporting-bot/employee?phone=${user.phoneNumber}`, {headers: {Authorization: token}});
      if (user.hrmUserId === hrmPhoneUser.data.id) {
        return !!user;
      } else {
        return await User.findByIdAndUpdate(user._id, {
          hrmUserId: hrmPhoneUser.data.id,
          email: hrmPhoneUser.data.email,
          name: hrmPhoneUser.data.name,
        });
      }
    } else {
      const hrmEmailUser = await axios.get(config.hrmHost + `/api/v2/attractor-reporting-bot/employee?email=${user.email}`, {headers: {Authorization: token}});
      if (user.hrmUserId === hrmEmailUser.data.id) {
        return !!user;
      } else {
        let phone = hrmEmailUser.data.phone;
        if (phone === null) {
          phone = '';
        } else if (phone) {
          phone = hrmEmailUser.data.phone[0] === '+' ?
            hrmEmailUser.data.phone : "+" + hrmEmailUser.data.phone;
        }
        return await User.findByIdAndUpdate(user._id, {
          hrmUserId: hrmEmailUser.data.id,
          phoneNumber: phone,
          name: hrmEmailUser.data.name,
        });
      }
    }
  } catch (e) {
    logger.error(
      undefined,
      'HRM sent error.',
      e
    );
    return null
  }

}

async function isActive(telegramUserId) {
  const user = await User.findOne({telegramUserId});
  return !!(user && user.active);
}

async function getHrmUserByPhone(phoneNumber) {
  let payload = {'payload': config.hrmPayload};
  let token = jwt.sign(payload, config.hrmSecretKey, {algorithm: 'HS256'});

  try {
    const response = await axios.get(config.hrmHost + `/api/v2/attractor-reporting-bot/employee?phone=${phoneNumber}`, {headers: {Authorization: token}});
    return {
      hrmUserId: response.data.id,
      name: response.data.name,
      email: response.data.email,
      phoneNumber: phoneNumber,
    }
  } catch (e) {
    logger.error(
      undefined,
      'HRM sent error.',
      {path: config.hrmHost + `/api/v2/attractor-reporting-bot/employee?phone=${phoneNumber}`},
      e
    );
    return null
  }
}

async function getHrmUserByEmail(email) {
  let payload = {'payload': config.hrmPayload};
  let token = jwt.sign(payload, config.hrmSecretKey, {algorithm: 'HS256'});

  try {
    const response = await axios.get(config.hrmHost + `/api/v2/attractor-reporting-bot/employee?email=${email}`, {headers: {Authorization: token}});
    let phone = response.data.phone;
    if (phone === null) {
      phone = '';
    } else if (phone) {
      phone = response.data.phone[0] === '+' ?
        response.data.phone : "+" + response.data.phone;
    }
    logger.debug(undefined, 'User found in HRM!');
    return {
      hrmUserId: response.data.id,
      name: response.data.name,
      email: response.data.email,
      phoneNumber: phone,
    }
  } catch (e) {
    logger.error(
      undefined,
      'HRM sent error.',
      {path: config.hrmHost + `/api/v2/attractor-reporting-bot/employee?email=${email}`},
      e
    );
    return null
  }
}

async function isUserExistWithEmail(email) {
  const user = await User.findOne({email: email});
  return !!user
}

async function isUserExistWithPhone(phone) {
  const user = await User.findOne({phoneNumber: phone});
  return !!user
}


module.exports = {
  isUserExist,
  isActive,
  getChooseAuthenticateBtns,
  createUser,
  getHrmUserByPhone,
  getHrmUserByEmail,
  isUserExistWithEmail,
  isUserExistWithPhone,
};
