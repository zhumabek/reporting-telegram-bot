module.exports = {
  REPORT_ALREADY_EXISTS: 'Вы уже сегодня создавали отчет по данному тикету',
  WANT_TO_EDIT_REPORT: 'Хотите отредактировать отчет?',
  ENTER_RECENT_REPORTS_SCENE: 'Перейти к редактированию отчета',
  SHOW_TICKET_INFO: (report, spentTime) => {
    return `Информация о тикете #${report.ticketNumber}: 
      Проект: ${report.projectName}
      Название: ${report.ticketName}
      Потрачено(ч): ${spentTime}
      Статус: in progress`
  },
  TIME_SPENT_EXCEEDS_LIMIT: (timeSpent = 0) => {
    return `Вы уже потратили ${timeSpent} часов из 12. Попробуйте еще раз`;
  },
  DESCRIPTION: 'Введите описание отчета',
  TIME_SPENT: 'Введите потраченное время',
  USE_NUMBERS: 'Ошибка: Введите заново только в цифрах',
  MUST_BE_NON_ZERO_VALUE: 'Ошибка: должен быть больше нуля',
  WRONG_TIME: 'Ошибка: Максимум 12 часов.',
  TICKET_STATUS: 'Выберите статус тикета',
  SEND: 'Вы хотите отправить отчет?',
  WRONG_STATUS: 'Ошибка: Несуществующий статус',
  FINAL_REPORT: 'Готовый отчет:',
  WRONG_ACTION: 'Ошибка: Неправильное действие',
};