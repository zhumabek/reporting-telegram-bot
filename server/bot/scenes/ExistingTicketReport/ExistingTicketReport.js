const WizardScene = require('telegraf/scenes/wizard');
const logger = require('../../../logger');

const sceneIds = require('../sceneIds');
const messages = require('./ExistingTicketReportMessages');
const reportMessages =require('../utils/reportMessages');
const ProjectManagementTool = require('../../../repositories/pmt/index');
const helpers = require('../utils/helpers');
const {
  getEnterRecentReportsButton,
  returnProjectId,
  actions
} = require('./ExistingTicketReportHelpers');

const existingTicketReportScene = new WizardScene(
  sceneIds.EXISTING_TICKET_REPORT,

  async ctx => {
    logger.debug(ctx, 'In existing ticket report.');
    ctx.wizard.state.messagesToDelete = ctx.wizard.state.messagesToDelete || [];
    ctx.wizard.state.messagesToDeleteAfterScene = [];
    const ticketId = ctx.scene.state.ticketId;
    const projectId = await returnProjectId(ticketId);
    const pmt = await ProjectManagementTool(projectId, ctx.from.id).create();
    ctx.scene.state.pmt = pmt;
    const ticket = await pmt.getTicketById(ticketId);

    ctx.wizard.state.report = {
      projectId: projectId,
      projectName: pmt.project.name,
      ticketId: ticketId,
      userId: pmt.user._id,
      ticketNumber: ticket.number,
      ticketName: ticket.name,
      description: '',
      timeSpent: 0,
      ticketStatus: '',
    };
    const totalTimeSpentForToday = await pmt.getUserTodayTimeSpent();
    if( totalTimeSpentForToday >= helpers.SPENT_TIME_LIMIT){
      logger.debug(ctx, 'Total spent time for to day reached limit');

      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(
          reportMessages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT,
          helpers.getTotalSpentTimeReachedLimitButtons()
        )
      );
      await ctx.wizard.next();

    } else {
      ctx.wizard.selectStep(1);
      return existingTicketReportScene.middleware()(ctx);
    }
  },

  async ctx => {
    const pmt = ctx.scene.state.pmt;
    const ticketId = ctx.scene.state.ticketId;
    const today = new Date().setHours(0, 0, 0, 0);
    const todayReport = await pmt.findReport({ticketId: ticketId, date:{$gt: today}});

    if(todayReport.length) {
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(messages.REPORT_ALREADY_EXISTS, helpers.getBackButton())
      );
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(messages.WANT_TO_EDIT_REPORT, getEnterRecentReportsButton(ticketId))
      );
    } else {
      const ticketSpentTime = await pmt.getTicketSpentTime(ticketId);
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(messages.SHOW_TICKET_INFO(ctx.wizard.state.report, ticketSpentTime))
      );
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(messages.DESCRIPTION, helpers.getBackButton())
      );
      await ctx.wizard.next()
    }
  },

  async ctx => {
    ctx.wizard.state.report.description = await ctx.message.text;
    ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.TIME_SPENT));
    await ctx.wizard.next();
  },

  async ctx => {
    const pmt = ctx.scene.state.pmt;
    let timeSpent = ctx.message.text;
    let totalTimeSpent = await pmt.getUserTodayTimeSpent();

    if(isNaN(timeSpent)) {
      ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.USE_NUMBERS));
      await helpers.goBack(ctx);

    } else if(timeSpent <= 0) {
      ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.MUST_BE_NON_ZERO_VALUE));
      await helpers.goBack(ctx);

    } else if(await helpers.checkIfTimeSpentExceedsLimit(timeSpent, totalTimeSpent)) {
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(messages.WRONG_TIME),
        await ctx.reply(messages.TIME_SPENT_EXCEEDS_LIMIT(totalTimeSpent))
    );
      await helpers.goBack(ctx);

    } else {
      ctx.wizard.state.report.timeSpent = timeSpent;
      ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.TICKET_STATUS, helpers.getStatusButtons()));
      await ctx.wizard.next();
    }
  },

  async ctx => {
    ctx.wizard.state.report.ticketStatus = await ctx.message.text;

    if(helpers.validTicketStatus(ctx.wizard.state.report.ticketStatus)) {
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(`${messages.FINAL_REPORT}\n ${helpers.getReport(ctx.wizard.state.report)}`),
        await ctx.reply(messages.SEND, helpers.getSendButtons())
      );
      await ctx.wizard.next();
    } else {
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(messages.WRONG_STATUS)
      );
      await helpers.goBack(ctx);
    }
  },

  async ctx => {
    const pmt = ctx.scene.state.pmt;
    if(ctx.message.text === reportMessages.NO_DONT_SEND_REPORT) {
      logger.debug(ctx, 'Existing create ticket report canceled.');
      await ctx.scene.enter(sceneIds.FINAL);

    } else if(ctx.message.text === reportMessages.YES_SEND_REPORT) {
      const newReport = await pmt.createReport(ctx.wizard.state.report);
      await pmt.updateTicket(newReport.ticketId, {status: newReport.ticketStatus});

      ctx.wizard.state.messagesToDeleteAfterScene.push(
        await ctx.reply(reportMessages.REPORT_CREATED)
      );
      await ctx.scene.enter(
        sceneIds.FINAL,
        {messagesToDelete: ctx.wizard.state.messagesToDeleteAfterScene}
        );
      logger.debug(ctx, 'Existing ticket report saved.');

    } else if(ctx.message.text === reportMessages.EDIT_CURRENT_REPORT) {
      const currentReport = ctx.wizard.state.report;
      await ctx.scene.enter(sceneIds.RECENT_REPORTS, {
        report: currentReport,
        messagesToDelete: ctx.wizard.state.messagesToDeleteAfterScene,
        fromExistingTicketReportEdit: true
      });

    } else {
      ctx.wizard.state.messagesToDelete.push(
        await ctx.reply(messages.WRONG_ACTION)
      );
      await helpers.goBack(ctx);
    }
  },
);

existingTicketReportScene.hears(reportMessages.GO_BACK_TO_PREVIOUS_SCENE, async ctx => {
  await ctx.scene.enter(sceneIds.EXISTING_TICKETS,
    {projectId: ctx.wizard.state.report.projectId});
});

existingTicketReportScene.hears(reportMessages.GO_TO_RECENT_REPORT_TICKETS, async ctx => {
  await ctx.scene.enter(sceneIds.RECENT_REPORT_TICKETS,
    {projectId: ctx.wizard.state.report.projectId});
});

existingTicketReportScene.action(RegExp(actions.ENTER_RECENT_REPORTS_SCENE), async ctx => {
  const pmt = ctx.scene.state.pmt;
  const ticketInfo = JSON.parse(ctx.callbackQuery.data);
  let today = new Date().setHours(0, 0, 0, 0);

  try {
    const currentUser = pmt.user;
    const report = await pmt.findOneReport({userId: currentUser._id, ticketId: ticketInfo.id, date: {$gt: today}});
    const ticket = await pmt.getTicketById(report.ticketId);
    if(report) {
      report.ticketNumber = ticket.number;
      report.ticketName = ticket.name;
      report.projectId = pmt.project._id;
      await ctx.scene.enter(
        sceneIds.RECENT_REPORTS,
        {ticketId: ticketInfo.id, report, fromExistingTicketReport: true}
        );
    }
  } catch (error) {
    logger.error(ctx, 'Error when find User or Report', error);
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(reportMessages.ERROR)
    );
  }
});

existingTicketReportScene.leave(async ctx => {
  await helpers.deleteMessages(ctx);
});

module.exports = existingTicketReportScene;