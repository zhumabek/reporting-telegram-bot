const User = require('../../../models/User');
const Ticket = require('../../../models/Ticket');
const Project = require('../../../models/Project');
const { Markup } = require("telegraf");
const messages = require('./ExistingTicketReportMessages');

const actions = {
  ENTER_RECENT_REPORTS_SCENE: 'enterRecentReport'
};

function getEnterRecentReportsButton(ticketId) {
  return Markup.inlineKeyboard([
    [Markup.callbackButton(messages.ENTER_RECENT_REPORTS_SCENE,
      JSON.stringify({a: actions.ENTER_RECENT_REPORTS_SCENE, id: ticketId})
    )],
  ]).extra();
}

async function returnProjectId(ticketId) {
  const ticket = await Ticket.findOne({_id: ticketId});
  return ticket.projectId;
}

async function returnUserId(ctx) {
  const loggedUser = await User.findOne({telegramUserId: ctx.from.id});
  return loggedUser._id;
}

async function returnTicketNumber(ctx) {
  const ticketId = await ctx.scene.state.ticketId;
  const ticket = await Ticket.findOne({_id: ticketId});
  return ticket.number;
}

async function returnTicketName(ctx) {
  const ticketId = await ctx.scene.state.ticketId;
  const ticket = await Ticket.findOne({_id: ticketId});
  return ticket.name;
}

async function returnProjectName(ctx) {
  const ticketId = await ctx.scene.state.ticketId;
  const ticket = await Ticket.findOne({_id: ticketId});
  const projectId = ticket.projectId;
  const project = await Project.findOne({_id: projectId});
  return project.name;
}

module.exports = {
  getEnterRecentReportsButton,
  returnProjectId,
  returnUserId,
  returnTicketNumber,
  returnTicketName,
  returnProjectName,
  actions,
};