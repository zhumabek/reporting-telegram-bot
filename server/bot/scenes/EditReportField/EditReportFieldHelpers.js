const User = require('../../../models/User');
const Ticket = require('../../../models/Ticket');
const Report = require('../../../models/Report');
const messages = require('./EditReportFieldMessages');
const {getBackButton, getStatusButtons} = require('../utils/helpers');

async function returnUserId(ctx) {
  const user = await User.findOne({telegramUserId: ctx.from.id});
  return user._id;
}

async function returnTicketNumber(reportId) {
  const report = await Report.findOne({_id: reportId});
  const ticket = await Ticket.findOne({_id: report.ticketId});
  return ticket.number;
}

async function returnTicketName(reportId) {
  const report = await Report.findOne({_id: reportId});
  const ticket = await Ticket.findOne({_id: report.ticketId});
  return ticket.name;
}

async function returnTicketId(reportId) {
  const report = await Report.findOne({_id: reportId});
  const ticket = await Ticket.findOne({_id: report.ticketId});
  return ticket._id;
}

async function returnRepDesc(reportId) {
  const report = await Report.findOne({_id: reportId});
  return report.description;
}

async function returnTimeSpent(reportId) {
  const report = await Report.findOne({_id: reportId});
  return report.timeSpent;
}

async function returnTicketStatus(reportId) {
  const report = await Report.findOne({_id: reportId});
  return report.ticketStatus;
}


function returnReportFieldInfo(fieldName, report){
  switch (fieldName) {
    case 'status': return `${messages.TICKET_STATUS}: ${report.ticketStatus}`
    case 'des': return `${messages.DESCRIPTION}: ${report.description}`
    case 'time': return `${messages.TIME_SPENT}: ${report.timeSpent}`
    default: return `${messages.DESCRIPTION}: ${report.description}`
  }
}

function getFieldActionMessage(fieldName){
  switch (fieldName) {
    case 'status': return messages.CHOOSE_TICKET_STATUS;
    case 'des': return messages.ENTER_DESCRIPTION;
    case 'time': return messages.ENTER_TIME_SPENT;
    default: return messages.ENTER_DESCRIPTION;
  }
}

function getFieldButtons(fieldName){
  return fieldName === 'status' ? getStatusButtons() : getBackButton();
}

async function updateStatus(reportId, newStatus){
  await Report.findOneAndUpdate({_id: reportId}, {ticketStatus: newStatus, editDate: new Date()});
  const ticketId = await returnTicketId(reportId);
  await Ticket.findOneAndUpdate({_id: ticketId}, {status: newStatus});
}

async function updateDescription(reportId, newDescription){
  await Report.findOneAndUpdate({_id: reportId}, {description: newDescription, editDate: new Date()});
}

async function updateTimeSpent(reportId, newTimeSpent){
  await Report.findOneAndUpdate({_id: reportId}, {timeSpent: newTimeSpent, editDate: new Date()});
}

module.exports = {
  returnUserId,
  returnTicketNumber,
  returnTicketName,
  returnTicketId,
  returnReportFieldInfo,
  getFieldActionMessage,
  getFieldButtons,
  updateStatus,
  updateDescription,
  updateTimeSpent
};