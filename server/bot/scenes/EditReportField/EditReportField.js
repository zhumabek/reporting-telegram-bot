const WizardScene = require('telegraf/scenes/wizard');
const logger = require('../../../logger');

const sceneIds = require('../sceneIds');
const messages =require('./EditReportFieldMessages');
const reportMessages =require('../utils/reportMessages');
const editReportHelpers = require('./EditReportFieldHelpers');
const { goBack, deleteMessages, changeScene, getTotalTimeSpent, checkIfTimeSpentExceedsLimit, getReportTimeSpent, resetScheduler } = require('../utils/helpers');

const editReportFieldScene = new WizardScene(sceneIds.EDIT_REPORT,
  async ctx => {
    logger.debug(ctx, 'In edit report field');
    ctx.wizard.state.messagesToDelete = ctx.wizard.state.messagesToDelete || [];
    ctx.wizard.state.messagesToDeleteAfterScene = [];
    let {report, field} = await ctx.scene.state;
    const currentReportFieldInfo = await editReportHelpers.returnReportFieldInfo(field, report);
    const fieldActionMessage = editReportHelpers.getFieldActionMessage(field);
    ctx.wizard.state.messagesToDelete.push(await ctx.reply(currentReportFieldInfo));
    ctx.wizard.state.messagesToDelete.push(await ctx.reply(fieldActionMessage, editReportHelpers.getFieldButtons(field)));
    await ctx.wizard.next();
  },
  async ctx => {
    let {report, field, fromExistingTicketReportEdit} = await ctx.scene.state;
    let {projectId, userId} = report;
    const message = ctx.message.text;
    switch (field) {
      case 'des':
        logger.debug(ctx, 'Edited report description.');
        report.description = message;
        return changeScene(ctx, sceneIds.RECENT_REPORTS, {...report});
      case 'status':
        logger.debug(ctx, 'Edited report status.');

        report.ticketStatus = message;
        return changeScene(ctx, sceneIds.RECENT_REPORTS, {...report});
      case 'time':
        const newTimeSpent = message;
        let totalTimeSpent = await getTotalTimeSpent(projectId, userId);
        let oldTimeSpent = 0;
        if(!fromExistingTicketReportEdit)
          oldTimeSpent = await getReportTimeSpent(report._id);

        if(isNaN(newTimeSpent)) {
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.USE_NUMBERS));
          await goBack(ctx);

        } else if (newTimeSpent <= 0) {
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.MUST_BE_NON_ZERO_VALUE));
          await goBack(ctx);

        } else if (fromExistingTicketReportEdit && await checkIfTimeSpentExceedsLimit(newTimeSpent, totalTimeSpent)) {
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.WRONG_TIME));
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.TIME_SPENT_EXCEEDS_LIMIT(totalTimeSpent)));
          await goBack(ctx);

        } else if (!fromExistingTicketReportEdit && await checkIfTimeSpentExceedsLimit(newTimeSpent, totalTimeSpent, oldTimeSpent)) {
            ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.WRONG_TIME));
            ctx.wizard.state.messagesToDelete.push(await ctx.reply(messages.TIME_SPENT_EXCEEDS_LIMIT(Math.abs(totalTimeSpent - oldTimeSpent))));
            await goBack(ctx);

        } else {
          logger.debug(ctx, 'Edited report spent time.');

          report.timeSpent = newTimeSpent;
          return changeScene(ctx, sceneIds.RECENT_REPORTS, {...report});
        }
    }
  }
);

editReportFieldScene.hears(reportMessages.GO_BACK_TO_PREVIOUS_SCENE, async ctx => {
  await changeScene(ctx, sceneIds.RECENT_REPORTS);
});

editReportFieldScene.leave(async ctx => {
  await deleteMessages(ctx);
});

module.exports = editReportFieldScene;