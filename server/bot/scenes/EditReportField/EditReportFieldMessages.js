module.exports = {
  TIME_SPENT_EXCEEDS_LIMIT: (timeSpent = 0) => {
    return `Вы уже потратили ${timeSpent} часов из 12. Попробуйте еще раз`;
  },
  DESCRIPTION: 'Нынешнее описание отчета',
  ENTER_DESCRIPTION: 'Введите новое описание отчета',
  TIME_SPENT: 'Потраченное время',
  ENTER_TIME_SPENT: 'Введите новое потраченное время',
  TICKET_STATUS: 'Нынешний статус тикета',
  CHOOSE_TICKET_STATUS: 'Выберите новый статус тикета',
  USE_NUMBERS: 'Ошибка: Введите заново только в цифрах',
  WRONG_TIME: 'Ошибка: Максимум - 12 часов',
  MUST_BE_NON_ZERO_VALUE: 'Ошибка: должен быть больше нуля',
  WRONG_STATUS: 'Ошибка: Несуществующий статус',
  WRONG_ACTION: 'Ошибка: Неправильное действие',
  REPORT: 'Предыдущий отчет:',
  FINAL_REPORT: 'Готовый отчет:',
  SEND: 'Вы хотите отправить отчет?',
  REPORT_EDITED: 'Отчет успешно отредактирован'
};