const WizardScene = require('telegraf/scenes/wizard');

const User = require('../../../models/User');
const Ticket = require('../../../models/Ticket');
const Report = require('../../../models/Report');
const Project = require('../../../models/Project');
const { TICKET_STATUS } = require('../../../models/constants');
const logger = require('../../../logger');

const sceneIds = require("../sceneIds");
const messages = require('./noTicketReportMessages');
const {
  NO_TICKET_REPORT_START_WORD,
  NO_TICKET_REPORT_START_NUMBER,
  SEPARATOR,
  createTicketNumber,
  getReportPreview,
  getSaveKeyboard
} = require('./noTicketReportHelpers');

const {
  deleteMessages,
  getBackButton,
  getTotalTimeSpent,
  checkIfTimeSpentExceedsLimit,
  SPENT_TIME_LIMIT
} = require('../utils/helpers');

const scene = new WizardScene(
  sceneIds.NO_TICKET_REPORT,


  //first step: initial step
  async (ctx) => {
    ctx.scene.state.messagesToDelete = ctx.scene.state.messagesToDelete || [];
    const user = await User.findOne({telegramUserId: ctx.from.id});
    const project = await Project.findOne({_id: ctx.scene.state.projectId});
    const totalTimeSpentForToday = await getTotalTimeSpent(user._id);

    if( totalTimeSpentForToday >= SPENT_TIME_LIMIT) {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT, getBackButton())
      );
      return
    }
    ctx.scene.state.ticket = {
      projectId: project._id,
      userId: user._id,
      number: NO_TICKET_REPORT_START_NUMBER,
      name: NO_TICKET_REPORT_START_WORD,
      status: TICKET_STATUS.CLOSED,
    };
    ctx.scene.state.report = {
      ticketId: null,
      userId: user._id,
      description: '',
      ticketStatus: TICKET_STATUS.CLOSED,
      time_spent: null,
      date: null,
      edit_date: null
    };
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.PROJECT_INFO(project)),
      await ctx.reply(messages.ENTER_REPORT_PAYLOAD, getBackButton())
    );
    await ctx.wizard.next();
  },


  //Second step: User write report description
  async (ctx) => {
    ctx.scene.state.report.description = ctx.message.text;

    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.ENTER_TIME_SPENT)
  );
    await ctx.wizard.next();
  },


  //Third step: User write spent time
  async (ctx) => {
    const timeSpent = ctx.message.text;
    let {userId} = ctx.scene.state.ticket;
    let totalTimeSpent = await getTotalTimeSpent(userId);

    if (isNaN(timeSpent)) {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.USE_NUMBERS)
      );

    } else if (timeSpent <= 0) {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.MUST_BE_NON_ZERO_VALUE)
      );

    } else if (await checkIfTimeSpentExceedsLimit(timeSpent, totalTimeSpent)) {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.WRONG_TIME),
        await ctx.reply(messages.TIME_SPENT_EXCEEDS_LIMIT(totalTimeSpent))
      );

    } else {
      ctx.scene.state.report.timeSpent = timeSpent;
      ctx.scene.state.ticket.number = await createTicketNumber(ctx.scene.state.ticket.projectId);
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(
          getReportPreview(ctx.scene.state.ticket, ctx.scene.state.report),
          getSaveKeyboard()
        )
      );
      await ctx.wizard.next();
    }
  },


  //Final step: Choose save, reset or edit report
  async (ctx) => {
    let msg;
    const choice = ctx.message.text;

    switch (choice) {
      case messages.SAVE_KEYBOARD.SAVE_REPORT:
        const ticket = new Ticket(ctx.scene.state.ticket);
        ctx.scene.state.report = {
          ...ctx.scene.state.report,
          date: new Date(),
          ticketId: ticket._id,
        };
        const report = new Report(ctx.scene.state.report);

        await ticket.save();
        await report.save();
        logger.debug(ctx, 'Created new report!');
        msg = await ctx.reply(messages.REPORT_SAVED);
        await ctx.scene.enter(sceneIds.FINAL, {messagesToDelete: [msg]});
        break;

      case messages.SAVE_KEYBOARD.CANCEL_REPORT:
        logger.debug(ctx, 'Canceled save additional report.');
        msg = await ctx.reply(messages.REPORT_DISCARDED);
        await ctx.scene.enter(sceneIds.FINAL, {messagesToDelete: [msg]});
        break;

      case messages.SAVE_KEYBOARD.EDIT_REPORT:
        const reportData = {
          ...ctx.scene.state.report,
          projectId: ctx.scene.state.ticket.projectId,
          ticketNumber: ctx.scene.state.ticket.number,
          ticketName: ctx.scene.state.ticket.name
        };

        await ctx.scene.enter(sceneIds.EDIT_NEW_TICKET_REPORT, {report: reportData});
        break;

      default:
        await ctx.reply(messages.WRONG_ACTION)
    }
  }
);

scene.hears(messages.BACK, async ctx => {
  await ctx.scene.enter(sceneIds.PROJECTS);
});

scene.hears('/start', async ctx => {
  await ctx.scene.enter(sceneIds.START);
});

scene.leave(async (ctx) => {
  await deleteMessages(ctx);
});

module.exports = scene;