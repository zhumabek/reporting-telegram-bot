const {Markup} = require("telegraf");
const Ticket = require('../../../models/Ticket');


const {SAVE_KEYBOARD} = require('./noTicketReportMessages');
const NO_TICKET_REPORT_START_WORD = 'Дополнительный отчет';
const NO_TICKET_REPORT_START_NUMBER = '9000';
const SEPARATOR = '_';

async function createTicketNumber(projectId) {
  const pattern = /([0-9])\w+/g;
  const isNumberAdditional = (ticketNumber) => pattern.test(ticketNumber);
  const tickets = await Ticket.find({projectId, name: NO_TICKET_REPORT_START_WORD}).sort({number: -1});
  for (const ticket of tickets) {
    if (ticket && isNumberAdditional(ticket.number) !== true) {
      for (const updateTicket of await Ticket.find({projectId, name: NO_TICKET_REPORT_START_WORD, number: ticket.number})) {
        const ticketsLastNumber = await Ticket.findOne({
          projectId,
          name: NO_TICKET_REPORT_START_WORD,
          number: {$regex: pattern}
        }).sort({number: -1}).limit(1);
        if (!ticketsLastNumber) {
          const lastTicketNum = parseInt(NO_TICKET_REPORT_START_NUMBER);
          return lastTicketNum + 1;
        }
        const lastTicketNumber = parseInt(ticketsLastNumber.number);
        const ticketNewNumber = lastTicketNumber + 1;
        await Ticket.findByIdAndUpdate(updateTicket._id, {number: ticketNewNumber});
      }
    } else if (ticket && ticket.number >= NO_TICKET_REPORT_START_NUMBER) {
      const ticketLast = await Ticket.findOne({
        projectId,
        name: NO_TICKET_REPORT_START_WORD
      }).sort({number: -1}).limit(1);
      const lastTicketNumber = parseInt(ticketLast.number);
      return lastTicketNumber + 1;
    }
  }
  return NO_TICKET_REPORT_START_NUMBER;
}

function getReportPreview(ticket, report) {
  return 'Отчет:\n' +
    `Номер тикета: ${ticket.number}\n` +
    `Название тикета: ${ticket.name}\n` +
    `Описание отчета: ${report.description}\n` +
    `Потраченное время: ${report.timeSpent}\n` +
    `Статус тикета: ${report.ticketStatus}\n`
}

function getSaveKeyboard() {
  return Markup.keyboard([
    [SAVE_KEYBOARD.SAVE_REPORT, SAVE_KEYBOARD.CANCEL_REPORT],
    [SAVE_KEYBOARD.EDIT_REPORT],
  ]).oneTime().resize().extra();
}

module.exports = {
  createTicketNumber,
  getReportPreview,
  getSaveKeyboard,
  NO_TICKET_REPORT_START_WORD,
  NO_TICKET_REPORT_START_NUMBER,
  SEPARATOR
};