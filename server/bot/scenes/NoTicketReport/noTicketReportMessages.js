module.exports = {
  PROJECT_INFO: (project) => {
    return `Проект: ${project.name}\n` +
      `Время отправки отчетов: ${project.reportsSendTime}:00`
  },
  ENTER_REPORT_PAYLOAD: 'Введите отчет',
  ENTER_TIME_SPENT: 'Введите потраченное время',
  SAVE_KEYBOARD: {
    SAVE_REPORT: 'Сохранить',
    CANCEL_REPORT: 'Сбросить',
    EDIT_REPORT: 'Редактировать отчет'
  },
  REPORT_SAVED: 'Отчет успешно сохранен!',
  REPORT_DISCARDED: 'Отчет успешно сброшен!',
  WRONG_ACTION: 'Не правилбное действие.',
  BACK: 'Назад',
  TOTAL_SPENT_TIME_REACHED_THE_LIMIT: 'Вы уже потратили 12 часов из 12. Больше вы не можете писать отчет по новому тикету.',
  USE_NUMBERS: 'Ошибка: Используйте цифры.',
  WRONG_TIME: 'Ошибка: Максимум - 12 часов',
  MUST_BE_NON_ZERO_VALUE: 'Ошибка: Должен быть больше нуля.',
  TIME_SPENT_EXCEEDS_LIMIT: (timeSpent=0) => {
    return `Вы уже потратили ${timeSpent} часов из 12. Попробуйте еще раз`;
  }
};