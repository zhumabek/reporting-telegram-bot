const Telegraf = require('telegraf');
const { BaseScene } = Telegraf;
const logger = require('../../../logger');

const ProjectManagementTool = require('../../../repositories/pmt/index');
const { projectTypeEnum } = require('../../../models/constants');
const messages = require('./existingTicketsMessages');
const { getBackButton, deleteMessages } = require('../utils/helpers');
const { actionNames, getTicketButtons, getNewTicketButton } = require('./existingTicketsHelpers');
const sceneIds = require("../sceneIds");

const existingTicketsScene = new BaseScene(sceneIds.EXISTING_TICKETS);

existingTicketsScene.enter(async (ctx) => {
  logger.debug(ctx, 'In existing tickets.');
  ctx.scene.state.messagesToDelete = ctx.scene.state.messagesToDelete || [];

  const projectId = ctx.scene.state.projectId;
  const pmt = await ProjectManagementTool(projectId, ctx.from.id).create();
  const tickets = await pmt.getUsersInProgressTickets();

  ctx.scene.state.messagesToDelete.push(
    await ctx.reply(messages.EXISTING_TICKET_REPORT, getBackButton())
  );

  if(!tickets.length && pmt.project.pmtType === projectTypeEnum.BASIC_PROJECT) {
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.NO_TICKETS, await getNewTicketButton(projectId))
    );

  } else if (!tickets.length && pmt.project.pmtType !== projectTypeEnum.BASIC_PROJECT){
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.NO_TICKETS_IN_REMOTE_PMT)
    );

  } else if(tickets.length) {
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.AVAILABLE_TICKETS, await getTicketButtons(tickets))
    );

  } else {
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.SOMETHING_IS_WRONG)
    );
  }
});

existingTicketsScene.hears(messages.BACK, async ctx => {
  await ctx.scene.enter(sceneIds.PROJECTS);
});

existingTicketsScene.action(RegExp(actionNames.CHOOSE_TICKET), async ctx => {
  const ticketInfo = JSON.parse(ctx.callbackQuery.data);
  await ctx.scene.enter(
    sceneIds.EXISTING_TICKET_REPORT,
    {ticketId: ticketInfo.param}
    );
});

existingTicketsScene.action(RegExp(actionNames.NEW_TICKET_REPORT), async ctx => {
  const projectInfo = JSON.parse(ctx.callbackQuery.data);
  await ctx.scene.enter(sceneIds.NEW_TICKET_REPORT, {projectId: projectInfo.p});
});

existingTicketsScene.leave(async ctx => {
  await deleteMessages(ctx);
});

module.exports = existingTicketsScene;