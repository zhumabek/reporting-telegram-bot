const { Markup } = require("telegraf");
const messages = require('./existingTicketsMessages');

const actionNames = {
  CHOOSE_TICKET: 'chooseTicket',
  NEW_TICKET_REPORT: 'newTicketReport'
};

async function getTicketButtons(tickets) {
  return Markup.inlineKeyboard(tickets.map(ticket => [
      Markup.callbackButton(`#${ticket.number} - ${ticket.name}`,
        JSON.stringify({action: actionNames.CHOOSE_TICKET, param: ticket.id})
      )
  ])).extra();
}

async function getNewTicketButton(projectId) {
  return Markup.inlineKeyboard([
      Markup.callbackButton(messages.CREATE_NEW_TICKET,
        JSON.stringify({a: actionNames.NEW_TICKET_REPORT, p: projectId})
      )
  ]).extra();
}

module.exports = {
  actionNames,
  getTicketButtons,
  getNewTicketButton,
};