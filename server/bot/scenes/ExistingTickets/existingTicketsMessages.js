module.exports = {
  NO_TICKETS: 'Список доступных тикетов пуст.',
  NO_TICKETS_IN_REMOTE_PMT: 'Не нащли тикеты со статусом In progress в системе управления проектами.',
  AVAILABLE_TICKETS: 'Выберите тикет для написания отчета.',
  SOMETHING_IS_WRONG: 'Что-то пошло не так!',
  BACK: 'Назад',
  IN_PROGRESS: 'in progress',
  CREATE_NEW_TICKET: 'Создать новый тикет',
  EXISTING_TICKET_REPORT: 'Отчет по предзаполненному тикету',
};