const Scene = require('telegraf/scenes/base');
const {Markup} = require("telegraf");
const logger = require('../../../logger');

const sceneIds = require('../sceneIds');
const messages = require('./confirmEmailMessages');
const {
  getKeyboard,
  activateUser
} = require('./confirmEmailHelpers');
const {
  sendConfirmationCodeToEmail,
  generateConfirmationCode,
  deleteUserByTelegramUserId
} = require('../utils/helpers');


const INTERVAL_TO_ABLE_SEND_EMAIL_AGAIN = 60 * 1000;
const scene = new Scene(sceneIds.CONFIRM_EMAIL);


scene.enter(async ctx => {
  ctx.scene.state.lastSentEmailAt = new Date().getTime();
  await ctx.reply(messages.SEND_CONFIRM_CODE, getKeyboard());
  logger.debug(ctx, 'User in confirm email with code.')
});


scene.hears(messages.buttons.SEND_CODE_AGAIN, async ctx => {
  const lastSentEmailAt = ctx.scene.state.lastSentEmailAt;
  if (new Date().getTime() < lastSentEmailAt + INTERVAL_TO_ABLE_SEND_EMAIL_AGAIN){
    return await ctx.reply(messages.WE_WONT_SEND_EMAIL_AGAIN)
  }

  const confirmCode = generateConfirmationCode();
  try{
    await sendConfirmationCodeToEmail(ctx.scene.state.email, confirmCode);
    ctx.scene.state.confirmCode = confirmCode;
    ctx.scene.state.lastSentEmailAt = new Date().getTime();

    await ctx.reply(messages.WE_SENT_CONFIRM_CODE_AGAIN);
    await ctx.scene.enter(sceneIds.CONFIRM_EMAIL, {...ctx.scene.state});
    logger.debug(ctx, 'Sent confirmation code to user email address.')

  } catch (e) {
    await ctx.reply(messages.ERROR_WHEN_SEND_EMAIL);
    logger.error(ctx, "Error when send email.", e)
  }
});


scene.hears(messages.buttons.TRY_ANOTHER_EMAIL_OR_PHONE, async ctx => {
  await deleteUserByTelegramUserId(ctx.from.id);
  await ctx.scene.enter(sceneIds.START)
});


scene.on('message', async ctx => {
  const confirmCode = ctx.scene.state.confirmCode;
  const message = ctx.message.text;

  if (message === confirmCode){
    await activateUser(ctx.from.id);
    await ctx.reply('_', Markup.removeKeyboard(true).extra());
    await ctx.scene.enter(sceneIds.PROJECTS);
    logger.debug(ctx, 'User activated successfully.')

  } else {
    await ctx.reply(messages.INCORRECT_CONFIRM_CODE);
  }
});


module.exports = scene;