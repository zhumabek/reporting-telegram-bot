const { Markup } = require('telegraf');

const User = require('../../../models/User');
const messages = require('./confirmEmailMessages');



function getKeyboard() {
  return Markup.keyboard([
    messages.buttons.SEND_CODE_AGAIN,
    messages.buttons.TRY_ANOTHER_EMAIL_OR_PHONE,
  ]).oneTime().resize().extra();
}


async function activateUser(telegramUserId){
  await User.findOneAndUpdate({telegramUserId}, {active: true})
}


module.exports = {
  getKeyboard,
  activateUser,
};
