function saveToSession(ctx, field, data) {
  ctx.session[field] = data;
}

module.exports = {
  saveToSession
};
