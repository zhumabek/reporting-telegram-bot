const { Markup } = require('telegraf');
const nodemailer = require('nodemailer');

const logger = require('../../../logger');
const config = require('../../../config');
const messages = require('./reportMessages');
const Report = require('../../../models/Report');
const Ticket = require('../../../models/Ticket');
const User = require('../../../models/User');

const SPENT_TIME_LIMIT = 12;

async function goBack(ctx) {
  await ctx.wizard.next();
  await ctx.wizard.back();
}

async function deleteUserByTelegramUserId(telegramUserId){
  await User.deleteOne({telegramUserId: telegramUserId});
}

async function goToBeginning(ctx) {
  await ctx.scene.enter(ctx.scene.current.id, {...ctx.scene.state})
}

function getBackButton() {
  return Markup.keyboard([messages.GO_BACK_TO_PREVIOUS_SCENE]).resize().extra();
}

async function changeScene(ctx, sceneId, newState = {}) {
  return ctx.scene.enter(sceneId, {...ctx.scene.state, ...newState});
}

async function changeWizardScene(ctx, sceneId, newState = {}) {
  return ctx.scene.enter(sceneId, {...ctx.wizard.state, ...newState});
}

function getStatusButtons() {
  return Markup.keyboard([
    messages.IN_PROGRESS,
    messages.CLOSED,
    messages.GO_BACK_TO_PREVIOUS_SCENE
  ]).oneTime().resize().extra();
}

function validTicketStatus(status) {
  return (status === messages.IN_PROGRESS) || (status === messages.CLOSED);
}

function getReport(report) {
  return `
    ${messages.TICKET_NUMBER}: ${report.ticketNumber}
    ${messages.TICKET_NAME}: ${report.ticketName}
    ${messages.TIME_SPENT}: ${report.timeSpent}
    ${messages.TICKET_STATUS}: ${report.ticketStatus}
    ${messages.DESCRIPTION}: ${report.description}
  `;
}

function getSendButtons() {
  return Markup.keyboard([
    messages.YES_SEND_REPORT,
    messages.NO_DONT_SEND_REPORT,
    messages.EDIT_CURRENT_REPORT,
  ]).oneTime().resize().extra();
}

function getTotalSpentTimeReachedLimitButtons() {
  return Markup.keyboard([
    messages.GO_TO_RECENT_REPORT_TICKETS,
    messages.GO_BACK_TO_PREVIOUS_SCENE,
  ]).oneTime().resize().extra();
}

function getNewTicketReportSendButtons() {
  return Markup.keyboard([
    [messages.YES_SEND_REPORT, messages.NO_DONT_SEND_REPORT],
    [messages.EDIT_NEW_TICKET_REPORT],
  ]).oneTime().resize().extra();
}

async function getUrlButton(ctx) {
  const bot = await ctx.telegram.getMe();
  return Markup.inlineKeyboard([
    Markup.urlButton(messages.GO_TO_BOT, `https://t.me/${bot.username}`),
  ]).extra()
}

async function updateTicketStatus(report){
  return Ticket.findOneAndUpdate({_id: report.ticketId}, {status: report.ticketStatus}, {})
}

async function createReport(report) {
  const newReport = new Report({
    ticketId: report.ticketId,
    userId: report.userId,
    description: report.description,
    timeSpent: report.timeSpent,
    ticketStatus: report.ticketStatus,
    date: new Date(),
  });
  await newReport.save();
  return newReport
}

async function getTicketSpentTime(ticketId){
  const reports = await Report.find({ticketId: ticketId});
  let spentTime = 0;
  reports.map(report => {
    spentTime += report.timeSpent
  });
  return spentTime;
}

async function deleteMessages(ctx) {
      await ctx.scene.state.messagesToDelete.map(async message => {
        try{
          await ctx.deleteMessage(message.message_id);
        } catch (e) {
          // pass
        }
      });
}

async function returnTicketNumber(report) {
  const ticket = await Ticket.findOne({_id: report.ticketId});
  return ticket.number;
}

async function returnTicketName(report) {
  const ticket = await Ticket.findOne({_id: report.ticketId});
  return ticket.name;
}

async function checkIfTimeSpentExceedsLimit(timeSpent, totalTime, oldTimeSpent = 0) {
  return !((totalTime-oldTimeSpent) + parseFloat(timeSpent) <= SPENT_TIME_LIMIT)
}

async function getTotalTimeSpent(userId) {

  let totalTime = 0;
  let arrayOfTimeSpentValues = [];
  let today = new Date().setHours(0,0,0,0);

  try {
    const ticketIds = await Ticket.find({userId}, '_id');
    const reportsForToday = await Report.find({ticketId:{$in: ticketIds}, date: {$gt: today}}, 'timeSpent');
    arrayOfTimeSpentValues = reportsForToday.map((doc) => doc.timeSpent);

  } catch (e) {
    logger.error(undefined, '', e);
  }

  if(arrayOfTimeSpentValues.length > 0){
    totalTime = arrayOfTimeSpentValues.reduce((a,b) => a+b);
  }

  return totalTime;
}

async function getReportTimeSpent(reportId) {
  let report;
  try {
    report = await Report.findOne({_id:reportId}, 'timeSpent');
  } catch (e) {
    logger.error(undefined, 'Cant find report by id', e);
  }
  return report.timeSpent;
}

async function sendConfirmationCodeToEmail(email, confirmCode){
  try {
    const transport = nodemailer.createTransport(config.smtp);
    const subject = 'Confirmation code for Attractor reporting bot';
    const message = {
      from: config.smtp.auth.user,
      to: email,
      subject: subject,
      html: getHtmlText(confirmCode)
    };
    await transport.sendMail(message);
    logger.debug(undefined, 'Message sent to email!')
  } catch (e) {
    logger.error(undefined, 'Could not send email.', e);
  }

}


function getHtmlText(code){
  return '<body style="font-family: \'Fira Code Medium\'; max-width: 600px; margin-left: auto; margin-right: auto;">\n' +
    '<h3>Attractor Reporting Bot</h3>\n' +
    `<p>Ваш код потверждения: <b>${code}</b></p>\n` +
    '<p>Срок действия кода истекает через 40 минут после получения этого письма.</p>' +
    '</body>'
}

function generateConfirmationCode(){
  const confrimCodeLength = 4;
  let result = '';
  const characters = '0123456789';
  const charactersLength = characters.length;

  for ( let i = 0; i < confrimCodeLength; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}


module.exports = {
  changeWizardScene,
  createReport,
  getSendButtons,
  getStatusButtons,
  getNewTicketReportSendButtons,
  getTotalSpentTimeReachedLimitButtons,
  getBackButton,
  getReport,
  getReportTimeSpent,
  validTicketStatus,
  goBack,
  updateTicketStatus,
  getTicketSpentTime,
  changeScene,
  goToBeginning,
  deleteMessages,
  returnTicketNumber,
  returnTicketName,
  checkIfTimeSpentExceedsLimit,
  getTotalTimeSpent,
  SPENT_TIME_LIMIT,

  sendConfirmationCodeToEmail,
  generateConfirmationCode,
  deleteUserByTelegramUserId,
  getUrlButton
};