const Telegraf = require('telegraf');
const { BaseScene } = Telegraf;
const logger = require('../../../logger');

const Project = require('../../../models/Project');
const { projectTypeEnum } = require('../../../models/constants');
const ProjectManagementTool = require('../../../repositories/pmt/index');
const User = require('../../../models/User');

const sceneIds = require("../sceneIds");
const messages = require('./projectMessages');
const { deleteMessages } = require('../utils/helpers');
const { getProjectButtons, actionNames, getBackKeyboard, getNextActionButtons, compareByName } = require('./projectHelpers');

const projectsScene = new BaseScene(sceneIds.PROJECTS);

projectsScene.enter(async (ctx) => {
  logger.debug(ctx, 'In projects scene.');
  ctx.scene.state.messagesToDelete = ctx.scene.state.messagesToDelete || [];
  const user = await User.findOne({telegramUserId: ctx.from.id});
  const projects = await Project.find({isClosed: false, members: user.id});
  projects.sort(compareByName);
  if (projects.length === 0){
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.NO_PROJECTS_YET)
    );
  } else {
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.CHOOSE_PROJECT, getProjectButtons(projects))
    )
  }
});

projectsScene.hears(messages.buttons.BACK, async ctx => await ctx.scene.enter(sceneIds.PROJECTS));

projectsScene.action(RegExp(actionNames.CHOOSE_NEXT_ACTION), async ctx => {
  await deleteMessages(ctx);

  const projectId = JSON.parse(ctx.callbackQuery.data).p;
  const project = await Project.findOne({_id: projectId});
  const pmt = await ProjectManagementTool(projectId, ctx.from.id).create();

  const msg = await ctx.reply(
      messages.CHOOSE_NEXT_ACTION(pmt.project.name, pmt.project.reportsSendTime),
      getNextActionButtons(projectId, pmt.project.pmtType !== projectTypeEnum.BASIC_PROJECT)
  );
  const msgBackKeyboard = await ctx.reply(messages.GO_BACK, getBackKeyboard());
  ctx.scene.state.messagesToDelete.push(msg, msgBackKeyboard);

  logger.debug(ctx, 'Chose project.', {projectName: pmt.project.name});
});

projectsScene.action(RegExp(actionNames.CHOOSE_EXISTING_TICKET), async ctx => {
  const projectData = JSON.parse(ctx.callbackQuery.data);
  await ctx.scene.enter(sceneIds.EXISTING_TICKETS, {projectId: projectData.p})
});

projectsScene.action(RegExp(actionNames.NEW_TICKET_REPORT), async ctx => {
  const projectData = JSON.parse(ctx.callbackQuery.data);
  await ctx.scene.enter(sceneIds.NEW_TICKET_REPORT, {projectId: projectData.p})
});

projectsScene.action(RegExp(actionNames.EDIT_RECENT_REPORT), async ctx => {
  const projectData = JSON.parse(ctx.callbackQuery.data);
  await ctx.scene.enter(sceneIds.RECENT_REPORT_TICKETS, {projectId: projectData.p})
});

projectsScene.action(
  RegExp(actionNames.NO_TICKET_REPORT),
  async ctx => {
    const projectData = JSON.parse(ctx.callbackQuery.data);
    await ctx.scene.enter(sceneIds.NO_TICKET_REPORT, {projectId: projectData.p})
  }
);

projectsScene.leave(async (ctx) => {
  await deleteMessages(ctx);
});

module.exports = projectsScene;
