const {Markup, Extra} = require("telegraf");
const messages = require('./projectMessages');

const actionNames = {
  CHOOSE_NEXT_ACTION: 'ChooseNextAction',
  CHOOSE_EXISTING_TICKET: 'ChooseExistingTickets',
  NEW_TICKET_REPORT: 'NewTicketReport',
  EDIT_RECENT_REPORT: 'EditRecentReport',
  NO_TICKET_REPORT: 'NoTicketReport'
};

function compareByName(a, b) {
  const nameA = a.name.toUpperCase();
  const nameB = b.name.toUpperCase();
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  return 0;
}

function getBackKeyboard() {
  return Markup.keyboard(
    [messages.buttons.BACK]
  ).resize().extra()
}

function getProjectButtons(projects) {
  return Extra.HTML().markup((m) =>
    m.inlineKeyboard(projects.map(project => [
      m.callbackButton(project.name, JSON.stringify({
        a: actionNames.CHOOSE_NEXT_ACTION,
        p: project._id
      }), false)
    ]), {})
  );
}

function getNextActionButtons(projectId, isConnectedToPMT) {
  if (isConnectedToPMT){
    return Markup.inlineKeyboard(
      [
        [
          Markup.callbackButton(
            messages.buttons.WRITE_REPORT,
            JSON.stringify({
              a: actionNames.CHOOSE_EXISTING_TICKET,
              p: projectId
            })),
        ],
        [
          Markup.callbackButton(
            messages.buttons.EDIT_RECENT_REPORT,
            JSON.stringify({
              a: actionNames.EDIT_RECENT_REPORT,
              p: projectId
            })),
        ],
        [
          Markup.callbackButton(
            messages.buttons.NO_TICKET_REPORT,
            JSON.stringify({
              a: actionNames.NO_TICKET_REPORT,
              p: projectId
            })),
        ]
      ]
    ).extra()
  }
  return Markup.inlineKeyboard(
    [
      [
        Markup.callbackButton(
          messages.buttons.NEW_TICKET_REPORT,
          JSON.stringify({
            a: actionNames.NEW_TICKET_REPORT,
            p: projectId
          })),
      ],
      [
        Markup.callbackButton(
          messages.buttons.EXISTING_TICKET_REPORT,
          JSON.stringify({
            a: actionNames.CHOOSE_EXISTING_TICKET,
            p: projectId
          })),
      ],
      [
        Markup.callbackButton(
          messages.buttons.EDIT_RECENT_REPORT,
          JSON.stringify({
            a: actionNames.EDIT_RECENT_REPORT,
            p: projectId
          })),
      ],
      [
        Markup.callbackButton(
          messages.buttons.NO_TICKET_REPORT,
          JSON.stringify({
            a: actionNames.NO_TICKET_REPORT,
            p: projectId
          })),
      ],
    ],
  ).extra()
}


module.exports = {
  compareByName,
  getProjectButtons,
  getBackKeyboard,
  getNextActionButtons,
  actionNames
};