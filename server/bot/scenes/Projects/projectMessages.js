module.exports = {
  NO_PROJECTS_YET: ' Вы успешно авторизованы! Но у вас нет проектов. ' +
    'Обратитесь к вашему менеджеру чтоб он добавил вас в проекты в админ панели.',
  CHOOSE_PROJECT: 'Выберите проект для написания отчета',
  CHOOSE_NEXT_ACTION: (projectName, reportsSendTime) => {
    return `Проект: ${projectName} \n Время отправки отчетов в группу: ${reportsSendTime} \n Выберите следующее действие`
  },
  GO_BACK: 'Если хотите вернуться назад нажмите на кнопку Назад',
  SOMETHING_WENT_WRONG: 'Простите, что-то пошло не так',
  buttons: {
    WRITE_REPORT: 'Написать отчет',
    EDIT_RECENT_REPORT: 'Редактировать отчет за сегодня',
    NEW_TICKET_REPORT: 'Отчет по новому тикету',
    EXISTING_TICKET_REPORT: 'Отчет по предзаполненному тикету',
    NO_TICKET_REPORT: 'Дополнительный отчет',
    BACK: 'Назад',
  }
};
