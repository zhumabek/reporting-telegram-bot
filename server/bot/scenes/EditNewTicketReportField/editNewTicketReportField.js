const WizardScene = require('telegraf/scenes/wizard');
const logger = require('../../../logger');

const sceneIds = require('../sceneIds');
const editReportFieldMessages = require('./editNewTicketReportFieldMessages');
const newTicketMessages = require('../NewTicketReport/NewTicketReportMessages');
const reportMessages =require('../utils/reportMessages');
const helpers = require('./editNewTicketReportFieldHelpers');
const { goBack, goToBeginning, changeScene, changeWizardScene, deleteMessages, checkIfTimeSpentExceedsLimit, getTotalTimeSpent, resetScheduler } = require('../utils/helpers');

const editNewTicketReportFieldScene = new WizardScene(sceneIds.EDIT_NEW_TICKET_REPORT_FIELD,
  async ctx => {
    let {report, field} = await ctx.scene.state;
    ctx.wizard.state.messagesToDelete = [];
    logger.debug(ctx, 'In edit new ticket report field.', {field: field});

    const currentReportFieldInfo = helpers.returnReportFieldInfo(field, report);
    const fieldActionMessage = helpers.getFieldActionMessage(field);
    ctx.wizard.state.messagesToDelete.push(await ctx.reply(currentReportFieldInfo));
    ctx.wizard.state.messagesToDelete.push(await ctx.reply(fieldActionMessage, helpers.getFieldButtons(field)));
    await ctx.wizard.next();
  },
  async ctx => {
    let { report, field } = await ctx.scene.state;
    let { projectId, userId }= report;
    const message = ctx.message.text;
    switch (field) {
      case 'tNumber':
        if( await helpers.validateTicketNumber(ctx)){
          logger.debug(ctx, 'Edited ticket number.');

          report.ticketNumber = message;
          return changeScene(ctx, sceneIds.EDIT_NEW_TICKET_REPORT, {returnedBack: true, ...report});
        }
        return;
      case 'tName':
        logger.debug(ctx, 'Edited ticket name.');

        report.ticketName = message;
        return changeScene(ctx, sceneIds.EDIT_NEW_TICKET_REPORT, {returnedBack: true, ...report});
      case 'des':
        logger.debug(ctx, 'Edited report description.');

        report.description = message;
        return changeScene(ctx, sceneIds.EDIT_NEW_TICKET_REPORT, {returnedBack: true, ...report});
      case 'status':
        logger.debug(ctx, 'Edited status.');

        report.ticketStatus = message;
        return changeScene(ctx, sceneIds.EDIT_NEW_TICKET_REPORT, {returnedBack: true, ...report});
      case 'time':
        const timeSpent = message;
        const totalTimeSpent = await getTotalTimeSpent(projectId, userId);

        if(isNaN(timeSpent)) {
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(editReportFieldMessages.USE_NUMBERS));
          await goBack(ctx);

        } else if(timeSpent <= 0) {
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(editReportFieldMessages.MUST_BE_NON_ZERO_VALUE));
          await goBack(ctx);

        } else if(await checkIfTimeSpentExceedsLimit(timeSpent, totalTimeSpent)) {
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(editReportFieldMessages.WRONG_TIME));
          ctx.wizard.state.messagesToDelete.push(await ctx.reply(editReportFieldMessages.TIME_SPENT_EXCEEDS_LIMIT(totalTimeSpent)));
          await goBack(ctx);

        } else {
          logger.debug(ctx, 'Edited ticket spent time.');

          report.timeSpent = timeSpent;
          return changeScene(ctx, sceneIds.EDIT_NEW_TICKET_REPORT, {returnedBack: true, ...report});
        }
    }
  },
  async ctx => {
    const message = await ctx.message.text;
    if(message === newTicketMessages.GO_TO_EXISTING_TICKETS) {
      await changeScene(ctx, sceneIds.EXISTING_TICKETS);
    } else if(message === newTicketMessages.ENTER_TICKET_NUMBER) {
      await goToBeginning(ctx);
    } else {
      ctx.wizard.state.messagesToDelete.push(await ctx.reply(newTicketMessages.WRONG_ACTION));
      await goBack(ctx);
    }
  }
);

editNewTicketReportFieldScene.leave(async ctx => {
  await deleteMessages(ctx);
});

editNewTicketReportFieldScene.hears(reportMessages.GO_BACK_TO_PREVIOUS_SCENE, ctx => {
  changeWizardScene(ctx, sceneIds.EDIT_NEW_TICKET_REPORT, {});
});

module.exports = editNewTicketReportFieldScene;