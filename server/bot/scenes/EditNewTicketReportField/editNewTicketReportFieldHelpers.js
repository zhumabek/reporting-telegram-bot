const messages = require('./editNewTicketReportFieldMessages');
const newTicketReportMessages = require('../NewTicketReport/NewTicketReportMessages');
const {getBackButton, getStatusButtons, goBack} = require('../utils/helpers');
const newTicketReportHelpers = require('../NewTicketReport/NewTicketReportHelpers');


function returnReportFieldInfo(fieldName, report){
  switch (fieldName) {
    case 'tNumber': return `${messages.TICKET_NUMBER}: ${report.ticketNumber}`
    case 'tName': return `${messages.TICKET_NAME}: ${report.ticketName}`
    case 'status': return `${messages.TICKET_STATUS}: ${report.ticketStatus}`
    case 'des': return `${messages.DESCRIPTION}: ${report.description}`
    case 'time': return `${messages.TIME_SPENT}: ${report.timeSpent}`
    default: return `${messages.DESCRIPTION}: ${report.description}`
  }
}

function getFieldActionMessage(fieldName){
  switch (fieldName) {
    case 'tNumber': return messages.ENTER_TICKET_NUMBER;
    case 'tName': return messages.ENTER_TICKET_NAME;
    case 'status': return messages.CHOOSE_TICKET_STATUS;
    case 'des': return messages.ENTER_DESCRIPTION;
    case 'time': return messages.ENTER_TIME_SPENT;
    default: return messages.ENTER_DESCRIPTION;
  }
}

function getFieldButtons(fieldName){
  return fieldName === 'status' ? getStatusButtons() : getBackButton();
}

function updateStatus(report, newStatus){
  return report.ticketStatus = newStatus;
}

function updateDescription(report, newDescription){
  return report.description = newDescription;
}

function updateTimeSpent(report, newTimeSpent){
  return report.timeSpent = newTimeSpent;
}

async function validateTicketNumber(ctx){
  let ticketNumber = ctx.message.text;
  if (await newTicketReportHelpers.checkIfTicketExists(ticketNumber, ctx.scene.state.report.projectId)) {
    if(await newTicketReportHelpers.checkIfTicketBelongsToUser(ticketNumber, ctx.scene.state.report.projectId, ctx.scene.state.report.userId)) {
      ctx.wizard.state.messagesToDelete.push(await ctx.reply(newTicketReportMessages.EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER));
      ctx.wizard.state.messagesToDelete.push(await ctx.reply(newTicketReportMessages.NEXT_ACTION, await newTicketReportHelpers.getNextActionButtons()));
      await ctx.wizard.next();
    } else {
      ctx.wizard.state.messagesToDelete.push(await ctx.reply(newTicketReportMessages.EXISTING_TICKET_NUMBER));
      await goBack(ctx);
    }
  } else {
    return true;
  }
}

module.exports = {
  returnReportFieldInfo,
  getFieldActionMessage,
  getFieldButtons,
  updateStatus,
  updateDescription,
  updateTimeSpent,
  validateTicketNumber
};