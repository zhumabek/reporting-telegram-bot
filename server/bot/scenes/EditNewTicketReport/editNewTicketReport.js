const Telegraf = require('telegraf');
const { BaseScene } = Telegraf;
const logger = require('../../../logger');

const reportMessages = require('../utils/reportMessages');
const editNewTicketReportMessages = require('./editNewTicketReportMessages');
const sceneIds = require('../sceneIds');
const helpers = require('./editNewTicketReportHelpers');
const reportHelpers = require('../utils/helpers');

const editNewTicketReportScene = new BaseScene(sceneIds.EDIT_NEW_TICKET_REPORT);

editNewTicketReportScene.enter(async ctx => {
  logger.debug(ctx, 'In edit new ticket report.');
  const {report} = ctx.scene.state;
  ctx.scene.state.messagesToDelete = [];
  ctx.scene.state.messagesToDelete.push(
    await ctx.reply(editNewTicketReportMessages.WELCOME, helpers.getSendButtons())
  );
  ctx.scene.state.messagesToDelete.push(
    await ctx.reply(
      `${editNewTicketReportMessages.FINAL_REPORT}\n ${reportHelpers.getReport(report)}`,
      helpers.getEditButtons(report.ticketNumber)
    )
  );
});


editNewTicketReportScene.hears(reportMessages.GO_BACK_TO_PREVIOUS_SCENE, async ctx => {
  await reportHelpers.changeScene(ctx, sceneIds.NEW_TICKET_REPORT);
});

editNewTicketReportScene.hears(reportMessages.YES_SEND_REPORT, async ctx => {
  try {
    await helpers.saveTicketAndReport(ctx.scene.state.report);
    logger.debug(ctx, 'Edited report saved!', ctx.scene.state.report);

    let msg = await ctx.reply(reportMessages.REPORT_CREATED);
    await reportHelpers.changeScene(ctx, sceneIds.FINAL, {report: {}, messagesToDelete: [msg]});

  } catch (e) {
    logger.error(ctx, 'Cant save new ticket report.', e);
    ctx.scene.state.messagesToDelete.push(await ctx.reply(reportMessages.COULD_NOT_SAVE_REPORT));
  }
});

editNewTicketReportScene.hears(reportMessages.NO_DONT_SEND_REPORT, async ctx => {
  logger.debug(ctx, 'Canceled save editing new ticket report');
  let msg = await ctx.reply(reportMessages.REPORT_CANCELED);
  await reportHelpers.changeScene(ctx, sceneIds.FINAL, {report: {}, messagesToDelete: [msg]});
});

editNewTicketReportScene.action(RegExp(helpers.actions.CHOOSE_FIELD), async ctx => {
  const reportInfo = JSON.parse(ctx.callbackQuery.data);
  await reportHelpers.changeScene(ctx, sceneIds.EDIT_NEW_TICKET_REPORT_FIELD, {field: reportInfo.f});
});

editNewTicketReportScene.leave(async ctx => {
  await reportHelpers.deleteMessages(ctx);
});

module.exports = editNewTicketReportScene;
