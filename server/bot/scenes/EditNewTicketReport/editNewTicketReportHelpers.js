const { Markup } = require("telegraf");
const editMessages = require("./editNewTicketReportMessages");
const reportMessages = require("../utils/reportMessages");
const Report = require('../../../models/Report');
const Ticket = require('../../../models/Ticket');

const {NO_TICKET_REPORT_START_WORD} = require('../NoTicketReport/noTicketReportHelpers');

exports.actions = {
  CHOOSE_FIELD: 'editField'
};

exports.getEditButtons = (ticketNumber) => {
  const stringNumber = String(ticketNumber);

  if (stringNumber.includes(NO_TICKET_REPORT_START_WORD)){
    return Markup.inlineKeyboard([
      [Markup.callbackButton(editMessages.EDIT_DESCRIPTION,
        JSON.stringify({a: this.actions.CHOOSE_FIELD, f: 'des'}))],
      [Markup.callbackButton(editMessages.EDIT_TIME_SPENT,
        JSON.stringify({a: this.actions.CHOOSE_FIELD, f: 'time'}))]
    ]).extra();
  }
  return Markup.inlineKeyboard([
    [Markup.callbackButton(editMessages.EDIT_TICKET_NUMBER,
      JSON.stringify({a: this.actions.CHOOSE_FIELD, f: 'tNumber'}))],
    [Markup.callbackButton(editMessages.EDIT_TICKET_NAME,
      JSON.stringify({a: this.actions.CHOOSE_FIELD, f: 'tName'}))],
    [Markup.callbackButton(editMessages.EDIT_TIME_SPENT,
      JSON.stringify({a: this.actions.CHOOSE_FIELD, f: 'time'}))],
    [  Markup.callbackButton(editMessages.EDIT_TICKET_STATUS,
      JSON.stringify({a: this.actions.CHOOSE_FIELD, f: 'status'}))],
    [Markup.callbackButton(editMessages.EDIT_DESCRIPTION,
      JSON.stringify({a: this.actions.CHOOSE_FIELD, f: 'des'}))],
  ]).extra();
};

exports.saveTicketAndReport = async (report) => {
  let {projectId, userId, ticketNumber, ticketName, ticketStatus, description, timeSpent} = report;
  const ticket = await Ticket.create({projectId,userId,number: ticketNumber,name: ticketName, status: ticketStatus});
  await Report.create({ticketId: ticket._id, userId, description, timeSpent, ticketStatus, date: new Date(), editDate: new Date()});
};

exports.getSendButtons = () => {
  return Markup.keyboard([
    [reportMessages.YES_SEND_REPORT, reportMessages.NO_DONT_SEND_REPORT],
    [reportMessages.GO_BACK_TO_PREVIOUS_SCENE],
  ]).oneTime().resize().extra();
}
