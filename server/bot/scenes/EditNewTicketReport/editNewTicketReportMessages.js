module.exports = {
  WELCOME: 'Что вы хотите изменить?',
  EDIT_TICKET_NUMBER: 'Отредактировать номер тикета',
  EDIT_TICKET_NAME: 'Отредактировать название тикета',
  EDIT_DESCRIPTION: 'Отредактировать описание отчета',
  EDIT_TIME_SPENT: 'Отредактировать потраченное время',
  EDIT_TICKET_STATUS: 'Отредактировать статус тикета',
  FINAL_REPORT: 'Готовый отчет:',
};