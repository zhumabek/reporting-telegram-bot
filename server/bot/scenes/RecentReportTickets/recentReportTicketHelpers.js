const { Markup } = require("telegraf");

const actions = {
  CHOOSE_REPORT_TICKET: 'chooseReportTicket',
};

async function getTicketButtons(tickets) {
  return Markup.inlineKeyboard(tickets.map(ticket => [
      Markup.callbackButton(`#${ticket.number} - ${ticket.name}`,
        JSON.stringify({a: actions.CHOOSE_REPORT_TICKET, id: ticket.id})),
    ])).extra();
}

module.exports = {
  getTicketButtons,
  actions,
};