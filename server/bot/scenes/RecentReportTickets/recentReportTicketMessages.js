module.exports = {
  YOU_ARE_AT_RECENT_REPORT_TICKETS_SCENE: 'Вы в разделе "Редактировать готовый отчет за сегодня"',
  NO_TICKETS: 'Нет доступных тикетов',
  AVAILABLE_TICKETS: 'Выберите тикет',
  ERROR: 'Ошибка',
  BACK: 'Назад',
};