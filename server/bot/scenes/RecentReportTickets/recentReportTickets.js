const Telegraf = require('telegraf');
const { BaseScene } = Telegraf;
const logger = require('../../../logger');

const Stage = require('telegraf/stage');
const mongoose = require('mongoose');
const messages = require('./recentReportTicketMessages');
const sceneIds = require('../sceneIds');
const Ticket = require('../../../models/Ticket');
const Report = require('../../../models/Report');
const User = require('../../../models/User');
const { deleteMessages, getBackButton, returnTicketName, returnTicketNumber, resetScheduler } = require('../utils/helpers');
const { getTicketButtons, actions } = require('./recentReportTicketHelpers');

const recentReportTicketScene = new BaseScene(sceneIds.RECENT_REPORT_TICKETS);

recentReportTicketScene.enter(async ctx => {
  logger.debug(ctx, 'In choose recent report tickets.');
  let ticketIds, tickets = [];
  ctx.scene.state.messagesToDelete = ctx.scene.state.messagesToDelete || [];
  ctx.scene.state.messagesToDelete.push(await ctx.reply(messages.YOU_ARE_AT_RECENT_REPORT_TICKETS_SCENE, await getBackButton()));
  const projectID = ctx.scene.state.projectId;
  let today = new Date().setHours(0,0,0,0);
  try {
    const loggedUser = await User.findOne({telegramUserId: ctx.from.id});
    const reports = await Report.find({userId: loggedUser._id, date: {$gt: today}}, 'ticketId');
    ticketIds = reports.map(doc => doc.ticketId);
    tickets = await Ticket.find({projectId: projectID, _id: {$in : ticketIds}});
  } catch (error) {
    await ctx.reply(messages.ERROR);
  }
  if(tickets.length > 0) {
    ctx.scene.state.messagesToDelete.push(await ctx.reply(messages.AVAILABLE_TICKETS, await getTicketButtons(tickets)));
  } else {
    ctx.scene.state.messagesToDelete.push(await ctx.reply(messages.NO_TICKETS));
  }
});

recentReportTicketScene.hears(messages.BACK, Stage.enter(sceneIds.PROJECTS));

recentReportTicketScene.action(RegExp(actions.CHOOSE_REPORT_TICKET), async ctx => {
  const ticketInfo = JSON.parse(ctx.callbackQuery.data);
  let today = new Date().setHours(0, 0, 0, 0);
  try {
    const currentUser = await User.findOne({telegramUserId: ctx.from.id});
    const report = await Report.findOne({userId: currentUser._id, ticketId: ticketInfo.id, date: {$gt: today}});
    if(report) {
      report.ticketNumber = await returnTicketNumber(report);
      report.ticketName = await returnTicketName(report);
      report.projectId = ctx.scene.state.projectId;
      await ctx.scene.enter(sceneIds.RECENT_REPORTS, {ticketId: ticketInfo.id, report, fromRecentReport: true});
    }
  } catch (error) {
    logger.error(ctx, 'Error when choose recent reports tickets.', error);
    ctx.scene.state.messagesToDelete.push(await ctx.reply(messages.ERROR));
  }
});

recentReportTicketScene.leave(async ctx => {
  await deleteMessages(ctx);
});

module.exports = recentReportTicketScene;
