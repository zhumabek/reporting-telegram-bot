module.exports = {
  START: 'start',
  CONFIRM_EMAIL: 'confirmEmail',
  JIRA_TICKETS: 'chooseJiraProject',
  PROJECTS: 'projects',
  EXISTING_TICKETS: 'existingTickets',
  RECENT_REPORTS: 'recentReports',
  RECENT_REPORT_TICKETS: 'recentReportTickets',
  NEW_TICKET_REPORT: 'newTicketReport',
  EDIT_NEW_TICKET_REPORT: 'EditNewTicketReport',
  EDIT_NEW_TICKET_REPORT_FIELD: 'EditNewTicketReportField',
  EXISTING_TICKET_REPORT: 'existingTicketReport',
  NO_TICKET_REPORT: 'NoTicketReport',
  FINAL: 'final',
  EDIT_REPORT: 'editReport',
};