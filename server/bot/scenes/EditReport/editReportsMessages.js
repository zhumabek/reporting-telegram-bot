module.exports = {
  NO_REPORTS: 'Нет доступных отчетов',
  NO_TICKETS: 'Нет доступных тикетов',
  AVAILABLE_REPORT_FOR_EDITING: 'Доступный отчет для редактирования',
  YOU_ARE_AT_RECENT_REPORTS_SCENE: 'Вы в разделе "Редактировать готовый отчет за сегодня"',
  EDIT_REPORT: 'Редактировать отчет',
  ERROR: 'Ошибка',
  BACK: 'Назад',
  EDIT_DESCRIPTION: 'Отредактировать описание',
  EDIT_TIME_SPENT: 'Отредактировать потраченное время',
  EDIT_TICKET_STATUS: 'Отредактировать статус тикета',
};