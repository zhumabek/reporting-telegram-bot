const Telegraf = require('telegraf');
const { BaseScene } = Telegraf;
const logger = require('../../../logger');

const messages = require('./editReportsMessages');
const reportMessages = require('../utils/reportMessages');
const sceneIds = require('../sceneIds');
const { getReport, deleteMessages } = require('../utils/helpers');
const helpers = require('./editReportsHelper');

const recentReportsScene = new BaseScene(sceneIds.RECENT_REPORTS);

recentReportsScene.enter(async ctx => {
  logger.debug(ctx, 'In edit reports scene.');
  ctx.scene.state.messagesToDelete = ctx.scene.state.messagesToDelete || [];
  const report = ctx.scene.state.report;
  ctx.scene.state.messagesToDelete.push(
    await ctx.reply(messages.AVAILABLE_REPORT_FOR_EDITING, helpers.getSendButtons()),
    await ctx.reply(getReport(report), await helpers.getEditButtons(report))
  );
});

recentReportsScene.hears(reportMessages.GO_BACK_TO_PREVIOUS_SCENE, async ctx => {
  let {projectId, ticketId} = ctx.scene.state.report;
  if(ctx.scene.state.fromExistingTicketReport || ctx.scene.state.fromExistingTicketReportEdit) {
    await ctx.scene.enter(sceneIds.EXISTING_TICKETS, {projectId: projectId, ticketId: ticketId});
  } else {
    await ctx.scene.enter(sceneIds.RECENT_REPORT_TICKETS, {projectId, ticketId});
  }
});

recentReportsScene.hears(reportMessages.YES_SEND_REPORT, async ctx => {
  let msg;
  const report = ctx.scene.state.report;
  try {
    if(ctx.scene.state.fromExistingTicketReportEdit){
      await helpers.saveReport(report);
      await helpers.updateTicket(report.ticketId, {status: report.ticketStatus});
      msg = await ctx.reply(reportMessages.REPORT_CREATED);
    } else {
      await helpers.updateReport(report);
      await helpers.updateTicket(report.ticketId, {status: report.ticketStatus});
      msg = await ctx.reply(reportMessages.REPORT_UPDATED);
    }
    logger.debug(ctx, 'Edited report saved!', report);
    await ctx.scene.enter(sceneIds.FINAL, {messagesToDelete: [msg]});
  } catch (e) {
    logger.debug(ctx, 'Error when save edited report!', e);
    ctx.scene.state.messagesToDelete.push(await ctx.reply(reportMessages.COULD_NOT_SAVE_REPORT));
  }
});

recentReportsScene.hears(reportMessages.NO_DONT_SEND_REPORT, async ctx => {
  logger.debug(ctx, 'Canceled save changes!');
  let msg = await ctx.reply(reportMessages.UPDATE_REPORT_CANCELED);
  await ctx.scene.enter(sceneIds.FINAL, {messagesToDelete: [msg]});
});


recentReportsScene.action(RegExp(helpers.actions.CHOOSE_REPORT), async ctx => {
  const reportInfo = JSON.parse(ctx.callbackQuery.data);
  await ctx.scene.enter(sceneIds.EDIT_REPORT, {
    report: ctx.scene.state.report,
    field: reportInfo.f,
    fromExistingTicketReport: ctx.scene.state.fromExistingTicketReport,
    fromExistingTicketReportEdit: ctx.scene.state.fromExistingTicketReportEdit,
    fromRecentReport: ctx.scene.state.fromRecentReport,
    projectId: ctx.scene.state.projectId
  });
});

recentReportsScene.leave(async ctx => {
  await deleteMessages(ctx);
});

module.exports = recentReportsScene;
