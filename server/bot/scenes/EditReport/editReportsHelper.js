const { Markup } = require("telegraf");
const messages = require('./editReportsMessages');
const Ticket = require('../../../models/Ticket');
const Report = require('../../../models/Report');
const reportMessages = require('../utils/reportMessages');
const {NO_TICKET_REPORT_START_WORD} = require('../NoTicketReport/noTicketReportHelpers');
const actions = {
  CHOOSE_REPORT: 'edit',
};

function getSendButtons() {
  return Markup.keyboard([
    [reportMessages.YES_SEND_REPORT, reportMessages.NO_DONT_SEND_REPORT],
    [reportMessages.GO_BACK_TO_PREVIOUS_SCENE],
  ]).oneTime().resize().extra();
}

async function getEditButtons(report) {
  const ticket = await Ticket.findOne({_id: report.ticketId});

  if (ticket.number.includes(NO_TICKET_REPORT_START_WORD)){
    return Markup.inlineKeyboard([
      [Markup.callbackButton(messages.EDIT_DESCRIPTION,
        JSON.stringify({a: actions.CHOOSE_REPORT, id: report._id, f: 'des'}))],
      [Markup.callbackButton(messages.EDIT_TIME_SPENT,
        JSON.stringify({a: actions.CHOOSE_REPORT, id: report._id, f: 'time'}))],
    ]).extra();
  }

  return Markup.inlineKeyboard([
    [Markup.callbackButton(messages.EDIT_DESCRIPTION,
      JSON.stringify({a: actions.CHOOSE_REPORT, id: report._id, f: 'des'}))],
    [Markup.callbackButton(messages.EDIT_TIME_SPENT,
      JSON.stringify({a: actions.CHOOSE_REPORT, id: report._id, f: 'time'}))],
    [Markup.callbackButton(messages.EDIT_TICKET_STATUS,
      JSON.stringify({a: actions.CHOOSE_REPORT, id: report._id, f: 'status'}))],
  ]).extra();
}

async function returnProjectId(ctx) {
  if(ctx.scene.state.ticketId) {
    const ticketId = await ctx.scene.state.ticketId;
    const ticket = await Ticket.findOne({_id: ticketId});
    return ticket.projectId;
  } else if(ctx.scene.state.reportId) {
    const reportId = await ctx.scene.state.reportId;
    const report = await Report.findOne({_id: reportId});
    const ticket = await Ticket.findOne({_id: report.ticketId});
    return ticket.projectId;
  }
}

async function updateReport(report) {
  let { _id, ticketStatus, description, timeSpent} = report;
  await Report.findByIdAndUpdate({_id}, {description, timeSpent, ticketStatus, editDate: new Date()});
};

async function saveReport(report) {
  let { ticketId, userId, ticketStatus, description, timeSpent} = report;
  await Report.create({ticketId, userId, description, timeSpent, ticketStatus, date: new Date(), editDate: new Date()});
};


async function updateTicket(ticketId, data){
  await Ticket.findOneAndUpdate({_id: ticketId}, {...data})
}


module.exports = {
  getEditButtons,
  returnProjectId,
  getSendButtons,
  updateReport,
  updateTicket,
  saveReport,
  actions,
};