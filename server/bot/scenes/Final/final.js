const Telegraf = require('telegraf');
const { BaseScene } = Telegraf;
const logger = require('../../../logger');

const sceneIds = require("../sceneIds");
const messages = require('./finalMessages');
const { getFinalButtons, actionNames } = require('./finalHelpers');
const { deleteMessages, resetScheduler } = require('../utils/helpers');

const finalScene = new BaseScene(sceneIds.FINAL);

finalScene.enter(async (ctx) => {
  logger.debug(ctx, 'In final scene.');
  ctx.scene.state.messagesToDelete = ctx.scene.state.messagesToDelete || [];
  ctx.scene.state.messagesToDelete.push(await ctx.reply(messages.WHAT_NEXT, getFinalButtons()));
});

finalScene.action(
  RegExp(actionNames.ENTER_CREATE_ANOTHER),
  async (ctx) => await ctx.scene.enter(sceneIds.PROJECTS)
);

finalScene.action(
  RegExp(actionNames.EXIT),
  async (ctx) => {
    logger.debug(ctx, 'Exited.');

    await ctx.reply(messages.BYE);
    await ctx.scene.leave();
  }
);

finalScene.leave(async ctx => {
  if(ctx.scene.state.messagesToDelete.length) {
    await deleteMessages(ctx);
  }
});

module.exports = finalScene;