module.exports = {
  WHAT_NEXT: 'Что хотите делать дальше?',
  BYE: 'Bye Bye!',
  buttons: {
    ENTER_CREATE_ANOTHER: 'Создать еще отчет',
    EXIT: 'Выйти'
  }
};