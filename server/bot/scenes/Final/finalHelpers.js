const {Extra} = require("telegraf");
const messages = require('./finalMessages');

const actionNames = {
  ENTER_CREATE_ANOTHER: 'enterToCreateAnother',
  EXIT: 'exit'
};

function getFinalButtons() {
  return Extra.HTML().markup((m) =>
    m.inlineKeyboard([
      m.callbackButton(
        messages.buttons.ENTER_CREATE_ANOTHER,
        actionNames.ENTER_CREATE_ANOTHER
        ),
      m.callbackButton(
        messages.buttons.EXIT,
        actionNames.EXIT
      )
  ]), {})
}

module.exports = {
  actionNames,
  getFinalButtons
};