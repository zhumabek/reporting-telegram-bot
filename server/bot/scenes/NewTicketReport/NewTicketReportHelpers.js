const { Markup } = require("telegraf");
const messages = require('./NewTicketReportMessages');
const User = require('../../../models/User');
const Ticket = require('../../../models/Ticket');
const Project = require('../../../models/Project');

async function returnUserId(ctx) {
  const user = await User.findOne({telegramUserId: ctx.from.id});
  return user._id;
}

async function createTicket(ctx, report) {
  const newTicket = new Ticket({
    projectId: report.projectId,
    userId: report.userId,
    number: report.ticketNumber,
    name: report.ticketName,
    status: report.ticketStatus,
  });
  await newTicket.save();

  return newTicket;
}

async function getProjectName(projectID) {
  const project = await Project.findOne({_id: projectID});
  return project.name;
}

async function checkIfTicketExists(ticketNumber, projectId) {
  const existingTicket = await Ticket.findOne({number: ticketNumber, projectId: projectId});
  return !!existingTicket;
}

async function checkIfTicketBelongsToUser(ticketNumber, projectId, userId) {
  const ticket = await Ticket.findOne({number: ticketNumber, projectId: projectId, userId: userId, status:'in progress'});
  return !!ticket;
}

async function getNextActionButtons() {
  return Markup.keyboard([
    messages.GO_TO_EXISTING_TICKETS,
    messages.ENTER_TICKET_NUMBER,
    messages.GO_BACK_TO_PREVIOUS_SCENE,
  ]).oneTime().resize().extra();
}


module.exports = {
  returnUserId,
  getProjectName,
  checkIfTicketExists,
  checkIfTicketBelongsToUser,
  getNextActionButtons,
  createTicket,
};