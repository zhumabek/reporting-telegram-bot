module.exports = {
  PROJECT_NAME: (projectName) => {
    return `Проект: ${projectName}`
  },
  TIME_SPENT_EXCEEDS_LIMIT: (timeSpent=0) => {
    return `Вы уже потратили ${timeSpent} часов из 12. Попробуйте еще раз`;
  },
  TICKET_NUMBER: 'Введите номер тикета',
  EXISTING_TICKET_NUMBER: 'Ошибка: Тикет с таким номером в данном проекте уже существует. Введите номер тикета заново!',
  EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER: 'Ошибка: В данном проекте у вас уже существует тикет с таким номером!',
  NEXT_ACTION: 'Что вы хотите сделать?',
  GO_TO_EXISTING_TICKETS: 'Вернуться в предзаполненные тикеты',
  ENTER_TICKET_NUMBER: 'Ввести номер тикета заново',
  TICKET_NAME: 'Введите название тикета',
  DESCRIPTION: 'Введите описание отчета',
  TIME_SPENT: 'Введите потраченное время',
  USE_NUMBERS: 'Ошибка: Введите заново только в цифрах',
  WRONG_TIME: 'Ошибка: Максимум - 12 часов',
  MUST_BE_NON_ZERO_VALUE: 'Ошибка: должен быть больше нуля',
  TICKET_STATUS: 'Выберите статус тикета',
  WRONG_STATUS: 'Ошибка: Несуществующий статус',
  SEND: 'Вы хотите отправить отчет?',
  WRONG_ACTION: 'Ошибка: Неправильное действие',
  FINAL_REPORT: 'Готовый отчет:',
  TICKET_CREATED: 'Тикет создан',
  GO_BACK_TO_PREVIOUS_SCENE: 'Назад',
};