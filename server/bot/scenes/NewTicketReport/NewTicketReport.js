const WizardScene = require('telegraf/scenes/wizard');
const logger = require('../../../logger');

const messages = require('./NewTicketReportMessages');
const reportMessages =require('../utils/reportMessages');
const sceneIds = require('../sceneIds');
const {
  getProjectName,
  checkIfTicketExists,
  checkIfTicketBelongsToUser,
  getNextActionButtons,
  createTicket,
  returnUserId
} = require('./NewTicketReportHelpers');
const {
  goBack,
  getStatusButtons,
  validTicketStatus,
  getReport,
  getNewTicketReportSendButtons,
  getTotalSpentTimeReachedLimitButtons,
  createReport,
  getBackButton,
  deleteMessages,
  checkIfTimeSpentExceedsLimit,
  getTotalTimeSpent,
  SPENT_TIME_LIMIT
} = require('../utils/helpers');


const newTicketReportScene = new WizardScene(sceneIds.NEW_TICKET_REPORT,
  async ctx => {
    logger.debug(ctx, 'Initial(0) step of newTicketReportScene');

    ctx.scene.state.messagesToDelete = ctx.scene.state.messagesToDelete || [];
    const userId = await returnUserId(ctx);
    const totalTimeSpentForToday = await getTotalTimeSpent(userId);

    if( totalTimeSpentForToday >= SPENT_TIME_LIMIT){
      logger.debug(ctx, 'Total spent time for to day reached limit');

      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(reportMessages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT, getTotalSpentTimeReachedLimitButtons())
      );
      ctx.wizard.next();

    } else {
      ctx.wizard.selectStep(1);
      return newTicketReportScene.middleware()(ctx);
    }
  },

  async ctx => {

    ctx.scene.state.report = {
      projectId: ctx.scene.state.projectId,
      ticketId: null,
      userId: await returnUserId(ctx),
      ticketNumber: 0,
      ticketName: '',
      description: '',
      timeSpent: 0,
      ticketStatus: '',
    };

    const projectName = await getProjectName(ctx.scene.state.projectId);
    logger.debug(ctx, 'In create new ticket report scene', {projectName});

    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.PROJECT_NAME(projectName)),
      await ctx.reply(messages.TICKET_NUMBER, getBackButton())
    );

    await ctx.wizard.next();
  },


  async ctx => {
    let ticketNumber = ctx.message.text;
    if (await checkIfTicketExists(ticketNumber, ctx.scene.state.projectId)) {
      if(await checkIfTicketBelongsToUser(ticketNumber, ctx.scene.state.projectId, ctx.scene.state.report.userId)) {
        ctx.scene.state.messagesToDelete.push(
          await ctx.reply(messages.EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER),
          await ctx.reply(messages.NEXT_ACTION, await getNextActionButtons())
        );
        await ctx.wizard.next();

      } else {
        ctx.scene.state.messagesToDelete.push(
          await ctx.reply(messages.EXISTING_TICKET_NUMBER),
        );
        await goBack(ctx);
      }

    } else {
      ctx.scene.state.report.ticketNumber = ticketNumber;
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.TICKET_NAME)
      );
      await ctx.wizard.next();
      await ctx.wizard.next();
    }

  },


  async ctx => {
    if(await ctx.message.text === messages.GO_TO_EXISTING_TICKETS) {
      await ctx.scene.enter(sceneIds.EXISTING_TICKETS, {projectId: ctx.scene.state.projectId});

    } else if(await ctx.message.text === messages.ENTER_TICKET_NUMBER) {
      await ctx.scene.enter(ctx.scene.current.id, {...ctx.scene.state});

    } else {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.WRONG_ACTION)
      );

      await goBack(ctx);
    }
  },


  async ctx => {
    ctx.scene.state.report.ticketName = ctx.message.text;
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.DESCRIPTION)
    );

    await ctx.wizard.next();
  },


  async ctx => {
    ctx.scene.state.report.description = ctx.message.text;
    ctx.scene.state.messagesToDelete.push(
      await ctx.reply(messages.TIME_SPENT)
    );
    await ctx.wizard.next();
  },


  async ctx => {
    const timeSpent = ctx.message.text;
    let {userId} = ctx.scene.state.report;
    let totalTimeSpent = await getTotalTimeSpent(userId);

    if(isNaN(timeSpent)) {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.USE_NUMBERS)
      );

      await goBack(ctx);
    } else if(timeSpent <= 0) {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.MUST_BE_NON_ZERO_VALUE)
      );

      await goBack(ctx);
    } else if(await checkIfTimeSpentExceedsLimit(timeSpent, totalTimeSpent)) {

      ctx.scene.state.messagesToDelete.push(await ctx.reply(messages.WRONG_TIME));
      ctx.scene.state.messagesToDelete.push(await ctx.reply(messages.TIME_SPENT_EXCEEDS_LIMIT(totalTimeSpent)));

      await goBack(ctx);
    } else {
      ctx.scene.state.report.timeSpent = timeSpent;
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.TICKET_STATUS, getStatusButtons())
      );

      await ctx.wizard.next();
    }
  },


  async ctx => {
    const ticketStatus = ctx.message.text;
    if(!validTicketStatus(ticketStatus)) {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.WRONG_STATUS)
      );
      await goBack(ctx);

    } else {
      ctx.scene.state.report.ticketStatus = ticketStatus;
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(`${messages.FINAL_REPORT} ${getReport(ctx.scene.state.report)}`),
        await ctx.reply(messages.SEND, getNewTicketReportSendButtons())
      );
      await ctx.wizard.next();
    }
  },


  async ctx => {
    if(ctx.message.text === reportMessages.NO_DONT_SEND_REPORT) {
      logger.debug(ctx, 'Canceled create new ticket report.');
      await ctx.scene.enter(sceneIds.FINAL);

    } else if(ctx.message.text === reportMessages.YES_SEND_REPORT) {
      const newTicket = await createTicket(ctx, ctx.scene.state.report);
      ctx.scene.state.report.ticketId = newTicket._id;
      await createReport(ctx.scene.state.report);

      logger.debug(ctx, 'Saved new ticket report.', {report: ctx.scene.state.report});
      const msg = await ctx.reply(reportMessages.REPORT_CREATED);
      await ctx.scene.enter(sceneIds.FINAL, {messagesToDelete: [msg]});

    } else if(ctx.message.text === reportMessages.EDIT_NEW_TICKET_REPORT) {
      await ctx.scene.enter(sceneIds.EDIT_NEW_TICKET_REPORT, {report: ctx.scene.state.report});

    } else {
      ctx.scene.state.messagesToDelete.push(
        await ctx.reply(messages.WRONG_ACTION)
      );
      await goBack(ctx);
    }
  },
);

newTicketReportScene.leave(async (ctx) => {
  await deleteMessages(ctx);
});

newTicketReportScene.hears(messages.GO_BACK_TO_PREVIOUS_SCENE, async ctx => {
  await ctx.scene.enter(sceneIds.PROJECTS);
});

newTicketReportScene.hears(reportMessages.GO_TO_RECENT_REPORT_TICKETS, async ctx => {
  await ctx.scene.enter(sceneIds.RECENT_REPORT_TICKETS, {projectId: ctx.scene.state.projectId});
});

module.exports = newTicketReportScene;