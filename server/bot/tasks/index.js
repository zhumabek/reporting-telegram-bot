const moment = require('moment');

const CronJob = require('cron').CronJob;
const logger = require('../../logger');

const User = require('../../models/User');
const Project = require('../../models/Project');
const Ticket = require('../../models/Ticket');
const Report = require('../../models/Report');
const {
  SCHEDULE_FOR_SEND_REPORTS,
  SCHEDULE_FOR_NOTIFY_TO_REPORT,
  SCHEDULE_FOR_NOTIFY_TO_REPORT_FOR_PROJECT,
  SCHEDULE_FOR_NOTIFY_TO_REPORT_TICKET_STATUS,
  TIME_ZONE
} = require('./constants');
const {
  getActiveUsers,
  filterTodayWorksUsers,
  filterDidntReportUsers,
  getMemberReportsMassageText,
  isTodayWeekend,
  isUserWorksToday,
  isUserAlreadyReportForProjectToday
} = require('./helpers');


function sendReportsToProjectChats(bot) {
  return new CronJob(SCHEDULE_FOR_SEND_REPORTS, async function () {

    let today = new Date();
    today.setHours(0, 0, 0, 0);
    const hour = moment().locale('ru').format('LT'); // because this will work every minute

    const projects = await Project.find({isClosed: false, reportsSendTime: hour});

    for (const project of projects) {
      for (const memberId of project.members) {

        let user = await User.findOne({_id: memberId, active: true});
        if (!user) {
          continue;
        }

        let tickets = await Ticket.find({userId: user._id, projectId: project._id});
        let message = await getMemberReportsMassageText(user, tickets, today);
        if (!message) {
          continue;
        }
        try {
          await bot.telegram.sendMessage(project.chatId, message);
          logger.debug(
            undefined,
            'Sent reports to group.',
            {chatId: project.chatId, projectName: project.name}
          )
        } catch (e) {
          logger.error(undefined, 'Error when send reports to project group!', e)
        }
      }
    }
  }, null, true, TIME_ZONE);
}


function notifyToReport(bot) {
  return new CronJob(SCHEDULE_FOR_NOTIFY_TO_REPORT, async function () {
    if (isTodayWeekend()) {
      return
    }

    let users = await getActiveUsers();
    users = await filterTodayWorksUsers(users);
    users = await filterDidntReportUsers(users);
    logger.debug(undefined, 'Filtered users to notify:', users);

    for (const user of users) {
      try {
        await bot.telegram.sendMessage(
          user.telegramChatId,
          "<b>Уведомление:</b> Вы не написали отчет за сегодня! Если вы не напишите отчет до 23:59:59, " +
          "ждите завтра злого ПМа с надувным молотком в гости. Есть возможность написать " +
          "отчёт вручную завтра и молить о прощении ПМа.\n\n/start чтобы начать.",
          {parse_mode: 'HTML'},
        );
        logger.debug(undefined, 'Notified user: ', {userName: user.name})
      } catch (e) {
        logger.error(undefined, 'Error when notify user!', e)
      }
    }
  }, null, true, TIME_ZONE)
}


function notifyToReportForProject(bot) {
  return new CronJob(
    SCHEDULE_FOR_NOTIFY_TO_REPORT_FOR_PROJECT,
    async function () {
      if (isTodayWeekend()) {
        return
      }

      const messages = {};
      const today = new Date();
      today.setHours(0, 0, 0, 0);
      const hour = moment().locale('ru').add(1, 'hour').format('LT');

      for (const project of await Project.find({isClosed: false, reportsSendTime: hour}, {
        name: true,
        members: true,
        notNotification: true
      })) {
        project.members = await User.find({_id: {$in: project.members}, role: 'user', active: true}, {
          name: true,
          hrmUserId: true
        });
        project.notNotification = await User.find({_id: {$in: project.notNotification}, role: 'user', active: true}, {
          name: true,
          hrmUserId: true
        });
        for (const userNotNotification of project.notNotification) {
          project.members = project.members.filter(developer => developer.name !== userNotNotification.name);
        }

        for (const user of await User.find({_id: {$in: project.members}, role: "user", active: true})) {
          if (!await isUserWorksToday(user)) {
            continue
          }
          if (await isUserAlreadyReportForProjectToday(user._id, project._id, today)) {
            logger.debug(undefined, 'User already reported for project:', {name: user.name, project: project.name});
            continue
          }

          if (messages[user.telegramChatId] === undefined) {
            messages[user.telegramChatId] = `${project.name}`;
            logger.debug(undefined, 'Adding user to notify: ', {name: user.name, project: project.name});
          } else {
            messages[user.telegramChatId] += `, ${project.name}`
          }
        }
      }
      for (const [chatId, projectsText] of Object.entries(messages)) {
        try {
          await bot.telegram.sendMessage(
            chatId,
            `<b>Уведомление:</b> Вы не написали отчет за сегодня по проектам <b>${projectsText}</b>! Если вы не напишите отчет до 23:59:59, ` +
            "ждите завтра злого ПМа с надувным молотком в гости. Есть возможность написать " +
            "отчёт вручную завтра и молить о прощении ПМа.\n\n/start чтобы начать.",
            {parse_mode: 'HTML'}
          );
          logger.debug(undefined, 'Notified user for projects ', {chatId, projects: projectsText})
        } catch (e) {
          logger.debug(
            undefined,
            "Error when notifying user to report for projects.",
            e
          )
        }

      }
    },
    null, true, TIME_ZONE
  )
}

function notifyToReportTicketStatusForMoreThanTwentyHours(bot) {
  return new CronJob(
    SCHEDULE_FOR_NOTIFY_TO_REPORT_TICKET_STATUS,
    async function () {
      if (isTodayWeekend()) {
        return
      }

      let projectManagers;
      let reportsList = [];
      let messagesUser = [];
      let messagesProjectManager = [];
      const hour = moment().subtract(20, 'hour').format();
      const hourTo = moment().subtract(24, 'hour').format();

      for (const project of await Project.find({isClosed: false}, {name: true, members: true})) {
        projectManagers = await User.find({
          _id: {$in: project.members},
          role: 'project manager',
          active: true
        }, {name: true, telegramChatId: true, hrmUserId: true});
        for (const ticket of await Ticket.find({projectId: project._id, status: 'in progress'})) {
          const reports = await Report.find({
            ticketId: ticket._id,
            ticketStatus: 'in progress',
            "date": {$gte: hourTo, $lte: hour}
          })
            .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name members'})})
            .populate('userId', 'name role hrmUserId telegramChatId');
          if (reports.length > 1) {
            reportsList.push(reports[0]);
          } else {
            reportsList.push(...reports);
          }
        }
      }

      for (const report of reportsList) {
        for (const projectManager of projectManagers) {
          if (!await isUserWorksToday(projectManager)) {
            continue
          }

          messagesProjectManager.push({
            projectManager: projectManager.telegramChatId,
            projectManagerName: projectManager.name,
            user: report.userId.name,
            project: report.ticketId.projectId.name,
            ticket: report.ticketId.number
          });
        }

        if (!await isUserWorksToday(report.userId)) {
          continue
        }

        if (report.userId.role === 'user') {
          messagesUser.push({
            user: report.userId.telegramChatId,
            userName: report.userId.name,
            project: report.ticketId.projectId.name,
            ticket: report.ticketId.number
          });
        }
      }

      for (const message of messagesUser) {
        const {user, userName, ticket, project} = message;
        try {
          await bot.telegram.sendMessage(
            user,
            `<b>Уведомление:</b> <b><i>Тикет № ${ticket}</i></b> в проекте <b>"${project}"</b> выполняется более 20 часов. возможно вы забыли закрыть тикет.`,
            {parse_mode: 'HTML'},
          );
          logger.debug(undefined, `Notified user ${userName} for ticket `, {
            chatId: user,
            ticket: ticket,
            projects: project
          })
        } catch (e) {
          logger.debug(
            undefined,
            `Error when notifying user ${userName} to report for ticket.`,
            e
          )
        }
      }

      for (const message of messagesProjectManager) {
        const {projectManager, projectManagerName, user, ticket, project} = message;
        try {
          await bot.telegram.sendMessage(
            projectManager,
            `<b>Уведомление:</b> <b><i>Тикет № ${ticket}</i></b> выполняется более 20 часов. Разработчиком <b><i>${user}</i></b> в проекте <b>"${project}"</b>`,
            {parse_mode: 'HTML'},
          );
          logger.debug(undefined, `Notified project manager ${projectManagerName} for ticket `, {
            chatId: projectManager,
            user: user,
            ticket: ticket,
            projects: project
          })
        } catch (e) {
          logger.debug(
            undefined,
            `Error when notifying project manager ${projectManagerName} to report for ticket.`,
            e
          )
        }
      }
    },
    null, true, TIME_ZONE
  )
}

function notifyToReportForProjectManager(bot) {
  return new CronJob(
    SCHEDULE_FOR_NOTIFY_TO_REPORT_FOR_PROJECT,
    async function () {
      if (isTodayWeekend()) {
        return
      }

      let messages = [];
      const today = new Date();
      const hour = moment().locale('ru').add(1, 'hour').format('LT');
      today.setHours(0, 0, 0, 0);

      for (const project of await Project.find({isClosed: false, reportsSendTime: hour}, {
        name: true,
        members: true,
        notNotification: true
      })) {
        for (const projectManager of await User.find({
          _id: {$in: project.members},
          role: 'project manager',
          active: true
        }, {name: true, telegramChatId: true, hrmUserId: true})) {
          project.members = await User.find({_id: {$in: project.members}, role: 'user', active: true}, {
            name: true,
            hrmUserId: true
          });
          project.notNotification = await User.find({_id: {$in: project.notNotification}, role: 'user', active: true}, {
            name: true,
            hrmUserId: true
          });

          for (const userNotNotification of project.notNotification) {
            project.members = project.members.filter(developer => developer.name !== userNotNotification.name);
          }
          for (const user of project.members) {
            if (!await isUserWorksToday(user)) {
              project.members = project.members.filter(developer => developer.name !== user.name);
              continue
            }

            if (await isUserAlreadyReportForProjectToday(user._id, project._id, today)) {
              project.members = project.members.filter(developer => developer.name !== user.name);
              logger.debug(undefined, 'User already reported for project:', {name: user.name, project: project.name});
            }
          }

          if (!await isUserWorksToday(projectManager)) {
            continue
          }

          if (project.members.length > 0) {
            messages.push({
              projectManager: projectManager.telegramChatId,
              projectManagerName: projectManager.name,
              project: project.name,
              users: project.members
            });
          }
        }
      }

      function messageForSend(user, developers, project) {
        if (user.length > 1) {
          return `<b>Уведомление:</b> Пользователи <i><b>${developers}</b></i> не написали отчет за сегодня по проекту "<b>${project}</b>".` +
            " Вам будет необходимо внести данные в систему вручную, после того как вы получите этот отчет в чате проекта."
        } else {
          return `<b>Уведомление:</b> Пользователь <i><b>${developers}</b></i> не написал отчет за сегодня по проекту "<b>${project}</b>".` +
            " Вам будет необходимо внести данные в систему вручную, после того как вы получите этот отчет в чате проекта."
        }
      }

      for (const message of messages) {
        const {projectManager, projectManagerName, users, project} = message;
        const developers = users.map(user => user.name).join(', ');
        try {
          await bot.telegram.sendMessage(
            projectManager,
            messageForSend(users, developers, project),
            {parse_mode: 'HTML'},
          );
          logger.debug(undefined, `Notified project manager ${projectManagerName} for projects `, {
            chatId: projectManager,
            users: developers,
            projects: project
          })
        } catch (e) {
          logger.debug(
            undefined,
            `Error when notifying project manager ${projectManagerName} to report for projects.`,
            e
          )
        }
      }
    },
    null, true, TIME_ZONE
  )
}


function notifyToReportTicketStatusForMoreThanSevenDays(bot) {
  return new CronJob(
    SCHEDULE_FOR_NOTIFY_TO_REPORT_TICKET_STATUS,
    async function () {
      if (isTodayWeekend()) {
        return
      }

      let messagesUserList = [];
      let messagesUser = [];
      let messagesProjectManagerList = [];
      let messagesProjectManager = [];
      const days = moment().subtract(7, 'days').format();

      for (const project of await Project.find({isClosed: false}, {name: true, members: true})) {
        for (const projectManager of await User.find({
          _id: {$in: project.members},
          role: 'project manager',
          active: true
        }, {name: true, telegramChatId: true, hrmUserId: true})) {
          project.members = await User.find({_id: {$in: project.members}, role: 'user', active: true}, {
            name: true,
            hrmUserId: true
          });
          for (const ticket of await Ticket.find({
            projectId: project._id,
            userId: {$in: project.members},
            status: 'in progress'
          })) {
            for (const report of await Report.find({
              ticketId: ticket._id,
              userId: {$in: project.members},
              ticketStatus: 'in progress',
              date: {$lte: days}
            }, {ticketId: true, userId: true})
              .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
              .populate('userId', 'name role hrmUserId telegramChatId')) {

              if (!await isUserWorksToday(projectManager)) {
                continue
              }
              messagesProjectManagerList.forEach(messPM => {
                if ((messPM.user === report.userId.name && messPM.project === report.ticketId.projectId.name && messPM.projectManager === projectManager.telegramChatId)) {
                  messPM.ticket += `, ${report.ticketId.number}`
                }
              })
              messagesProjectManagerList.push({
                projectManager: projectManager.telegramChatId,
                projectManagerName: projectManager.name,
                user: report.userId.name,
                project: report.ticketId.projectId.name,
                ticket: report.ticketId.number
              });

              if (!await isUserWorksToday(report.userId)) {
                continue
              }
              messagesUserList.forEach(messUser => {
                if ((messUser.userName === report.userId.name && messUser.project === report.ticketId.projectId.name)) {
                  messUser.ticket.push(report.ticketId.number)
                }
              })
              if (report.userId.role === 'user') {
                messagesUserList.push({
                  user: report.userId.telegramChatId,
                  userName: report.userId.name,
                  project: report.ticketId.projectId.name,
                  ticket: [report.ticketId.number]
                });
              }
            }
          }
        }
      }

      messagesProjectManagerList.map(messPM => {
        const sumTicket = messagesProjectManagerList.filter(mess => messPM.user === mess.user && messPM.project === mess.project && messPM.projectManager === mess.projectManager)
        if (sumTicket.length > 1) {
          messagesProjectManager.push(sumTicket[0]);
        } else {
          messagesProjectManager.push(...sumTicket);
        }
      })

      messagesUserList.map(messPM => {
        const sumTicket = messagesUserList.filter(mess => messPM.userName === mess.userName && messPM.project === mess.project)
        if (sumTicket.length > 1) {
          messagesUser.push(sumTicket[0]);
        } else {
          messagesUser.push(...sumTicket);
        }
      })

      const messagesPMSend = messagesProjectManager.filter((e, i, a) => a.indexOf(e) === i);
      const messagesUserSend = messagesUser.filter((e, i, a) => a.indexOf(e) === i);

      for (const message of messagesUserSend) {
        const {user, userName, ticket, project} = message;
        const tickets = ticket.filter((e, i, a) => a.indexOf(e) === i).join(', ');
        try {
          await bot.telegram.sendMessage(
            user,
            `<b>Уведомление:</b> <b><i>Тикет № ${tickets}</i></b> в проекте <b>"${project}"</b> был создан более 7 дней назад, возможно вы забыли закрыть тикет.`,
            {parse_mode: 'HTML'},
          );
          logger.debug(undefined, `Notified user ${userName} for ticket `, {
            chatId: user,
            ticket: ticket,
            projects: project
          })
        } catch (e) {
          logger.debug(
            undefined,
            `Error when notifying user ${userName} to report for ticket.`,
            e
          )
        }
      }

      for (const message of messagesPMSend) {
        const {projectManager, projectManagerName, user, ticket, project} = message;
        try {
          await bot.telegram.sendMessage(
            projectManager,
            `<b>Уведомление:</b> У разработчика <b><i>${user}</i></b> в проекте <b>"${project}"</b> <b><i>Тикет № ${ticket}</i></b> были созданы более 7 дней назад`,
            {parse_mode: 'HTML'},
          );
          logger.debug(undefined, `Notified project manager ${projectManagerName} for ticket `, {
            chatId: projectManager,
            user: user,
            ticket: ticket,
            projects: project
          })
        } catch (e) {
          logger.debug(
            undefined,
            `Error when notifying project manager ${projectManagerName} to report for ticket.`,
            e
          )
        }
      }
    },
    null, true, TIME_ZONE
  )
}

module.exports = {
  sendReportsToProjectChats,
  notifyToReportForProject,
  notifyToReport,
  notifyToReportForProjectManager,
  notifyToReportTicketStatusForMoreThanTwentyHours,
  notifyToReportTicketStatusForMoreThanSevenDays,
};
