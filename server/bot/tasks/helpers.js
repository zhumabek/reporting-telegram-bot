const axios = require('axios');
const jwt = require('jsonwebtoken');
const config = require('../../config');
const logger = require('../../logger');

const Report = require('../../models/Report');
const Ticket = require('../../models/Ticket');
const User = require('../../models/User');
const { SATURDAY, SUNDAY } = require('./constants');


async function getMemberReportsMassageText(user, tickets, today){
  const messageTitle = `Отчеты: ${user.name} ${user.email}\n\n`;
  let message = messageTitle;

  for(let i=0; i<tickets.length; i++){
    let ticket = tickets[i];
    let todayReports = await Report.find({userId: user._id, ticketId: ticket._id, date: {$gt: today}});
    for(let j=0; j<todayReports.length; j++){
      let report = todayReports[j];
      message += `Тикет: #${ticket.number}\nНазвание: ${ticket.name}\nСтатус: ${report.ticketStatus}\nПотрачено часов: ${report.timeSpent}\nОписание: ${report.description}\n\n`;
    }
  }
  if (messageTitle === message){
    return null
  }
  return message
}

async function getActiveUsers() {
  return await User.find({role: 'user', active: true});
}
async function filterTodayWorksUsers(users){
  const filteredUsers = [];
  for (let i=0; i < users.length; i++){
    let user = users[i];
    if (await isUserWorksToday(user)){
      filteredUsers.push(user)
    }
  }
  return filteredUsers;
}
async function filterDidntReportUsers(users){
  let today = new Date().setHours(0,0,0,0);
  let filteredUsers = [];

  for (let i=0; i < users.length; i++){
    let user = users[i];
    const reports = await Report.find({userId: user._id, date: {$gt: today}});
    if (reports.length === 0) {
      filteredUsers.push(user)
    }
  }

  return filteredUsers;
}

async function isUserWorksToday(user) {
  let payload = {'payload': config.hrmPayload};
  let token = jwt.sign(payload, config.hrmSecretKey, {algorithm: 'HS256'});

  try{
    const response = await axios.get(config.hrmHost + `/api/v2/attractor-reporting-bot/employee/${user.hrmUserId}/time_sheet_sheet`, {headers: {Authorization: token}});
    logger.debug(
      undefined,
      `${user.name} time sheet:`,
      response.data.sheet
    );
    return response.data.sheet[new Date().getDate()-1] !== 0

  } catch (e) {
    logger.error(
      undefined,
      `HRM sent error when check user ${user.name} for active in schedule.`,
      {status: e.response.status, statusText: e.response.statusText}
    );
    const error_type = String(e.response.status).charAt(0);
    if (error_type === '5'){
      return true
    } else {
      return false
    }
  }
}

async function isUserAlreadyReportForProjectToday(userId, projectId, today){
  const tickets = await Ticket.find({userId, projectId});
  const ticketIds = tickets.map((ticket) => {return ticket._id});
  const reports = await Report.find({ticketId: {$in: ticketIds}, date: {$gt: today}});

  return reports.length !== 0;
}

function isTodayWeekend(){
  const today = new Date();
  return today.getDay() === SATURDAY || today.getDay() === SUNDAY;
}

module.exports = {
  getActiveUsers,
  filterTodayWorksUsers,
  filterDidntReportUsers,
  getMemberReportsMassageText,
  isUserAlreadyReportForProjectToday,
  isTodayWeekend,
  isUserWorksToday
};