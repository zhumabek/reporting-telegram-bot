const Telegraf = require('telegraf');
const Stage = require('telegraf/stage');
const { leave } = Stage;

const authMiddleware = require('./middlewares/auth');
const customSessionMiddleware = require('./middlewares/customSession');
const sceneIds = require('./scenes/sceneIds');
const {getUrlButton} = require('./scenes/utils/helpers');
const {COMMAND_IS_NOT_APPLICABLE_HERE} = require('./scenes/utils/reportMessages');
const config = require('../config.js');

const startScene = require('./scenes/Start/start');
const confirmEmailScene = require('./scenes/ConfirmEmail/confirmEmail');
const projectsScene = require('./scenes/Projects/projects');
const existingTicketsScene = require('./scenes/ExistingTickets/existingTickets');
const recentReportsScene = require('./scenes/EditReport/editReport');
const recentReportTicketsScene = require('./scenes/RecentReportTickets/recentReportTickets');
const newTicketReportScene = require('./scenes/NewTicketReport/NewTicketReport');
const editNewTicketReportScene = require('./scenes/EditNewTicketReport/editNewTicketReport');
const editNewTicketReportFieldScene = require('./scenes/EditNewTicketReportField/editNewTicketReportField');
const existingTicketReport = require('./scenes/ExistingTicketReport/ExistingTicketReport');
const finalScene = require('./scenes/Final/final');
const editReportField = require('./scenes/EditReportField/EditReportField');
const noTicketReport = require('./scenes/NoTicketReport/noTicketReport');


const bot = new Telegraf(
  config.telegram.bot_token,
  {
    telegram: {
      webhookReply: false
    }
  });

const stage = new Stage([
  startScene,
  confirmEmailScene,
  projectsScene,
  existingTicketsScene,
  recentReportTicketsScene,
  recentReportsScene,
  newTicketReportScene,
  editNewTicketReportScene,
  editNewTicketReportFieldScene,
  existingTicketReport,
  finalScene,
  editReportField,
  noTicketReport,
]);
stage.command('leave', leave());

bot.use(customSessionMiddleware({ttl: 2400}));
bot.use(authMiddleware);
bot.use(stage.middleware());

bot.command('start', async (ctx) => {
  const chatType = ctx.chat.type;
  if(chatType !== 'private') {
    await ctx.reply(COMMAND_IS_NOT_APPLICABLE_HERE, await getUrlButton(ctx));
  } else {
    await ctx.scene.enter(sceneIds.START);
  }
});

module.exports = bot;