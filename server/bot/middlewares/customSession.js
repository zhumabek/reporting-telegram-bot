const { deleteMessages, deleteUserIfStillInactive } = require('./helpers');


module.exports = function (opts) {
  const options = {
    property: 'session',
    store: new Map(),
    getSessionKey: (ctx) => ctx.from && ctx.chat && `${ctx.from.id}:${ctx.chat.id}`,
    ...opts
  };

  const ttlMs = options.ttl && options.ttl * 1000;

  return (ctx, next) => {
    const key = options.getSessionKey(ctx);
    if (!key) {
      return next(ctx)
    }
    const now = new Date().getTime();
    return Promise.resolve(options.store.get(key))
      .then((state) => state || { session: {} })
      .then(({ session, expires }) => {
        if (expires && expires < now) {
          if (session !== {} && session.__scenes && session.__scenes.state){
            deleteMessages(ctx, Object.assign({}, session));
            deleteUserIfStillInactive(ctx.from.id);
            ctx.reply('Состояние чата перезагружено из за долгой не активности.' +
              '\n/start, чтобы начать заново.');

            options.store.set(key, {
              session: {},
              expires: ttlMs ? now + ttlMs : null
            });
            return
          }
          session = {}
        }

        Object.defineProperty(ctx, options.property, {
          get: function () { return session },
          set: function (newValue) { session = { ...newValue } }
        });

        return next(ctx).then(() => {
          if (session.__scenes && !Object.keys(session.__scenes).length){
            session.__scenes = undefined
          }
          options.store.set(key, {
            session,
            expires: ttlMs ? now + ttlMs : null
          })
        })
      })
  }
};