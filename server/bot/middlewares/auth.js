const sceneIds = require('../scenes/sceneIds');
const {
  isUserExist,
  isActive
} = require('./helpers');


module.exports = async function (ctx, next) {
  if (await isUserExist(ctx.from.id) && await isActive(ctx.from.id)){
    return next()

  } else if (
    !(ctx.session.__scenes) ||
    ctx.session.__scenes.current === sceneIds.START ||
    ctx.session.__scenes.current === sceneIds.CONFIRM_EMAIL
  ){
    return next()

  } else {
    ctx.session.__scenes.current = sceneIds.START;
    return next()
  }
};