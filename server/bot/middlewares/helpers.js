const User = require('../../models/User');


async function isUserExist(telegramUserId){
  const user = await User.findOne({telegramUserId});
  return !!user;
}

async function isActive(telegramUserId){
  const user = await User.findOne({telegramUserId});
  return !!(user && user.active);
}

async function deleteMessages(ctx, session) {
  if (session.__scenes.state.messagesToDelete){
    await session.__scenes.state.messagesToDelete.forEach(async message => {
      try{
        await ctx.deleteMessage(message.message_id);
      } catch (e) {
        // pass
      }
    });
  }

  if (session.__scenes.state.messagesToDeleteAfterScene){
    await session.__scenes.state.messagesToDeleteAfterScene.forEach(async message => {
      try{
        await ctx.deleteMessage(message.message_id);
      } catch (e) {
        // pass
      }
    });
  }
}

async function deleteUserIfStillInactive(telegramUserId){
  try{
    await User.findOneAndDelete({telegramUserId, active: false})
  } catch (e) {
    //pass
  }
}

module.exports = {
  isUserExist,
  isActive,
  deleteMessages,
  deleteUserIfStillInactive
};
