const Ticket = require('../../models/Ticket');
const Report = require('../../models/Report');
const {createTicketNumber} = require("../../bot/scenes/NoTicketReport/noTicketReportHelpers");

exports.getTickets = async (req, res, next) => {
  const projectId = req.params.id;
  try {
    const tickets = await Ticket.find(req.user.role === 'user' ? {projectId: projectId, userId: req.user._id} : {projectId: projectId}).populate('userId', 'name').sort({number: -1});

    const ticketsWithDates = [];
    for (let i = 0; i < tickets.length; i++) {
      const ticketReports = await Report.find({ticketId: tickets[i]._id});
      const ticketItem = {...tickets[i]._doc};
      if (ticketReports[0]) {
        await ticketReports.sort((a, b) => new Date(a.date) - new Date(b.date));
        ticketItem.date = ticketReports[0].date;
      } else {
        ticketItem.date = 'Отсутствует';
      }
      ticketsWithDates.push(ticketItem);
    }
    await ticketsWithDates.sort((a, b) => new Date(b.date) - new Date(a.date));

    return res.send(ticketsWithDates);
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getTicketNumber = async (req, res, next) => {
  const projectId = req.params.id;
  try {
    const newTicketNumber = await createTicketNumber(projectId);
    return res.status(200).send({message: 'New ticket number item was successfully fetched!', newTicketNumber});
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }

};
