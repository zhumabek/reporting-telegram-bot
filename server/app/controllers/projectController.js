const Project = require('../../models/Project');
const JiraProject = require('../../models/JiraProject');
const User = require('../../models/User');
const {validationResult} = require('express-validator');
const {projectTypeEnum} = require('../../models/constants');
const {
  getStringValueOfEnum,
  sendNotificationToProjectMembers,
  addPmtUser
} = require('../utils/helpers');
const JiraRequester = require('../utils/requester');

exports.getAllProjectsInProgress = async (req, res, next) => {
  try {
    const isActive = req.params.active === 'active';
    const isUserRolePM = req.user.role === 'project manager';
    const isUserRoleUser = req.user.role === 'user';
    let projects;
    let message;

    if (isActive) {
      projects = await Project.find((isUserRolePM || isUserRoleUser) ?
        {isClosed: false, members: req.user._id} :
        {isClosed: false}
      ).sort('name');
      message = 'Active projects were fetched successfully';

    } else {
      const projectSort = [['isClosed', 1], ['name', 1]];
      projects = await Project.find((isUserRolePM || isUserRoleUser) ? {members: req.user._id} : null).sort(projectSort);
      message = "All projects were fetched successfully"
    }

    const resultProjects = projects.map(project => {
      let r = {...project._doc};
      r.pmtType = getStringValueOfEnum(r.pmtType);
      return r;
    });

    res.status(200).json({message, projects: resultProjects});

  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.getProjectMembers = async (req, res, next) => {
  const projectId = req.params.id;
  try {
    const project = await Project.findById({_id: projectId});
    if (!project) {
      let error = new Error("Project not found!!");
      error.statusCode = 404;
      throw error;
    }
    const members = await User.find({_id: {$in: project.members}},'name email role');
    res.status(200).json({message: 'Project members were fetched successfully', members});
  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.getNotMembers = async (req, res, next) => {
  let projectId = req.params.id;
  try {
    const project = await Project.findById(projectId, 'members');
    const members = await User.find({active: true, _id: {$nin: project.members}}, 'name email');
    res.status(200).json({message: 'Not a member users were fetched successfully', members});
  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.addProjectMember = async (req, res, next) => {
  const projectId = req.params.id;
  const newlyAddedUserId = req.body.userId;
  const pmtUserIdentifier = req.body.pmtUserIdentifier;
  try {
    const project = await Project.findById({_id: projectId});
    if (!project) {
      let error = new Error("Project not found!!");
      error.statusCode = 404;
      throw error;
    }

    project.members = [newlyAddedUserId, ...project.members];
    project.save();

    await addPmtUser(project.pmtType, newlyAddedUserId, pmtUserIdentifier);

    res.status(200).json({message: 'A new member successfully has been added to the project!'});
  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.addProjectMemberNotNotification = async (req, res, next) => {
  const projectId = req.params.id;
  const newlyAddedUserId = req.body.userId;
  try {
    const project = await Project.findById({_id: projectId});
    if (!project) {
      let error = new Error("Project not found!!");
      error.statusCode = 404;
      throw error;
    }
    project.notNotification = [newlyAddedUserId, ...project.notNotification];
    project.save();

    res.status(200).json({message: `Разработчик не будет получать уведомления по данному проекту!`});
  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.deleteProjectMember = async (req, res, next) => {
  const projectId = req.params.projectId;
  const userId = req.params.userId;
  try {
    const project = await Project.findByIdAndUpdate(projectId, { $pull: { members: userId }});
    if (!project) {
      let error = new Error("Project not found. Project member can't be deleted");
      error.statusCode = 404;
      throw error;
    }

    res.status(200).json({message: 'Project member successfully has been deleted!'});
  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.getProjectNotNotification = async (req, res, next) => {
  const projectId = req.params.id;
  try {
    const project = await Project.findById({_id: projectId});
    if (!project) {
      let error = new Error("Project not found!!");
      error.statusCode = 404;
      throw error;
    }
    const notNotification = await User.find({_id: {$in: project.notNotification}},'name email role');
    res.status(200).send({message: 'Project members who do not receive notifications have been successfully selected.', notNotification});
  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.deleteProjectMemberNotNotification = async (req, res, next) => {
  const projectId = req.params.projectId;
  const userId = req.params.userId;
  try {
    const project = await Project.findByIdAndUpdate(projectId, { $pull: { notNotification: userId }});
    if (!project) {
      let error = new Error("Project not found. Project member who do not receive notifications can't be deleted");
      error.statusCode = 404;
      throw error;
    }

    res.status(200).json({message: `Разработчик будет получать уведомления по данному проекту!`});
  } catch (error) {
    if (!error.statusCode) {
      error.status = 500;
    }
    next(error);
  }
};

exports.closeProject = async (req, res, next) => {
  const projectId = req.params.id;
  try {
    const project = await Project.findOne({_id: projectId});
    if (project.members.length === 0) {
      if (req.body.status === 'active') {
        project.isClosed = false
      } else if (req.body.status === 'inactive') {
        project.isClosed = true
      }
      await project.save();
      res.status(200).json({message: 'Project status has been successfully changed', project: project});
    } else {
      let error = new Error('There are active users in this project!');
      error.statusCode = 400;
      throw error;
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getProject = async (req, res, next) => {
  let projectToSend = {};

  try {
    const project = await Project.findById(req.params.id);
    if(!project) {
      let error = new Error('Project not found!');
      error.statusCode = 404;
      throw error;
    }
    projectToSend.data = project;

    if(project.pmtType === projectTypeEnum.JIRA_PROJECT){
      const jiraProject = await JiraProject.findOne({projectId: project._id});

      if(!jiraProject){
        let error = new Error('Jira project credentials not found!');
        error.statusCode = 404;
        throw error;
      }
      projectToSend.credentials = jiraProject;
    }

    res.status(200).json({message: 'Project item was successfully fetched!', project: projectToSend});
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.updateProject = async (req, res, next) => {
  let { name, chatId, reportsSendTime, isCheckedToNotify, pmtType, key, domainName, apiAccessEmail, apiAccessToken} = req.body.data;
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      let validationErrorMessage = 'Validation failed!';
      errors.array().forEach(e => {
        validationErrorMessage = validationErrorMessage.concat(`\n` + e.msg)
      });
      const error = new Error(validationErrorMessage);
      error.statusCode = 422;
      error.data = errors.array();
      throw error;
    }

    const project = await Project.findById(req.params.id);
    const oldReportSendTime = project.reportsSendTime;

    project.name = name;
    project.chatId = chatId;
    project.reportsSendTime = reportsSendTime;
    project.pmtType = pmtType;
    const updatedProject = await project.save();

    if(!updatedProject) {
      let error = new Error("Project can't be updated. Project not found!");
      error.statusCode = 404;
      throw error;
    }

    if(updatedProject.pmtType === projectTypeEnum.JIRA_PROJECT){
      const updatedProjectCredentials = await JiraProject.findOneAndUpdate({projectId: req.params.id}, {key, domainName, apiAccessEmail, apiAccessToken});

      if(!updatedProjectCredentials) {
        let error = new Error("Project credentials can't be updated. Not found!");
        error.statusCode = 404;
        throw error;
      }
    }

    res.status(200).json({message: 'Project was successfully updated'});

    const newReportSendTime = updatedProject.reportsSendTime;
    if(newReportSendTime !== oldReportSendTime && isCheckedToNotify){
      await sendNotificationToProjectMembers(project, oldReportSendTime)
    }

  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.addProject = async (req, res, next) => {
  let { name, chatId, reportsSendTime, pmtType, key, domainName, apiAccessEmail, apiAccessToken} = req.body.data;
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      let validationErrorMessage = 'Validation failed!';
      errors.array().forEach(e => {
        validationErrorMessage = validationErrorMessage.concat(`\n` + e.msg)
      });
      const error = new Error(validationErrorMessage);
      error.statusCode = 422;
      error.data = errors.array();
      throw error;
    }
    const addedProject = await Project.create({name, chatId, members: [], notNotification: [], pmtType, reportsSendTime});

    if(pmtType === projectTypeEnum.JIRA_PROJECT){
      await JiraProject.create({projectId: addedProject._id, key, domainName, apiAccessEmail, apiAccessToken });
    }

    res.status(201).json({message: 'Project was successfully created', projectId: addedProject._id});
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.getJiraUsers = async (req, res, next) => {
  try {
    const projectId = req.params.id;
    const project = await JiraProject.findOne({projectId});

    const jiraRequester = new JiraRequester(project.domainName, project.apiAccessEmail, project.apiAccessToken);
    let users = await jiraRequester.get(`user/assignable/multiProjectSearch?projectKeys=${project.key}`);
    users = users.map(user => {
      return {
        accountId: user.accountId,
        displayName: user.displayName,
      }
    });

    res.status(201).json({message: 'Jira users were fetched successfully', users});
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};