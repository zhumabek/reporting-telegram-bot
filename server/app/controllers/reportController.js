const Report = require('../../models/Report');
const Project = require('../../models/Project');
const Ticket = require('../../models/Ticket');
const User = require('../../models/User');
const config = require('../../config');
const Excel = require('exceljs');
const moment = require('moment');
const axios = require('axios');
const jwt = require('jsonwebtoken');

exports.getReportsByProject = async (req, res, next) => {
  const projectId = req.params.id;
  try {
    const tickets = await Ticket.find(req.user.role === 'user' ? {projectId: projectId, userId: req.user._id} : {projectId: projectId});
    const ticketIds = tickets.map(ticket => ticket.id);
    let reports = [];
    for (let id of ticketIds) {
      const report = await Report.find({ticketId: id})
        .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
        .populate('userId', 'name');
      reports.push(...report);
    }
    reports.sort((a, b) => new Date(b.date) - new Date(a.date));

    if (!reports.length) {
      return res.status(404).json({message: 'Отчетов по этому проекту не найдено'});
    } else {
      return res.status(200).json({message: 'Отчеты были успешно получены', reports: reports});
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getReportsByUser = async (req, res, next) => {
  try {
    let reports = [];
    const report = await Report.find({userId: req.params.id})
      .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
      .populate('userId', 'name');
    reports.push(...report);

    reports.sort((a, b) => new Date(b.date) - new Date(a.date));
    if (!reports.length) {
      return res.status(404).json({message: 'В эти даты не найдено отчетов'});
    } else {
      return res.status(200).json({message: 'Отчеты были успешно получены', reports: reports});
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getReportsByTicket = async (req, res, next) => {
  const ticketId = req.params.id;
  try {
    let reports = [];
    const report = await Report.find({ticketId: ticketId}).populate('ticketId', 'name number').populate('userId', 'name');
    reports.push(...report);
    reports.sort((a, b) => new Date(b.date) - new Date(a.date));
    if (!reports.length) {
      return res.status(404).json({message: 'Отчеты по этому тикету не найдены'});
    } else {
      return res.status(200).json({message: 'Отчеты были успешно получены', reports: reports});
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.previewReportsByProject = async (req, res, next) => {
  try {
    let reports = [];
    const project = await Project.findOne({_id: req.params.id});
    const tickets = await Ticket.find({projectId: project.id});
    const ticketIds = tickets.map(ticket => ticket.id);
    for (let id of ticketIds) {
      const report = await Report.find({
        ticketId: id,
        "date": {$gte: `${req.params.from} 00:00:00`, $lte: `${req.params.before} 23:59:59`}
      })
        .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
        .populate('userId', 'name');
      reports.push(...report);
    }
    reports.sort((a, b) => new Date(b.date) - new Date(a.date));
    if (!reports.length) {
      return res.status(404).json({message: 'В эти даты не найдено отчетов'});
    } else {
      return res.status(200).json({message: 'Отчеты были успешно получены', reports: reports});
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.previewReportsByUser = async (req, res, next) => {
  try {
    let reports = [];
    const report = await Report.find({
      userId: req.params.id,
      "date": {$gte: `${req.params.from} 00:00:00`, $lte: `${req.params.before} 23:59:59`}
    })
      .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
      .populate('userId', 'name');
    reports.push(...report);

    reports.sort((a, b) => new Date(b.date) - new Date(a.date));
    if (!reports.length) {
      return res.status(404).json({message: 'В эти даты не найдено отчетов'});
    } else {
      return res.status(200).json({message: 'Отчеты были успешно получены', reports: reports});
    }
  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.saveReportsByProject = async (req, res, next) => {
  try {
    const notTicketPhrase = 'Дополнительный отчет';
    let reports = [];
    const project = await Project.findOne({_id: req.params.id});
    const tickets = await Ticket.find({projectId: project.id, name: {$ne: notTicketPhrase}});
    const ticketIds = tickets.map(ticket => ticket.id);
    for (let id of ticketIds) {
      const report = await Report.find({
        ticketId: id,
        "date": {$gte: `${req.params.from} 00:00:00`, $lte: `${req.params.before} 23:59:59`}
      })
        .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
        .populate('userId', 'name');
      reports.push(...report);
    }
    reports.sort((a, b) => new Date(b.date) - new Date(a.date));
    const workbook = new Excel.Workbook();
    const worksheet = await workbook.addWorksheet(`${project.name}`);
    worksheet.columns = [
      {header: 'Date', key: 'date', width: 11},
      {header: 'User', key: 'user_name', width: 18},
      {header: 'Ticket number', key: 'ticket_number', width: 8, alignment: {vertical: 'middle', horizontal: 'center'}},
      {header: 'Ticket title', key: 'ticket_title', width: 30},
      {header: 'Description', key: 'description', width: 30},
      {header: 'Time spent', key: 'time_spent', width: 6},
      {header: 'Ticket status', key: 'ticket_status', width: 10},
    ];

    reports.map(report => {
      worksheet.addRow({
        date: moment(report.date).locale('ru').format('L'),
        user_name: report.userId.name,
        ticket_number: report.ticketId.number,
        ticket_title: report.ticketId.name,
        description: report.description,
        time_spent: report.timeSpent,
        ticket_status: report.ticketStatus
      });
    });

    worksheet.views = [
      {state: 'frozen', ySplit: 1},
    ];
    worksheet.getColumn('ticket_number').eachCell((cell) => {
      cell.alignment = {vertical: 'middle', horizontal: 'right'};
    });
    worksheet.getRow(1).height = 30;
    worksheet.getRow(1).eachCell((cell) => {
      cell.font = {bold: true};
      cell.border = {
        bottom: {style: 'double'}
      };
      cell.alignment = {vertical: 'middle', horizontal: 'center', wrapText: true};
    });

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", `attachment; filename=invoice project project.name ${moment(req.params.from).format('L')} and ${moment(req.params.before).format('L')}.xlsx`);
    res.charset = 'UTF-8';

    workbook.xlsx.write(res)
      .then(function () {
        if (!reports.length) {
          res.status(404).json({message: 'В эти даты не найдено отчетов'});
        } else {
          res.status(200).end();
        }
      });

  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.saveReportsByUser = async (req, res, next) => {
  const {id, from, before} = req.params;
  try {
    let reports = [];

    const user = await User.findOne({_id: id}, {name: true, hrmUserId: true});

    let payload = {'payload': config.hrmPayload};
    let token = jwt.sign(payload, config.hrmSecretKey, {algorithm: 'HS256'});

    const fromDate = moment(from).locale('ru').format('L');
    const toDate = moment(before).locale('ru').format('L');

    const timeSheets = await axios.get(`${config.hrmHost}/api/v2/attractor-reporting-bot/count-work-days?from_date=${fromDate}&to_date=${toDate}&employee_id=${user.hrmUserId}`, {headers: {Authorization: token}})
      .then(response => {
        return response.data;
      }).catch((error) => {
        error.response
      });

    const report = await Report.find({
      userId: id,
      "date": {$gte: `${from} 00:00:00`, $lte: `${before} 23:59:59`}
    })
      .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
      .populate('userId', 'name');
    reports.push(...report);

    reports.sort((a, b) => new Date(b.date) - new Date(a.date));
    const workbook = new Excel.Workbook();
    const worksheet = await workbook.addWorksheet(`${user.name}`);
    worksheet.columns = [
      {header: 'Project', key: 'project', width: 11},
      {header: 'Date', key: 'date', width: 11},
      {header: 'User', key: 'user_name', width: 18},
      {header: 'Ticket number', key: 'ticket_number', width: 8, alignment: {vertical: 'middle', horizontal: 'center'}},
      {header: 'Ticket title', key: 'ticket_title', width: 30},
      {header: 'Description', key: 'description', width: 30},
      {header: 'Time spent', key: 'time_spent', width: 6},
      {header: 'Ticket status', key: 'ticket_status', width: 10},
      {header: 'Work days', key: 'work_days', width: 10},
    ];

    reports.map(report => {
      worksheet.addRow({
        project: report.ticketId.projectId.name,
        date: moment(report.date).locale('ru').format('L'),
        user_name: report.userId.name,
        ticket_number: report.ticketId.number,
        ticket_title: report.ticketId.name,
        description: report.description,
        time_spent: report.timeSpent,
        ticket_status: report.ticketStatus
      });
    });

    const totalsRow = worksheet.addRow([
      '',
      '',
      '',
      '',
      '',
      'Sum time spent',
      generateWeeklyTotalsCell(worksheet, 'G', reports.length),
      '',
      '',
      '',
    ]);

    const cell = worksheet.getCell('J1');
    if (timeSheets !== undefined) {
      cell.value = timeSheets.work_days;
    } else {
      cell.value = 0;
    }

    function generateWeeklyTotalsCell(worksheet, columnLetter, totalDataRows) {
      const firstDataRow = 1 + 1;
      const lastDataRow = firstDataRow + totalDataRows - 1;

      const firstCellReference = `${columnLetter}${firstDataRow}`;
      const lastCellReference = `${columnLetter}${lastDataRow}`;
      const sumRange = `${firstCellReference}:${lastCellReference}`;

      return {
        formula: `SUM(${sumRange})`,
      };
    }

    worksheet.views = [
      {state: 'frozen', ySplit: 1},
    ];
    worksheet.getColumn('ticket_number').eachCell((cell) => {
      cell.alignment = {vertical: 'middle', horizontal: 'right'};
    });
    worksheet.getRow(1).height = 30;
    worksheet.getRow(1).eachCell((cell) => {
      cell.font = {bold: true};
      cell.border = {
        bottom: {style: 'double'}
      };
      cell.alignment = {vertical: 'middle', horizontal: 'center', wrapText: true};
    });

    totalsRow.eachCell((cell) => {
      cell.font = {bold: true};
      cell.border = {
        top: {style: 'thin'}, bottom: {style: 'double'},
      };
    });

    res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    res.setHeader("Content-Disposition", `attachment; filename=invoice development user.name ${moment(from).format('L')} and ${moment(before).format('L')}.xlsx`);
    res.charset = 'UTF-8';

    workbook.xlsx.write(res)
      .then(function () {
        if (!reports.length) {
          res.status(404).json({message: 'В эти даты не найдено отчетов'});
        } else {
          res.status(200).end();
        }
      });

  } catch (err) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    next(err);
  }
};

exports.getReport = async (req, res, next) => {
  try {
    const reportId = req.params.id;
    const report = await Report.findById(reportId)
      .populate({path: 'ticketId', populate: ({path: 'projectId', select: 'name'})})
      .populate('userId', 'name');

    res.status(200).json({message: 'Report item was successfully fetched!', report});

  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.addReport = async (req, res, next) => {
  const {
    ticketNumber, ticketStatus,
    reportDescription, spentTime,
    ticketName, projectId, userId, date
  } = req.body;

  try {
    const ticket = await Ticket.findOne({projectId: projectId, number: ticketNumber});
    let updatedTicket;

    if (ticket) {
      updatedTicket = await Ticket.findByIdAndUpdate(ticket._id, {status: ticketStatus});
    } else {
      updatedTicket = await Ticket.create({
        projectId,
        userId,
        number: ticketNumber,
        name: ticketName,
        status: ticketStatus,
      })
    }

    await Report.create({
      ticketId: updatedTicket._id,
      userId: userId,
      description: reportDescription,
      timeSpent: spentTime,
      ticketStatus: updatedTicket.status,
      date: date
    });

    res.status(201).json({message: 'Report was successfully created!'})

  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.updateReport = async (req, res, next) => {
  const {ticketId, ticketStatus, reportDescription, spentTime, date, ticketNumber, ticketName} = req.body;
  const reportId = req.params.id;
  try {
    await Ticket.findByIdAndUpdate(ticketId, {status: ticketStatus, number: ticketNumber, name: ticketName});
    await Report.findByIdAndUpdate(reportId, {
      description: reportDescription,
      timeSpent: spentTime,
      ticketStatus: ticketStatus,
      editDate: date
    });

    res.status(200).json({message: 'Report was successfully updated!'});

  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};