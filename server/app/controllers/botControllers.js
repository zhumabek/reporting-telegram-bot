const User = require('../../models/User');



async function bindUser(req, res) {
  const successMessage = 'Activated Successfully! Send to bot /start to start working!';
  const notFoundMessage = 'User not found!';

  const userId = req.params.id;
  let user;

  try {
    user = await User.findOne({ _id: userId });
  } catch (e) {
    user = null;
  }

  if (user){
    user.active = true;
    await user.save();
    res.status(200).json({message: successMessage})

  } else {
    res.status(404).json({error: notFoundMessage})
  }
}

module.exports = {
  bindUser
};