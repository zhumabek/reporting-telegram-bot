const User = require('../../models/User');

exports.currentSession = async (req, res) => {
  try {
    const pattern = /\w+@\w+.\w+/;
    const isEmail = pattern.test(req.body.someLogin);

    let user;
    if (!isEmail) {
      user = await User.findOne({phoneNumber: req.body.someLogin});
    } else {
      user = await User.findOne({email: req.body.someLogin})
    }

    if (!user) {
      return res.status(400).send({message: 'User does not exist'});
    }

    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) {
      return res.status(400).send({message: 'Password incorrect!'})
    }

    user.generateToken();
    await user.save();
    return res.send({message: 'Login successful', user});
  } catch (error) {
    return res.status(500).send({message: 'Eternal server error', error})
  }
};

exports.deleteSession = async (req, res) => {
  try {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};
    if (!token) {
      return res.send(success)
    }

    const user = await User.findOne({token});
    if (!user) {
      return res.send(success)
    }

    user.generateToken();
    await user.save();
    return res.send(success);
  } catch (e) {
    return res.status(500).send(e)
  }

};

exports.authUser = async (req, res) => {
  const token = req.get('Authorization');
  if (!token) {
    return res.status(401).send({error: 'Authorization headers not present'});
  }

  const user = await User.findOne({token});
  if (!user) {
    return res.status(401).send({error: 'Token incorrect'});
  }

  user.password = req.body.password;
  await user.save();
  res.sendStatus(200);
};

exports.getAllUsers = async (req, res) => {
  try {
    const userFind = {email: true, phoneNumber: true, name: true, role: true, active: true};
    const userSort = [['active', -1], ['name', 1]];
    const users = await User.find({}, userFind).sort(userSort);
    return res.send(users);
  } catch (e) {
    return res.status(500).send(e)
  }
};

exports.getAllActiveUsers = async (req, res) => {
  try {
    const userFind = {email: true, phoneNumber: true, name: true, role: true, active: true};
    const userSort = [['active', -1], ['name', 1]];
    const users = await User.find(req.user.role === 'user' ? {_id: req.user._id} : {active: true}, userFind).sort(userSort);
    return res.send(users);
  } catch (e) {
    return res.status(500).send(e)
  }
};

exports.changeRoleUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (req.body.status === 'active') {
      user.active = true
    } else if (req.body.status === 'inactive') {
      user.active = false
    }
    if (req.user.role === 'admin') {
      user.role = req.body.role;
      user.password = req.body.password;
    }
    user.generateToken();
    await user.save();
    res.sendStatus(200);
  } catch (e) {
    return res.status(500).send(e)
  }
};