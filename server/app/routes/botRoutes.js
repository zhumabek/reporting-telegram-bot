const express = require('express');

const botControllers = require('../controllers/botControllers');


const router = express.Router();

router.get('/bind_user/:id', botControllers.bindUser);

module.exports = router;
