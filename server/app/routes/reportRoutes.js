const express = require('express');
const isAuth = require('../../middleware/auth');
const permit = require('../../middleware/permit');
const reportController = require('../controllers/reportController');
const router = express.Router();

router.get('/:id', [isAuth, permit('admin', 'project manager', 'user')], reportController.getReportsByProject);
router.get('/user/:id', [isAuth, permit('admin', 'project manager', 'user')], reportController.getReportsByUser);
router.get('/ticket/:id', [isAuth, permit('admin', 'project manager', 'user')], reportController.getReportsByTicket);
router.get('/viewReportsByProject/:id&dateFrom=:from&dateBefore=:before', [isAuth, permit('admin', 'project manager', 'user')], reportController.previewReportsByProject);
router.get('/reportsByProject/:id&dateFrom=:from&dateBefore=:before', [isAuth, permit('admin', 'project manager', 'user')], reportController.saveReportsByProject);
router.get('/viewReportsByUser/:id&dateFrom=:from&dateBefore=:before', [isAuth, permit('admin', 'project manager', 'user')], reportController.previewReportsByUser);
router.get('/reportsByUser/:id&dateFrom=:from&dateBefore=:before', [isAuth, permit('admin', 'project manager', 'user')], reportController.saveReportsByUser);
router.post('/', [isAuth, permit('admin', 'project manager')], reportController.addReport);
router.get('/get/:id', [isAuth, permit('admin', 'project manager')], reportController.getReport);
router.put('/update/:id', [isAuth, permit('admin', 'project manager')], reportController.updateReport);

module.exports = router;