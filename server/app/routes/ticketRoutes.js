const express = require('express');
const isAuth = require('../../middleware/auth');
const permit = require('../../middleware/permit');
const ticketController = require('../controllers/ticketController');
const router = express.Router();

router.get('/get/:id', [isAuth, permit('admin', 'project manager', 'user')], ticketController.getTickets);
router.get('/getTicketNumber/:id', [isAuth, permit('admin', 'project manager', 'user')], ticketController.getTicketNumber);

module.exports = router;