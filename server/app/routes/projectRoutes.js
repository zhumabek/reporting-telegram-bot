const express = require('express');
const isAuth = require('../../middleware/auth');
const permit = require('../../middleware/permit');
const projectController = require('../controllers/projectController');
const Project = require('../../models/Project');
const router = express.Router();
const {body} = require('express-validator');


router.get('/get/all&status=:active', [isAuth, permit('admin', 'project manager', 'user')], projectController.getAllProjectsInProgress);
router.get('/:id', [isAuth, permit('admin', 'project manager')], projectController.getProject);
router.get('/:id/members', [isAuth, permit('admin', 'project manager')], projectController.getProjectMembers);
router.get('/:id/not/members', [isAuth, permit('admin', 'project manager')], projectController.getNotMembers);
router.get('/:id/not_notification', [isAuth, permit('admin', 'project manager')], projectController.getProjectNotNotification);
router.put('/close/:id', [isAuth, permit('admin', 'project manager')], projectController.closeProject);
router.put('/:id/add/member', [isAuth, permit('admin', 'project manager')], projectController.addProjectMember);
router.put('/:id/add/not_notification', [isAuth, permit('admin', 'project manager')], projectController.addProjectMemberNotNotification);
router.delete('/:projectId/delete/member/:userId', [isAuth, permit('admin', 'project manager')], projectController.deleteProjectMember);
router.delete('/:projectId/delete/not_notification/:userId', [isAuth, permit('admin', 'project manager')], projectController.deleteProjectMemberNotNotification);
router.get('/:id/jira/users', [isAuth, permit('admin', 'project manager')], projectController.getJiraUsers);


router.put('/:id',
  [isAuth, permit('admin', 'project manager')],
  [
    body('data.name')
      .trim()
      .custom(async (value, {req}) => {
        const currentProject = await Project.findById(req.params.id);
        const projectFoundByName = await Project.findOne({name: value, isClosed: false});
        if(currentProject.name !== value && projectFoundByName){
          return Promise.reject('Project with this name already exists!');
        }
      }),
    body('data.reportsSendTime')
      .trim()
      .not()
      .isEmpty(),
    body('data.chatId')
      .trim()
      .matches(/-\d{9}/)
      .withMessage('chatId not matches format: "-111111111"')
      .custom(async (value, {req}) => {
        const currentProject = await Project.findById(req.params.id);
        const projectFoundByName = await Project.findOne({chatId: value});
        if(currentProject.chatId !== value && projectFoundByName){
          return Promise.reject('Project with this chatId already exists!');
        }
      })
  ],
  projectController.updateProject
);


router.post('/',
  [isAuth, permit('admin', 'project manager')],
  [
    body('data.name')
      .trim()
      .custom(value => {
        return Project.findOne({name: value, isClosed: false}).then(project => {
          if (project) {
            return Promise.reject('Project with this name already exists!');
          }
        });
      }),
    body('data.reportsSendTime')
      .trim()
      .not()
      .isEmpty()
      .withMessage("Reports send time can't be empty!"),
    body('data.chatId')
      .trim()
      .matches(/-\d{9}/)
      .withMessage('chatId not matches format: "-111111111"')
      .custom(value => {
        return Project.findOne({chatId: value}).then(project => {
          if (project) {
            return Promise.reject('Project with this chatId already exists!');
          }
        });
      })
  ],
  projectController.addProject
);

module.exports = router;