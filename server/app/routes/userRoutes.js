const express = require('express');
const userController = require('../controllers/userController');
const isAuth = require('../../middleware/auth');
const permit = require('../../middleware/permit');
const router = express.Router();

router.post('/sessions', userController.currentSession);
router.delete('/sessions', userController.deleteSession);
router.put('/', userController.authUser);
router.get('/', [isAuth, permit('admin', 'project manager')], userController.getAllUsers);
router.get('/active', [isAuth, permit('admin', 'project manager', 'user')], userController.getAllActiveUsers);
router.patch('/data_change/:id', [isAuth, permit('admin', 'project manager')], userController.changeRoleUser);

module.exports = router;