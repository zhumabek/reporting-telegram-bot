const axios = require('axios');

class JiraRequester {
  constructor(domain, accessEmail, accessToken) {
    this.domain = domain;
    this.email = accessEmail;
    this.token = accessToken;
    this.instance = axios.create({
      baseURL: `https://${this.domain}.atlassian.net/rest/api/2/`,
      timeout: 30000,
      responseType: 'json',
      auth: {
        username: this.email,
        password: this.token
      }
    });

  }

  get(url, param = null) {
    const instance = this.instance;
    return new Promise(function (resolve, reject) {
      instance.get(url, {params: param})
        .then(resp => {
          if (resp && resp.data) {
            resolve(resp.data);
          }
          else {
            reject(null);
          }
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  post(url, param) {
    const instance = this.instance;
    return new Promise(function (resolve, reject) {
      instance.post(url, param).then(resp => {
        if (resp && resp.data) {
          resolve(resp.data);
        }
        else {
          reject(null);
        }
      }).catch(error => {
        reject(error);
      });
    });
  }

}

module.exports = JiraRequester;
