const {projectTypeEnumDescriptions, projectTypeEnum} = require('../../models/constants');
const bot = require('../../bot/bot');
const JiraUser = require('../../models/JiraUser');
const User = require('../../models/User');


exports.sendNotificationToProjectMembers = async (project, oldTime) => {
  try {
    const projectMembers = await User.find({_id: {$in: project.members}});
    await Promise.all(
      projectMembers.map(user => {
        if(user.role === 'user'){
          bot.telegram.sendMessage(
            user.telegramChatId ,
            `<b>Уведомление:</b> время отправки отчетов в группу проекта <b>${project.name}</b> изменилась с <i>${oldTime}</i> на <i>${project.reportsSendTime}</i>`,
            {parse_mode: 'HTML'},
          )
        }
      })
    )
  } catch(error) {
    console.log(error);
  }
};

exports.addPmtUser = async (projectType, userId, pmtUserIdentifier) => {
  if(projectType === projectTypeEnum.BASIC_PROJECT){
    return;
  }
  else {
    const jiraUser = await JiraUser.findOne({userId});
    if(!jiraUser){
      await JiraUser.create({userId, identifier: pmtUserIdentifier});
    }
  }
};

exports.getStringValueOfEnum = (enumValue) => {
  return projectTypeEnumDescriptions[enumValue];
};
