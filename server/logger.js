require('dotenv').config();

const util = require('util');
const winston = require('winston');
const format = winston.format;
const config = require('./config');


function prepareMessage(ctx, msg, ...data) {
  const formattedMessage = data.length ? util.format(msg, ...data) : msg;

  if (ctx && ctx.from){
    return `[${ctx.from.id}/${ctx.from.username}]: ${formattedMessage}`;
  }
  return `: ${formattedMessage}`;
}

const { combine, timestamp, printf } = format;
const logFormat = printf(info => {
  return `[${info.timestamp}][${info.level}]${info.message}`;
});

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      level: config.debug ? 'debug': 'error'
    }),
    new winston.transports.File({ filename: 'logs/debug.log', level: 'debug'}),
  ],
  format: combine(timestamp(), format.splat(), logFormat)
});

if (process.env.NODE_ENV==='test'){
  logger.transports.forEach(logger => {
    logger.silent = true
  });
}
if (!(process.env.NODE_ENV==='test') && config.debug){
  logger.debug(': logging initialized at debug level');
}

const loggerWithCtx = {
  debug: (ctx, msg, ...data) =>
    logger.debug(prepareMessage(ctx, msg, ...data)),
  error: (ctx, msg, ...data) =>
    logger.error(prepareMessage(ctx, msg, ...data))
};

module.exports = loggerWithCtx;
