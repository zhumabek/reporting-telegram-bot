const axios = require('axios');
const jwt = require('jsonwebtoken');
const dateFormat = require('dateformat');

const config = require('./config');

const SECRET_KEY = 'for_intern_test';
const today = new Date();
const EXP_DAYS = 7;
let dateAfterExp_days = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + EXP_DAYS);

const token = jwt.sign({
  email: 'main@email.com',
  password: 'admin',
  exp_date: dateFormat(dateAfterExp_days, 'yyyy-mm-dd')
}, config.hrmSecretKey);

request = {
  method: "post",
  url: config.hrmHost + `/api/employees/3/time-sheets/`,
  data: {
    year: today.getFullYear(),
    month: today.getMonth()+1,
  },
  headers: {
    Authorization: `Bearer ${token}`
  },
  json: true
};

async function get(){
  try{
    const response = await axios(request);
    console.log(response.data);
    return true
  } catch (e) {
    logger.error(
      undefined,
      'HRM sent error when check user for active in schedule.',
      {status: e.response.status, statusText: e.response.statusText}
    );
    return undefined
  }
}

get();