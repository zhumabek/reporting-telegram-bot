const {NO_TICKET_REPORT_START_WORD} = require('./bot/scenes/NoTicketReport/noTicketReportHelpers');

const mongoose = require('mongoose');
const config = require('./config');

const Project = require('./models/Project');
const Ticket = require('./models/Ticket');
const Report = require('./models/Report');

async function main() {
  await mongoose.connect(config.dbUrl, config.mongoOptions);
  console.log('Connected to db');

  const projects = await Project.find();
  for (let i = 0; i < projects.length; i++) {
    try {
      const tickets = await Ticket.find({projectId: projects[i]._id, name: NO_TICKET_REPORT_START_WORD});
      console.log('Finding additional tickets for project ' + projects[i]._id);
      for (let i = 0; i < tickets.length; i++) {
        const ticketReport = await Report.findOne({ticketId: tickets[i]._id});
        tickets[i]._doc.date = ticketReport.date;
      }
      tickets.sort((a, b) => new Date(a.date) - new Date(b.date));
      console.log('Here they are: \n' + tickets);

      for (let i = 0; i < tickets.length; i++) {
        let ticket = tickets[i];
        console.log('\nTicket number was ' + ticket.number);
        ticket.number = 9000 + i;
        console.log('and ticket number changed to ' + ticket.number + '.');
        delete ticket._doc.date;
        await ticket.save();
      }
    } catch (e) {
      console.log('Error ^ \n');
    }
  }

  console.log('\nClosing connection to db');
  await mongoose.connection.close();
  console.log('\nFinished successfully!')
}

main()
  .then(() => {
  });
