const axios = require('axios');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const config = require('./config');

const User = require('./models/User');

async function main(){
  await mongoose.connect(config.dbUrl, config.mongoOptions);
  console.log('Connected to db');

  const users = await User.find();
  for (let i=0; i< users.length; i++){
    try{
      console.log('Getting hrm user for ' + users[i].email + '.....');

      let user = users[i];
      let token = jwt.sign({email: user.email}, config.hrmSecretKey);
      const response = await axios.post(config.hrmHost + '/api/bind/token/', {token});

      user.hrmUserId = response.data.profile.id;
      user.save()
    } catch (e) {
      console.log('Error when getting hrm user ^ \n');
    }

  }
  console.log('\nClosing connection to db');
  await mongoose.connection.close();
  console.log('\nFilling users field: hrmUserId finished successfully!')
}

main()
  .then(() => {});
