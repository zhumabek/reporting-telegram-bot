const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');
const Project = require('./models/Project');
const JiraProject = require('./models/JiraProject');
const Report = require('./models/Report');
const Ticket = require('./models/Ticket');
const {projectTypeEnum} = require('./models/constants');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;
  await connection.dropDatabase();

  const users = await User.create(
    {
      phoneNumber: '+996555111111',
      email: 'john_one@gmail.com',
      name: 'rysbai_coder',
      role: 'user',
      active: true,
      telegramUserId: makeid(10),
      telegramChatId: makeid(10)
    },
    {
      phoneNumber: '+996555222222',
      email: 'nurbek_one@gmail.com',
      name: 'evlmnd',
      role: 'user',
      active: true,
      password: '123456',
      token: nanoid(),
      telegramUserId: makeid(10),
      telegramChatId: makeid(10)
    },
    {
      phoneNumber: '+996555333333',
      email: 'vasya_one@gmail.com',
      name: 'zhumabek12',
      role: 'project manager',
      active: true,
      password: 'pm12345',
      token: nanoid(),
      telegramUserId: makeid(10),
      telegramChatId: makeid(10)
    },
    {
      phoneNumber: '+996555444444',
      email: 'qwerty@gmail.com',
      name: 'adievkadyrbek',
      role: 'user',
      active: true,
      telegramUserId: makeid(10),
      telegramChatId: makeid(10)
    },
    {
      phoneNumber: '+996550672597',
      email: 'info@attractor-software.com',
      name: 'SuperAdmin',
      role: 'admin',
      active: true,
      password: 'admin345Aa',
      token: nanoid(),
      telegramUserId: makeid(10)
    },
  );

  const [projectA, projectB, projectX, closedProject, fromJiraProject1, fromJiraProject2] = await Project.create(
    {name: 'Project A', isClosed: false, chatId: '-321130738', members: [users[0]._id, users[1]._id, users[2]._id]},
    {name: 'Project B', isClosed: false, chatId: '-321130738', members: [users[0]._id, users[1]._id, users[2]._id]},
    {name: 'Project X', isClosed: false, chatId: '-321130738', members: [users[0]._id, users[1]._id, users[2]._id]},
    {name: 'Closed project', isClosed: true, chatId: '-321130738', members: [users[0]._id, users[1]._id, users[2]._id]},
    {name: 'Some example jira project', isClosed: false, chatId: '-111111111', members: [], pmtType: projectTypeEnum.JIRA_PROJECT},
    {name: 'Second example jira project', isClosed: false, chatId: '-222222222', members: [], pmtType: projectTypeEnum.JIRA_PROJECT},
    {name: 'Project C', isClosed: false, chatId: '-909090909', members: [], pmtType: projectTypeEnum.BASIC_PROJECT},
    {name: 'Project D', isClosed: false, chatId: '-987654321', members: [], pmtType: projectTypeEnum.JIRA_PROJECT},
  );

  const jiraProjects = await JiraProject.create(
    {projectId: fromJiraProject1._id, domainName: 'some_example_ira_project', key:'SEJP', apiAccessEmail: 'email@examle.com', apiAccessToken: 'asdnwwdoUIHOInwqwjpmKklamSLAMDKLMEDLKLS'},
    {projectId: fromJiraProject2._id, domainName: 'second_example_jira_project', key:'SECEJP', apiAccessEmail: 'bot@examle.com', apiAccessToken: 'dshjcaiiu;sissdcsdYGsakIIIIIOOOOl'},
  );

  const tickets = await Ticket.create(
    {projectId: projectA._id, userId: users[0]._id, number: '1', name: 'Create fixtures', status: 'closed'},
    {projectId: projectA._id, userId: users[0]._id, number: '2', name: 'Fix fixtures', status: 'in progress'},
    {projectId: projectA._id, userId: users[1]._id, number: '3', name: 'Add new field to fixtures', status: 'closed'},
    {projectId: projectA._id, userId: users[1]._id, number: '4', name: 'Add one more field to fixtures', status: 'in progress'},
    {projectId: projectA._id, userId: users[2]._id, number: '5', name: 'Add something', status: 'in progress'},
    {projectId: projectA._id, userId: users[2]._id, number: '6', name: 'Add something more', status: 'closed'},
    {projectId: projectA._id, userId: users[3]._id, number: '7', name: 'Add feature', status: 'in progress'},
    {projectId: projectA._id, userId: users[3]._id, number: '8', name: 'Add more feature', status: 'in progress'},
    {projectId: projectA._id, userId: users[4]._id, number: '9', name: 'Code review', status: 'in progress'},
    {projectId: projectA._id, userId: users[4]._id, number: '10', name: 'Add new feature', status: 'in progress'},

    {projectId: projectB._id, userId: users[0]._id, number: '11', name: 'Fix something', status: 'closed'},
    {projectId: projectB._id, userId: users[0]._id, number: '12', name: 'New task', status: 'in progress'},
    {projectId: projectB._id, userId: users[1]._id, number: '13', name: 'New issue', status: 'in progress'},
    {projectId: projectB._id, userId: users[1]._id, number: '14', name: 'One more task', status: 'closed'},
    {projectId: projectB._id, userId: users[2]._id, number: '15', name: 'One more issue', status: 'in progress'},
    {projectId: projectB._id, userId: users[2]._id, number: '16', name: 'New bug', status: 'closed'},
    {projectId: projectB._id, userId: users[3]._id, number: '17', name: 'One more issue', status: 'in progress'},
    {projectId: projectB._id, userId: users[3]._id, number: '18', name: 'New bug', status: 'closed'},
    {projectId: projectB._id, userId: users[4]._id, number: '19', name: 'One more issue', status: 'in progress'},
    {projectId: projectB._id, userId: users[4]._id, number: '20', name: 'New bug', status: 'closed'},

    {projectId: projectX._id, userId: users[0]._id, number: '21', name: 'Fix something', status: 'closed'},
    {projectId: projectX._id, userId: users[0]._id, number: '22', name: 'New task', status: 'in progress'},
    {projectId: projectX._id, userId: users[1]._id, number: '23', name: 'New issue', status: 'in progress'},
    {projectId: projectX._id, userId: users[1]._id, number: '24', name: 'One more task', status: 'closed'},
    {projectId: projectX._id, userId: users[2]._id, number: '25', name: 'One more issue', status: 'in progress'},
    {projectId: projectX._id, userId: users[2]._id, number: '26', name: 'New bug', status: 'closed'},
    {projectId: projectX._id, userId: users[3]._id, number: '27', name: 'One more issue', status: 'in progress'},
    {projectId: projectX._id, userId: users[3]._id, number: '28', name: 'New bug', status: 'closed'},
    {projectId: projectX._id, userId: users[4]._id, number: '29', name: 'One more issue', status: 'in progress'},
    {projectId: projectX._id, userId: users[4]._id, number: '30', name: 'New bug', status: 'closed'},
  );
  const reports = await Report.create(
    {ticketId: tickets[0]._id, userId: users[0]._id, description: 'Report-1', timeSpent: 1, ticketStatus: 'in progress', date: 'October 18, 2019 03:24:00'},
    {ticketId: tickets[0]._id, userId: users[0]._id, description: 'Report-2', timeSpent: 2, ticketStatus: 'closed', date: 'October 18, 2019 03:24:00'},
    {ticketId: tickets[1]._id, userId: users[0]._id, description: 'Report-3', timeSpent: 2, ticketStatus: 'in progress', date: 'October 18, 2019 03:24:00'},
    {ticketId: tickets[2]._id, userId: users[1]._id, description: 'Report-4', timeSpent: 2, ticketStatus: 'in progress', date: 'October 16, 2019 03:24:00'},
    {ticketId: tickets[2]._id, userId: users[1]._id, description: 'Report-5', timeSpent: 2, ticketStatus: 'closed', date: 'November 17, 2019 03:24:00'},
    {ticketId: tickets[3]._id, userId: users[1]._id, description: 'Report-6', timeSpent: 3, ticketStatus: 'in progress', date: 'November 18, 2019 03:24:00'},
    {ticketId: tickets[4]._id, userId: users[2]._id, description: 'Report-7', timeSpent: 4, ticketStatus: 'in progress', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[5]._id, userId: users[2]._id, description: 'Report-8', timeSpent: 4, ticketStatus: 'in progress', date: 'November 18, 2019 03:24:00'},
    {ticketId: tickets[5]._id, userId: users[2]._id, description: 'Report-9', timeSpent: 4, ticketStatus: 'closed', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[6]._id, userId: users[3]._id, description: 'Report-10', timeSpent: 4, ticketStatus: 'closed', date: 'October 19, 2019 03:24:00'},
    {ticketId: tickets[7]._id, userId: users[3]._id, description: 'Report-11', timeSpent: 4, ticketStatus: 'in progress', date: 'October 19, 2019 03:24:00'},
    {ticketId: tickets[8]._id, userId: users[4]._id, description: 'Report-12', timeSpent: 4, ticketStatus: 'closed', date: 'October 19, 2019 03:24:00'},
    {ticketId: tickets[9]._id, userId: users[4]._id, description: 'Report-13', timeSpent: 4, ticketStatus: 'in progress', date: 'October 19, 2019 03:24:00'},

    {ticketId: tickets[10]._id, userId: users[0]._id, description: 'Report-14', timeSpent: 1, ticketStatus: 'in progress', date: 'November 16, 2019 03:24:00'},
    {ticketId: tickets[11]._id, userId: users[0]._id, description: 'Report-15', timeSpent: 1, ticketStatus: 'closed', date: 'November 17, 2019 03:24:00'},
    {ticketId: tickets[12]._id, userId: users[1]._id, description: 'Report-16', timeSpent: 2, ticketStatus: 'in progress', date: 'November 18, 2019 03:24:00'},
    {ticketId: tickets[13]._id, userId: users[1]._id, description: 'Report-17', timeSpent: 2, ticketStatus: 'in progress', date: 'November 18, 2019 03:24:00'},
    {ticketId: tickets[14]._id, userId: users[2]._id, description: 'Report-18', timeSpent: 2, ticketStatus: 'in progress', date: 'October 16, 2019 03:24:00'},
    {ticketId: tickets[15]._id, userId: users[2]._id, description: 'Report-19', timeSpent: 2, ticketStatus: 'closed', date: 'October 17, 2019 03:24:00'},
    {ticketId: tickets[15]._id, userId: users[2]._id, description: 'Report-20', timeSpent: 3, ticketStatus: 'in progress', date: 'October 18, 2019 03:24:00'},
    {ticketId: tickets[17]._id, userId: users[3]._id, description: 'Report-21', timeSpent: 4, ticketStatus: 'in progress', date: 'October 19, 2019 03:24:00'},
    {ticketId: tickets[17]._id, userId: users[3]._id, description: 'Report-22', timeSpent: 4, ticketStatus: 'in progress', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[18]._id, userId: users[4]._id, description: 'Report-23', timeSpent: 4, ticketStatus: 'in progress', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[19]._id, userId: users[4]._id, description: 'Report-24', timeSpent: 4, ticketStatus: 'in progress', date: 'November 19, 2019 03:24:00'},

    {ticketId: tickets[20]._id, userId: users[0]._id, description: 'Report-25', timeSpent: 1, ticketStatus: 'in progress', date: 'November 16, 2019 03:24:00'},
    {ticketId: tickets[20]._id, userId: users[0]._id, description: 'Report-26', timeSpent: 1, ticketStatus: 'closed', date: 'November 17, 2019 03:24:00'},
    {ticketId: tickets[21]._id, userId: users[0]._id, description: 'Report-27', timeSpent: 2, ticketStatus: 'in progress', date: 'November 18, 2019 03:24:00'},
    {ticketId: tickets[22]._id, userId: users[1]._id, description: 'Report-28', timeSpent: 2, ticketStatus: 'in progress', date: 'November 18, 2019 03:24:00'},
    {ticketId: tickets[23]._id, userId: users[1]._id, description: 'Report-29', timeSpent: 2, ticketStatus: 'in progress', date: 'October 16, 2019 03:24:00'},
    {ticketId: tickets[22]._id, userId: users[1]._id, description: 'Report-30', timeSpent: 2, ticketStatus: 'closed', date: 'October 17, 2019 03:24:00'},
    {ticketId: tickets[24]._id, userId: users[2]._id, description: 'Report-31', timeSpent: 3, ticketStatus: 'in progress', date: 'October 18, 2019 03:24:00'},
    {ticketId: tickets[24]._id, userId: users[2]._id, description: 'Report-32', timeSpent: 4, ticketStatus: 'in progress', date: 'November 18, 2019 03:24:00'},
    {ticketId: tickets[26]._id, userId: users[3]._id, description: 'Report-33', timeSpent: 4, ticketStatus: 'in progress', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[26]._id, userId: users[3]._id, description: 'Report-34', timeSpent: 4, ticketStatus: 'closed', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[27]._id, userId: users[3]._id, description: 'Report-35', timeSpent: 4, ticketStatus: 'in progress', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[28]._id, userId: users[4]._id, description: 'Report-36', timeSpent: 4, ticketStatus: 'closed', date: 'November 19, 2019 03:24:00'},
    {ticketId: tickets[29]._id, userId: users[4]._id, description: 'Report-37', timeSpent: 4, ticketStatus: 'in progress', date: 'November 19, 2019 03:24:00'},
);

  await connection.close();
};

function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

run().catch(error => {
  console.error('Something went wrong', error);
});


