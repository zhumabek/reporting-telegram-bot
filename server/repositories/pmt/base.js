const Project = require('../../models/Project');
const User = require('../../models/User');
const Ticket = require('../../models/Ticket');
const Report = require('../../models/Report');

function NonePMTRepo(project, user) {

  return {
    project,
    user,
    SPENT_TIME_LIMIT: 12,
    getUsersInProgressTickets: async function() {
      return await Ticket.find({
        projectId: this.project._id,
        userId: this.user._id,
        status: 'in progress'
      })
    },
    getProjectData: async function() {
      //pass
    },
    getUserTodayTimeSpent: async function () {
      let totalTime = 0;
      let today = new Date().setHours(0,0,0,0);

      const ticketIds = await Ticket.find({projectId: this.project._id, userId: this.user._id}, '_id');
      const reportsForToday = await Report.find({ticketId:{$in: ticketIds}, date: {$gt: today}}, 'timeSpent');

      reportsForToday.forEach((report) => {
        totalTime += report.timeSpent;
      });
      return totalTime;
    },

    getTicketSpentTime: async function(ticketId){
      const reports = await Report.find({ticketId: ticketId});
      let spentTime = 0;
      reports.forEach(report => {
        spentTime += report.timeSpent
      });
      return spentTime;
    },

    getTicketById: async function (ticketId) {
      return Ticket.findOne({_id: ticketId});
    },

    findReport: async function (data){
      return await Report.find(data);
    },
    findOneReport: async function (data){
      return Report.findOne(data);
    },
    createTicket: async function(data) {
      //pass
    },
    updateTicket: async function(ticketId, data) {
      const ticket = await Ticket.findOneAndUpdate({_id: ticketId}, data);
      await ticket.save();

      return ticket;
    },
    deleteTicket: async function(ticketId) {
      //pass
    },
    createReport: async function(data) {
      data.date = new Date();
      const report = await Report(data);
      await report.save();

      return report;
    },
    updateReport: async function(reportId, data) {
      //pass
    },
    deleteReport: async function(ReportId, data) {
      //pass
    }
  }
}

module.exports = {
  create: async (projectId, userId) => {

    let project = await Project.findOne({_id: projectId});
    let user = await User.findOne({_id: userId});
    project = project.toObject();
    user = user.toObject();

    return NonePMTRepo(
      {...project},
      {...user}
    )
  }
};
