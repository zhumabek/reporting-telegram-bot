const Project = require('../../models/Project');
const User = require('../../models/User');
const { projectTypeEnum } = require('../../models/constants');
const JiraPMTRepoFactory = require('./jira');
const NonePMTRepoFactory = require('./base');

module.exports = function (projectId, telegramUserId) {
    return {
        create: async function() {
            const user = await User.findOne({telegramUserId});
            const pmtRepoFactory = await this._getPMSFactory();
            return pmtRepoFactory.create(projectId, user._id);
        },
        _getPMSFactory: async function() {
            const project = await Project.findOne({_id: projectId});

            switch (project.pmtType) {
                case projectTypeEnum.JIRA_PROJECT:
                    return JiraPMTRepoFactory;

                default:
                    return NonePMTRepoFactory
            }
        }
    };
};