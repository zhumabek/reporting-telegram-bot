const axios = require('axios');
const logger = require('../../logger');
const BasicPMTRepoFactory = require('./base');
const Ticket = require('../../models/Ticket');
const JiraUserDirectory = require('../../models/JiraUser');
const JiraProjectDirectory = require('../../models/JiraProject');


function JiraPMTRepo(
  basicPMT,
  jiraUserDirectory,
  jiraProjectDirectory
) {

  return {
    ...basicPMT,
    jiraProjectDirectory,
    jiraUserDirectory,

    getUsersInProgressTickets: async function() {
      const ticketsListData = await this._apiGetUsersInProgressTickets();
      const tickets = [];
      for (let i=0; i<ticketsListData.length; i++){
        tickets.push(
          await this.getTicketOrCreate(ticketsListData[i].number, ticketsListData[i].name)
        )
      }

      return tickets;
    },

    getTicketOrCreate: async function(number, name) {
      let ticket = await Ticket.findOne({
        projectId: this.project._id,
        userId: this.user._id,
        number,
        name
      });

      if (!ticket){
        ticket = Ticket({
          projectId: this.project._id,
          userId: this.user._id,
          number,
          name
        });
        await ticket.save();
      }
      return ticket
    },

    _apiGetUsersInProgressTickets: async function(){
      const path = 'https://' +
        this.jiraProjectDirectory.accessEmail +
        ':' + this.jiraProjectDirectory.accessToken +
        '@' + this.jiraProjectDirectory.domain +
        `/rest/api/2/search?jql=assignee="${this.jiraUserDirectory.identifier}"` +
        `AND project="${this.jiraProjectDirectory.key}" AND status in ("In Progress" )`;

      try{
        const response = await axios.get(path);
        return await this._parseResponseToSimplify(response.data);
      } catch (e) {
        logger.error(undefined, 'Error when get tickets', e);
        return []
      }
    },
    _parseResponseToSimplify: async function(response){
      logger.debug(undefined, 'Response from jira', response);
      const tickets = [];
      for (let i=0; i < response.issues.length; i ++){
        tickets.push({
          number: response.issues[i].key,
          name: response.issues[i].fields.description,
        })
      }

      return tickets;
    }
  }
}

module.exports = {
  create: async (projectId, userId) => {
    const basicPMT = await BasicPMTRepoFactory.create(projectId, userId);
    const jiraProjectDirectory = await JiraProjectDirectory.findOne({projectId});
    let jiraUserDirectory = await JiraUserDirectory.findOne({userId: userId});
    jiraUserDirectory = jiraUserDirectory.toObject();

    return JiraPMTRepo(
      basicPMT,
      {...jiraUserDirectory},
      {
        accessEmail: jiraProjectDirectory.apiAccessEmail,
        accessToken: jiraProjectDirectory.apiAccessToken,
        key: jiraProjectDirectory.key,
        domain: jiraProjectDirectory.domainName
      }
    )
  }
};
