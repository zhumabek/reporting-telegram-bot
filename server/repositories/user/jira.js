const JiraUser = require('../../models/JiraUser');


module.exports = async function (userId, projectId) {
    const jiraUserDirectory = await JiraUser.findOne({userId});
    return {
        userId: userId,
        projectId: projectId,
        jiraUserIdentifier: jiraUserDirectory.jiraUserIdentifier ? jiraUserDirectory.jiraUserIdentifier : null,

        getJiraUserIdentifier: () => {
            return this.jiraUserIdentifier;
        }
    }
};