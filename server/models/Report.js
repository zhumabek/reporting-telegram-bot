const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReportSchema = new Schema({
  ticketId: {
    type: Schema.Types.ObjectId,
    ref: 'Ticket',
    required: true
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  description: {
    type: String
  },
  timeSpent: {
    type: Number,
    required: true
  },
  ticketStatus: {
    type: String
  },
  date: {
    type: Date,
    required: true
  },
  editDate: {
    type: String
  },
  isDeleted: {
    type: Boolean
  }
});

const Report = mongoose.model('Report', ReportSchema);

module.exports = Report;

