const mongoose = require('mongoose');
const {projectTypeEnum} = require('./constants');
const Schema = mongoose.Schema;

const ProjectSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  isClosed: {
    type: Boolean,
    required: false,
    default: false
  },
  chatId: {
    type: String,
    required: true
  },
  members: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
  }],
  notNotification: [{
    type: Schema.Types.ObjectId,
    ref: 'User',
  }],
  reportsSendTime: {
    type: String,
    default: '22:00',
    required: true,
  },
  pmtType: {
    type: Number,
    enum: [projectTypeEnum.BASIC_PROJECT, projectTypeEnum.JIRA_PROJECT],
    default: projectTypeEnum.BASIC_PROJECT
  }
});

const Project = mongoose.model('Project', ProjectSchema);

module.exports = Project;

