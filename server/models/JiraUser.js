const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const JiraUser = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  identifier: {
    type: String,
    required: true
  }
});

const model = mongoose.model('JiraUser', JiraUser);

module.exports = model;