exports.projectTypeEnum = {
  BASIC_PROJECT: 0,
  JIRA_PROJECT: 1,
};

exports.projectTypeEnumDescriptions = {
  [this.projectTypeEnum.BASIC_PROJECT]: "Обычный проект",
  [this.projectTypeEnum.JIRA_PROJECT]: "Проект с Jira"
};

exports.TICKET_STATUS = {
  IN_PROGRESS: 'in progress',
  CLOSED: 'closed'
};
