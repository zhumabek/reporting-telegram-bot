const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const JiraProjectSchema = new Schema({
  projectId: {
    type: Schema.Types.ObjectId,
    ref: 'Project',
  },
  key: {
    type: String,
    required: true,
    unique: true
  },
  domainName: {
    type: String,
    required: true,
  },
  apiAccessEmail: {
    type: String,
    required: true
  },
  apiAccessToken: {
    type: String,
    required: true
  }
});

const JiraProject = mongoose.model('JiraProject', JiraProjectSchema);

module.exports = JiraProject;

