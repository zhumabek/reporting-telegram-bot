const assert = require('chai').assert;
const mongoose = require('mongoose');

const config = require('../config');
const scene = require('../bot/scenes/ExistingTickets/existingTickets');
const sceneIds = require('../bot/scenes/sceneIds');
const messages = require('../bot/scenes/ExistingTickets/existingTicketsMessages');
const { actionNames } = require('../bot/scenes/ExistingTickets/existingTicketsHelpers');
const { projectTypeEnum } = require('../models/constants');
const dataSet = require('./utils/dataSet');
const helper = require('./utils/helpers');


describe('Testing existingTickets scene', () => {
  beforeEach((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });
  afterEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });


  it(`Scene name should be ${sceneIds.EXISTING_TICKETS}`, (done) => {
    assert.equal(sceneIds.EXISTING_TICKETS, scene.id);
    done()
  });


  it('Should return existing, in progress tickets', async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        },
      },
    });
    ctx.debug.setTestState({
      projectId: project._id,
      messagesToDelete: []
    });

    await scene.enterMiddleware()(ctx);

    const actualText = ctx.debug.replies[1].extra.reply_markup.inline_keyboard[0][0].text;
    const expectBtnText = `#${ticket.number} - ${ticket.name }`;
    const actualActionName = JSON.parse(ctx.debug.replies[1].extra.reply_markup.inline_keyboard[0][0].callback_data).action;

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.EXISTING_TICKET_REPORT, messages.AVAILABLE_TICKETS]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.EXISTING_TICKET_REPORT, messages.AVAILABLE_TICKETS]
    );
    assert.equal(expectBtnText, actualText);
    assert.equal(actionNames.CHOOSE_TICKET, actualActionName)
  });


  it('Should return No Tickets and button to create new ticket if project without integration to PMT and available tickets not yet', async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        },
      },
    });
    ctx.debug.setTestState({
      projectId: project._id,
      messagesToDelete: []
    });

    await scene.enterMiddleware()(ctx);
    const actualText = ctx.debug.replies[1].extra.reply_markup.inline_keyboard[0][0].text;
    const actualActionName = JSON.parse(ctx.debug.replies[1].extra.reply_markup.inline_keyboard[0][0].callback_data).a;

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.EXISTING_TICKET_REPORT, messages.NO_TICKETS]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.EXISTING_TICKET_REPORT, messages.NO_TICKETS]
    );
    assert.equal(actualText, messages.CREATE_NEW_TICKET);
    assert.equal(actualActionName ,actionNames.NEW_TICKET_REPORT)
  });

  it('Should return only No Tickets message if project with integration to PMT and cant find in progress tickets', async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject({...dataSet.projectData, pmtType: projectTypeEnum.JIRA_PROJECT});
    await helper.createTestJiraUser(user._id);
    await helper.createTestJiraProject(project._id);

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        },
      },
    });
    ctx.debug.setTestState({
      projectId: project._id,
      messagesToDelete: []
    });

    await scene.enterMiddleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.EXISTING_TICKET_REPORT, messages.NO_TICKETS_IN_REMOTE_PMT]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.EXISTING_TICKET_REPORT, messages.NO_TICKETS_IN_REMOTE_PMT]
    );
  });


  it('Should enter to create new ticket scene when user enter createReportForNewTicketBtn', async () =>{
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const data = JSON.stringify({a: actionNames.NEW_TICKET_REPORT, p: project.id});

    const ctx = helper.makeMockContext({
      callback_query: {
        from: {
          chat: {id: 1234, username: user.name },
          username: user.name,
          id: user.telegramUserId
        },
        data
      }
    });

    await scene.middleware()(ctx);
    assert.equal(ctx.debug.currentScene, sceneIds.NEW_TICKET_REPORT)
  });



  it('Should enter back to Projects scene when user send "Назад"', async () => {
    const user = await helper.createTestUser();

    const ctx = helper.makeMockContext({
      message: {
        text: 'Назад',
        chat: {id: 1234, username: user.name},
        from: {
          username: user.name,
          id: user.telegramUserId
        },
      },
    });
    await scene.middleware()(ctx);
    assert.equal(ctx.debug.currentScene, sceneIds.PROJECTS);
  });


  it('Should enter to writeReportScene when I choose ticket', async () => {
    const user = await helper.createTestUser();
    const ticketId = '12inefksj;oeewf';
    const data = JSON.stringify({action: actionNames.CHOOSE_TICKET, params: ticketId});

    const ctx = helper.makeMockContext({
      callback_query: {
        from: {
          chat: {id: 1234, username: user.name },
          username: user.name,
          id: user.telegramUserId
        },
        data
      }
    });
    ctx.debug.setTestState({
      messagesToDelete: [{
        message_id: 'wfqwfwef',
        chat: {
          id: 'fwfwoefeoeofeof'
        }
      }]
    });

    await scene.middleware()(ctx);
    assert.equal(ctx.debug.currentScene, sceneIds.EXISTING_TICKET_REPORT)
  })
});


