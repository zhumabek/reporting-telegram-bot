const assert = require('chai').assert;
const mongoose = require('mongoose');
const config = require('../config');
const helpers = require('./utils/helpers');
const sceneIds = require('../bot/scenes/sceneIds');
const scene = require('../bot/scenes/EditReportField/EditReportField');
const messages = require('../bot/scenes/EditReportField/EditReportFieldMessages');
const reportMessages = require('../bot/scenes/utils/reportMessages');
const Report = require('../models/Report');
const Ticket = require('../models/Ticket');
const { returnUserId, returnTicketNumber, returnTicketName, returnTicketId, returnReportFieldInfo, getFieldActionMessage,
  getFieldButtons, updateStatus, updateDescription, updateTimeSpent } = require('../bot/scenes/EditReportField/EditReportFieldHelpers');

const steps = {
  UNDEFINED: null,
  FIRST: 0,
  SECOND: 1,
};

describe('Testing EditReport scene', () => {
  let user, project, ticket, report;

  before(async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    user = await helpers.createTestUser();
    project = await helpers.createTestProject();
    ticket = await helpers.createTestTicket(user._id, project._id);
    report = await helpers.createTestReport(ticket._id, user._id);
    report.ticketNumber = ticket.number;
    report.ticketName = ticket.name;
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });

  it(`Scene name should be ${sceneIds.EDIT_REPORT}`, () => {
    assert.equal(sceneIds.EDIT_REPORT, scene.id);
  });

  it('Bot should enter to Recent Reports scene, when back button is pressed', async () => {
    const ctx = helpers.makeMockContext({message: {text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE, chat: {id: 1234, username: user.name },
        from: {username: user.name, id: user.telegramUserId}}});
    ctx.debug.setTestSession({cursor: steps.FIRST});
    ctx.scene.state.reportId = report._id;
    await scene.middleware()(ctx);
    const actual = ctx.debug.currentScene;
    const expected = sceneIds.RECENT_REPORTS;
    assert.equal(actual, expected);
  });

  describe('Testing returnUserId()', () => {
    it('returnUserId() should return userID', async () => {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      const actual = await returnUserId(ctx);
      const expected = user._id;
      assert.deepEqual(actual, expected);
    });
  });

  describe('Testing returnTicketNumber()', () => {
    it('returnTicketNumber() should return ticket number', async () => {
      const actual = await returnTicketNumber(report._id);
      const expected = ticket.number;
      assert.equal(actual, expected);
    });
  });

  describe('Testing returnTicketName()', () => {
    it('returnTicketName() should return ticket name', async () => {
      const actual = await returnTicketName(report._id);
      const expected = ticket.name;
      assert.equal(actual, expected);
    });
  });

  describe('Testing returnTicketId()', () => {
    it('returnTicketId() should return ticket ID', async () => {
      const actual = await returnTicketId(report._id);
      const expected = ticket._id;
      assert.deepEqual(actual, expected);
    });
  });

  describe('Testing returnReportFieldInfo()', () => {
    const description = 'des';
    const timeSpent = 'time';
    const status = 'status';
    const wrongField = 'NO_SUCH_FIELD';
    it('returnReportFieldInfo() should return type String', async () => {
      const actual = await returnReportFieldInfo(description, report._id);
      assert.isString(actual);
    });
    it(`returnReportFieldInfo() should return "${messages.DESCRIPTION}:" and report description, when field name is "des"`, async () => {
      const actual = await returnReportFieldInfo(description, report);
      const expected = `${messages.DESCRIPTION}: ${report.description}`;
      assert.equal(actual, expected);
    });
    it(`returnReportFieldInfo() should return "${messages.TIME_SPENT}:" and report timeSpent, when field name is "time"`, async () => {
      const actual = await returnReportFieldInfo(timeSpent, report);
      const expected = `${messages.TIME_SPENT}: ${report.timeSpent}`;
      assert.equal(actual, expected);
    });
    it(`returnReportFieldInfo() should return "${messages.TICKET_STATUS}:" and report ticketStatus, when field name is "status"`, async () => {
      const actual = await returnReportFieldInfo(status, report);
      const expected = `${messages.TICKET_STATUS}: ${report.ticketStatus}`;
      assert.equal(actual, expected);
    });
    it(`returnReportFieldInfo() should return "${messages.DESCRIPTION}:" and report description, when there is no such a field`, async () => {
      const actual = await returnReportFieldInfo(wrongField, report);
      const expected = `${messages.DESCRIPTION}: ${report.description}`;
      assert.equal(actual, expected);
    });
  });

  describe('Testing getFieldActionMessage()', () => {
    const description = 'des';
    const timeSpent = 'time';
    const status = 'status';
    const wrongField = 'NO_SUCH_FIELD';
    it('getFieldActionMessage() should return type String', async () => {
      const actual = await getFieldActionMessage(description);
      assert.isString(actual);
    });
    it(`getFieldActionMessage() should return "${messages.ENTER_DESCRIPTION}, when field name is "des"`, async () => {
      const actual = await getFieldActionMessage(description);
      const expected = `${messages.ENTER_DESCRIPTION}`;
      assert.equal(actual, expected);
    });
    it(`getFieldActionMessage() should return "${messages.ENTER_TIME_SPENT}, when field name is "time"`, async () => {
      const actual = await getFieldActionMessage(timeSpent);
      const expected = `${messages.ENTER_TIME_SPENT}`;
      assert.equal(actual, expected);
    });
    it(`getFieldActionMessage() should return "${messages.CHOOSE_TICKET_STATUS}, when field name is "status"`, async () => {
      const actual = await getFieldActionMessage(status);
      const expected = `${messages.CHOOSE_TICKET_STATUS}`;
      assert.equal(actual, expected);
    });
    it(`getFieldActionMessage() should return "${messages.ENTER_DESCRIPTION}, when there is no such a field`, async () => {
      const actual = await getFieldActionMessage(wrongField);
      const expected = `${messages.ENTER_DESCRIPTION}`;
      assert.equal(actual, expected);
    });
  });

  describe('Testing getFieldButtons()', () => {
    const description = 'des';
    const timeSpent = 'time';
    const status = 'status';
    const wrongField = 'NO_SUCH_FIELD';
    it('getFieldButtons() should return three buttons: "in progress", "closed", "Назад", when fieldName is "status"', async () => {
      const result = await getFieldButtons(status);
      const actual = result.reply_markup.keyboard;
      assert.equal(actual[0], reportMessages.IN_PROGRESS);
      assert.equal(actual[1], reportMessages.CLOSED);
      assert.equal(actual[2], reportMessages.GO_BACK_TO_PREVIOUS_SCENE);
    });
    it('getFieldButtons() should return back button, when fieldName is "des", "time", or wrong FieldName', async () => {
      const desResult = getFieldButtons(description);
      const timeResult = getFieldButtons(timeSpent);
      const wrongResult = getFieldButtons(wrongField);
      assert.equal(desResult.reply_markup.keyboard[0], reportMessages.GO_BACK_TO_PREVIOUS_SCENE);
      assert.equal(timeResult.reply_markup.keyboard[0], reportMessages.GO_BACK_TO_PREVIOUS_SCENE);
      assert.equal(wrongResult.reply_markup.keyboard[0], reportMessages.GO_BACK_TO_PREVIOUS_SCENE);
    });
  });

  describe('Testing updateStatus()', () => {
    const inProgress = reportMessages.IN_PROGRESS;
    const closed = reportMessages.CLOSED;
    it('updateStatus() should update the status of the created report', async () => {
      report.ticketStatus = closed;
      await updateStatus(report._id, inProgress);
      const updatedReport = await Report.findOne({_id: report._id});
      const newStatus = updatedReport.ticketStatus;
      assert.equal(newStatus, inProgress);
    });
    it('updateStatus() should also update the status of the ticket', async () => {
      ticket.status = closed;
      await updateStatus(report._id, inProgress);
      const updatedTicket = await Ticket.findOne({_id: ticket._id});
      const newStatus = updatedTicket.status;
      assert.equal(newStatus, inProgress);
    });
  });

  describe('Testing updateDescription()', () => {
    it('updateDescription() should update the description of the report', async () => {
      const prevDes = 'Old description';
      const newDes = 'New description';
      report.description = prevDes;
      await updateDescription(report._id, newDes);
      const updatedReport = await Report.findOne({_id: report._id});
      const newDescription = updatedReport.description;
      assert.equal(newDescription, newDes);
    });
  });

  describe('Testing updateTimeSpent()', () => {
    it('updateTimeSpent() should update the timeSpent of the report', async () => {
      const prevTime = 2;
      const newTime = 7;
      report.timeSpent = prevTime;
      await updateTimeSpent(report._id, newTime);
      const updatedReport = await Report.findOne({_id: report._id});
      const newDescription = updatedReport.timeSpent;
      assert.equal(newDescription, newTime);
    });
  });

  describe('Bot should return field information of the report, when the user enters the EditReportField scene', async () => {
    const description = 'des';
    const timeSpent = 'time';
    const status = 'status';
    it('Bot should return description information of the report, if field = "des"', async () => {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.UNDEFINED});
      ctx.scene.state.report = report;
      ctx.scene.state.field = description;
      await scene.middleware()(ctx);
      const actual = ctx.debug.replies[0].message;
      const expected = `${messages.DESCRIPTION}: ${report.description}`;
      assert.equal(actual, expected);
    });
    it('Bot should return time spent information of the report, if field = "time"', async () => {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.UNDEFINED});
      ctx.scene.state.report = report;
      ctx.scene.state.field = timeSpent;
      await scene.middleware()(ctx);
      const actual = ctx.debug.replies[0].message;
      const expected = `${messages.TIME_SPENT}: ${report.timeSpent}`;
      assert.equal(actual, expected);
    });
    it('Bot should return ticket status information of the report, if field = "status"', async () => {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.UNDEFINED});
      ctx.scene.state.report = report;
      ctx.scene.state.field = status;
      await scene.middleware()(ctx);
      const actual = ctx.debug.replies[0].message;
      const expected = `${messages.TICKET_STATUS}: ${report.ticketStatus}`;
      assert.equal(actual, expected);
    });
  });

  describe('Bot should update the field name, which the user chooses', () => {
    const description = 'des';
    const timeSpent = 'time';
    const status = 'status';
    const successfullyEnteredSceneId = sceneIds.RECENT_REPORTS;
    it('Bot should update description, if chosen field name is "des" and should enter "RECENT REPORTS" scene', async () => {
      const ctx = helpers.makeMockContext({message: {text: "NEW DESCRIPTION", chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.SECOND});
      ctx.scene.state.report = report;
      ctx.scene.state.field = description;
      await scene.middleware()(ctx);
      const updatedReport = ctx.scene.state.report;
      const actual = updatedReport.description;
      const expected = 'NEW DESCRIPTION';
      const currentScene = ctx.debug.currentScene;
      assert.equal(actual, expected, "new description that was given was 'NEW DESCRIPTION'");
      assert.equal(currentScene, successfullyEnteredSceneId);
    });
    it('Bot should update time spent, if chosen field name is "time" and a message should appear if update was successful', async () => {
      const ctx = helpers.makeMockContext({message: {text: 11, chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.SECOND});
      ctx.scene.state.report = report;
      ctx.scene.state.field = timeSpent;
      await scene.middleware()(ctx);
      const updatedReport = ctx.scene.state.report;
      const actual = updatedReport.timeSpent;
      const expected = 11;
      const currentScene = ctx.debug.currentScene;
      assert.equal(actual, expected, "new time spent that was given was '11'");
      assert.equal(currentScene, successfullyEnteredSceneId);
    });
    it('Bot should update ticket status, if chosen field name is "status" and a message should appear if update was successful', async () => {
      const ctx = helpers.makeMockContext({message: {text: "CLOSED", chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.SECOND});
      ctx.scene.state.report = report;
      ctx.scene.state.field = status;
      await scene.middleware()(ctx);
      const updatedReport = ctx.scene.state.report;
      const actual = updatedReport.ticketStatus;
      const expected = 'CLOSED';
      const currentScene = ctx.debug.currentScene;
      assert.equal(actual, expected, "new ticket status that was given was 'CLOSED'");
      assert.equal(currentScene, successfullyEnteredSceneId);
    });
  });

  describe('Bot should delete all unnecessary messages after the user leaves the scene', () => {
    it('messagesToDelete array should contain some messages', async () => {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.FIRST});
      ctx.scene.state.report = report;
      await scene.middleware()(ctx);
      const messages = ctx.debug.getMessagesToDeleteTexts();
      assert.notEqual(messages.length, 0);
    });
    it('Bot should delete all messages in messagesToDelete array after the user leaves the scene', async () => {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.debug.setTestSession({cursor: steps.FIRST});
      ctx.scene.state.report = report;
      await scene.middleware()(ctx);
      const messages = ctx.debug.getMessagesToDeleteTexts();
      assert.notEqual(messages.length, 0);
      ctx.scene.leave();
      const newMessages = ctx.debug.getMessagesToDeleteTexts();
      assert.equal(newMessages.length, 0);
    });
  });
});

