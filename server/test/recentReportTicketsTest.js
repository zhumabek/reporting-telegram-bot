const assert = require('chai').assert;
const mongoose = require('mongoose');

const config = require('../config');
const scene = require('../bot/scenes/RecentReportTickets/recentReportTickets');
const sceneIds = require('../bot/scenes/sceneIds');
const messages = require('../bot/scenes/RecentReportTickets/recentReportTicketMessages');
const { actions } = require('../bot/scenes/RecentReportTickets/recentReportTicketHelpers');
const helper = require('./utils/helpers');



describe('Testing recentReportTickets scene', () => {
  beforeEach((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });
  afterEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });


  it('should return tickets for which wroten reports today', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    await helper.createTestReport(ticket._id, user._id);

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        },
      },
    });
    ctx.debug.setTestState({
      projectId: project._id,
      messagesToDelete: []
    });

    await scene.enterMiddleware()(ctx);

    const expectBtnText = `#${ticket.number} - ${ticket.name }`;
    const actualActionName = JSON.parse(ctx.debug.replies[1].extra.reply_markup.inline_keyboard[0][0].callback_data).action;

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.YOU_ARE_AT_RECENT_REPORT_TICKETS_SCENE, messages.AVAILABLE_TICKETS]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.YOU_ARE_AT_RECENT_REPORT_TICKETS_SCENE, messages.AVAILABLE_TICKETS]
    );
    assert.deepEqual(
      ctx.debug.getKeyboardsText(),
      [messages.BACK]
    );
    assert.deepEqual(ctx.debug.getButtonsText(), [expectBtnText]);
    assert.equal(actions.CHOOSE_TICKET, actualActionName)
  });


  it('should send no tickets if list of today reports is empty', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        },
      },
    });
    ctx.debug.setTestState({
      projectId: project._id,
      messagesToDelete: []
    });

    await scene.enterMiddleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.YOU_ARE_AT_RECENT_REPORT_TICKETS_SCENE, messages.NO_TICKETS]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.YOU_ARE_AT_RECENT_REPORT_TICKETS_SCENE, messages.NO_TICKETS]
    );
    assert.deepEqual(
      ctx.debug.getKeyboardsText(),
      [messages.BACK]
    );
  });


  it(`Should enter to existing tickets scene if I send Back`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: messages.BACK,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestState({
      projectId: project._id,
      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.PROJECTS);
  });


  it('should enter editReportField scene if I choosed ticket', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const report = await helper.createTestReport(ticket._id, user._id);
    const data = JSON.stringify({a: actions.CHOOSE_REPORT_TICKET, id: ticket.id});

    const ctx = helper.makeMockContext({
      callback_query: {
        from: {
          chat: {id: 1234, username: user.name },
          username: user.name,
          id: user.telegramUserId
        },
        data
      }
    });

    await scene.middleware()(ctx);
    assert.equal(ctx.debug.currentScene, sceneIds.RECENT_REPORTS)
  });
});