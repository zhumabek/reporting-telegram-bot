const expect = require('chai').expect;
const sinon = require('sinon');
const Scene = require('../bot/scenes/EditNewTicketReportField/editNewTicketReportField');
const WizardContext = require('telegraf/scenes/wizard/context');
const testHelper = require('./utils/helpers');
const Ticket = require('../models/Ticket');
const editNewTicketReportFieldMessages = require('../bot/scenes/EditNewTicketReportField/editNewTicketReportFieldMessages');
const newTicketReportMessages = require('../bot/scenes/NewTicketReport/NewTicketReportMessages');
const reportMessages = require('../bot/scenes/utils/reportMessages');
const sceneIds = require("../bot/scenes/sceneIds");
const {dummyReportObject} = require('./utils/dataSet');

describe('EditNewTicketReportField scene tests', function () {
  beforeEach(function (done) {
    testHelper.connectToDB()
      .then(() => done());
  });

  afterEach(function (done) {
    testHelper.closeDbConnection().then(() => done());
  });

  describe( 'After entering EditNewTicketReportField scene, First step ', function() {
    it(`if "ticket number" field was received as a payload, it should reply with messages that contains ticket number information`, async function () {
      const fieldName = 'tNumber';
      const ctx = testHelper.makeMockContext();
      ctx.scene.state.report = dummyReportObject;
      ctx.scene.state.field = fieldName;
      ctx.scene.session = {cursor: 0};

      await Scene.middleware()(ctx);
      let fieldInfoMessage = ctx.debug.replies[0].message;
      let fieldPromptMessage = ctx.debug.replies[1].message;
      let buttons = ctx.debug.replies[1].extra.reply_markup.keyboard;
      expect(fieldInfoMessage).to.include(editNewTicketReportFieldMessages.TICKET_NUMBER);
      expect(fieldPromptMessage).to.include(editNewTicketReportFieldMessages.ENTER_TICKET_NUMBER);
      expect(buttons).to.be.an('array').that.is.not.empty;
      expect(buttons.length).equal(1);
      expect(ctx.wizard.cursor).equal(1);
    });

    it(`if "ticket name" field was received as a payload, it should reply with messages that contains ticket name information`, async function () {
      const fieldName = 'tName';
      const ctx = testHelper.makeMockContext();
      ctx.scene.state.report = dummyReportObject;
      ctx.scene.state.field = fieldName;
      ctx.scene.session = {cursor: 0};

      await Scene.middleware()(ctx);
      let fieldInfoMessage = ctx.debug.replies[0].message;
      let fieldPromptMessage = ctx.debug.replies[1].message;
      let buttons = ctx.debug.replies[1].extra.reply_markup.keyboard;
      expect(fieldInfoMessage).to.include(editNewTicketReportFieldMessages.TICKET_NAME);
      expect(fieldPromptMessage).to.include(editNewTicketReportFieldMessages.ENTER_TICKET_NAME);
      expect(buttons).to.be.an('array').that.is.not.empty;
      expect(buttons.length).equal(1);
    });

    it(`if "ticket status" field was received as a payload, it should reply with messages that contains ticket status information and should return status buttons`, async function () {
      const fieldName = 'status';
      const ctx = testHelper.makeMockContext();
      ctx.scene.state.report = dummyReportObject;
      ctx.scene.state.field = fieldName;
      ctx.scene.session = {cursor: 0};

      await Scene.middleware()(ctx);
      let fieldInfoMessage = ctx.debug.replies[0].message;
      let fieldPromptMessage = ctx.debug.replies[1].message;
      let buttons = ctx.debug.replies[1].extra.reply_markup.keyboard;
      expect(fieldInfoMessage).to.include(editNewTicketReportFieldMessages.TICKET_STATUS);
      expect(fieldPromptMessage).to.include(editNewTicketReportFieldMessages.CHOOSE_TICKET_STATUS);
      expect(buttons).to.be.an('array').that.is.not.empty;
      expect(buttons.length).equal(3);
    });

    it(`if "report description" field was received as a payload, it should reply with message that contains report description information`, async function () {
      const fieldName = 'des';
      const ctx = testHelper.makeMockContext();
      ctx.scene.state.report = dummyReportObject;
      ctx.scene.state.field = fieldName;
      ctx.scene.session = {cursor: 0};

      await Scene.middleware()(ctx);
      let fieldInfoMessage = ctx.debug.replies[0].message;
      let fieldPromptMessage = ctx.debug.replies[1].message;
      let buttons = ctx.debug.replies[1].extra.reply_markup.keyboard;
      expect(fieldInfoMessage).to.include(editNewTicketReportFieldMessages.DESCRIPTION);
      expect(fieldPromptMessage).to.include(editNewTicketReportFieldMessages.ENTER_DESCRIPTION);
      expect(buttons).to.be.an('array').that.is.not.empty;
      expect(buttons.length).equal(1);
    });

    it(`if "report timeSpent" field was received as a payload, it should reply with message that contains spent time information`, async function () {
      const fieldName = 'time';
      const ctx = testHelper.makeMockContext();
      ctx.scene.state.report = dummyReportObject;
      ctx.scene.state.field = fieldName;
      ctx.scene.session = {cursor: 0};

      await Scene.middleware()(ctx);
      let fieldInfoMessage = ctx.debug.replies[0].message;
      let fieldPromptMessage = ctx.debug.replies[1].message;
      let buttons = ctx.debug.replies[1].extra.reply_markup.keyboard;
      expect(fieldInfoMessage).to.include(editNewTicketReportFieldMessages.TIME_SPENT);
      expect(fieldPromptMessage).to.include(editNewTicketReportFieldMessages.ENTER_TIME_SPENT);
      expect(buttons).to.be.an('array').that.is.not.empty;
      expect(buttons.length).equal(1);
    });

    it(`should enter second step after successfully replying all required messages that contains certain field information and related prompt message`, async function () {
      const fieldName = 'time';
      const ctx = testHelper.makeMockContext();
      ctx.scene.state.report = dummyReportObject;
      ctx.scene.state.field = fieldName;
      ctx.scene.session = {cursor: 0};

      await Scene.middleware()(ctx);
      expect(ctx.wizard.cursor).equal(1);
    });
  });

  describe( 'In a second step of a EditNewTicketReportField WizardScene', function() {
    describe( 'If received state contains field name that equals "tNumber" or in other words "ticket number"', function() {
      describe( 'It should validate user response message', function() {
        describe('if response ticket number is busy by another ticket in the DB', function () {
          it(`it should reply with '${newTicketReportMessages.EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER}' message`, async function () {
            const fieldName = 'tNumber';
            const dummyResponseNumber = 111;
            const ctx = testHelper.makeMockContext({message: {text: dummyResponseNumber}});
            ctx.scene.state.report = dummyReportObject;
            ctx.scene.state.field = fieldName;
            ctx.scene.session = {cursor: 1};

            sinon.stub(Ticket, 'findOne').returns(true);

            await Scene.middleware()(ctx);
            let repliedMsg = ctx.debug.replies[0].message;
            expect(repliedMsg).equal(newTicketReportMessages.EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER);
            Ticket.findOne.restore()
          });

          it(`it should reply with '${newTicketReportMessages.NEXT_ACTION}' message and
           should return nextActionButtons: '${newTicketReportMessages.GO_TO_EXISTING_TICKETS}', '${newTicketReportMessages.ENTER_TICKET_NUMBER}', '${newTicketReportMessages.GO_BACK_TO_PREVIOUS_SCENE}'`, async function () {
            const fieldName = 'tNumber';
            const dummyResponseNumber = 111;
            const ctx = testHelper.makeMockContext({message: {text: dummyResponseNumber}});
            ctx.scene.state.report = dummyReportObject;
            ctx.scene.state.field = fieldName;
            ctx.scene.session = {cursor: 1};

            sinon.stub(Ticket, 'findOne').returns(true);

            await Scene.middleware()(ctx);
            let repliedMsg = ctx.debug.replies[1].message;
            let returnedActionButtons = ctx.debug.replies[1].extra.reply_markup.keyboard;
            expect(repliedMsg).equal(newTicketReportMessages.NEXT_ACTION);
            expect(returnedActionButtons).to.be.an('array').that.is.not.empty;
            expect(returnedActionButtons.length).equal(3);
            Ticket.findOne.restore()
          });

          it(`it should go to next third step`, async function () {
            const fieldName = 'tNumber';
            const dummyResponseNumber = 111;
            const ctx = testHelper.makeMockContext({message: {text: dummyResponseNumber}});
            ctx.scene.state.report = dummyReportObject;
            ctx.scene.state.field = fieldName;
            ctx.scene.session = {cursor: 1};

            sinon.stub(Ticket, 'findOne').returns(true);

            await Scene.middleware()(ctx);
            let step = ctx.wizard.cursor;
            expect(step).equal(2);
            Ticket.findOne.restore();
          });
        });

        it(`if response message valid number, it should update current ticket number with new number and should go to '${sceneIds.EDIT_NEW_TICKET_REPORT}' scene with valid state that contains updated ticket number value`, async function () {
          const fieldName = 'tNumber';
          const dummyResponseNumber = 111;
          const ctx = testHelper.makeMockContext({message: {text: dummyResponseNumber}});
          ctx.scene.state.report = dummyReportObject;
          ctx.scene.state.field = fieldName;
          ctx.scene.session = {cursor: 1};

          await Scene.middleware()(ctx);
          let {ticketNumber} = ctx.scene.state.report;
          expect(ticketNumber).equal(dummyResponseNumber);
          expect(ctx.debug.currentScene).equal(sceneIds.EDIT_NEW_TICKET_REPORT);
        });
      });
    });

    describe('If received state contains field name that equals "tName" or in other words "ticket name"', function () {
      it(`should update current ticket name with new name and should go to '${sceneIds.EDIT_NEW_TICKET_REPORT}' scene with valid state that contains updated ticket name value`, async function () {
        const fieldName = 'tName';
        const dummyResponseName = "I'm a new ticket name";
        const ctx = testHelper.makeMockContext({message: {text: dummyResponseName}});
        ctx.scene.state.report = dummyReportObject;
        ctx.scene.state.field = fieldName;
        ctx.scene.session = {cursor: 1};

        await Scene.middleware()(ctx);
        let {ticketName} = ctx.scene.state.report;
        expect(ticketName).equal(dummyResponseName);
        expect(ctx.debug.currentScene).equal(sceneIds.EDIT_NEW_TICKET_REPORT);
      });
    });

    describe('If received state contains field name that equals "des" or in other words "report description"', function () {
      it(`should update current report description with new description and should go to '${sceneIds.EDIT_NEW_TICKET_REPORT}' scene with valid state that contains updated report description value`, async function () {
        const fieldName = 'des';
        const dummyResponseDescription = "I'm a report description lalalala";
        const ctx = testHelper.makeMockContext({message: {text: dummyResponseDescription}});
        ctx.scene.state.report = dummyReportObject;
        ctx.scene.state.field = fieldName;
        ctx.scene.session = {cursor: 1};

        await Scene.middleware()(ctx);
        let {description} = ctx.scene.state.report;
        expect(description).equal(dummyResponseDescription);
        expect(ctx.debug.currentScene).equal(sceneIds.EDIT_NEW_TICKET_REPORT);
      });
    });

    describe('If received state contains field name that equals "status" or in other words "ticket status"', function () {
      it(`should update current ticket status with new status value("in progress" or "closed") and should go to '${sceneIds.EDIT_NEW_TICKET_REPORT}' scene with valid state that contains updated ticket status value`, async function () {
        const fieldName = 'status';
        const dummyResponseStatus = "in progress";
        const ctx = testHelper.makeMockContext({message: {text: dummyResponseStatus}});
        ctx.scene.state.report = dummyReportObject;
        ctx.scene.state.field = fieldName;
        ctx.scene.session = {cursor: 1};

        await Scene.middleware()(ctx);
        let {ticketStatus} = ctx.scene.state.report;
        expect(ticketStatus).equal(dummyResponseStatus);
        expect(ctx.debug.currentScene).equal(sceneIds.EDIT_NEW_TICKET_REPORT);
      });
    });

    describe('If received state contains field name that equals "time" or in other words "time spent"', function () {
      describe('If response message value not Type of Integer', function () {
        it(`should reply with '${editNewTicketReportFieldMessages.USE_NUMBERS}' message and should not go to next step`, async function () {
          const fieldName = 'time';
          const dummyResponseTimeValue = "I'm not an Integer";
          const ctx = testHelper.makeMockContext({message: {text: dummyResponseTimeValue}});
          ctx.scene.state.report = dummyReportObject;
          ctx.scene.state.field = fieldName;
          ctx.scene.session = {cursor: 1};

          await Scene.middleware()(ctx);
          let repliedMsg = ctx.debug.replies[0].message;
          let step = ctx.wizard.cursor;
          expect(repliedMsg).equal(editNewTicketReportFieldMessages.USE_NUMBERS);
          expect(step).equal(1);
        });
      });

      describe('If response message value greater than 24', function () {
        it(`should reply with '${editNewTicketReportFieldMessages.WRONG_TIME}' message and should not go to next step`, async function () {
          const fieldName = 'time';
          const dummyResponseTimeValue = 34;
          const ctx = testHelper.makeMockContext({message: {text: dummyResponseTimeValue}});
          ctx.scene.state.report = dummyReportObject;
          ctx.scene.state.field = fieldName;
          ctx.scene.session = {cursor: 1};

          await Scene.middleware()(ctx);
          let repliedMsg = ctx.debug.replies[0].message;
          let step = ctx.wizard.cursor;
          expect(repliedMsg).equal(editNewTicketReportFieldMessages.WRONG_TIME);
          expect(step).equal(1);
        });
      });

      describe('If response message value is valid time', function () {
        it(`should update current spent time with new value and should go to '${sceneIds.EDIT_NEW_TICKET_REPORT}' scene with valid state that contains updated spent time value`, async function () {
          const fieldName = 'time';
          const dummyResponseTimeValue = 2;
          const ctx = testHelper.makeMockContext({message: {text: dummyResponseTimeValue}});
          ctx.scene.state.report = dummyReportObject;
          ctx.scene.state.field = fieldName;
          ctx.scene.session = {cursor: 1};

          await Scene.middleware()(ctx);
          let {timeSpent} = ctx.scene.state.report;
          expect(timeSpent).equal(dummyResponseTimeValue);
          expect(ctx.debug.currentScene).equal(sceneIds.EDIT_NEW_TICKET_REPORT);
        });
      });
    });
  });

  describe( 'In third step of a EditNewTicketReportField WizardScene', function() {
    describe( `If received response message equals '${newTicketReportMessages.GO_TO_EXISTING_TICKETS}'`, function() {
      it(`bot should send user to '${sceneIds.EXISTING_TICKET_REPORT}' scene`, async function () {
        const dummyResponseActionMessage = newTicketReportMessages.GO_TO_EXISTING_TICKETS;
        const ctx = testHelper.makeMockContext({message: {text: dummyResponseActionMessage}});
        ctx.scene.session = {cursor: 2};

        await Scene.middleware()(ctx);
        expect(ctx.debug.currentScene).equal(sceneIds.EXISTING_TICKETS);
      });
    });

    describe( `If received response message equals '${newTicketReportMessages.ENTER_TICKET_NUMBER}'`, function() {
      it(`bot should send user to the beginning of current wizard scene, in other words to the first step`, async function () {
        const dummyResponseActionMessage = newTicketReportMessages.ENTER_TICKET_NUMBER;
        const ctx = testHelper.makeMockContext({message: {text: dummyResponseActionMessage}});
        ctx.scene.session = {cursor: 2};
        await Scene.middleware()(ctx);
        let currentStep = ctx.scene.session.cursor;
        expect(currentStep).equal(0);
      });
    });

    describe( `If received response message not equals '${newTicketReportMessages.ENTER_TICKET_NUMBER}' or '${newTicketReportMessages.GO_TO_EXISTING_TICKETS}'`, function() {
      it(`bot should reply with '${newTicketReportMessages.WRONG_ACTION}' and should not go to next step`, async function () {
        const dummyResponseActionMessage = "Naruto Shippuden";
        const ctx = testHelper.makeMockContext({message: {text: dummyResponseActionMessage}});
        ctx.scene.session = {cursor: 2};

        await Scene.middleware()(ctx);
        let currentStep = ctx.wizard.cursor;
        let repliedMsg = ctx.debug.replies[0].message;
        expect(repliedMsg).equal(newTicketReportMessages.WRONG_ACTION);
        expect(currentStep).equal(2);
      });
    });
  });

  describe('General', function () {
    describe('If "go back to previous scene" button was clicked', function () {
      it(`should go to '${sceneIds.EDIT_NEW_TICKET_REPORT}' scene`, async function () {
        const dummyResponseActionMessage = reportMessages.GO_BACK_TO_PREVIOUS_SCENE;
        const ctx = testHelper.makeMockContext({message: {text: dummyResponseActionMessage}});
        ctx.scene.session = {cursor: 2};

        await Scene.middleware()(ctx);
        let currentScene = ctx.debug.currentScene;
        expect(currentScene).equal(sceneIds.EDIT_NEW_TICKET_REPORT);
      })
    });
  });

});