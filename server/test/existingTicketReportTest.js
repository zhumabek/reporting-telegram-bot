const assert = require('chai').assert;
const sinon = require('sinon');
const mongoose = require('mongoose');

const config = require('../config');
const helper = require('./utils/helpers');
const ProjectManagementTool = require('../repositories/pmt/index');
const {reportData, ticketData} = require('./utils/dataSet');

const sceneIds = require('../bot/scenes/sceneIds');
const scene = require('../bot/scenes/ExistingTicketReport/ExistingTicketReport');
const sceneMessages = require('../bot/scenes/ExistingTicketReport/ExistingTicketReportMessages');
const botMessages = require('../bot/scenes/utils/reportMessages');
const sceneHelpers = require('../bot/scenes/utils/helpers');
const { actions } = require('../bot/scenes/ExistingTicketReport/ExistingTicketReportHelpers');


const steps = {
  UNDEFINED: null,
  FIRST: 0,
  SECOND: 1,
  THIRD: 2,
  FOURTH: 3,
  FIFTH: 4,
  SIXTH: 5,
  SEVENTH: 6,
  EIGHTH: 7,
  NINTH: 8,
};


describe('Testing existingTicketReport scene.', () => {
  beforeEach((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });
  afterEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });
  describe("Initial(0) step",() => {

    describe("If sum of all report's spent time for to day reached the limit of 12 hours", () => {

      it(`should reply with '${botMessages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT}' message`, async () => {
        const user = await helper.createTestUser();
        const project = await helper.createTestProject();
        const ticket = await helper.createTestTicket(user._id, project._id);
        const reportData = {
          userId: null,
          ticketId: null,
          description: 'Example description',
          timeSpent: 12,
          ticketStatus: 'in progress',
          date: new Date(),
          editDate: 'null',
          isDeleted: false
        };
        const report = await helper.createTestReport(ticket._id, user._id, reportData);
        report.ticketNumber = ticket.number;
        report.ticketName = ticket.name;
        const ctx = helper.makeMockContext({message: {chat: {id: 1234, username: user.name },
            from: { username: user.name, id: user.telegramUserId }}
        });
        ctx.debug.setTestState({ticketId: ticket._id});
        ctx.debug.setTestSession({cursor: steps.UNDEFINED});


        await scene.middleware()(ctx);
        const actual = ctx.debug.replies[0].message;
        const expected = botMessages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT;
        assert.equal(actual, expected);
      });

      it(`should reply with 'Go back' and 'Go to edit existing reports' buttons`, async () => {
        const user = await helper.createTestUser();
        const project = await helper.createTestProject();
        const ticket = await helper.createTestTicket(user._id, project._id);
        const reportData = {
          userId: null,
          ticketId: null,
          description: 'Example description',
          timeSpent: 12,
          ticketStatus: 'in progress',
          date: new Date(),
          editDate: 'null',
          isDeleted: false
        };
        const report = await helper.createTestReport(ticket._id, user._id, reportData);
        report.ticketNumber = ticket.number;
        report.ticketName = ticket.name;
        const ctx = helper.makeMockContext({
          message: {
            chat: {id: 1234, username: user.name },
            from: { username: user.name, id: user.telegramUserId }
          }});
        ctx.debug.setTestState({ticketId: ticket._id});
        ctx.debug.setTestSession({cursor: steps.UNDEFINED});


        await scene.middleware()(ctx);
        const actual = ctx.debug.replies[0].extra.reply_markup.keyboard;
        const expected = [[botMessages.GO_TO_RECENT_REPORT_TICKETS],[botMessages.GO_BACK_TO_PREVIOUS_SCENE]];
        assert.deepEqual(actual, expected);
      });
    });

    describe("If button 'Перейти в редактирование отчетов' was clicked", () => {
      it(`should enter 'RecentReportTickets' scene and scene id should be equal to RecentReportTickets`, async () => {
        const ctx = helper.makeMockContext({message: {text: botMessages.GO_TO_RECENT_REPORT_TICKETS}});
        ctx.debug.setTestState({report: {projectId: '23124234121'}});
        ctx.debug.setTestSession({cursor: steps.UNDEFINED});


        await scene.middleware()(ctx);
        const actual = ctx.debug.currentScene;
        const expected = sceneIds.RECENT_REPORT_TICKETS;
        assert.equal(actual, expected);
      });
    });
  });


  it(`Should send '${sceneMessages.REPORT_ALREADY_EXISTS}', if report was already created today on chosen ticket`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const report = await helper.createTestReport(ticket._id, user._id);
    report.ticketNumber = ticket.number;
    report.ticketName = ticket.name;
    const ctx = helper.makeMockContext({message: {chat: {id: 1234, username: user.name },
        from: { username: user.name, id: user.telegramUserId }}
    });
    ctx.debug.setTestState({ticketId: ticket._id});
    ctx.debug.setTestSession({cursor: steps.UNDEFINED});
    await scene.middleware()(ctx);
    const actual = ctx.debug.replies[0].message;
    const expected = sceneMessages.REPORT_ALREADY_EXISTS;
    assert.equal(actual, expected);
  });

  it(`Should send '${sceneMessages.WANT_TO_EDIT_REPORT}', if report was already created today on chosen ticket`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const report = await helper.createTestReport(ticket._id, user._id);
    report.ticketNumber = ticket.number;
    report.ticketName = ticket.name;
    const ctx = helper.makeMockContext({message: {chat: {id: 1234, username: user.name },
        from: { username: user.name, id: user.telegramUserId }}
    });
    ctx.debug.setTestState({ticketId: ticket._id});
    ctx.debug.setTestSession({cursor: steps.UNDEFINED});
    await scene.middleware()(ctx);
    const actual = ctx.debug.replies[1].message;
    const expected = sceneMessages.WANT_TO_EDIT_REPORT;
    assert.equal(actual, expected);
  });

  it(`Should return '${sceneMessages.ENTER_RECENT_REPORTS_SCENE}' button, if report was already created today on chosen ticket`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const report = await helper.createTestReport(ticket._id, user._id);
    report.ticketNumber = ticket.number;
    report.ticketName = ticket.name;
    const ctx = helper.makeMockContext({message: {chat: {id: 1234, username: user.name },
        from: { username: user.name, id: user.telegramUserId }}
    });
    ctx.debug.setTestState({ticketId: ticket._id});
    ctx.debug.setTestSession({cursor: steps.UNDEFINED});
    await scene.middleware()(ctx);
    const actual = ctx.debug.replies[1].extra.reply_markup.inline_keyboard[0][0].text;
    const expected = sceneMessages.ENTER_RECENT_REPORTS_SCENE;
    assert.equal(actual, expected);
  });

  it(`Should enter RecentReports, when ${sceneMessages.ENTER_RECENT_REPORTS_SCENE} button is pressed`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const report = await helper.createTestReport( ticket._id, user._id);
    const pmt = await ProjectManagementTool(project._id, user.telegramUserId).create();

    const data = JSON.stringify({a: actions.ENTER_RECENT_REPORTS_SCENE, id: ticket._id});
    const ctx = helper.makeMockContext({callback_query: {
      from: {chat: {id: 1234, username: user.name }, username: user.name, id: user.telegramUserId}, data }
    });
    ctx.debug.setTestSession({cursor: steps.UNDEFINED});
    ctx.debug.setTestState({
      pmt: pmt,
      report
    });
    await scene.middleware()(ctx);
    const actual = ctx.debug.currentScene;
    const expected = sceneIds.RECENT_REPORTS;
    assert.equal(actual, expected);
  });

  //First or enter step.
  it('Should send info about ticket and to enter description of new report', async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestState({ticketId: ticket._id});
    ctx.debug.setTestSession({cursor: steps.UNDEFINED});

    await scene.middleware()(ctx);

    const expectedFirstMessage = sceneMessages.SHOW_TICKET_INFO(
      ctx.scene.state.report,
      await sceneHelpers.getTicketSpentTime(ticket._id)
    );
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [expectedFirstMessage, sceneMessages.DESCRIPTION]
    );
    assert.deepEqual(
      ctx.debug.getKeyboardsText(),
      [botMessages.GO_BACK_TO_PREVIOUS_SCENE]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [expectedFirstMessage, sceneMessages.DESCRIPTION]
    );
    assert.equal(ctx.scene.session.cursor, steps.THIRD)
  });


  //Second step or user enter description
  it('Should enter to next when I send description', async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: reportData.description,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.THIRD});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });
    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [sceneMessages.TIME_SPENT],
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [sceneMessages.TIME_SPENT],
    );
    assert.equal(ctx.scene.session.cursor, steps.FOURTH)
  });

  it(`Should enter to existing tickets scene if I send Back`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: botMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.SECOND});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.EXISTING_TICKETS);
  });

  //Third step or user enter time spent
  it('Should send to enter ticket status when user send a valid timeSpent', async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const pmt = await ProjectManagementTool(project._id, user.telegramUserId).create();

    const ctx = helper.makeMockContext({
      message: {
        text: reportData.timeSpent,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      pmt,
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [sceneMessages.TICKET_STATUS],
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [sceneMessages.TICKET_STATUS],
    );
    assert.deepEqual(ctx.scene.session.cursor, steps.FIFTH);
  });


  it('should send use numbers if user send non numbers', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const pmt = await ProjectManagementTool(project._id, user.telegramUserId).create();

    const ctx = helper.makeMockContext({
      message: {
        text: 'invalid number',
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      pmt,
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [sceneMessages.USE_NUMBERS],
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [sceneMessages.USE_NUMBERS],
    );
    assert.deepEqual(ctx.scene.session.cursor, steps.FOURTH);
  });


  it('should send to enter lesser number than 12 if I send greater than 12', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const pmt = await ProjectManagementTool(project._id, user.telegramUserId).create();

    const ctx = helper.makeMockContext({
      message: {
        text: 24 + 1,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      pmt,
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);

    assert(
      ctx.debug.replies[0],
      sceneMessages.WRONG_TIME,
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [sceneMessages.WRONG_TIME, sceneMessages.TIME_SPENT_EXCEEDS_LIMIT()],
    );
    assert.deepEqual(ctx.scene.session.cursor, steps.FOURTH);
  });

  
  it('should enter to existing tickets scene when user send Back', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: botMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.THIRD});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.EXISTING_TICKETS);
  });


  //Fourth step when user send ticket status
  it('should ask to send report or not or edit if user send valid ticket status', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: reportData.ticketStatus,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.FIFTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    const firstMessageText = `${sceneMessages.FINAL_REPORT}\n ${sceneHelpers.getReport(ctx.wizard.state.report)}`;

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [firstMessageText, sceneMessages.SEND],
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [firstMessageText, sceneMessages.SEND],
    );
    assert.deepEqual(ctx.scene.session.cursor, steps.SIXTH);
  });


  it('should send invalid ticket status if user send invalid status', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: 'invalid status',
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.FIFTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [sceneMessages.WRONG_STATUS],
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [sceneMessages.WRONG_STATUS]
    );
    assert.deepEqual(ctx.scene.session.cursor, steps.FIFTH);
  });


  it('should enter to existing tickets scene when user send Back', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: botMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.EXISTING_TICKETS);
  });



  //Fifth step when user decide to send or not or edit.
  it('should send that report is created if user send Send', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const pmt = await ProjectManagementTool(project._id, user.telegramUserId).create();

    const ctx = helper.makeMockContext({
      message: {
        text: botMessages.YES_SEND_REPORT,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.SIXTH});
    ctx.debug.setTestState({
      pmt: pmt,
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: [],
      messagesToDeleteAfterScene: []
    });

    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [botMessages.REPORT_CREATED],
    );
    assert.deepEqual(ctx.debug.currentScene, sceneIds.FINAL);
    assert.doesNotHaveAllKeys(ctx.scene.state, 'report')
  });


  it('should send report canceled if user send dont send', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: botMessages.NO_DONT_SEND_REPORT,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.SIXTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: [],
      messagesToDeleteAfterScene: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.FINAL);
  });


  it('should enter to edit scene if user send save and send', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: botMessages.EDIT_CURRENT_REPORT,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.SIXTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: [],
      messagesToDeleteAfterScene: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.RECENT_REPORTS);
  });


  it('should send wrong action if user send something else', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);

    const ctx = helper.makeMockContext({
      message: {
        text: 'something else',
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.SIXTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketId: ticket._id,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: [],
      messagesToDeleteAfterScene: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [sceneMessages.WRONG_ACTION],
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [sceneMessages.WRONG_ACTION]
    );

    assert.deepEqual(ctx.scene.session.cursor, steps.SIXTH);
  });
});