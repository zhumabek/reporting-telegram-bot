const assert = require('chai').assert;
const proxyquire = require('proxyquire');
const sinon = require('sinon');
const mongoose = require('mongoose');

const config = require('../config');

const { PROJECTS, START, CONFIRM_EMAIL } = require('../bot/scenes/sceneIds');
const scene = require('../bot/scenes/ConfirmEmail/confirmEmail');
const messages = require('../bot/scenes/ConfirmEmail/confirmEmailMessages');
const { userData } = require('./utils/dataSet');

const { makeMockContext, createTestUser } = require('./utils/helpers');

mongoose.Promise = global.Promise;



describe('Test confirm email scene', () => {
  beforeEach((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });
  afterEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });


  it('should send message to send confirm code', async function () {
    const user = await createTestUser({...userData, active: false});
    const ctx = makeMockContext({
      message: {
        from: {id: user.telegramUserId, username: user.name}
      }}
    );
    ctx.debug.setTestState({
      email: 'some.email@example.com',
      confirmCode: 1234
    });

    await scene.enterMiddleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.SEND_CONFIRM_CODE]
    );
  });


  it('should send not able to send code again if he send in one minute after last send code', async function () {
    const user = await createTestUser({...userData, active: false});
    const pastConfirmCode = 1234;
    const ctx = makeMockContext({
      message: {
        text: messages.buttons.SEND_CODE_AGAIN,
        from: {id: user.telegramUserId, username: user.name}
      }}
    );
    ctx.debug.setTestState({
      email: 'some.email@example.com',
      confirmCode: pastConfirmCode,
      lastSentEmailAt: new Date().getTime() - 58 * 1000 // -58 seconds
    });
    const sendConfirmationCodeToEmail = sinon.stub();
    const scene = proxyquire('../bot/scenes/ConfirmEmail/confirmEmail', {
      '../utils/helpers': {sendConfirmationCodeToEmail}
    });

    await scene.middleware()(ctx);

    assert.equal(sendConfirmationCodeToEmail.calledOnce, false);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.WE_WONT_SEND_EMAIL_AGAIN]
    );
  });

  it('should send another confirm code if I send to send another confirm code', async function () {
    const user = await createTestUser({...userData, active: false});
    const pastConfirmCode = 1234;
    const ctx = makeMockContext({
      message: {
        text: messages.buttons.SEND_CODE_AGAIN,
        from: {id: user.telegramUserId, username: user.name}
      }}
    );
    ctx.debug.setTestState({
      email: 'some.email@example.com',
      confirmCode: pastConfirmCode,
      lastSentEmailAt: new Date().getTime() - 62000 // -100 second.
    });
    const sendConfirmationCodeToEmail = sinon.stub();
    const scene = proxyquire('../bot/scenes/ConfirmEmail/confirmEmail', {
      '../utils/helpers': {sendConfirmationCodeToEmail}
    });

    await scene.middleware()(ctx);

    assert.equal(sendConfirmationCodeToEmail.calledOnce, true);
    assert.notEqual(ctx.scene.state.confirmCode, pastConfirmCode);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.WE_SENT_CONFIRM_CODE_AGAIN]
    );
  });


  it('should send message about error if exception was raised when send email', async function () {
    const ctx = makeMockContext({
      message: {
        from: {id: 45698565},
        chat: {id: 1234567654},
        text: messages.buttons.SEND_CODE_AGAIN
      }
    });
    ctx.debug.setTestState({
      email: 'some.email@example.com',
      confirmCode: 7899,
      lastSentEmailAt: new Date().getTime() - 62000 // -62 second.
    });
    const sendConfirmationCodeToEmail = sinon.stub().throws();
    const scene = proxyquire('../bot/scenes/ConfirmEmail/confirmEmail', {
      '../utils/helpers': {
        sendConfirmationCodeToEmail,
      }
    });

    await scene.middleware()(ctx);

    assert.equal(sendConfirmationCodeToEmail.calledOnce, true);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.ERROR_WHEN_SEND_EMAIL]
    );
  });


  it('should delete this inactive user and enter to start scene', async function () {
    const ctx = makeMockContext({
      message: {
        from: {id: 45698565},
        chat: {id: 1234567654},
        text: messages.buttons.TRY_ANOTHER_EMAIL_OR_PHONE
      }
    });
    ctx.debug.setTestState({
      email: 'some.email@example.com',
      confirmCode: 7899
    });
    const deleteUserByTelegramUserId = sinon.stub();
    const scene = proxyquire('../bot/scenes/ConfirmEmail/confirmEmail', {
      '../utils/helpers': {
        deleteUserByTelegramUserId,
      }
    });

    await scene.middleware()(ctx);

    assert.equal(deleteUserByTelegramUserId.calledOnce, true);
    assert.equal(ctx.debug.currentScene, START)
  });


  it('should activate user and enter to projects scene if sent confirm code is correct', async function () {
    const confirmCode = 1234;
    const ctx = makeMockContext({
      message: {
        from: {id: 45698565},
        chat: {id: 1234567654},
        text: confirmCode
      }
    });
    ctx.debug.setTestState({
      email: 'some.email@example.com',
      confirmCode: confirmCode
    });
    const activateUser = sinon.stub();
    const scene = proxyquire('../bot/scenes/ConfirmEmail/confirmEmail', {
      './confirmEmailHelpers': {
        activateUser,
      }
    });

    await scene.middleware()(ctx);

    assert.equal(activateUser.calledOnce, true);
    assert.equal(
      ctx.debug.replies[0].extra.reply_markup.remove_keyboard,
      true
    );
    assert.equal(ctx.debug.currentScene, PROJECTS)
  });


  it('should send that code is incorrect if sent code is incorrect', async function () {
    const confirmCode = 1234;
    const incorrectConfirmCode = confirmCode + 1234;
    const ctx = makeMockContext({
      message: {
        from: {id: 45698565},
        chat: {id: 1234567654},
        text: incorrectConfirmCode
      }
    });
    ctx.debug.setTestState({
      email: 'some.email@example.com',
      confirmCode: confirmCode
    });

    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.INCORRECT_CONFIRM_CODE]
    )
  });
});