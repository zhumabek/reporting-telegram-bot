const expect = require('chai').expect;
const assert = require('chai').assert;
const sinon = require('sinon');

const testData = require('./utils/dataSet');
const helperFunc = require('./utils/helpers');
const ProjectManagementTool = require('../repositories/pmt/index');
const { projectTypeEnum }  = require('../models/constants');
const Project = require('../models/Project');
const messages = require('../bot/scenes/Projects/projectMessages');
const scene = require('../bot/scenes/Projects/projects');
const sceneIds = require("../bot/scenes/sceneIds");
const {actionNames, compareByName} = require("../bot/scenes/Projects/projectHelpers");

describe('Bot Projects Scene', function () {
  beforeEach(function (done) {
    helperFunc.connectToDB()
      .then(() => {
        done();
      });
  });

  afterEach(function (done) {
    helperFunc.closeDbConnection().then(() => done());
  });

  it('should return alphabetically sorted projects', async function () {
    const user = await helperFunc.createTestUser();
    await helperFunc.createTestProject({name: 'ZZZ', isClosed: false,
      members: [user._id], chatId: 'exampleGroupId'});
    await helperFunc.createTestProject({name: 'bbb', isClosed: false,
      members: [user._id], chatId: 'exampleGroupId'});
    await helperFunc.createTestProject({name: 'AAA', isClosed: false,
      members: [user._id], chatId: 'exampleGroupId'});
    const projects = await Project.find();
    projects.sort(compareByName);
    const sortedProjectNames = projects.map(project => project.name);
    const sortedNames = ['AAA', 'bbb', 'ZZZ'];
    assert.sameOrderedMembers(sortedProjectNames, sortedNames);
  });

  it(`should reply with "${messages.CHOOSE_PROJECT}" and return not empty projects array, if projects item length greater than 0 `, async function () {
    let initialMessage, projectButtons = null;
    const user = await helperFunc.createTestUser();
    await helperFunc.createTestProject({name: 'exampleName', isClosed: false, members: [user._id], chatId: 'exampleGroupId'})
    const ctx = helperFunc.makeMockContext({ message: {from: { id: user.telegramUserId, username: user.name}}});
    Object.assign(ctx, {
      reply: function (message, extra = undefined) {
          projectButtons = extra;
          initialMessage = message;
        },
      },
    );

    await scene.enterMiddleware()(ctx);
    expect(initialMessage).equal(messages.CHOOSE_PROJECT);
    expect(projectButtons).to.have.deep.include({parse_mode: 'HTML'});
    expect(projectButtons.reply_markup.inline_keyboard).to.be.an('array').that.is.not.empty;
  });

  it(`should reply with "${messages.NO_PROJECTS_YET}" message if no projects were found`, async function () {
    let initialMessage = null;
    const user = await helperFunc.createTestUser();
    const ctx = helperFunc.makeMockContext({ message: {from: { id: user.telegramUserId, username: user.name}}});
    Object.assign(ctx, {
      reply: function (message, extra = undefined) {initialMessage = message},
    });

    sinon.stub(Project, 'find').returns([]);
    await scene.enterMiddleware()(ctx);
    expect(initialMessage).equal(messages.NO_PROJECTS_YET);
    Project.find.restore();
  });


  it(`scene id should  be equal to ${sceneIds.PROJECTS} after clicking goBackToChooseProject button`, async function () {
    const  ctx = helperFunc.makeMockContext({
      message: {text: messages.buttons.BACK}
    });

    await scene.middleware()(ctx);
    expect(ctx.debug.currentScene).equal(sceneIds.PROJECTS);
  });

  describe("when ChooseNextAction was triggered" ,function () {
    it(`should successfully load report action buttons`, async function () {
      const project = await helperFunc.createTestProject();
      const user = await helperFunc.createTestUser();
      const pmt = await ProjectManagementTool(project._id, user.telegramUserId).create();
      const ctx = helperFunc.makeMockContext({
        callback_query: {
          from: {id: user.telegramUserId},
          data: JSON.stringify(
            {a: actionNames.CHOOSE_NEXT_ACTION, p: project._id}
            )
        }
      });
      ctx.debug.setTestState({
        pmt: pmt,
        messagesToDelete: []
      });

      await scene.middleware()(ctx);

      assert.deepEqual(
        ctx.debug.replyMessages(),
        [
          messages.CHOOSE_NEXT_ACTION(project.name, project.reportsSendTime),
          messages.GO_BACK
        ]
      );
      assert.deepEqual(
        ctx.debug.getButtonsText(),
        [
          messages.buttons.NEW_TICKET_REPORT,
          messages.buttons.EXISTING_TICKET_REPORT,
          messages.buttons.EDIT_RECENT_REPORT,
          messages.buttons.NO_TICKET_REPORT
        ])
    });


    it(
      'should send only two option: Report and Edit report if chose project have connected to remote management tool',
      async function () {
        const user = await helperFunc.createTestUser();
        const project = await helperFunc.createTestProject({...testData.projectData, pmtType: projectTypeEnum.JIRA_PROJECT});
        await helperFunc.createTestJiraProject(project._id);
        await helperFunc.createTestJiraUser(user._id);
        const pmt = await ProjectManagementTool(project._id, user.telegramUserId).create();
        const ctx = helperFunc.makeMockContext({
          callback_query: {
            from: {id: user.telegramUserId},
            data: JSON.stringify(
              {a: actionNames.CHOOSE_NEXT_ACTION, p: project._id}
            )
          }
        });
        ctx.debug.setTestState({
          pmt: pmt,
          messagesToDelete: []
        });

        await scene.middleware()(ctx);

        assert.deepEqual(
          ctx.debug.replyMessages(),
          [
            messages.CHOOSE_NEXT_ACTION(project.name, project.reportsSendTime),
            messages.GO_BACK
          ]
        );
        assert.deepEqual(
          ctx.debug.getButtonsText(),
          [
            messages.buttons.WRITE_REPORT,
            messages.buttons.EDIT_RECENT_REPORT,
            messages.buttons.NO_TICKET_REPORT,
          ])
    });
  });

  describe("when CHOOSE_EXISTING_TICKET action was triggered", function () {
    it(`should successfully enter ${sceneIds.EXISTING_TICKETS} scene with valid state`, function (done) {
      const projectId = 123;
      const ctx = helperFunc.makeMockContext({
        callback_query: { data: JSON.stringify({a: actionNames.CHOOSE_EXISTING_TICKET, p: projectId})}
      });

      scene.middleware()(ctx, () => {}).then(() => {
        expect(ctx.debug.currentScene).equal(sceneIds.EXISTING_TICKETS);
        expect(ctx.scene.state).to.deep.include({projectId: projectId});
        done();
      });
    });
  });
  });

  describe("when NEW_TICKET_REPORT action was triggered", function () {
    it(`should successfully enter ${sceneIds.NEW_TICKET_REPORT} scene with valid state`, function (done) {
      const projectId = 123;
      const ctx = helperFunc.makeMockContext({
        callback_query: { data: JSON.stringify({a: actionNames.NEW_TICKET_REPORT, p: projectId})}
      });

      scene.middleware()(ctx, () => {}).then(() => {
        expect(ctx.debug.currentScene).equal(sceneIds.NEW_TICKET_REPORT);
        expect(ctx.scene.state).to.deep.include({projectId: projectId});
        done();
      });
    });

  });

  describe("when EDIT_RECENT_REPORT action was triggered", function () {
    it(`should successfully enter ${sceneIds.RECENT_REPORTS} scene with valid state`, function (done) {
      const projectId = 123;
      const ctx = helperFunc.makeMockContext({
        callback_query: { data: JSON.stringify({a: actionNames.EDIT_RECENT_REPORT, p: projectId})}
      });

      scene.middleware()(ctx, () => {}).then(() => {
        expect(ctx.debug.currentScene).equal(sceneIds.RECENT_REPORT_TICKETS);
        expect(ctx.scene.state).to.deep.include({projectId: projectId});
        done();
      });
    });
});
