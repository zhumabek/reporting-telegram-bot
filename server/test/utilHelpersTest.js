const assert = require('chai').assert;
const expect = require('chai').expect;
const mongoose = require('mongoose');
const config = require('../config');
const Ticket = require('../models/Ticket');
const helpers = require('./utils/helpers');
const sceneIds = require('../bot/scenes/sceneIds');
const Report = require('../models/Report');
const messages = require('../bot/scenes/utils/reportMessages');
const { changeWizardScene, createReport, getSendButtons, getStatusButtons, getBackButton,
  getReport, validTicketStatus, goBack, updateTicketStatus, getTicketSpentTime,
  changeScene, goToBeginning, getNewTicketReportSendButtons } = require('../bot/scenes/utils/helpers');

describe('Testing helpers functions', async function() {
  let user, project, ticket, report;

  before(async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    user = await helpers.createTestUser();
    project = await helpers.createTestProject();
    ticket = await helpers.createTestTicket(user._id, project._id);
    report = await helpers.createTestReport(ticket._id, user._id);
    report.ticketNumber = ticket.number;
    report.ticketName = ticket.name;
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });

  describe('Testing getReport()', function () {
    it('getReport() should return string', function () {
      const result = getReport(report);
      assert.typeOf(result, 'string');
    });
    it('report argument in getReport() should be type of Object', function () {
      assert.isObject(report);
    });
    it('report argument in getReport() should have filled ticketName', function () {
      expect(report.ticketName).to.not.be.empty;
    });
    it('report argument in getReport() should have filled description', function () {
      expect(report.description).to.not.be.empty;
    });
    it('report argument in getReport() should have filled timeSpent', function () {
      assert.isNumber(report.timeSpent);
    });
    it('report argument in getReport() should have filled ticketStatus', function () {
      expect(report.ticketStatus).to.not.be.empty;
    });
  });

  describe('Testing getBackButton()', function () {
    const result = getBackButton();
    it('should return an object', function () {
      assert.isObject(result);
    });
    it('should return an array of buttons', function () {
      assert.isArray(result.reply_markup.keyboard);
    });
    it('should return a button with "Назад" message', function () {
      assert.equal(result.reply_markup.keyboard[[0]], messages.GO_BACK_TO_PREVIOUS_SCENE);
    });
  });

  describe('Testing getStatusButtons', function () {
    const result = getStatusButtons();
    it('should return an object', function () {
      assert.isObject(result);
    });
    it('should return an array of buttons', function () {
      assert.isArray(result.reply_markup.keyboard);
    });
    it(`should return an array of buttons with messages: ${messages.IN_PROGRESS}, ${messages.CLOSED}, ${messages.GO_BACK_TO_PREVIOUS_SCENE}`, function () {
      assert.equal(result.reply_markup.keyboard[[0]], messages.IN_PROGRESS);
      assert.equal(result.reply_markup.keyboard[[1]], messages.CLOSED);
      assert.equal(result.reply_markup.keyboard[[2]], messages.GO_BACK_TO_PREVIOUS_SCENE);
    });
  });

  describe('Testing validTicketStatus', function () {
    const inProgress = validTicketStatus('in progress');
    const closed = validTicketStatus('closed');
    const wrongStatus = validTicketStatus('asdafaw');
    it('should return boolean', function () {
      assert.isBoolean(inProgress);
    });
    it('should return true only if an argument is valid ticketStatus', function () {
      assert.equal(inProgress, true);
      assert.equal(closed, true);
    });
    it('should return false if an argument is invalid ticketStatus', function () {
      assert.equal(wrongStatus, false);
    });
  });

  describe('Testing getSendButtons', function () {
    const result = getSendButtons();
    it('should return an object', function () {
      assert.isObject(result);
    });
    it('should return an array of buttons', function () {
      assert.isArray(result.reply_markup.keyboard);
    });
    it(`should return an array of buttons with messages: ${messages.YES_SEND_REPORT}, ${messages.NO_DONT_SEND_REPORT}, ${messages.EDIT_CURRENT_REPORT}`, function () {
      assert.equal(result.reply_markup.keyboard[[0]], messages.YES_SEND_REPORT);
      assert.equal(result.reply_markup.keyboard[[1]], messages.NO_DONT_SEND_REPORT);
      assert.equal(result.reply_markup.keyboard[[2]], messages.EDIT_CURRENT_REPORT);
    });
  });

  describe('Testing createReport()', function () {
    let result;
    it('should return an object', async function () {
      result = await createReport(report);
      assert.isObject(result);
    });
    it('should return a new report with filled ticketId', function () {
      assert.isNotNull(result.ticketId);
    });
    it('should return a new report with filled userId', function () {
      assert.isNotNull(result.userId);
    });
    it('should return a new report with filled description', function () {
      assert.isDefined(result.description);
    });
    it('should return a new report with filled timeSpent', function () {
      assert.isNotNull(result.timeSpent);
    });
    it('should return a new report with filled ticketStatus', function () {
      assert.isDefined(result.ticketStatus);
    });
    it('should return a new report with filled date', function () {
      assert.isDefined(result.date);
    });
    it('should save a new report to data base', async function () {
      const reportCreated = await Report.findOne({_id: result._id});
      const actual = reportCreated._id;
      const expected = result._id;
      assert.deepEqual(actual, expected);
    });
  });

  describe('Testing getTicketSpentTime()', function () {
    it('should return a number', async function () {
      const actual = await getTicketSpentTime(report.ticketId);
      assert.isNumber(actual);
    });
    it('should return a sum of report timeSpent for a given ticket', async function () {
      const newReport = await helpers.createTestReport(ticket._id, user._id);
      const reports = await Report.find();
      const expected = reports.reduce((acc, report) => {
        return acc + report.timeSpent;
      }, 0);
      const actual = await getTicketSpentTime(newReport.ticketId);
      assert.equal(actual, expected);
    });
  });

  describe('Testing updateTicketStatus()', function () {
    it('should return a ticket, which is an object', async function () {
      const result = await updateTicketStatus(report);
      assert.isObject(result);
    });
    it('should find a ticket and update its status with ticketStatus from a report', async function () {
      await updateTicketStatus(report);
      const changedTicket = await Ticket.findOne({_id: report.ticketId});
      const expected = report.ticketStatus;
      const actual = changedTicket.status;
      assert.equal(actual, expected);
    });
  });

  describe('Testing changeScene()', function () {
    it('should change the scene to given scene', async function () {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name }, from: {username: user.name, id: user.id}}});
      const currentScene = sceneIds.EDIT_REPORT;
      await changeScene(ctx, sceneIds.PROJECTS);
      const changedScene = ctx.debug.currentScene;
      assert.notEqual(currentScene, changedScene);
    });
  });

  describe('Testing changeWizardScene()', function () {
    it('should change the wizard scene to given scene', async function () {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name }, from: {username: user.name, id: user.id}}});
      const currentScene = sceneIds.NEW_TICKET_REPORT;
      await changeWizardScene(ctx, sceneIds.EDIT_REPORT);
      const changedScene = ctx.debug.currentScene;
      assert.notEqual(currentScene, changedScene);
    });
  });

  describe('Testing goToBeginning()', function () {
    it('should start the current scene again', async function () {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name }, from: {username: user.name, id: user.id}}});
      ctx.scene.current.id = sceneIds.EDIT_REPORT;
      const currentScene = ctx.scene.current.id;
      await goToBeginning(ctx);
      const changedScene = ctx.debug.currentScene;
      assert.equal(currentScene, changedScene);
    });
  });

  describe('Testing goBack()', function () {
    it('should restart the current step in a scene', async function () {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name }, from: {username: user.name, id: user.id}}});
      ctx.wizard.step = 5;
      const currentStep = ctx.wizard.step;
      await goBack(ctx);
      const changedStep = ctx.wizard.step;
      assert.equal(currentStep, changedStep);
    });
  });

  describe('Testing getNewTicketReportSendButtons()', function () {
    const result = getNewTicketReportSendButtons();
    it('should return an object', function () {
      assert.isObject(result);
    });
    it('should return an array of buttons', function () {
      assert.isArray(result.reply_markup.keyboard);
    });
    it(`should return an array of buttons with messages: ${messages.YES_SEND_REPORT}, ${messages.NO_DONT_SEND_REPORT}, ${messages.EDIT_NEW_TICKET_REPORT}`, function () {
      assert.equal(result.reply_markup.keyboard[0][0], messages.YES_SEND_REPORT);
      assert.equal(result.reply_markup.keyboard[0][1], messages.NO_DONT_SEND_REPORT);
      assert.equal(result.reply_markup.keyboard[1][0], messages.EDIT_NEW_TICKET_REPORT);
    });
  });
});