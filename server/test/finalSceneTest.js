const assert = require('chai').assert;
const mongoose = require('mongoose');

const config = require('../config');
const scene = require('../bot/scenes/Final/final');
const sceneIds = require('../bot/scenes/sceneIds');
const messages = require('../bot/scenes/Final/finalMessages');
const { actionNames } = require('../bot/scenes/Final/finalHelpers');
const helper = require('./utils/helpers');



describe('Testing existingTickets scene', () => {
  beforeEach((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });
  afterEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });


  it(`Scene id should be ${sceneIds.FINAL}`, () => {
    assert.equal(scene.id, sceneIds.FINAL)
  });


  it(`Scene should send ${messages.WHAT_NEXT} and buttons`, async () => {
    const user = await helper.createTestUser();

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });

    await scene.enterMiddleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.WHAT_NEXT]);
    assert.deepEqual(ctx.debug.getButtonsText(), [messages.buttons.ENTER_CREATE_ANOTHER, messages.buttons.EXIT])
  });


  it(`Should enter to projects scene if I click ${messages.buttons.ENTER_CREATE_ANOTHER}`, async () => {
    const user = await helper.createTestUser();

    const ctx = helper.makeMockContext({
      callback_query: {
        from: {
          chat: {id: 1234, username: user.name },
          username: user.name,
          id: user.telegramUserId
        },
        data: actionNames.ENTER_CREATE_ANOTHER
      }
    });

    await scene.middleware()(ctx);
    assert.equal(ctx.debug.currentScene, sceneIds.PROJECTS);
  });



  it(`Should send ${messages.BYE} than leave from scene if I click ${messages.buttons.EXIT}`, async () => {
    const user = await helper.createTestUser();

    const ctx = helper.makeMockContext({
      callback_query: {
        from: {
          chat: {id: 1234, username: user.name },
          username: user.name,
          id: user.telegramUserId
        },
        data: actionNames.EXIT
      }
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.BYE]);
    assert.equal(ctx.debug.currentScene, '');
  });
});