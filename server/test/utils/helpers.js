const mongoose = require('mongoose');
const config = require('../../config.js');
const Context = require('telegraf/core/context');
const Telegram = require('telegraf/core/network/client');

const {
  userData,
  projectData,
  ticketData,
  reportData,
  jiraProjectData,
} = require('./dataSet');

const User = require('../../models/User');
const Project = require('../../models/Project');
const Ticket = require('../../models/Ticket');
const Report = require('../../models/Report');
const JiraProject = require('../../models/JiraProject');
const JiraUser = require('../../models/JiraUser');

exports.connectToDB = () => {
  return mongoose.connect(config.dbUrl, config.mongoOptions);
};

exports.closeDbConnection = () => {
  return mongoose.connection.dropDatabase().then(() => mongoose.disconnect());
};

exports.makeMockContext = (update = {}, contextExtra = {}) => {
  const tg = new Telegram('', {});
  tg.callApi = (method, data) => {
    console.log(`mocked tg callApi ${method}`, data);
  };

  const ctx = new Context(update, tg, {});
  Object.assign(
    ctx,
    {
      session: {},
      debug: {
        currentScene: '',
        replies: [],
        replyMessages: () => ctx.debug.replies.map(({ message }) => message),
        getMessagesToDeleteTexts: () => ctx.scene.state.messagesToDelete.map(({ text }) => text),
        getButtonsText: () => {
          const buttonsText = [];
          ctx.debug.replies.map(({extra}) => {
            if(extra && extra.reply_markup.inline_keyboard){
              extra.reply_markup.inline_keyboard.map(row => {
                row.forEach(({text}) => buttonsText.push(text));
              });
            }
          });

          return buttonsText;
        },

        getKeyboardsText: () => {
          const keyboardsText = [];
          ctx.debug.replies.map(({extra}) => {
            if(extra && extra.reply_markup.keyboard){
              extra.reply_markup.keyboard.map(row => {
                row.forEach((text) => keyboardsText.push(text))
              });
            }
          });
          return keyboardsText
        },

        setTestState: (state) => {
          ctx.scene.state = state
        },
        setTestSession: (session) => {
          ctx.scene.session = session
        },
        setTestCurrentScene: (sceneId) => {
          ctx.scene.current = {id: sceneId}
        }
      },
    },
    contextExtra,
  );
  ctx.reply = (message, extra = undefined) => {
    ctx.debug.replies.push({ message, extra });
    return {
      message_id: 'itsJustTestingId',
      chat: {
        id: 'TestingIdToo'
      },
      text: message
    }
  };
  ctx.editMessageText = async (message, extra=undefined) =>{
    await ctx.debug.replies.push({ message, extra });
  };
  ctx.scene = {
    enter: async (sceneName, initialState) => {
      ctx.debug.currentScene = sceneName;
      ctx.scene.state = initialState;
      ctx.scene.session = {cursor: 0};
    },
    leave: () => {
      ctx.debug.currentScene = '';
      ctx.scene.state.messagesToDelete = [];
    },
    reenter: async (initialState) => {
      ctx.scene.state = initialState;
      ctx.scene.session = {cursor: 0};
    },
    state: {messagesToDelete: [], messagesToDeleteAfterScene: []},
    current: {id:ctx.debug.currentScene},
  };
  ctx.from = {
    username: ''
  };
  ctx.wizard = {
    step: 0,
    state: {},
    next: () => ctx.wizard.step++,
    back: () => ctx.wizard.step--,
  };

  return ctx;
};

exports.createTestUser = async (data=userData) => {
  const instance = new User(data);
  await instance.save();
  return instance
};

exports.createTestProject = async (data=projectData) => {
  const instance = new Project(data);
  await instance.save();
  return instance
};

exports.createTestJiraProject = async (projectId, data = jiraProjectData) => {
  const jiraProject = new JiraProject({...data, projectId});
  await jiraProject.save();
  return jiraProject;
};

exports.createTestJiraUser = async (userId, identifier='someIdentifier') => {
  const jiraUser = new JiraUser({userId, identifier});
  await jiraUser.save();
  return jiraUser;
};

exports.createTestTicket = async (userId, projectId, data=ticketData) => {
  data.userId = userId;
  data.projectId = projectId;
  const instance = new Ticket(data);
  await instance.save();
  return instance
};

exports.createTestReport = async (ticketId, userId, data=reportData) => {
  data.ticketId = ticketId;
  data.userId = userId;
  const instance = new Report(data);
  await instance.save();
  return instance;
};

exports.getTicketById = async (_id) => {
  return Ticket.findById(_id)
};