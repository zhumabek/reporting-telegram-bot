module.exports = {
  userData: {
    name: 'exampleUsername',
    phoneNumber: '996111111111',
    email: '@example.com',
    telegramUserId: 'someId',
    active: true,
  },
  projectData: {
    name: 'exampleName',
    isClosed: false,
    chatId: 'exampleGroupId',
  },
  jiraProjectData: {
    projectId: null,
    domainName: 'someDomain',
    key: 'JiraSubProjectKey',
    apiAccessEmail: 'accessEmail@gmail.com',
    apiAccessToken: 'accessToken'
  },
  ticketData: {
    projectId: null,
    userId: null,
    number: '12345',
    name: 'ExampleName',
    status: 'in progress'
  },
  reportData: {
    userId: null,
    ticketId: null,
    description: 'Example description',
    timeSpent: 10,
    ticketStatus: 'in progress',
    date: new Date(),
    editDate: 'null',
    isDeleted: false
  },
  dummyReportObject : {
    projectId: '5dce87a6a6fc2731c2947403',
    ticketId: null,
    userId: '5dce87cd38d4e43201bb8c5b',
    ticketNumber: '32',
    ticketName: 'TicketName',
    description: 'Report description',
    timeSpent: '3',
    ticketStatus: 'in progress'
  },
};