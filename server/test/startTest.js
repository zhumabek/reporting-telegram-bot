const assert = require('chai').assert;
const mongoose = require('mongoose');
const proxyquire = require('proxyquire');
const sinon = require('sinon');

const config = require('../config');

const { PROJECTS, START, CONFIRM_EMAIL } = require('../bot/scenes/sceneIds');
const startScene = require('../bot/scenes/Start/start');
const messages = require('../bot/scenes/Start/startMessages');
const { userData } = require('./utils/dataSet');

const { makeMockContext, createTestUser } = require('./utils/helpers');

mongoose.Promise = global.Promise;


describe('Test startScene', () => {
  beforeEach((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });
  afterEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });


  it('Scene id should be equal to "start"', ()=> {
    assert.equal(START, startScene.id);
  });


  it('Scene should enter user to projects scene if user is authenticated and active', async () => {
    const user = await createTestUser();
    const ctx = makeMockContext({
      message: {
        from: {id: user.telegramUserId, username: user.name}
      }});

    await startScene.enterMiddleware()(ctx);
    assert.equal(PROJECTS, ctx.debug.currentScene);
  });


  it('Should send message for help authenticate and keyboards if user unauthorized', async () => {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        }
      }
    });

    await startScene.enterMiddleware()(ctx);
    const actualGetContactKbrdText = ctx.debug.replies[0].extra.reply_markup.keyboard[0][0].text;
    const actualAuthWithEmailKbrdText = ctx.debug.replies[0].extra.reply_markup.keyboard[1][0].text;

    assert.deepEqual(ctx.debug.replyMessages(), [messages.CHOOSE_AUTH_WITH]);
    assert.equal(actualGetContactKbrdText, messages.button.SEND_MY_PHONE);
    assert.equal(actualAuthWithEmailKbrdText, messages.button.AUTH_WITH_EMAIL);

    // First keyboard is requesting contact
    assert.equal(ctx.debug.replies[0].extra.reply_markup.keyboard[0][0].request_contact, true)
  });


  it('should send message for help authenticate and keyboards if user inactive', async function () {
    const user = await createTestUser({...userData, active: false});
    const ctx = makeMockContext({
      message: {
        from: {id: user.telegramUserId, username: user.name}
      }});

    await startScene.enterMiddleware()(ctx);
    const actualGetContactKbrdText = ctx.debug.replies[0].extra.reply_markup.keyboard[0][0].text;
    const actualAuthWithEmailKbrdText = ctx.debug.replies[0].extra.reply_markup.keyboard[1][0].text;

    assert.deepEqual(ctx.debug.replyMessages(), [messages.CHOOSE_AUTH_WITH]);
    assert.equal(actualGetContactKbrdText, messages.button.SEND_MY_PHONE);
    assert.equal(actualAuthWithEmailKbrdText, messages.button.AUTH_WITH_EMAIL);

    // First keyboard is requesting contact
    assert.equal(ctx.debug.replies[0].extra.reply_markup.keyboard[0][0].request_contact, true)
  });


  it('Should enter projects scene if my phone exist in hrm', async ()=>{
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        chat: {
          id: 123456789
        },
        contact: {
          phone_number: userData.phoneNumber,
          first_name: userData.name,
          user_id: not_exist_id
        }
      }
    });
    const getHrmUserByPhone = sinon.stub().returns({
      email: userData.email,
      phoneNumber: userData.phoneNumber,
      name: userData.name
    });
    const scene = proxyquire('../bot/scenes/Start/start', {
      './startHelpers': { getHrmUserByPhone }
    });
    await scene.middleware()(ctx);


    assert.equal(getHrmUserByPhone.calledOnce, true);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.WELCOME]);
    assert.equal(ctx.debug.currentScene, PROJECTS);
  });


  it(`Should send to enter email if I send ${messages.button.AUTH_WITH_EMAIL}`, async () => {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        text: messages.button.AUTH_WITH_EMAIL
      }
    });

    await startScene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.ENTER_YOUR_EMAIL]);
  });


  it('Should sent activation code to user email if email found in hrm', async ()=> {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        chat: {
          id: 1234567654
        },
        text: userData.email
      }
    });
    const sendConfirmationCodeToEmail = sinon.stub();
    const getHrmUserByEmail = sinon.stub().returns({
      email: userData.email,
      phoneNumber: userData.phoneNumber,
      name: userData.name
    });
    const scene = proxyquire('../bot/scenes/Start/start', {
      './startHelpers': {getHrmUserByEmail},
      '../utils/helpers': {sendConfirmationCodeToEmail}
    });


    await scene.middleware()(ctx);

    assert.equal(getHrmUserByEmail.calledOnce, true);
    assert.equal(sendConfirmationCodeToEmail.calledOnce, true);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.WE_SENT_MAIL_TO_YOUR_EMAIL]);
  });

  it('Should enter to confirm email scene if email found in hrm', async ()=> {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        chat: {
          id: 1234567654
        },
        text: userData.email
      }
    });
    const sendConfirmationCodeToEmail = sinon.stub();
    const getHrmUserByEmail = sinon.stub().returns({
      email: userData.email,
      phoneNumber: userData.phoneNumber,
      name: userData.name
    });
    const scene = proxyquire('../bot/scenes/Start/start', {
      './startHelpers': {getHrmUserByEmail, },
      '../utils/helpers': {sendConfirmationCodeToEmail}
    });


    await scene.middleware()(ctx);

    assert.equal(ctx.debug.currentScene, CONFIRM_EMAIL);
  });


  it('should send message about error if exception was raised when send email', async function () {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        chat: {
          id: 1234567654
        },
        text: userData.email
      }
    });
    const sendConfirmationCodeToEmail = sinon.stub().throws();
    const getHrmUserByEmail = sinon.stub().returns({
      email: userData.email,
      phoneNumber: userData.phoneNumber,
      name: userData.name
    });
    const deleteUserByTelegramUserId = sinon.stub();
    const scene = proxyquire('../bot/scenes/Start/start', {
      './startHelpers': {getHrmUserByEmail},
      '../utils/helpers': {
        sendConfirmationCodeToEmail,
        deleteUserByTelegramUserId
      }
    });


    await scene.middleware()(ctx);

    assert.equal(getHrmUserByEmail.calledOnce, true);
    assert.equal(sendConfirmationCodeToEmail.calledOnce, true);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.ERROR_WHEN_SEMD_EMAIL]
      );
  });


  it('should try to delete user by telegram user id if exception was raised when send email', async function () {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        chat: {
          id: 1234567654
        },
        text: userData.email
      }
    });
    const sendConfirmationCodeToEmail = sinon.stub().throws();
    const getHrmUserByEmail = sinon.stub().returns({
      email: userData.email,
      phoneNumber: userData.phoneNumber,
      name: userData.name
    });
    const deleteUserByTelegramUserId = sinon.stub();
    const scene = proxyquire('../bot/scenes/Start/start', {
      './startHelpers': {getHrmUserByEmail},
      '../utils/helpers': {
        sendConfirmationCodeToEmail,
        deleteUserByTelegramUserId
      }
    });


    await scene.middleware()(ctx);

    assert.equal(getHrmUserByEmail.calledOnce, true);
    assert.equal(sendConfirmationCodeToEmail.calledOnce, true);
    assert.equal(deleteUserByTelegramUserId.calledOnce, true)
  });


  it(`Should send ${messages.CANT_FIND_ACTIVE_USER_BY_NUMBER} if user not found with phone in hrm`, async () => {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        contact: {
          phone_number: userData.phoneNumber,
          first_name: userData.name,
          user_id: not_exist_id
        }
      }
    });
    const getHrmUserByPhone = sinon.stub().returns(null);
    const scene = proxyquire('../bot/scenes/Start/start', {
      './startHelpers': { getHrmUserByPhone }
    });
    await scene.middleware()(ctx);


    assert.equal(getHrmUserByPhone.calledOnce, true);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.CANT_FIND_ACTIVE_USER_BY_NUMBER]);
  });


  it('should send user with this phone already exist if I send already exist contact', async function () {
    const user = await createTestUser({...userData, active: false});
    const ctx = makeMockContext({
      message: {
        from: {
          id: user.telegramUserId
        },
        contact: {
          phone_number: user.phoneNumber,
          first_name: user.name,
          user_id: user.telegramUserId
        }
      }
    });
    await startScene.middleware()(ctx);

    assert.deepEqual(ctx.debug.replyMessages(), [messages.USER_WITH_THIS_NUMBER_ALREADY_EXIST]);
  });


  it('should send user with this email already exist if I send already exist email', async function () {
    const user = await createTestUser({...userData, active: false});
    const ctx = makeMockContext({
      message: {
        from: {
          id: user.telegramUserId
        },
        chat: {
          id: 1234567654
        },
        text: user.email
      }
    });

    await startScene.middleware()(ctx);

    assert.deepEqual(ctx.debug.replyMessages(), [messages.USER_WITH_THIS_EMAIL_ALREADY_EXIST]);
  });


  it(`should send about successfully exit if send ${messages.button.EXIT}`, async function () {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        text: messages.button.EXIT
      }
    });
    const deleteUserByTelegramUserId = sinon.stub();
    const scene = proxyquire('../bot/scenes/Start/start', {
      '../utils/helpers': {deleteUserByTelegramUserId}

    });

    await scene.middleware()(ctx);

    assert.equal(deleteUserByTelegramUserId.calledOnce, true);
    assert.equal(ctx.scene.current.id, '');
    assert.deepEqual(ctx.debug.replyMessages(), [messages.SUCCESS_EXITED]);
  });


  it(`should reenter scene if I send /start`, async function () {
    const not_exist_id = '1234567890-';
    const ctx = makeMockContext({
      message: {
        from: {
          id: not_exist_id
        },
        text: '/start'
      }
    });
    ctx.scene.current.id = START;
    await startScene.middleware()(ctx);

    assert.equal(ctx.scene.current.id, START);
    assert.deepEqual(ctx.debug.replyMessages(), ['_']);
    assert.equal(ctx.debug.replies[0].extra.reply_markup.remove_keyboard, true)
  });
});
