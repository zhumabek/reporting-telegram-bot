const expect = require('chai').expect;
const assert = require('chai').assert;
const sinon = require('sinon');
const Scene = require('../bot/scenes/EditNewTicketReport/editNewTicketReport');
const helperFunc = require('./utils/helpers');
const Ticket = require('../models/Ticket');
const editNewTicketReportMessages = require('../bot/scenes/EditNewTicketReport/editNewTicketReportMessages');
const {NO_TICKET_REPORT_START_WORD} = require('../bot/scenes/NoTicketReport/noTicketReportHelpers');
const reportMessages = require('../bot/scenes/utils/reportMessages');
const sceneIds = require("../bot/scenes/sceneIds");
const {dummyReportObject} = require('./utils/dataSet');
const {actions} = require("../bot/scenes/EditNewTicketReport/editNewTicketReportHelpers");
const {getReport} = require("../bot/scenes/utils/helpers");

describe('EditNewTicketReport scene tests', function () {
  beforeEach(function (done) {
    helperFunc.connectToDB()
      .then(() => {
        done();
      });
  });

  afterEach(function (done) {
    helperFunc.closeDbConnection().then(() => done());
  });


  describe( 'After entering EditNewTicketReport scene', function() {
    it(`should reply with '${sceneIds.EDIT_NEW_TICKET_REPORT}' message and should return array of sendButtons`, async function () {
      const ctx = helperFunc.makeMockContext();
      ctx.scene.state.report = dummyReportObject;

      await Scene.enterMiddleware()(ctx);
      let msgToTest = ctx.debug.replies[0].message;
      let sendButtons = ctx.debug.replies[0].extra.reply_markup.keyboard;
      expect(msgToTest).equal(editNewTicketReportMessages.WELCOME);
      expect(sendButtons).to.be.an('array').that.is.not.empty;
      expect(sendButtons.length).equal(2);
    });

    it(`should reply with message that contains report information and should return to allow edit all properties`, async function () {
      const ctx = helperFunc.makeMockContext();
      ctx.scene.state.report = dummyReportObject;

      await Scene.enterMiddleware()(ctx);
      let msgToTest = ctx.debug.replies[1].message;
      let editFieldButtons = ctx.debug.replies[1].extra.reply_markup.inline_keyboard;
      expect(msgToTest).to.include(editNewTicketReportMessages.FINAL_REPORT);
      expect(msgToTest).to.include(getReport(dummyReportObject));
      assert.deepEqual(
        ctx.debug.getButtonsText(),
        [
          editNewTicketReportMessages.EDIT_TICKET_NUMBER,
          editNewTicketReportMessages.EDIT_TICKET_NAME,
          editNewTicketReportMessages.EDIT_TIME_SPENT,
          editNewTicketReportMessages.EDIT_TICKET_STATUS,
          editNewTicketReportMessages.EDIT_DESCRIPTION
        ]
      );
      expect(editFieldButtons.length).equal(5);
    });

    it('should allow to edit only description and spent time', async function () {
      const ctx = helperFunc.makeMockContext();
      ctx.debug.setTestState({
        report: {
          ...dummyReportObject,
          ticketNumber: NO_TICKET_REPORT_START_WORD + '1'
        }
      });

      await Scene.enterMiddleware()(ctx);

      assert.deepEqual(
        ctx.debug.getButtonsText(),
        [
          editNewTicketReportMessages.EDIT_DESCRIPTION,
          editNewTicketReportMessages.EDIT_TIME_SPENT
        ]
      );
    });
  });
  describe( 'If "GO_BACK_TO_PREVIOUS_SCENE" button was clicked', function() {
    it(`scene id should be equal to '${sceneIds.NEW_TICKET_REPORT}'`, async function () {
      const ctx = helperFunc.makeMockContext({message: {text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE}});

      await Scene.middleware()(ctx);
      expect(ctx.debug.currentScene).equal(sceneIds.NEW_TICKET_REPORT);
    });
  });

  describe( 'If "YES_SEND_REPORT" button was clicked', function() {
    it(`should successfully create new ticket and save new report`, async function () {
      const ctx = helperFunc.makeMockContext({message: {text: reportMessages.YES_SEND_REPORT}});
      ctx.scene.state.report = dummyReportObject;

      await Scene.middleware()(ctx);
      let msgToTest = ctx.debug.replies[0].message;
      expect(msgToTest).equal(reportMessages.REPORT_CREATED);
    });

    it(`should reply with ${reportMessages.COULD_NOT_SAVE_REPORT} message if failed to save report`, async function () {
      const ctx = helperFunc.makeMockContext({message: {text: reportMessages.YES_SEND_REPORT}});
      ctx.scene.state.report = dummyReportObject;

      sinon.stub(Ticket,'create').throws(new Error('Failed'));

      await Scene.middleware()(ctx);
      let msgToTest = ctx.debug.replies[0].message;
      expect(msgToTest).equal(reportMessages.COULD_NOT_SAVE_REPORT);
      Ticket.create.restore();
    });

    it(`after successfully saving report should enter to ${sceneIds.FINAL} scene`, async function () {
      const ctx = helperFunc.makeMockContext({message: {text: reportMessages.YES_SEND_REPORT}});
      ctx.scene.state.report = dummyReportObject;

      await Scene.middleware()(ctx);
      expect(ctx.debug.currentScene).equal(sceneIds.FINAL);
    });
  });

  describe( 'If "NO_DONT_SEND_REPORT" button was clicked', function() {
    it(`should reply with '${reportMessages.REPORT_CANCELED}' message`, async function () {
      const ctx = helperFunc.makeMockContext({message: {text: reportMessages.NO_DONT_SEND_REPORT}});
      ctx.scene.state.report = dummyReportObject;

      await Scene.middleware()(ctx);
      let msgToTest = ctx.debug.replies[0].message;
      expect(msgToTest).equal(reportMessages.REPORT_CANCELED);
    });

    it(`should enter to ${sceneIds.FINAL} scene`, async function () {
      const ctx = helperFunc.makeMockContext({message: {text: reportMessages.NO_DONT_SEND_REPORT}});
      ctx.scene.state.report = dummyReportObject;

      await Scene.middleware()(ctx);
      expect(ctx.debug.currentScene).equal(sceneIds.FINAL);
    });
  });

  describe("when CHOOSE_FIELD action was triggered" ,function () {
    it(`should enter to '${sceneIds.EDIT_NEW_TICKET_REPORT_FIELD}' scene with valid state params`, async function () {
      const fieldName = 'tNumber';
      const ctx = helperFunc.makeMockContext({
        callback_query: {data: JSON.stringify({a: actions.CHOOSE_FIELD, f: fieldName})}
      });

      await Scene.middleware()(ctx);
      expect(ctx.debug.currentScene).equal(sceneIds.EDIT_NEW_TICKET_REPORT_FIELD);
      expect(ctx.scene.state).to.deep.include({field: fieldName});
    });
  });

});