
const assert = require('chai').assert;
const mongoose = require('mongoose');

const config = require('../config');
const scene = require('../bot/scenes/NewTicketReport/NewTicketReport');
const sceneIds = require('../bot/scenes/sceneIds');
const messages = require('../bot/scenes/NewTicketReport/NewTicketReportMessages');
const reportMessages = require('../bot/scenes/utils/reportMessages');
const {getReport}  = require("../bot/scenes/utils/helpers");

const { ticketData, reportData } = require('./utils/dataSet');
const helper = require('./utils/helpers');


const steps = {
  UNDEFINED: null,
  FIRST: 0,
  SECOND: 1,
  THIRD: 2,
  FOURTH: 3,
  FIFTH: 4,
  SIXTH: 5,
  SEVENTH: 6,
  EIGHTH: 7,
  NINTH: 8,
};


describe('Testing newTicketReport scene', () => {
  before((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });

  after(async () => {
    await mongoose.disconnect();
  });

  afterEach(async () => {
    await mongoose.connection.dropDatabase();
  });

  describe("Initial(0) step",() => {

    describe("If sum of all report's spent time for to day reached the limit of 12 hours", () => {

      it(`should reply with '${reportMessages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT}' message`, async () => {
        const user = await helper.createTestUser();
        const project = await helper.createTestProject();
        const ticket = await helper.createTestTicket(user._id, project._id);
        const reportData = {
          userId: null,
          ticketId: null,
          description: 'Example description',
          timeSpent: 12,
          ticketStatus: 'in progress',
          date: new Date(),
          editDate: 'null',
          isDeleted: false
        };
        const report = await helper.createTestReport(ticket._id, user._id, reportData);
        report.ticketNumber = ticket.number;
        report.ticketName = ticket.name;
        const ctx = helper.makeMockContext({message: {chat: {id: 1234, username: user.name },
            from: { username: user.name, id: user.telegramUserId }}
        });
        ctx.debug.setTestState({projectId: project._id});
        ctx.debug.setTestSession({cursor: steps.FIRST});


        await scene.middleware()(ctx);
        const actual = ctx.debug.replies[0].message;
        const expected = reportMessages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT;
        assert.equal(actual, expected);
      });

      it(`should reply with 'Go back' and 'Go to edit existing reports' buttons`, async () => {
        const user = await helper.createTestUser();
        const project = await helper.createTestProject();
        const ticket = await helper.createTestTicket(user._id, project._id);
        const reportData = {
          userId: null,
          ticketId: null,
          description: 'Example description',
          timeSpent: 12,
          ticketStatus: 'in progress',
          date: new Date(),
          editDate: 'null',
          isDeleted: false
        };
        const report = await helper.createTestReport(ticket._id, user._id, reportData);
        report.ticketNumber = ticket.number;
        report.ticketName = ticket.name;
        const ctx = helper.makeMockContext({
          message: {
            chat: {id: 1234, username: user.name },
            from: { username: user.name, id: user.telegramUserId }
          }});
        ctx.debug.setTestState({projectId: project._id});
        ctx.debug.setTestSession({cursor: steps.FIRST});


        await scene.middleware()(ctx);
        const actual = ctx.debug.replies[0].extra.reply_markup.keyboard;
        const expected = [[reportMessages.GO_TO_RECENT_REPORT_TICKETS],[reportMessages.GO_BACK_TO_PREVIOUS_SCENE]];
        assert.deepEqual(actual, expected);
      });
    });

    describe("If button 'Перейти в редактирование отчетов' was clicked", () => {
      it(`should enter 'RecentReportTickets' scene and scene id should be equal to RecentReportTickets`, async () => {
        const ctx = helper.makeMockContext({message: {text: reportMessages.GO_TO_RECENT_REPORT_TICKETS}});
        ctx.debug.setTestState({report: {projectId: '23124234121'}});
        ctx.debug.setTestSession({cursor: steps.UNDEFINED});


        await scene.middleware()(ctx);
        const actual = ctx.debug.currentScene;
        const expected = sceneIds.RECENT_REPORT_TICKETS;
        assert.equal(actual, expected);
      });
    });
  });



  it(`Scene id should be ${sceneIds.NEW_TICKET_REPORT}`, async ()=> {
    assert.equal(scene.id, sceneIds.NEW_TICKET_REPORT)
  });


  // First step
  it(`Should send ${messages.TICKET_NUMBER}`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        },
      },
    });
    ctx.debug.setTestSession({cursor: steps.SECOND});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.PROJECT_NAME(project.name), messages.TICKET_NUMBER]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.PROJECT_NAME(project.name), messages.TICKET_NUMBER]
    );
    assert.equal(ctx.scene.state.messagesToDelete[1].text, messages.TICKET_NUMBER);

    assert.equal(ctx.scene.session.cursor, steps.THIRD);
  });


  //Second step
  it(`Should send ${messages.TICKET_NAME} when user send valid ticket number.`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: ticketData.number,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.THIRD});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.TICKET_NAME]);
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.TICKET_NAME]
    );
    assert.equal(ctx.scene.session.cursor, steps.FIFTH);
  });



  it(`Should enter to projects scene if I send Back`, async () => {
    const user = await helper.createTestUser();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.SECOND});

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.PROJECTS);
  });



  it(`Should send ${messages.EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER} when user send already exist ticket number.`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ticket = await helper.createTestTicket(user._id, project._id);
    const existingTicketNumber = ticket.number;

    const ctx = helper.makeMockContext({
      message: {
        text: existingTicketNumber,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.THIRD});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(
      ctx.debug.replyMessages(),
      [messages.EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER, messages.NEXT_ACTION]
    );
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [messages.EXISTING_TICKET_NUMBER_AND_BELONGS_TO_USER, messages.NEXT_ACTION]
    );
    assert.equal(ctx.scene.session.cursor, steps.FOURTH);
  });

  //Third step
  it(`Should enter to projects scene if I send Back`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.THIRD});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.PROJECTS);
  });


  it(`Should enter to tickets scene if send ${messages.GO_TO_EXISTING_TICKETS}`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: messages.GO_TO_EXISTING_TICKETS,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.EXISTING_TICKETS);
  });



  it(`Should enter to beginning of scene if send ${messages.ENTER_TICKET_NUMBER}`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: messages.ENTER_TICKET_NUMBER,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.equal(ctx.debug.currentScene, sceneIds.NEW_TICKET_REPORT);
  });


  it(`Should send wrong action if I send anything else`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: 'Any wrong message',
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.WRONG_ACTION]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.WRONG_ACTION]);
    assert.equal(ctx.scene.session.cursor, steps.FOURTH)
  });



  //Fourth scene
  it(`Should enter to projects scene if I send Back`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.FOURTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.PROJECTS);
  });


  it(`Should send to ${messages.DESCRIPTION} when i send ticket name`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: ticketData.name,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.FIFTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.DESCRIPTION]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.DESCRIPTION]);
    assert.equal(ctx.scene.session.cursor, steps.SIXTH)
  });


  //Fifth scene
  it(`Should enter to projects scene if user send Back`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.FIFTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.PROJECTS);
  });


  it(`Should send to send spent time if I send report description`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportData.description,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.SIXTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.TIME_SPENT]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.TIME_SPENT]);
    assert.equal(ctx.scene.session.cursor, steps.SEVENTH)
  });


  //Sixth scene
  it(`Should enter to projects scene if user send Back`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.SIXTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.PROJECTS);
  });


  it(`Should send to use numbers if i send with non numbers`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: 'it is not numbers',
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.SEVENTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.USE_NUMBERS]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.USE_NUMBERS]);
    assert.equal(ctx.scene.session.cursor, steps.SEVENTH)
  });


  it(`Should send ${messages.WRONG_TIME}, if send more than 12`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: 25,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.SEVENTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.WRONG_TIME, messages.TIME_SPENT_EXCEEDS_LIMIT()]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.WRONG_TIME, messages.TIME_SPENT_EXCEEDS_LIMIT()]);
    assert.equal(ctx.scene.session.cursor, steps.SEVENTH)
  });


  it(`Should send ${messages.TICKET_STATUS}, if send more than valid time`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportData.timeSpent,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.SEVENTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.TICKET_STATUS]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.TICKET_STATUS]);
    assert.equal(ctx.scene.session.cursor, steps.EIGHTH)
  });


  //Seventh step
  it(`Should enter to projects scene if user send Back`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.GO_BACK_TO_PREVIOUS_SCENE,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.SEVENTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.currentScene, sceneIds.PROJECTS);
  });


  it(`Should send to enter valid status if I send another that "in progress" or "closed"`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: 'invalid status',
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.EIGHTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.deepEqual(ctx.debug.replyMessages(), [messages.WRONG_STATUS]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.WRONG_STATUS]);
    assert.equal(ctx.scene.session.cursor, steps.EIGHTH)
  });


  it(`Should send ready report and ${messages.NEXT_ACTION} with choices.`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportData.ticketStatus,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.EIGHTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);

    const expectedReadyMessage = `${messages.FINAL_REPORT} ${await getReport(ctx.scene.state.report)}`;
    const actualKeyboardTexts = [];
    actualKeyboardTexts.push(ctx.debug.replies[1].extra.reply_markup.keyboard[0][0]);
    actualKeyboardTexts.push(ctx.debug.replies[1].extra.reply_markup.keyboard[0][1]);
    actualKeyboardTexts.push(ctx.debug.replies[1].extra.reply_markup.keyboard[1][0]);

    assert.deepEqual(ctx.debug.replyMessages(), [expectedReadyMessage, messages.SEND]);
    assert.deepEqual(
      ctx.debug.getMessagesToDeleteTexts(),
      [expectedReadyMessage, messages.SEND]);

    assert.deepEqual(
      actualKeyboardTexts,
      [
        reportMessages.YES_SEND_REPORT,
        reportMessages.NO_DONT_SEND_REPORT,
        reportMessages.EDIT_NEW_TICKET_REPORT,
      ]);

    assert.equal(ctx.scene.session.cursor, steps.NINTH)
  });

  //Eight step
  it(`Should send report doesnt saved if send${reportMessages.NO_DONT_SEND_REPORT}`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.NO_DONT_SEND_REPORT,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.NINTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.equal(ctx.debug.currentScene, sceneIds.FINAL);
  });



  it(`Should send that report saved if send ${reportMessages.YES_SEND_REPORT}`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.YES_SEND_REPORT,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.NINTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);
    assert.equal(ctx.debug.replyMessages()[0], reportMessages.REPORT_CREATED);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [reportMessages.REPORT_CREATED]);
    assert.equal(ctx.debug.currentScene, sceneIds.FINAL);
  });



  it(`Should enter to start if send ${reportMessages.EDIT_NEW_TICKET_REPORT}`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: reportMessages.EDIT_NEW_TICKET_REPORT,
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.NINTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });

    await scene.middleware()(ctx);

    assert.equal(ctx.debug.currentScene, sceneIds.EDIT_NEW_TICKET_REPORT);
  });


  it(`Should wrong action if I send another`, async () => {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();

    const ctx = helper.makeMockContext({
      message: {
        text: 'another word',
        chat: {id: 1234, username: user.name },
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestCurrentScene(sceneIds.NEW_TICKET_REPORT);
    ctx.debug.setTestSession({cursor: steps.NINTH});
    ctx.debug.setTestState({
      projectId: project._id,
      report: {
        ...reportData,
        ticketNumber: ticketData.number,
        ticketName: ticketData.name,
        userId: user._id,
        projectId: project._id,
      },

      messagesToDelete: []
    });
    await scene.middleware()(ctx);

    assert.deepEqual(ctx.debug.replyMessages(), [messages.WRONG_ACTION]);
    assert.deepEqual(ctx.debug.getMessagesToDeleteTexts(), [messages.WRONG_ACTION]);

  });

});
