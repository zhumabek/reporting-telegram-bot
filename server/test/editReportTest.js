const assert = require('chai').assert;
const mongoose = require('mongoose');
const helpers = require('./utils/helpers');
const { reportData, ticketData } = require('./utils/dataSet');
const config = require('../config');

const sceneIds = require('../bot/scenes/sceneIds');
const scene = require('../bot/scenes/EditReport/editReport');
const messages = require('../bot/scenes/EditReport/editReportsMessages');
const commonMessages = require('../bot/scenes/utils/reportMessages');
const {NO_TICKET_REPORT_START_WORD} = require('../bot/scenes/NoTicketReport/noTicketReportHelpers');
const Report = require('../models/Report');
const { actions } = require('../bot/scenes/EditReport/editReportsHelper');


describe('Testing EditReport scene', () => {
  let user, project, ticket, report;

  before(async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    user = await helpers.createTestUser();
    project = await helpers.createTestProject();
    ticket = await helpers.createTestTicket(user._id, project._id);
    report = await helpers.createTestReport(ticket._id, user._id);
    report.ticketNumber = ticket.number;
    report.ticketName = ticket.name;
  });

  after(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });

  it(`Scene name should be ${sceneIds.RECENT_REPORTS}`, () => {
    assert.equal(sceneIds.RECENT_REPORTS, scene.id);
  });

  it('should send buttons to choose edit what', async function () {
    const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
        from: {username: user.name, id: user.telegramUserId}}});
    ctx.debug.setTestState({
      ticketId: ticket._id,
      report
    });

    await scene.enterMiddleware()(ctx);

    assert.deepEqual(
      ctx.debug.getButtonsText(),
      [
        messages.EDIT_DESCRIPTION,
        messages.EDIT_TIME_SPENT,
        messages.EDIT_TICKET_STATUS
      ]
    )
  });

  it('should allow edit only description and time spent if it additional report', async function () {
    const additionalTicket = await helpers.createTestTicket(user._id, project._id, {
      ...ticketData,
      number: NO_TICKET_REPORT_START_WORD + '1'
    });
    const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
        from: {username: user.name, id: user.telegramUserId}}});
    ctx.debug.setTestState({
      ticketId: additionalTicket._id,
      report: {
        ...report,
        ticketId: additionalTicket._id,
        ticketNumber: NO_TICKET_REPORT_START_WORD + '1',
      }
    });

    await scene.enterMiddleware()(ctx);

    assert.deepEqual(
      ctx.debug.getButtonsText(),
      [
        messages.EDIT_DESCRIPTION,
        messages.EDIT_TIME_SPENT,
      ]
    )
  });


  describe('Testing a case, when a user comes from RecentReportTickets', () => {
    it('Bot should show all reports written today', async () => {
      const ctx = helpers.makeMockContext({message: {chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.scene.state.ticketId = ticket._id;
      ctx.scene.state.report = report;
      await scene.enterMiddleware()(ctx);
      let today = new Date().setHours(0, 0, 0, 0);
      const actual = ctx.debug.replies.length - 1;
      const reports = await Report.find({userId: user._id, ticketId: ticket.id, date: {$gt: today}});
      const expected = reports.length;
      assert.equal(actual, expected);
    });
  });

  describe('Testing back button', () => {
    it('Bot should return to ExistingTickets, if ctx.scene.state.fromExistingTicketReport = true', async () => {
      const ctx = helpers.makeMockContext({message: {text: 'Назад', chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.scene.state.fromExistingTicketReport = true;
      ctx.scene.state.report = {...report, projectId: project._id};
      ctx.scene.state.reportId = report._id;
      await scene.middleware()(ctx);
      const actual = ctx.debug.currentScene;
      const expected = sceneIds.EXISTING_TICKETS;
      assert.equal(actual, expected);
    });
    it('Bot should return to RecentReportTickets, if ctx.scene.state.fromExistingTicketReport = false', async () => {
      const ctx = helpers.makeMockContext({message: {text: 'Назад', chat: {id: 1234, username: user.name },
          from: {username: user.name, id: user.telegramUserId}}});
      ctx.scene.state.fromExistingTicketReport = false;
      ctx.scene.state.report = {...report, projectId: project._id};
      ctx.scene.state.ticketId = ticket._id;
      await scene.middleware()(ctx);
      const actual = ctx.debug.currentScene;
      const expected = sceneIds.RECENT_REPORT_TICKETS;
      assert.equal(actual, expected);
    });
  });

  it('When a editField is pressed, scene should change to EditReportField', async () => {
    const data = JSON.stringify({action: actions.CHOOSE_REPORT});
    const ctx = helpers.makeMockContext({callback_query: {from: {chat: {id: 1234, username: user.name }, username: user.name,
          id: user.telegramUserId}, data}});
    await scene.middleware()(ctx);
    const actual = ctx.debug.currentScene;
    const expected = sceneIds.EDIT_REPORT;
    assert.equal(actual, expected);
  });

  it(`should update report and ticket when I send ${commonMessages.YES_SEND_REPORT}`, async function () {
    const user = await helpers.createTestUser();
    const project = await helpers.createTestUser();
    const ticket = await helpers.createTestTicket(user._id, project._id, {ticketStatus: 'closed', ...ticketData});
    const report = await helpers.createTestReport(ticket._id, user._id);
    const ctx = helpers.makeMockContext({
      message: {text: commonMessages.YES_SEND_REPORT, chat: {id: 1234, username: user.name },

        from: {username: user.name, id: user.telegramUserId}}
    });
    ctx.debug.setTestState({
      fromExistingTicketReportEdit: false,
      messagesToDelete: [],
      projectId: project._id,
      report: {
        _id: report._id,
        ticketStatus: 'in progress',
        projectId: project._id,
        ticketId: ticket._id,
        ...reportData
      }

    });

    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [
        commonMessages.REPORT_UPDATED
      ]
    );
    assert.equal(ctx.debug.currentScene, sceneIds.FINAL);
    assert.notEqual(
      ticket.status,
      helpers.getTicketById(ticket._id).status
    )
  });

  it('should create report if user came from report for existing tickets', async function () {
    const user = await helpers.createTestUser();
    const project = await helpers.createTestUser();
    const ticket = await helpers.createTestTicket(user._id, project._id, {ticketStatus: 'closed', ...ticketData});
    const ctx = helpers.makeMockContext({
      message: {text: commonMessages.YES_SEND_REPORT, chat: {id: 1234, username: user.name },

        from: {username: user.name, id: user.telegramUserId}}
    });
    ctx.debug.setTestState({
      fromExistingTicketReportEdit: true,
      messagesToDelete: [],
      projectId: project._id,
      report: {
        ticketStatus: 'in progress',
        projectId: project._id,
        ticketId: ticket._id,
        ...reportData
      }

    });

    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [
        commonMessages.REPORT_CREATED
      ]
    );
    assert.equal(ctx.debug.currentScene, sceneIds.FINAL);
    assert.notEqual(
      ticket.status,
      helpers.getTicketById(ticket._id).status
    )
  });

  it('should send that changes didnt saved if I send Not save', async function () {
    const user = await helpers.createTestUser();
    const project = await helpers.createTestUser();
    const ticket = await helpers.createTestTicket(user._id, project._id, {ticketStatus: 'closed', ...ticketData});
    const ctx = helpers.makeMockContext({
      message: {text: commonMessages.NO_DONT_SEND_REPORT, chat: {id: 1234, username: user.name },

        from: {username: user.name, id: user.telegramUserId}}
    });
    ctx.debug.setTestState({
      fromExistingTicketReportEdit: true,
      messagesToDelete: [],
      projectId: project._id,
      report: {
        ticketStatus: 'in progress',
        projectId: project._id,
        ticketId: ticket._id,
        ...reportData
      }

    });

    await scene.middleware()(ctx);

    assert.deepEqual(
      ctx.debug.replyMessages(),
      [
        commonMessages.UPDATE_REPORT_CANCELED
      ]
    );
    assert.equal(ctx.debug.currentScene, sceneIds.FINAL);
  });
});