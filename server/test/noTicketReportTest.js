const assert = require('chai').assert;
const mongoose = require('mongoose');

const config = require('../config');
const scene = require('../bot/scenes/NoTicketReport/noTicketReport');
const sceneIds = require('../bot/scenes/sceneIds');
const messages = require('../bot/scenes/NoTicketReport/noTicketReportMessages');
const {
  NO_TICKET_REPORT_START_WORD,
  getReportPreview

} = require('../bot/scenes/NoTicketReport/noTicketReportHelpers');

const helper = require('./utils/helpers');
const { reportData, ticketData } = require('./utils/dataSet');
const steps = {
  UNDEFINED: null,
  FIRST: 0,
  SECOND: 1,
  THIRD: 2,
  FOURTH: 3,
  FIFTH: 4,
  SIXTH: 5,
  SEVENTH: 6,
  EIGHTH: 7,
  NINTH: 8,
};


describe('Testing existingTickets scene', () => {
  beforeEach((done) => {
    mongoose.connect(config.dbUrl, config.mongoOptions)
      .then(() => {
        done();
      })
  });
  afterEach(async () => {
    await mongoose.connection.dropDatabase();
    await mongoose.disconnect();
  });


  describe('Testing first step', () => {
    it('should not allow to write report if I already have wrote reports with time spent sum > 12', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ticket = await helper.createTestTicket(user._id, project._id);
      const report = await helper.createTestReport(ticket._id, user._id, {
        ...reportData,
        timeSpent: 12,
        date: new Date()
      });
      const ctx = helper.makeMockContext({
        message: {
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestState({projectId: project._id});
      ctx.debug.setTestSession({current: steps.UNDEFINED});

      await scene.middleware()(ctx);

      assert.deepEqual(
        ctx.debug.replyMessages(),
        [
          messages.TOTAL_SPENT_TIME_REACHED_THE_LIMIT
        ]
      );
      assert.deepEqual(ctx.debug.getKeyboardsText(), [messages.BACK]);
    });

    it('should send project info and to enter report with back keyboard', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestState({projectId: project._id});
      ctx.debug.setTestSession({current: steps.UNDEFINED});

      await scene.middleware()(ctx);

      assert.deepEqual(
        ctx.debug.replyMessages(),
        [
          messages.PROJECT_INFO(project),
          messages.ENTER_REPORT_PAYLOAD
        ]
      );
      assert.deepEqual(
        ctx.debug.getKeyboardsText(),
        [
          messages.BACK
        ]
      )
    });


    it('should enter to next step', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestState({projectId: project._id});
      ctx.debug.setTestSession({current: steps.UNDEFINED});

      await scene.middleware()(ctx);

      assert.equal(ctx.scene.session.cursor, steps.SECOND)
    });
  });


  describe('Testing second step, user enter his report', () => {
    it('should send to enter spent time and enter to next step', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: 'some description of report',
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.SECOND});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: reportData,
        ticket: ticketData
      });

      await scene.middleware()(ctx);

      assert.deepEqual(ctx.debug.replyMessages(), [ messages.ENTER_TIME_SPENT ]);
      assert.equal(ctx.scene.session.cursor, steps.THIRD);
    });
  });


  describe('Testing third step: user enter spent time for ticket', function () {
    it('should send to use number if I sent not number text', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: 'some not number text',
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.THIRD});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: reportData,
        ticket: ticketData
      });

      await scene.middleware()(ctx);

      assert.deepEqual(ctx.debug.replyMessages(), [ messages.USE_NUMBERS ]);
      assert.equal(ctx.scene.session.cursor, steps.THIRD);
    });

    it('should send to use number greater than 0 if I sent 0', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: '0',
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.THIRD});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: reportData,
        ticket: ticketData
      });

      await scene.middleware()(ctx);

      assert.deepEqual(ctx.debug.replyMessages(), [ messages.MUST_BE_NON_ZERO_VALUE ]);
      assert.equal(ctx.scene.session.cursor, steps.THIRD);
    });

    it('should send time exited from 12 for today reports', async function () {
      const first_report_time_spent = 9;
      const new_report_time_spent = 3.1; // 9 + 3.1 > 12
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ticket = await helper.createTestTicket(user._id, project._id);
      const report = await helper.createTestReport(ticket._id, user._id, {
        ...reportData,
        timeSpent: first_report_time_spent,
        date: new Date()
      });
      const ctx = helper.makeMockContext({
        message: {
          text: String(new_report_time_spent),
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.THIRD});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: reportData,
        ticket: ticketData
      });

      await scene.middleware()(ctx);

      assert.deepEqual(
        ctx.debug.replyMessages(),
        [
          messages.WRONG_TIME,
          messages.TIME_SPENT_EXCEEDS_LIMIT(first_report_time_spent)
        ]
      );
      assert.equal(ctx.scene.session.cursor, steps.THIRD);
    });

    it('should send report preview and keyboards to choose next action', async function () {
      const valid_time = 12;
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: String(valid_time),
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.THIRD});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: reportData,
        ticket: ticketData
      });

      await scene.middleware()(ctx);

      assert.deepEqual(
        ctx.debug.replyMessages(),
        [
          getReportPreview(ticketData, {...reportData, timeSpent: String(valid_time)})
        ]
      );
      assert.equal(ctx.scene.session.cursor, steps.FOURTH);
    });
  });


  describe('Testing final scene', () => {
    it('should save report if say save!', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: messages.SAVE_KEYBOARD.SAVE_REPORT,
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.FOURTH});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: {
          ...reportData,
          userId: user._id
        },
        ticket: {
          ...ticketData,
          userId: user._id,
          projectId: project._id
        }
      });

      await scene.middleware()(ctx);

      assert.deepEqual(ctx.debug.replyMessages(), [messages.REPORT_SAVED]);
      assert.equal(ctx.debug.currentScene, sceneIds.FINAL)
    });

    it('should discard report if I say discard', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: messages.SAVE_KEYBOARD.CANCEL_REPORT,
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.FOURTH});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        reportData,
        ticketData
      });

      await scene.middleware()(ctx);

      assert.deepEqual(ctx.debug.replyMessages(), [messages.REPORT_DISCARDED]);
      assert.equal(ctx.debug.currentScene, sceneIds.FINAL)
    });

    it('should enter to edit new ticket report if I say edit report', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: messages.SAVE_KEYBOARD.EDIT_REPORT,
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.FOURTH});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: reportData,
        ticket: ticketData
      });

      await scene.middleware()(ctx);

      assert.equal(ctx.debug.currentScene, sceneIds.EDIT_NEW_TICKET_REPORT)
    });

    it('should send wrong action if I send another message not from keyboard', async function () {
      const user = await helper.createTestUser();
      const project = await helper.createTestProject();
      const ctx = helper.makeMockContext({
        message: {
          text: 'Wrong action',
          chat: {id: 1234, username: user.name},
          from: {
            username: user.name,
            id: user.telegramUserId
          }
        }
      });
      ctx.debug.setTestSession({cursor: steps.FOURTH});
      ctx.debug.setTestState({
        messagesToDelete: [],
        projectId: project._id,
        report: reportData,
        ticket: ticketData
      });

      await scene.middleware()(ctx);

      assert.deepEqual(ctx.debug.replyMessages(), [messages.WRONG_ACTION]);
    });
  });

  it('should enter to project scene if I send Back', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ctx = helper.makeMockContext({
      message: {
        text: messages.BACK,
        chat: {id: 1234, username: user.name},
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.UNDEFINED});
    ctx.debug.setTestState({
      messagesToDelete: [],
      projectId: project._id,
      report: reportData,
      ticket: ticketData
    });

    await scene.middleware()(ctx);

    assert.equal(ctx.debug.currentScene, sceneIds.PROJECTS)
  });

  it('should enter to start scene if enter /start', async function () {
    const user = await helper.createTestUser();
    const project = await helper.createTestProject();
    const ctx = helper.makeMockContext({
      message: {
        text: '/start',
        chat: {id: 1234, username: user.name},
        from: {
          username: user.name,
          id: user.telegramUserId
        }
      }
    });
    ctx.debug.setTestSession({cursor: steps.UNDEFINED});
    ctx.debug.setTestState({
      messagesToDelete: [],
      projectId: project._id,
      report: reportData,
      ticket: ticketData
    });

    await scene.middleware()(ctx);

    assert.equal(ctx.debug.currentScene, sceneIds.START)
  });
});