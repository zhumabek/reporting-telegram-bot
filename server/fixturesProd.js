const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.deleteMany();
  }

  await User.create(
    {
      phoneNumber: '+996550672597',
      email: 'info@attractor-software.com',
      name: 'SuperAdmin',
      role: 'admin',
      active: true,
      password: 'Admin597Super',
      token: nanoid(),
      telegramUserId: nanoid(10)
    },
  );

  await connection.close();
};

run().catch(error => {
  console.error('Something went wrong', error);
});


