const config = require('./config.js');
const express = require('express');
const cors = require('cors');
const axios = require('axios');
const mongoose = require('mongoose');
const logger = require('./logger');
const bot = require('./bot/bot');
const jobs = require('./bot/tasks');

const userRoutes = require('./app/routes/userRoutes');
const projectRoutes = require('./app/routes/projectRoutes');
const ticketRoutes = require('./app/routes/ticketRoutes');
const reportRoutes = require('./app/routes/reportRoutes');
const botRoutes = require('./app/routes/botRoutes');

let port = 8000;

if (process.env.NODE_ENV === 'test') {
  port = 8010;
}

const app = express();
console.log('### Bot token:', config.telegram.bot_token, ' ###');
mongoose.connect(config.dbUrl, config.mongoOptions).then(async () => {
  if (config.debug){
    await axios.get(`https://api.telegram.org/bot${config.telegram.bot_token}/deleteWebhook`);
    bot.startPolling();
    logger.debug(undefined, 'Bot started in long polling mode!')

  } else {
    await bot.telegram.setWebhook(`https://${config.hostName}/api/${config.telegram.bot_token}`);
    app.use(await bot.webhookCallback(`/api/${config.telegram.bot_token}`));
    logger.debug(undefined, 'Bot started in webhook mode!');
  }

  jobs.sendReportsToProjectChats(bot);
  jobs.notifyToReportForProject(bot);
  jobs.notifyToReportForProjectManager(bot);
  jobs.notifyToReportTicketStatusForMoreThanTwentyHours(bot);
  jobs.notifyToReportTicketStatusForMoreThanSevenDays(bot);
  jobs.notifyToReport(bot);

  app.use(express.json());
  app.use(express.static('public'));
  app.use(cors());

  app.use('/api/users', userRoutes);
  app.use('/api/projects', projectRoutes);
  app.use('/api/tickets', ticketRoutes);
  app.use('/api/reports', reportRoutes);
  app.use('/api/bot', botRoutes);

  app.use((error, req, res, next) => {
    console.log(error);
    const status = error.statusCode || 500;
    const message = error.message;
    const data = error.data;
    res.status(status).json({message: message, data: data});
  });


  app.listen(port, () => {
    logger.debug(undefined, `Server started on ${port} port`);
  });
});