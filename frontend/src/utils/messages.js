import {NotificationManager} from 'react-notifications';

export function showSuccess(message) {
  NotificationManager.success(message);
}

export function showError(message) {
  NotificationManager.error(message);
}

export function showInfo(message) {
  NotificationManager.info(message);
}

export function showWarning(message) {
  NotificationManager.warning(message);
}

