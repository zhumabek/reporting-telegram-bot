import moment from 'moment'

export const FORMAT_DATE_DB = 'YYYY-MM-DD HH:mm:ss';
export const FORMAT_DATE = 'DD.MM.YYYY';
export const DATE_PICKER_DATE_FORMAT = 'dd.MM.yyyy';
export const DATE_PICKER_PLACEHOLDER = 'дд.мм.гггг';
export const TIME_PICKER_MIN_TIME = moment().hour(12).toDate();
export const TIME_PICKER_MAX_TIME = moment().hour(23).toDate();

export const BASIC_PROJECT_ENUM = 0;
export const JIRA_PROJECT_ENUM = 1;

export const BASIC_PROJECT_ENUM_DESCRIPTION = 'Обычный проект';
export const JIRA_PROJECT_ENUM_DESCRIPTION = 'Проект с JIRA';

export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const formatDate = (date, format = FORMAT_DATE) => {
    return moment(date).format(format);
};

export const formatTime = (date, format) => {
    return moment(date).format(format);
};

export const convertFromHourToDate = (hour) => {
    return moment(hour, 'LT').toDate();
};

