import axios from 'axios';
import constans from './constans';
import store from "../store/configureStore";
import {showError} from "./messages";

const instance = axios.create({
  baseURL: constans.apiURL
});

instance.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = store.getState().users.user.token;
  } catch (e) {
    // do nothing, user is not logged in
  }

  return config;
});

instance.interceptors.response.use(
  (response) => { return response },
  (error) => {
    if(error.response && error.response.data.message){
      showError(error.response.data.message);
    }
    return Promise.reject(error);
  });


export default instance;