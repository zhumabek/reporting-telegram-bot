const constans = {
  apiURL: 'https://reporting-bot.dev.attractor-software.com/api'
};

switch(process.env.REACT_APP_ENV) {
  case 'demo':
    constans.apiURL = 'https://reporting-bot.dev.attractor-software.com/api';
    break;
  case 'prod':
    constans.apiURL = 'https://reporting-bot.attractor-software.com/api';
    break;
  case 'staging':
    constans.apiURL = 'https://reporting-bot.staging.attractor-software.com/api';
    break;
  case 'localhost':
    constans.apiURL = 'http://localhost:8000/api';
    break;
  case 'test':
    constans.apiURL = 'http://localhost:8010/api';
    break;
  default:
}

export default constans;