import React from 'react';
import Login from "./containers/Login/Login";
import {Redirect, Route, Switch} from 'react-router-dom';
import Projects from "./containers/Projects/Projects";
import AddProject from "./containers/Projects/AddProject";
import Tickets from "./containers/Tickets/Tickets";
import UsersPage from "./containers/UsersPage/UsersPage";
import Reports from "./containers/Reports/Reports";
import AddReport from "./containers/Reports/AddReport";
import AddUsersToProject from "./containers/Projects/AddProjectMembers";

const ProtectedRoute = ({isAllowed, ...props}) => {
  return isAllowed ? <Route {...props} /> : <Redirect to="/login"/>
};

const Routes = ({user}) => {
  return (
    <Switch>
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager')}
        path="/"
        exact
        component={Projects}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager')}
        path="/project"
        exact
        component={AddProject}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager')}
        path="/project/:id"
        exact
        component={AddProject}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager')}
        path="/project/:id/members"
        exact
        component={AddUsersToProject}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager') || (user && user.role === 'user')}
        path="/tickets"
        exact
        component={Tickets}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager')}
        path="/users"
        exact
        component={UsersPage}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager') || (user && user.role === 'user')}
        path="/reports"
        exact
        component={Reports}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager')}
        path="/report"
        exact
        component={AddReport}
      />
      <ProtectedRoute
        isAllowed={(user && user.role === 'admin') || (user && user.role === 'project manager')}
        path="/report/:id"
        exact
        component={AddReport}
      />
      <Route exact path="/login" component={Login}/>
    </Switch>
  );
};

export default Routes;