import React, {Component, Fragment} from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

export class ConfirmButton extends Component {

  state = {
    isOpen: false,
  };

  toggle =  () => {
    this.setState({isOpen: !this.state.isOpen});
  };

  onConfirm = () => {
    this.props.onConfirm();
    this.toggle();
  };

  render() {
      let props = {...this.props};
      delete props.onConfirm;
      return (
          <Fragment>
            {this.state.isOpen ? (
              <Modal isOpen={this.state.isOpen} toggle={this.toggle}>
                <ModalHeader toggle={this.toggle}/>
                <ModalBody>
                  <div className='text-center'>
                    <span className='fa fa-exclamation-circle pb-3' style={{fontSize: '100px', color: 'orange'}}/>
                    <h3>{this.props.title}</h3>
                  </div>
                </ModalBody>
                <ModalFooter className='d-flex justify-content-center'>
                  <Button color="primary" size='lg' onClick={this.onConfirm}>Да</Button>{' '}
                  <Button color="secondary" size='lg' onClick={this.toggle}>Отмена</Button>
                </ModalFooter>
              </Modal>
              ) : null }

              <Button onClick={this.toggle} {...props}>
                {this.props.children}
            </Button>
          </Fragment>
      )
    }
}