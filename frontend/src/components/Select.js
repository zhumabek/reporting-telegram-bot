import React from 'react';
import RSelect from 'react-select';

export default function Select(props) {
  let {value, options, onChange, placeholder,busy, isMulti, labelKey, valueKey, ...rest} = props;
  return (
    <RSelect
      value={value}
      getOptionLabel={(option) => option[labelKey]}
      getOptionValue={(option) => option[valueKey]}
      noResultsText="пусто"
      isLoading={busy}
      isMulti={isMulti}
      options={options || []}
      placeholder={placeholder}
      onChange={onChange}
      {...rest}/>
  );
}




