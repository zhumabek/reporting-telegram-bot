import React, {Fragment} from 'react';
import {Col, FormFeedback, FormGroup, Input, Label} from "reactstrap";

export default function ProjectForm(props) {
  return (
    <Fragment>
      <FormGroup row>
        <Col>
          <Input
            type={props.type}
            name={props.inputName}
            placeholder={props.placeholder}
            required
            value={props.value}
            onChange={props.changeInput}
          />
        </Col>
      </FormGroup>
    </Fragment>
    );
}

export function FormGroupInput(props) {

  const {label, children, feedback, labelSize, inputSize, className} = props;

  return (
    <FormGroup row className={className}>
      <Label xl={labelSize}>
        {label}
      </Label>
      <Col xl={inputSize}>
        {children}
        <FormFeedback>{feedback}</FormFeedback>
      </Col>
    </FormGroup>
  );
}
