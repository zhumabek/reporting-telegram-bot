import React from 'react';
import DPicker,{registerLocale} from "react-datepicker";
import moment from "moment";
import ru from 'date-fns/locale/ru';
import {InputGroup} from "reactstrap";
import {DATE_PICKER_DATE_FORMAT, DATE_PICKER_PLACEHOLDER} from "../utils/helpers";

registerLocale("ru", ru);

export default function DatePicker(props) {

  const {minDate, maxDate, disabled, placeholder, value, onChange, ...rest} = props;
  let val;
  if(value)
    val = moment(value).toDate();

  return (
    <InputGroup>
      <DPicker selected={val} className='form-control'
               onChange={onChange}
               todayButton={"сегодня"}
               placeholderText={placeholder || DATE_PICKER_PLACEHOLDER}
               dateFormat={DATE_PICKER_DATE_FORMAT}
               minDate={minDate}
               maxDate={maxDate}
               peekNextMonth
               showMonthDropdown
               showYearDropdown
               dropdownMode="select"
               locale='ru'
               disabled={disabled || false}
               popperModifiers={{
                 flip: { behavior: ['bottom'] },
                 preventOverflow: { enabled: false },
                 hide: { enabled: false }
               }}
               {...rest}
      />
    </InputGroup>

  );
}
