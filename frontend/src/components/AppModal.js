import React from 'react'
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

const AppModal = (props) =>  {
  const newProps = {...props};
  delete newProps.onConfirm;
  return (
      <Modal isOpen={props.showModal} toggle={props.toggle} {...newProps}>
        <ModalHeader toggle={props.toggle}/>
        <ModalBody>
          {props.children}
        </ModalBody>
        <ModalFooter className='d-flex justify-content-center'>
          <Button color="primary" size='lg'
                  disabled={props.canConfirm}
                  onClick={() => props.onConfirm()}>
            Да
          </Button>{' '}
          <Button color="secondary" size='lg' onClick={props.toggle}>Отмена</Button>
        </ModalFooter>
      </Modal>
  )
};

export default AppModal;