import React, {Component} from 'react';
import ReactTable from 'react-table'

const translations = {
  previousText: '<',
  nextText: '>',
  loadingText: 'ЗАГРУЗКА...',
  rowsText: 'строк',
  pageText: 'стр.',
  ofText: 'из',
  noDataText: 'нет данных',
};

export default class AppTable extends Component {

  filterMethod(filter, row) {
    if (filter.id === 'index') {
      return String(row._index + 1).includes(filter.value)
    } else {
      return String(row[filter.id]).toLocaleLowerCase().includes(filter.value.toLocaleLowerCase())
    }
  }

  render() {
    const {data, columns, pageSize, onClick, busy, showRowNumbers, ...rest} = this.props;
    let c = [];
    if (showRowNumbers) {
      c.push({
        Header: "№",
        accessor: "index",
        Cell: row => <div className="text-center">{row.index + 1}</div>, width: 70
      });
    }

    c.push(...columns);
    return <ReactTable
      {...translations}
      filterable
      defaultFilterMethod={this.filterMethod}
      data={data}
      columns={c}
      loading={busy}
      placeholder={'выбор'}
      defaultPageSize={pageSize || 25}
      className="-striped -highlight"
      {...rest}
    />
  }
}
