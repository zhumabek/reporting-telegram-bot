import React, {Fragment} from 'react';
import {Button, Navbar, NavbarBrand, NavbarToggler, UncontrolledCollapse,} from 'reactstrap';
import {NavLink} from 'react-router-dom';
import NavMenu from './NavMenu';

import '../../assets/css/Button.css';

const Header = ({user, logout}) => {
  return (
    <Fragment>
      <Navbar color="light" light fixed="top" expand="md" className='shadow-sm'>
        {user && user.role === 'user'
          ? <NavbarBrand tag={NavLink} to="/tickets" className="py-0" style={{fontWeight: 'bold'}}>
              Bot-Reporting
            </NavbarBrand>
          : <NavbarBrand tag={NavLink} to="/" className="py-0" style={{fontWeight: 'bold'}}>
              Bot-Reporting
            </NavbarBrand>}

        <NavbarToggler id="main-nav-toggler"/>
        {user ? (
          <UncontrolledCollapse navbar toggler="#main-nav-toggler" className={'justify-content-between'}>

            <NavMenu user={user} className="align-items-center mr-auto"/>
            <h6>Привет, {user.name}!</h6>
            <Button className='bot-btn-st' color={'primary'} onClick={logout}>
              <i className="fa fa-fw fa-user"/>Выйти
            </Button>
          </UncontrolledCollapse>
        ) : null}
      </Navbar>
    </Fragment>
  );

};

export default Header;