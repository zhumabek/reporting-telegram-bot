import React, {Fragment} from 'react';
import {Nav, NavItem, NavLink as NavLinkRS} from 'reactstrap';
import {NavLink} from "react-router-dom";

import '../../assets/css/Button.css';

const NavMenu = ({user}) => {
  return (
    <Nav navbar>
      {user.role === 'user'
        ? <Fragment>
          <NavItem className="px-2 text-center">
            <NavLinkRS to={'/tickets'} tag={NavLink} exact className="p-1 bot-btn-st">Тикеты</NavLinkRS>
          </NavItem>
          <NavItem className="px-2 text-center">
            <NavLinkRS to={'/reports'} tag={NavLink} exact className="p-1 bot-btn-st">Отчеты</NavLinkRS>
          </NavItem>
        </Fragment>
        : (user.role === 'admin' || user.role === 'project manager')
          ? <Fragment>
            <NavItem className="px-2 text-center">
              <NavLinkRS to={'/'} tag={NavLink} exact className="p-1 bot-btn-st">Проекты</NavLinkRS>
            </NavItem>
            <NavItem className="px-2 text-center">
              <NavLinkRS to={'/users'} tag={NavLink} exact className="p-1 bot-btn-st">Пользователи</NavLinkRS>
            </NavItem>
            <NavItem className="px-2 text-center">
              <NavLinkRS to={'/tickets'} tag={NavLink} exact className="p-1 bot-btn-st">Тикеты</NavLinkRS>
            </NavItem>
            <NavItem className="px-2 text-center">
              <NavLinkRS to={'/reports'} tag={NavLink} exact className="p-1 bot-btn-st">Отчеты</NavLinkRS>
            </NavItem>
          </Fragment>
          : null
      }
    </Nav>
  )
};

export default NavMenu;
