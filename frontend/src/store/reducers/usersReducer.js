import * as actionTypes from '../actions/usersActions';

const initialState = {
  loginError: null,
  user: null,
  users: [],
  allUsers: [],
  allActiveUsers: [],
};
const userReducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.LOGIN_USER_SUCCESS:
      return {...state, user: action.user, loginError: null};
    case actionTypes.LOGIN_USER_FAILURE:
      return {...state, loginError: action.error};
    case actionTypes.LOGOUT_USER:
      return {...state, user: null};
    case actionTypes.GET_USERS:
      return {...state, users: action.users};
    case actionTypes.FETCH_USERS_SUCCESS:
      return {...state, allUsers: action.allUsers};
    case actionTypes.GET_ACTIVE_USERS:
      return {...state, allActiveUsers: action.allActiveUsers};
    default:
      return state;
  }
};

export default userReducer;