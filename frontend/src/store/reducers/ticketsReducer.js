import * as actionTypes from '../actions/ticketsActions';

const initialState = {
  singleProjectTickets: [],
  ticketById: [],
  ticketNumber: null,
};
const ticketReducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.GET_TICKET_BY_ID_PROJECT_SUCCESS:
      return {...state, singleProjectTickets: action.data};
    case actionTypes.GET_TICKET_NUMBER_BY_ID_PROJECT_SUCCESS:
      return {...state, ticketNumber: action.ticketNumber};
    case actionTypes.RESET_SINGLE_PROJECT_TICKETS:
      return {...state, singleProjectTickets: []};
    default:
      return state;
  }
};

export default ticketReducer;