import * as actionTypes from '../actions/reportsActions';
import {updateObject} from "../../utils/helpers";

const initialState = {
  reports: [],
  report: [],
  projectMembers: [],
  reportItem: null
};

const reportReducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.GET_REPORTS_BY_PROJECT:
      return updateObject(state, {reports: action.reports});
    case actionTypes.GET_REPORTS_BY_TICKET:
      return updateObject(state,{report: action.report});
    case actionTypes.GET_REPORT_BY_ID:
      return updateObject(state,{reportItem: action.reportItem});
    case actionTypes.PREVIEW_REPORTS_BY_PROJECT:
      return updateObject(state, {reports: action.reports});
    case actionTypes.PREVIEW_REPORTS_BY_USER:
      return updateObject(state, {reports: action.reports});
    case actionTypes.SET_PROJECT_MEMBERS:
      return updateObject(state,{projectMembers: action.projectMembers});
    case actionTypes.RESET_REPORTS:
      return updateObject(state,{...state, reports: []});
    default:
      return state;
  }
};

export default reportReducer;