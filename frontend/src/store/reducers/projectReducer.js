import * as actionTypes from '../actions/projectActions';
import {updateObject} from "../../utils/helpers";

const initialState = {
  projects: [],
  projectById: null,
  notMemberUsers: [],
  addedUsers: [],
  jiraUsers: [],
  notNotification: [],
};

const resetProjectMembers = (state) => {
  return updateObject(state, {addedUsers: []})
};

const getProjects = (state, action) => {
  return updateObject(state, {projects: action.projects})
};

const setProjectMembers = (state, action) => {
  return updateObject(state, {addedUsers: action.members})
};

const getNotMemberUsers = (state, action) => {
  return updateObject(state, {notMemberUsers: action.notMemberUsers})
};

const setProjectMembersNotNotification = (state, action) => {
  return updateObject(state, {notNotification: action.membersNotNotification})
};

const addUserToTheProject = (state, action) => {
  const updatedAddedUsers = [action.user, ...state.addedUsers];
  return updateObject(state, {addedUsers: updatedAddedUsers});
};

const addUserToTheProjectNotNotification = (state, action) => {
  const updatedUsersNotNotification = [action.user, ...state.notNotification];
  return updateObject(state, {notNotification: updatedUsersNotNotification});
};

const projectReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_PROJECTS:
      return getProjects(state, action);
    case actionTypes.ADD_USER_TO_THE_PROJECT:
      return addUserToTheProject(state, action);
    case actionTypes.ADD_USER_TO_THE_PROJECT_NOT_NOTIFICATION:
      return addUserToTheProjectNotNotification(state, action);
    case actionTypes.SET_PROJECT_MEMBERS:
      return setProjectMembers(state, action);
    case actionTypes.GET_NOT_MEMBER_USERS:
      return getNotMemberUsers(state, action);
    case actionTypes.SET_PROJECT_MEMBERS_NOT_NOTIFICATION:
      return setProjectMembersNotNotification(state, action);
    case actionTypes.RESET_PROJECT_MEMBERS:
      return resetProjectMembers(state);
    case actionTypes.GET_PROJECT_BY_ID_SUCCESS:
      return updateObject(state,{projectById: action.data});
    case actionTypes.SET_JIRA_USERS:
      return updateObject(state, {jiraUsers: action.jiraUsers});
    default:
      return state;
  }
};

export default projectReducer;
