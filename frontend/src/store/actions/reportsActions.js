import axios from '../../utils/axios-api';
import FileSaver from 'file-saver';
import {showSuccess} from "../../utils/messages";
import {push} from 'connected-react-router';
import moment from "moment";

export const RESET_REPORTS = 'RESET_REPORTS';
export const GET_REPORTS_BY_PROJECT = 'GET_REPORTS_BY_PROJECT';
export const GET_REPORTS_BY_TICKET = 'GET_REPORTS_BY_TICKET';
export const PREVIEW_REPORTS_BY_PROJECT = 'PREVIEW_REPORTS_BY_PROJECT';
export const PREVIEW_REPORTS_BY_USER = 'PREVIEW_REPORTS_BY_USER';
export const SAVE_REPORTS_BY_PROJECT = 'SAVE_REPORTS_BY_PROJECT';
export const SET_PROJECT_MEMBERS = 'SET_PROJECT_MEMBERS';
export const GET_REPORT_BY_ID = 'GET_REPORT_BY_ID';

const previewReportsByProjectSuccess = reports => ({type: PREVIEW_REPORTS_BY_PROJECT, reports: reports});
const previewReportsByUserSuccess = reports => ({type: PREVIEW_REPORTS_BY_USER, reports: reports});
const saveReportsSuccess = reports => ({type: SAVE_REPORTS_BY_PROJECT, reports: reports});
const getReportsSuccess = reports => ({type: GET_REPORTS_BY_PROJECT, reports: reports});
const getReportsByTicketSuccess = report => ({type: GET_REPORTS_BY_TICKET, report});
export const resetReports = () => ({type: RESET_REPORTS});
const setProjectMembers = members => ({type: SET_PROJECT_MEMBERS, projectMembers: members});
const getReportItem = reportItem => ({type: GET_REPORT_BY_ID, reportItem});

export const getReportsByProject = id => {
  return dispatch => {
    return axios.get(`/reports/${id}`)
      .then(response => {
      dispatch(getReportsSuccess(response.data.reports));
      showSuccess(response.data.message);
      })
      .catch(err => console.log(err));
  }
};

export const getReportsByUser = id => {
  return dispatch => {
    return axios.get(`/reports/user/${id}`)
      .then(response => {
      dispatch(getReportsSuccess(response.data.reports));
      showSuccess(response.data.message);
      })
      .catch(err => console.log(err));
  }
};

export const getReportsByTicket = id => {
  return dispatch => {
    return axios.get(`/reports/ticket/${id}`)
      .then(response => {
      dispatch(getReportsByTicketSuccess(response.data.reports));
      showSuccess(response.data.message);
      })
      .catch(err => console.log(err));
  }
};

export const previewReportsByProject = (id, from, before) => {
  return dispatch => {
    return axios.get(`/reports/viewReportsByProject/${id}&dateFrom=${from}&dateBefore=${before}`)
      .then(response => {
        dispatch(previewReportsByProjectSuccess(response.data.reports));
        showSuccess(response.data.message);
      }).catch(err => {
        console.log(err);
        dispatch(resetReports());
      });
  }
};
export const previewReportsByUser = (id, from, before) => {
  return dispatch => {
    return axios.get(`/reports/viewReportsByUser/${id}&dateFrom=${from}&dateBefore=${before}`)
      .then(response => {
        dispatch(previewReportsByUserSuccess(response.data.reports));
        showSuccess(response.data.message);
      }).catch(err => {
        console.log(err);
        dispatch(resetReports());
      });
  }
};

export const saveReportsByProject = (id, from, before, project) => {
  return dispatch => {
    return axios.get(`/reports/reportsByProject/${id}&dateFrom=${from}&dateBefore=${before}`, {responseType: "blob"})
      .then(response => {
        dispatch(saveReportsSuccess(response.data));
        showSuccess('Файл был успешно сохранен');
        const blob = new Blob([response.data], {type: 'vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8'});
        FileSaver.saveAs(blob, `invoice project ${project} ${moment(from).format('L')} and ${moment(before).format('L')}.xlsx`);
    }).catch(err => {
      console.log(err);
      dispatch(resetReports());
      });
  }
};

export const saveReportsByUser = (id, from, before, user) => {
  return dispatch => {
    return axios.get(`/reports/reportsByUser/${id}&dateFrom=${from}&dateBefore=${before}`, {responseType: "blob"})
      .then(response => {
        dispatch(saveReportsSuccess(response.data));
        showSuccess('Файл был успешно сохранен');
        const blob = new Blob([response.data], {type: 'vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8'});
        FileSaver.saveAs(blob, `invoice development ${user} ${moment(from).format('L')} and ${moment(before).format('L')}.xlsx`);
    }).catch(err => {
      console.log(err);
      dispatch(resetReports());
      });
  }
};

export const getProjectMembers = (projectId) => {
  return (dispatch) => {
    return axios.get(`projects/${projectId}/members`)
      .then(r => {
        dispatch(setProjectMembers(r.data.members))
      })
      .catch(err => console.log(err))
  };
};

export const getReportById = (id) => {
  return (dispatch) => {
    return axios.get(`reports/get/${id}`)
      .then(r => {
        dispatch(getReportItem(r.data.report));
      })
      .catch(err => console.log(err))
  };
};

export const addReport = (data) => {
  return (dispatch) => {
    return axios.post('reports/', {...data})
      .then(r => {
        showSuccess(r.data.message);
        dispatch(push('/reports'))
      })
      .catch(err => console.log(err))
  };
};

export const updateReport = (id, data) => {
  return (dispatch) => {
    return axios.put(`reports/update/${id}`, {...data})
      .then(r => {
        showSuccess(r.data.message);
        dispatch(push('/reports'))
      })
      .catch(err => console.log(err))
  };
};
