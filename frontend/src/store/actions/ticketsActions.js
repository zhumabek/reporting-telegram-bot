import axios from '../../utils/axios-api';
import {showError} from "../../utils/messages";

export const GET_TICKET_BY_ID_PROJECT_SUCCESS = 'GET_TICKET_BY_ID_PROJECT_SUCCESS';
export const GET_TICKET_NUMBER_BY_ID_PROJECT_SUCCESS = 'GET_TICKET_NUMBER_BY_ID_PROJECT_SUCCESS';
export const RESET_SINGLE_PROJECT_TICKETS = 'RESET_SINGLE_PROJECT_TICKETS';

export const fetchTicketByIdProjectSuccess = data => ({type: GET_TICKET_BY_ID_PROJECT_SUCCESS, data});
export const fetchTicketNumberByIdProjectSuccess = ticketNumber => ({type: GET_TICKET_NUMBER_BY_ID_PROJECT_SUCCESS, ticketNumber});
export const resetSingleProjectTickets = () => ({type: RESET_SINGLE_PROJECT_TICKETS});

export const getTicketByIdProject = id => {
  return dispatch => {
    return axios.get(`/tickets/get/${id}`).then(response => {
      dispatch(fetchTicketByIdProjectSuccess(response.data));
      if (response.data.length === 0) {
        showError('Тикеты отсутствуют!');
      }
    })
  }
};

export const getTicketNumberByIdProject = id => {
  return dispatch => {
    return axios.get(`/tickets/getTicketNumber/${id}`).then(response => {
      dispatch(fetchTicketNumberByIdProjectSuccess(response.data));
    })
  }
};
