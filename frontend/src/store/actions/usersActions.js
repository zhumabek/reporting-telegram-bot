import axios from '../../utils/axios-api';
import {push} from 'connected-react-router';
import {showError, showSuccess} from "../../utils/messages";

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';
export const LOGOUT_USER = 'LOGOUT_USER';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const PATCH_USER_SUCCESS = 'PATCH_USER_SUCCESS';
export const PATCH_REPORTS_OPTIONAL_SUCCESS = 'PATCH_REPORTS_OPTIONAL_SUCCESS';
export const GET_USERS = 'GET_USERS';
export const GET_ACTIVE_USERS = 'GET_ACTIVE_USERS';

export const loginUserSuccess = user => ({type: LOGIN_USER_SUCCESS, user});
export const loginUserFailure = error => ({type: LOGIN_USER_FAILURE, error});
export const fetchAllUsersSuccess = allUsers => ({type: FETCH_USERS_SUCCESS, allUsers});
export const fetchAllActiveUsersSuccess = allActiveUsers => ({type: GET_ACTIVE_USERS, allActiveUsers});
export const patchUserSuccess = user => ({type: PATCH_USER_SUCCESS, user});
export const patchReportsOptionalSuccess = user => ({type: PATCH_REPORTS_OPTIONAL_SUCCESS, user});

export const loginUser = userData => {
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user));
        showSuccess('Logged in successfully');
        if (response.data.user.role === 'user') {
          dispatch(push('/tickets'));
        } else {
          dispatch(push('/'));
        }
      },
      error => {
        dispatch(loginUserFailure(error));
      }
    )
  }
};


export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const config = {headers: {'Authorization': token}};

    return axios.delete('/users/sessions', null, config).then(
      () => {
        dispatch({type: LOGOUT_USER});
        showSuccess('Logged out');
        dispatch(push('/'));
      },
      error => {
        showError('Could not logout!')
      }
    )
  }
};

export const getAllUsers = () => {
  return (dispatch) => {
    return axios.get('/users')
      .then(
        response => {
          dispatch(fetchAllUsersSuccess(response.data));
        }
      ).catch(err => {
        if (err.message === 'Request failed with status code 401') {
          console.log(err);
          dispatch({type: LOGOUT_USER});
          dispatch(push('/login'));
        } else {
          console.log(err);
        }
      });
  }
};

export const getAllActiveUsers = () => {
  return (dispatch) => {
    return axios.get('/users/active')
      .then(
        response => {
          dispatch(fetchAllActiveUsersSuccess(response.data));
        }
      ).catch(err => {
        if (err.message === 'Request failed with status code 401') {
          console.log(err);
          dispatch({type: LOGOUT_USER});
          dispatch(push('/login'));
        } else {
          console.log(err);
        }
      });
  }
};

export const patchUser = (id, role, password, status) => {
  return (dispatch) => {
    return axios.patch(`/users/data_change/${id}`, {id, role: role, password: password, status: status}).then(
      () => {
        dispatch(patchUserSuccess());
        dispatch(getAllUsers());
        showSuccess(`Данные пользователя изменены!`);
      },
      error => {
        showError('У вас нет прав доступа!')
      }
    );
  };
};