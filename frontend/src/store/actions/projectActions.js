import {push} from 'connected-react-router';
import axios from '../../utils/axios-api';
import {showInfo, showSuccess} from "../../utils/messages";
import {LOGOUT_USER} from "./usersActions";


export const GET_PROJECTS = 'GET_PROJECTS';
export const SET_JIRA_USERS = 'SET_JIRA_USERS';
export const GET_PROJECT_BY_ID_SUCCESS = 'GET_PROJECT_BY_ID_SUCCESS';
export const SET_PROJECT_MEMBERS = 'SET_PROJECT_MEMBERS';
export const GET_NOT_MEMBER_USERS = 'GET_NOT_MEMBER_USERS';
export const SET_PROJECT_MEMBERS_NOT_NOTIFICATION = 'SET_PROJECT_MEMBERS_NOT_NOTIFICATION';
export const RESET_PROJECT_MEMBERS = 'RESET_PROJECT_MEMBERS';
export const ADD_USER_TO_THE_PROJECT = 'ADD_USER_TO_THE_PROJECT';
export const ADD_USER_TO_THE_PROJECT_NOT_NOTIFICATION = 'ADD_USER_TO_THE_PROJECT_NOT_NOTIFICATION';
export const REMOVE_USER_NOT_NOTIFICATION = 'REMOVE_USER_NOT_NOTIFICATION';

export const setProjectMembers = members => ({type: SET_PROJECT_MEMBERS, members});
export const setJiraUsers = users => ({type: SET_JIRA_USERS, jiraUsers: users});
export const fetchNotMemberUsers = notMemberUsers => ({type: GET_NOT_MEMBER_USERS, notMemberUsers});
export const setProjectMembersNotNotification = membersNotNotification => ({type: SET_PROJECT_MEMBERS_NOT_NOTIFICATION, membersNotNotification});
export const addUserToTheProject = user => ({type: ADD_USER_TO_THE_PROJECT, user: user});
export const addUserToTheProjectNotNotification = user => ({type: ADD_USER_TO_THE_PROJECT_NOT_NOTIFICATION, user: user});
export const removeUserNotNotification = id => ({type: REMOVE_USER_NOT_NOTIFICATION, id});
export const fetchProjectsSuccess = projects => ({type: GET_PROJECTS, projects: projects});
export const getProjectByIdSuccess = data => ({type: GET_PROJECT_BY_ID_SUCCESS, data});


export const addProject = data => {
  return dispatch => {
    return axios.post('/projects', {data}).then((r) => {
        dispatch(push(`/project/${r.data.projectId}/members`));
        showSuccess(r.data.message);
      },
      error => {
        console.log(error);
      }
    )
  }
};

export const getProjects = (status) => {
  return (dispatch) => {
    return axios.get(`projects/get/all&status=${status}`)
      .then(r => {
        dispatch(fetchProjectsSuccess(r.data.projects))
      })
      .catch(err => {
        if (err.message === 'Request failed with status code 401') {
          console.log(err);
          dispatch({type: LOGOUT_USER});
          dispatch(push('/login'));
        } else {
          console.log(err);
        }
      })
  };
};

export const getProjectMembers = (projectId) => {
  return (dispatch) => {
    return axios.get(`projects/${projectId}/members`)
      .then(r => {
        dispatch(setProjectMembers(r.data.members))
      })
      .catch(err => console.log(err))
  };
};

export const getNotMemberUsers = (projectId) => {
  return dispatch => {
    return axios.get(`projects/${projectId}/not/members`)
      .then(r => {
        dispatch(fetchNotMemberUsers(r.data.members))
      })
      .catch(err => console.log(err))
  };
};

export const getProjectMembersNotNotification = (projectId) => {
  return (dispatch) => {
    return axios.get(`projects/${projectId}/not_notification/`)
      .then(r => {
        dispatch(setProjectMembersNotNotification(r.data.notNotification))
      })
      .catch(err => console.log(err))
  };
};

export const closeProject = (id, status) => {
  return (dispatch) => {
    axios.put(`projects/close/${id}`, {id, status: status})
      .then(r => {
        showSuccess(r.data.message);
        dispatch(getProjects('inactive'));
      })
      .catch(err => {
        console.log(err)
      })
  };
};

export const getProjectById = id => {
  return dispatch => {
    return axios.get(`/projects/` + id).then(response => {
      dispatch(getProjectByIdSuccess(response.data.project));
    })
  }
};

export const editProject = (data, id) => {
  return dispatch => {
    return axios.put(`/projects/` + id, {data}).then((r) => {
        showSuccess(r.data.message);
      },
      error => {
        console.log(error);
      }
    )
  }
};

export const addProjectMember = (projectId, data) => {
  return dispatch => {
    return axios.put(`/projects/${projectId}/add/member`, {...data}).then((r) => {
        showSuccess(r.data.message);
        dispatch(getProjectMembers(projectId));
        dispatch(getNotMemberUsers(projectId));
      },
      error => {
        console.log(error);
      }
    )
  }
};

export const addProjectMemberNotNotification = (projectId, data) => {
  return dispatch => {
    return axios.put(`/projects/${projectId}/add/not_notification`, {...data}).then((r) => {
        showInfo(r.data.message);
        dispatch(getProjectMembersNotNotification(projectId));
      },
      error => {
        console.log(error);
      }
    )
  }
};

export const deleteProjectMember = (projectId, userId) => {
  return dispatch => {
    return axios.delete(`/projects/${projectId}/delete/member/${userId}`)
      .then((r) => {
        showSuccess(r.data.message);
        dispatch(getProjectMembers(projectId));
        dispatch(getNotMemberUsers(projectId));
      })
      .catch(err => {
        console.log(err)
      });
  }
};

export const deleteProjectMemberNotNotification = (projectId, userId) => {
  return dispatch => {
    return axios.delete(`/projects/${projectId}/delete/not_notification/${userId}`)
      .then((r) => {
        showSuccess(r.data.message);
        dispatch(getProjectMembersNotNotification(projectId));
      })
      .catch(err => {
        console.log(err)
      });
  }
};

export const getJiraUsers = (projectId) => {
  return dispatch => {
    return axios.get(`/projects/${projectId}/jira/users`)
      .then(r => {
        dispatch(setJiraUsers(r.data.users))
      })
      .catch(err => {
        console.log(err)
      });
  }
};



