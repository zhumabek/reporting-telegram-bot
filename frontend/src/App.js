import React, {Component, Fragment} from 'react';
import Routes from './Routes';
import {Container} from "reactstrap";
import './App.css';
import Header from "./components/Header/header";
import {NotificationContainer} from "react-notifications";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/usersActions";

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className='app'>
          <NotificationContainer/>
          <Header
            user={this.props.user}
            logout={this.props.logoutUser}
          />
          <div className='app-body'>
            <Container fluid style={{marginTop: 60}}>
              <Routes
                user={this.props.user}
              />
            </Container>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
