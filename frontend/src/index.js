import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {ConnectedRouter} from 'connected-react-router';
import store, {history} from './store/configureStore';
import App from './App';
import * as serviceWorker from './serviceWorker';
import moment from 'moment';
import 'moment/locale/ru';

import 'bootstrap/dist/css/bootstrap.min.css';
import '@coreui/coreui/scss/coreui.scss';
import '@fortawesome/fontawesome-free/css/all.css';
import 'react-table/react-table.css';
import 'react-notifications/lib/notifications.css';
import "react-datepicker/dist/react-datepicker.css";
import './index.css';

let defaultLang = 'ru';
moment.locale(defaultLang);

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
