import React, {Component} from 'react';
import AppTable from "../../components/AppTable";
import {Col, Row, Container, Spinner, Button, Form} from "reactstrap";
import {connect} from "react-redux";
import {getTicketByIdProject, resetSingleProjectTickets} from "../../store/actions/ticketsActions";
import {getProjects} from "../../store/actions/projectActions";
import Select from "../../components/Select";
import {FormGroupInput} from "../../components/ProjectForm/ProjectForm";
import ModalTicket from "./ModalTicket";
import {getReportsByTicket} from "../../store/actions/reportsActions";
import {formatDate} from "../../utils/helpers";

class Tickets extends Component {
  state = {
    selectedProject: null,
    isReady: false,
    isModal: false,
    ticket: null
  };

  hideModal = () => {
    this.setState({
      isModal: false,
      ticket: null
    });
  };

  showModal = (ticket) => {
    this.setState({
      isModal: true,
      ticket: ticket
    })
  };

  componentDidMount() {
    this.props.onGetProjects('all').then(() => {
      this.setState({isReady: !this.state.isReady})
    });
  }

  componentWillUnmount() {
    this.props.onResetTickets();
  }

  submitFormHandler = event => {
    event.preventDefault();
    let projectId = this.state.selectedProject._id;
    if (projectId) {
      this.props.onGetTickets(projectId);
      this.props.onResetTickets();
    }
  };


  render() {
    if (!this.state.isReady) {
      return <Spinner color="success"/>
    }
    const columns = [
      {
        Header: `Список тикетов к проекту ${this.state.selectedProject ? this.state.selectedProject.name : ''}`,
        columns: [
          {Header: "№ тикета", accessor: "number", width: 100},
          {Header: "Наименование", accessor: "name"},
          {Header: "Пользователь", accessor: "userId.name", width: 200},
          {Header: "Статус", accessor: "status", width: 150},
          {Header: "Дата", accessor: "date", width: 110, Cell: date => formatDate(date.value)}
        ]
      }
    ];

    return (
      <div>
        <Container>
          <Row className="pt-3">
            <Col xs={12}>
              <Form onSubmit={this.submitFormHandler}>
                <FormGroupInput labelSize={2} inputSize={10} label='Выберите проект'>
                  <Row>
                    <Col sm={9}>
                      <Select
                        options={this.props.projects}
                        placeholder={'Поиск...'}
                        valueKey="_id" labelKey="name"
                        isSearchable
                        value={this.state.selectedProject}
                        onChange={selectedProject => this.setState({selectedProject})}
                      />
                    </Col>
                    <Col sm={3}>
                      <Button disabled={this.state.selectedProject === null} className='bot-btn-st' color='primary' type='submit'>
                        <span className='fa fa-search'/>{' '}Выбрать
                      </Button>
                    </Col>
                  </Row>
                </FormGroupInput>
              </Form>
            </Col>
            <Col xs={12}>
              <AppTable
                getTdProps={(state, rowInfo) => {
                  return {
                    onClick: (e, handleOriginal) => {
                      if (rowInfo) {
                        this.props.onGetReport(rowInfo.original._id);
                        this.showModal(rowInfo.original);
                      }
                      if (handleOriginal) {
                        handleOriginal();
                      }
                    }
                  };
                }}
                columns={columns}
                data={this.props.tickets}
              />
              {
                this.state.ticket &&
                <ModalTicket
                  isOpen={this.state.isModal}
                  toggle={this.hideModal}
                  ticket={this.state.ticket}
                  reports={this.props.report}
                />
              }
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tickets: state.tickets.singleProjectTickets,
  projects: state.projects.projects,
  report: state.reports.report,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onGetTickets: (id) => dispatch(getTicketByIdProject(id)),
  onResetTickets: () => dispatch(resetSingleProjectTickets()),
  onGetProjects: (status) => dispatch(getProjects(status)),
  onGetReport: (id) => dispatch(getReportsByTicket(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tickets)