import React from 'react';
import {
  Button,
  Modal,
  ModalBody, ModalFooter,
  ModalHeader,
} from "reactstrap";


const ModalTicket = ({isOpen, toggle, ticket, reports}) => {

  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Тикет №{ticket.number}</ModalHeader>
      <ModalBody>
        <h5><b>Наименованиие:</b> {ticket.name}</h5>
        {reports && reports.map(report => {
          return (
            <div key={report._id}>
              <p><b>Описание отчета:</b> {report.description}</p>
              <p><b>Потраченное время:</b> {report.timeSpent} ч.</p>
            </div>
          )
        })}
        <p><b>Всего потрачено времени:</b> {reports.reduce((acc, count) => acc += count.timeSpent, 0)} ч.</p>
        <p><b>Статус:</b> {ticket.status}</p>
        <p><b>Пользователь:</b> {ticket.userId.name}</p>
      </ModalBody>
      <ModalFooter>
        <Button className='bot-btn-st' color="secondary" onClick={toggle}>Cancel</Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalTicket;