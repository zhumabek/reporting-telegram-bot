import React from 'react';
import {
  Button,
  Modal,
  ModalBody, ModalFooter,
  ModalHeader,
} from "reactstrap";

const ModalReport = ({isOpen, toggle, state, history, user}) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Тикет №{state.ticketId.number}</ModalHeader>
      <ModalBody>
        <p><b>Наименованиие тикета:</b> {state.ticketId.name}</p>
        <p><b>Описание отчета:</b> {state.description}</p>
        <p><b>Статус:</b> {state.ticketStatus}</p>
        <p><b>Потраченное время:</b> {state.timeSpent} ч.</p>
        <p><b>Пользователь:</b> {state.userId.name}</p>
      </ModalBody>
      <ModalFooter>
        <Button className='bot-btn-st' color="secondary" onClick={toggle}>Cancel</Button>
        {user.role === 'user' ? null :
        <Button className='bot-btn-st' color="primary" onClick={() => {
          toggle();
          history.push(`/report/${state._id}`);
        }}>Отредактировать отчет</Button>}
      </ModalFooter>
    </Modal>
  );
};

export default ModalReport;