import React, {Component} from 'react';
import AppTable from "../../components/AppTable";
import {Button, Col, CustomInput, Form, FormGroup, Label, Row, Spinner} from "reactstrap";
import {connect} from 'react-redux';
import * as actions from '../../store/actions/reportsActions';
import {getReportsByUser} from '../../store/actions/reportsActions';
import {getProjects} from "../../store/actions/projectActions";
import {FormGroupInput} from "../../components/ProjectForm/ProjectForm";
import Select from "../../components/Select";
import DatePicker from "../../components/DatePicker";
import {formatDate} from "../../utils/helpers";
import ModalReport from "./ModalReport";
import {getAllActiveUsers} from "../../store/actions/usersActions";

import '../../assets/css/Button.css';

class Reports extends Component {

  state = {
    from: null,
    before: null,
    selectedProject: null,
    selectedUser: null,
    isSave: false,
    isModal: false,
    isProject: false,
    isDevelopment: false,
    report: null
  };

  downloadReportsFileHandler = event => {
    event.preventDefault();
    let {selectedProject, selectedUser, from, before} = this.state;
    let startDate = formatDate(from, 'YYYY-MM-DD');
    let endDate = formatDate(before, 'YYYY-MM-DD');
    if (this.state.selectedUser === null) {
      this.props.onSaveReportsByProject(selectedProject._id, startDate, endDate, selectedProject.name).then(() => {
        this.setState({isSave: false});
      });
    } else {
      this.props.onSaveReportsByUser(selectedUser._id, startDate, endDate, selectedUser.name).then(() => {
        this.setState({isSave: false});
      });
    }

  };

  getPreviewReportsHandler = () => {
    let {selectedProject, selectedUser, from, before} = this.state;
    let startDate = formatDate(from, 'YYYY-MM-DD');
    let endDate = formatDate(before, 'YYYY-MM-DD');
    if (this.state.selectedUser === null) {
      this.props.onPreviewReportsByProject(selectedProject._id, startDate, endDate);
    } else {
      this.props.onPreviewReportsByUser(selectedUser._id, startDate, endDate);
    }
  };

  areFormFieldsFilled() {
    let {selectedProject, selectedUser, from, before} = this.state;
    return (selectedProject || selectedUser) && from && before;
  }

  toggleSave = () => {
    this.getPreviewReportsHandler();
    if (this.props.reports.length > 0) {
      this.setState({isSave: true});
    }
  };

  toggleSelectProject = () => {
    this.props.onCleanReportsTable();
    this.setState({
      isProject: true,
      isDevelopment: false,
      from: null,
      before: null,
      selectedProject: null,
      selectedUser: null,
    });
  };

  toggleSelectDevelopment = () => {
    if (this.props.user.role === 'user') {
      this.props.onCleanReportsTable();
      this.props.onFetchReportByUsers(this.props.user._id);
      this.setState({
        isProject: false,
        isDevelopment: true,
        from: null,
        before: null,
        selectedUser: this.props.user,
        selectedProject: null,
      });
    } else {
      this.props.onCleanReportsTable();
      this.setState({
        isProject: false,
        isDevelopment: true,
        from: null,
        before: null,
        selectedUser: null,
        selectedProject: null,
      });
    }
  };

  hideModal = () => {
    this.setState({
      isModal: false,
      report: null
    });
  };

  showModal = (report) => {
    this.setState({
      isModal: true,
      report: report
    })
  };

  componentDidMount() {
    this.props.onGetProjects('all');
    this.props.onFetchUsers();
  }

  componentWillUnmount() {
    this.props.onCleanReportsTable();
  }

  render() {
    if (!this.props.reports) {
      return <Spinner color="success"/>
    }
    const columns = [
      {
        Header: "Список отчетов",
        columns: [
          {Header: "Проект", accessor: "ticketId.projectId.name", width: 200},
          {Header: "Пользователь", accessor: "userId.name", width: 200},
          {Header: "№ тикета", accessor: "ticketId.number", width: 100},
          {Header: "Описание", accessor: "description"},
          {Header: "Время", accessor: "timeSpent", width: 100},
          {Header: "Статус", accessor: "ticketStatus", width: 130},
          {Header: "Дата", accessor: "date", width: 110, Cell: date => formatDate(date.value)}
        ]
      }
    ];

    return (
      <div>
        <div style={{width: '80%', margin: 'auto', padding: 15}}>
          <Form autoComplete='off'>
            <Row>
              <Col xl={12}>
                <FormGroup>
                  <Row>
                    <Col xl={9}>
                      <Label for="exampleCheckbox">Отчеты</Label>
                      <div>
                        <CustomInput
                          type="radio"
                          id="exampleCustomRadio"
                          name="customRadio"
                          label="По проекту"
                          checked={this.state.isProject === true}
                          onChange={this.toggleSelectProject}
                          inline
                        />
                        <CustomInput
                          type="radio"
                          id="exampleCustomRadio2"
                          name="customRadio"
                          label="По разработчику"
                          checked={this.state.isDevelopment === true}
                          onChange={this.toggleSelectDevelopment}
                          inline
                        />
                      </div>
                    </Col>
                    {this.props.user.role === 'user' ? null :
                      <Col xl={3}>
                          <Button
                            className='bot-btn-st'
                            color='light'
                            onClick={() => this.props.history.push('/report')}>
                            Написать отчет
                          </Button>
                      </Col>}
                  </Row>
                </FormGroup>
              </Col>
              {this.state.isProject === true ?
                <Col xl={12}>
                  <FormGroupInput labelSize={2} inputSize={10} label='Выберите проект'>
                    <Row>
                      <Col>
                        <Select
                          options={this.props.projects}
                          placeholder={'Выберите проект...'}
                          name='Выберите проект...'
                          valueKey="_id" labelKey="name"
                          isSearchable
                          value={this.state.selectedProject}
                          onChange={(selectedProject) => {
                            this.setState({
                              selectedProject,
                              from: null,
                              before: null,
                              isSave: false
                            });
                            this.props.onGetReportsByProject(selectedProject._id);
                            this.props.onCleanReportsTable();
                          }}
                        />
                      </Col>
                    </Row>
                  </FormGroupInput>
                </Col> : null}
              {this.state.isDevelopment === true ?
                <Col xl={12}>
                  <FormGroupInput labelSize={2} inputSize={10} label='Выберите разработчика'>
                    <Row>
                      <Col>
                        <Select
                          options={this.props.users}
                          placeholder={'Выберите разработчика...'}
                          name='Выберите разработчика...'
                          valueKey="_id" labelKey="name"
                          isSearchable
                          value={this.props.user.role === 'user' ? this.props.user : this.state.selectedUser}
                          onChange={(selectedUser) => {
                            this.setState({
                              selectedUser,
                              from: null,
                              before: null,
                              isSave: false
                            });
                            this.props.onFetchReportByUsers(selectedUser._id);
                            this.props.onCleanReportsTable();
                          }}
                        />
                      </Col>
                    </Row>
                  </FormGroupInput>
                </Col> : null}
              <Col>
                <FormGroupInput labelSize={2} inputSize={3} label='дата начала'>
                  <DatePicker
                    name="dateFrom"
                    value={this.state.from}
                    maxDate={this.state.before}
                    onChange={date => this.setState({from: date})}
                  />
                </FormGroupInput>
                <FormGroupInput labelSize={2} inputSize={3} label='дата окончания'>
                  <Row>
                    <Col>
                      <DatePicker
                        name="dateBefore"
                        value={this.state.before}
                        minDate={this.state.from}
                        onChange={date => this.setState({before: date})}
                      />
                    </Col>
                  </Row>
                </FormGroupInput>
                <FormGroupInput>
                  <Row>
                    <Col xl={2}>
                      <Button
                        style={{marginBottom: '15px'}}
                        className='bot-btn-st'
                        color='primary'
                        onClick={this.toggleSave}
                        disabled={!this.areFormFieldsFilled()}>
                        Посмотреть
                      </Button>
                    </Col>
                    <Col xl={2}>
                      <Button
                        className='bot-btn-st'
                        color='success'
                        onClick={this.downloadReportsFileHandler}
                        disabled={!this.state.isSave || !this.props.reports.length}>
                        Скачать
                      </Button>
                    </Col>
                  </Row>
                </FormGroupInput>
              </Col>
              <Col xl={12}>
                <AppTable
                  columns={columns}
                  data={this.props.reports}
                  getTdProps={(state, rowInfo) => {
                    return {
                      onClick: (e, handleOriginal) => {
                        if (rowInfo) {
                          this.showModal(rowInfo.original);
                        }
                        if (handleOriginal) {
                          handleOriginal();
                        }
                      }
                    };
                  }}
                />
                {
                  this.state.report &&
                  <ModalReport
                    isOpen={this.state.isModal}
                    toggle={this.hideModal}
                    state={this.state.report}
                    history={this.props.history}
                    user={this.props.user}
                  />
                }
              </Col>
            </Row>
          </Form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  users: state.users.allActiveUsers,
  projects: state.projects.projects,
  reports: state.reports.reports,
});

const mapDispatchToProps = dispatch => ({
  onGetProjects: (status) => dispatch(getProjects(status)),
  onGetReportsByProject: (projectId) => dispatch(actions.getReportsByProject(projectId)),
  onPreviewReportsByProject: (projectId, from, before) => dispatch(actions.previewReportsByProject(projectId, from, before)),
  onPreviewReportsByUser: (userId, from, before) => dispatch(actions.previewReportsByUser(userId, from, before)),
  onSaveReportsByProject: (projectId, from, before, project) => dispatch(actions.saveReportsByProject(projectId, from, before, project)),
  onSaveReportsByUser: (userId, from, before, user) => dispatch(actions.saveReportsByUser(userId, from, before, user)),
  onCleanReportsTable: () => dispatch(actions.resetReports()),
  onFetchUsers: () => dispatch(getAllActiveUsers()),
  onFetchReportByUsers: (id) => dispatch(getReportsByUser(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Reports);