import React, {Component} from 'react';
import {Button, Card, CardBody, CardHeader, Container, Form, Input, Label, Spinner} from "reactstrap";
import {connect} from 'react-redux';
import {FormGroupInput} from "../../components/ProjectForm/ProjectForm";
import Select from "../../components/Select";
import {getProjects} from "../../store/actions/projectActions";
import {addReport, getProjectMembers, getReportById, updateReport} from "../../store/actions/reportsActions";
import DatePicker from "../../components/DatePicker";
import {formatDate} from "../../utils/helpers";
import {getTicketNumberByIdProject} from "../../store/actions/ticketsActions";

import '../../assets/css/Button.css';

class AddReport extends Component {

  state = {
    selectedProject: null,
    selectedUser: null,
    date: '',
    ticketNumber: '',
    ticketName: '',
    reportDescription: '',
    spentTime: '',
    ticketStatus: '',
    ticketId: '',
    isReady: false,
    isAdditionalReport: false,
  };

  async componentDidMount() {
    let reportId = this.props.match.params.id;
    if (reportId) {
      await this.props.onGetReport(reportId);
      const report = this.props.report;
      await this.props.onGetProjectMembers(report.ticketId.projectId._id);
      this.setState({
        selectedProject: report.ticketId.projectId,
        selectedUser: report.userId,
        date: report.date,
        ticketNumber: report.ticketId.number,
        ticketName: report.ticketId.name,
        reportDescription: report.description,
        spentTime: report.timeSpent,
        ticketStatus: {id: report.ticketStatus, name: report.ticketStatus},
        ticketId: report.ticketId._id,
      });
    }
    await this.props.onGetProjects('active');
    this.setState(previousState => ({isReady: !previousState.isReady}));
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  projectSelectHandler = async (selectedProject) => {
    this.setState({isReady: false, selectedProject, selectedUser: null});
    await this.props.onGetProjectMembers(selectedProject._id);
    await this.props.onTicketNumber(selectedProject._id);
    this.setState(previousState => ({isReady: !previousState.isReady}));
  };

  submitFormHandler = event => {
    event.preventDefault();
    let {
      ticketStatus, spentTime, reportDescription,
      ticketName, selectedProject, selectedUser,
      ticketNumber, date, ticketId
    } = this.state;

    const data = {
      projectId: selectedProject._id,
      userId: selectedUser._id,
      ticketId,
      ticketNumber,
      ticketName,
      reportDescription,
      spentTime,
      ticketStatus: ticketStatus.name,
      date: formatDate(date, 'YYYY-MM-DD')
    };

    const reportId = this.props.match.params.id;
    if (reportId) {
      this.props.onUpdateReport(reportId, data);
    } else {
      this.props.onAddReport(data);
    }
  };

  areFormFieldsFilled() {
    let {
      ticketStatus, spentTime, reportDescription,
      ticketName, selectedProject, ticketNumber,
      selectedUser, date
    } = this.state;

    return ticketStatus && spentTime && reportDescription
      && ticketNumber && selectedProject
      && ticketName && selectedUser && date;
  }

  toggleSelectNewTicketReport = () => {
    if (this.state.isAdditionalReport) {
      this.setState({
        ticketNumber: '',
        ticketName: '',
        ticketStatus: '',
      })
    }
  };

  toggleSelectAdditionalReport = () => {
    if (!this.state.isAdditionalReport) {
      this.setState({
        ticketNumber: this.props.ticketAdditionalReport.newTicketNumber,
        ticketName: 'Дополнительный отчет',
        ticketStatus: {id: "closed", name: "closed"},
      })
    }
  };

  render() {
    let {
      isReady, ticketNumber, ticketStatus, reportDescription,
      selectedProject, selectedUser, spentTime,
      ticketName, date, isAdditionalReport,
    } = this.state;

    const isEditMode = this.props.match.params.id ? true : false;
    const isToggleAdditionalReport = isEditMode && ticketName === 'Дополнительный отчет';

    if (!isReady) {
      return <Spinner color="success"/>
    }

    return (
      <Container>
        <Card>
          <CardHeader>
            {isEditMode ? "Редактирование отчета" : "Добавление отчета"}
          </CardHeader>
          <CardBody>
            <Form onSubmit={this.submitFormHandler} autoComplete='off'>
              <FormGroupInput labelSize={3} inputSize={8} label='Выберите проект'>
                <Select
                  options={this.props.projects}
                  placeholder="Выберите проект"
                  name="Выберите проект"
                  valueKey="_id" labelKey="name"
                  isSearchable
                  required
                  isDisabled={isEditMode}
                  value={selectedProject}
                  onChange={(selectedProject) => this.projectSelectHandler(selectedProject)}
                />
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={8} label='Выберите пользователя'>
                <Select
                  options={this.props.projectMembers}
                  placeholder="Выберите пользователя"
                  name="Выберите пользователя"
                  valueKey="_id" labelKey="name"
                  isSearchable
                  required
                  isDisabled={isEditMode}
                  value={selectedUser}
                  onChange={(selectedUser) => this.setState({selectedUser})}
                />
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={3} label='Выберите дату отчета'>
                <DatePicker
                  name="date"
                  value={date}
                  placeholder='Дата отчета'
                  required
                  onChange={date => this.setState({date})}
                  maxDate={new Date()}
                />
              </FormGroupInput>
              {!selectedProject || (isEditMode && ticketName !== 'Дополнительный отчет') ? null :
                <FormGroupInput labelSize={3} inputSize={7} className='align-items-center'
                                label='Дополнительный отчет'>
                  <Label className="switch switch-3d switch-primary switch-pill">
                    <Input type="checkbox" className="switch-input"
                           checked={isToggleAdditionalReport ? !isAdditionalReport : isAdditionalReport}
                           onChange={() => {
                             this.setState(prevState => ({isAdditionalReport: !prevState.isAdditionalReport}));
                             this.toggleSelectAdditionalReport();
                             this.toggleSelectNewTicketReport();
                           }}/>
                    <span className="switch-slider"/>
                  </Label>
                </FormGroupInput>
              }
              <FormGroupInput labelSize={3} inputSize={5} label='Введите номер тикета'>
                {
                  isAdditionalReport === false ?
                    <Input type='text'
                           name='ticketNumber'
                           placeholder='Номер тикета'
                           required
                           disabled={isToggleAdditionalReport ? isEditMode : null}
                           value={ticketNumber}
                           onChange={this.inputChangeHandler}/> :
                    <Input type='text'
                           name='ticketNumber'
                           placeholder='Номер тикета'
                           required
                           disabled={isAdditionalReport}
                           value={ticketNumber}
                           onChange={this.inputChangeHandler}/>
                }
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={8} label='Введите наименование тикета'>
                {
                  isAdditionalReport === false ?
                    <Input type='text'
                           name='ticketName'
                           placeholder='Наименование тикета'
                           required
                           disabled={isToggleAdditionalReport ? isEditMode : null}
                           value={ticketName}
                           onChange={this.inputChangeHandler}/> :
                    <Input type='text'
                           name='ticketName'
                           placeholder='Наименование тикета'
                           required
                           disabled={isAdditionalReport}
                           value={ticketName}
                           onChange={this.inputChangeHandler}/>
                }
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={8} label='Введите описание отчета'>
                <Input type='textarea'
                       name='reportDescription'
                       rows="4"
                       placeholder='Описание отчета'
                       required
                       value={reportDescription}
                       onChange={this.inputChangeHandler}/>
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={5} label='Введите потраченное время'>
                <Input type='number'
                       name='spentTime'
                       placeholder='Потраченное время'
                       required
                       value={spentTime}
                       onChange={this.inputChangeHandler}/>
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={5} label='Выберите статус тикета'>
                {
                  isAdditionalReport === false ?
                    <Select
                      options={ticketStatuses}
                      placeholder='Выберите статус'
                      name='Выберите статус'
                      valueKey="id" labelKey="name"
                      required
                      isDisabled={isToggleAdditionalReport ? isEditMode : null}
                      value={ticketStatus}
                      onChange={(ticketStatus) => this.setState({ticketStatus})}
                    /> :
                    <Select
                      options={ticketStatuses}
                      placeholder='Выберите статус'
                      name='Выберите статус'
                      valueKey="id" labelKey="name"
                      required
                      isDisabled={isAdditionalReport}
                      value={ticketStatus}
                      onChange={(ticketStatus) => this.setState({ticketStatus})}
                    />
                }
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={8}>
                <Button className='bot-btn-st' color='danger' onClick={() => this.props.history.goBack()}>Назад</Button>{' '}
                <Button className='bot-btn-st' color='primary' type='submit' disabled={!this.areFormFieldsFilled()}>Сохранить отчет</Button>
              </FormGroupInput>
            </Form>
          </CardBody>
        </Card>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  projects: state.projects.projects,
  projectMembers: state.reports.projectMembers,
  report: state.reports.reportItem,
  ticketAdditionalReport: state.tickets.ticketNumber
});

const mapDispatchToProps = dispatch => ({
  onGetProjects: () => dispatch(getProjects()),
  onGetProjectMembers: (id) => dispatch(getProjectMembers(id)),
  onGetReport: (id) => dispatch(getReportById(id)),
  onAddReport: (data) => dispatch(addReport(data)),
  onUpdateReport: (id, data) => dispatch(updateReport(id, data)),
  onTicketNumber: (id) => dispatch(getTicketNumberByIdProject(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddReport);

const ticketStatuses = [
  {id: "in progress", name: "in progress"},
  {id: "closed", name: "closed"},
];