import React, {Component} from 'react';
import ProjectForm, {FormGroupInput} from "../../components/ProjectForm/ProjectForm";
import {connect} from "react-redux";
import {
  addProject,
  addUserToTheProject,
  addUserToTheProjectNotNotification,
  editProject,
  getProjectById,
  getProjectMembers,
  getProjectMembersNotNotification,
  removeUserFromTheProject,
  removeUserNotNotification,
  resetProjectMembers,
} from "../../store/actions/projectActions";
import {getNotMemberUsers} from "../../store/actions/usersActions";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Container,
  CustomInput,
  Form,
  FormGroup,
  Input,
  Label,
  Row
} from "reactstrap";
import AppTable from "../../components/AppTable";
import Select from "../../components/Select";
import DatePicker from "../../components/DatePicker";
import {convertFromHourToDate, formatTime, TIME_PICKER_MAX_TIME, TIME_PICKER_MIN_TIME} from "../../utils/helpers";

import '../../assets/css/Button.css';
import {showInfo, showSuccess} from "../../utils/messages";

class AddProject extends Component {
  state = {
    name: '',
    chatId: '',
    selectedUser: null,
    reportsSendTime: null,
    isChecked: true,
    showNotificationSwitcher: false
  };

  async componentDidMount() {
    let projectId = this.props.match.params.id;
    if (projectId) {
      await this.props.onSetProjectMembers(projectId);
      await this.props.onSetProjectMembersNotNotification(projectId);
      await this.props.onGetProject(projectId);
      this.setState({
        name: this.props.project.name,
        chatId: this.props.project.chatId,
        reportsSendTime: convertFromHourToDate(this.props.project.reportsSendTime)
      });
      await this.props.getNotMemberUsers();
    }
    await this.props.getNotMemberUsers();
  }

  componentWillUnmount() {
    this.props.resetProjectMembers();
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();
    let projectId = this.props.match.params.id;
    let reportsSendTime = formatTime(this.state.reportsSendTime, 'LT');
    let data = {
      name: this.state.name,
      chatId: this.state.chatId,
      reportsSendTime: reportsSendTime,
      isCheckedToNotify: this.state.isChecked
    };
    if (projectId) {
      this.props.updateProject(data, projectId);
    } else {
      this.props.addProject(data);
    }
  };

  canAddUser() {
    return this.state.selectedUser;
  }

  onAddUserClick = () => {
    this.props.onAddUser(this.state.selectedUser);
    this.setState({selectedUser: null});
    this.props.getNotMemberUsers();
  };

  onRemoveUserClick = (id) => {
    this.props.onRemoveUser(id);
    this.props.onRemoveUserNotNotification(id);
    this.props.getNotMemberUsers();
  };

  setReportsOptional = (id) => {
    if (!this.isCheckedUserNotNotification(id._id)) {
      this.props.onAddUserNotNotification(id);
      showInfo(`Разработчик ${id.name} не будет получать уведомления по данному проекту!`);
    } else {
      this.props.onRemoveUserNotNotification(id._id);
      showSuccess(`Разработчик ${id.name} будет получать уведомления по данному проекту!`);
    }
  };

  isCheckedUserNotNotification = (id) => {
    for (const userNotNotification of this.props.notNotification) {
      const userChecked = id === userNotNotification._id;
      if (userChecked === true) return userChecked;
    }
  };

  render() {
    const columns = [
      {
        Header: "Добавленные пользователи к проекту",
        columns: [
          {Header: "Наименование", accessor: "name"},
          {Header: "E-mail", accessor: "email"},
          {
            width: 182, filterable: false, getTdProps: true, sortable: false,
            Cell: row => (
              row.original.role === 'user'
                ? <FormGroup>
                  <div>
                    <CustomInput
                      type="checkbox"
                      id="reportsOptional"
                      label="Отчет необязателен"
                      defaultChecked={this.isCheckedUserNotNotification(row.original._id)}
                    />
                  </div>
                </FormGroup>
                : null
            )
          },
          {
            width: 40, filterable: false,
            Cell: row => {
              return <Button size={'sm'}
                             color={'danger'}
                             title={'Вы действительно хотите удалить пользователя из проекта'}
                             onClick={(e) => {
                               e.stopPropagation();
                               this.onRemoveUserClick(row.original._id)
                             }}>
                <i className="fa fa-minus"/>
              </Button>;
            }
          }

        ]
      }
    ];
    let isEditMode = this.props.match.params.id;

    return (
      <Container>
        <Card>
          <CardHeader>
            {isEditMode ? 'Редактирование проекта' : 'Добавление проекта'}
          </CardHeader>
          <CardBody>
            <Form onSubmit={this.submitFormHandler}>
              <FormGroupInput labelSize={3} inputSize={7} label='Наименование проекта'>
                <ProjectForm
                  type='text'
                  inputName='name'
                  value={this.state.name}
                  changeInput={this.inputChangeHandler}
                  placeholder="Введите имя проекта"
                />
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={7} label='Введите сhatId телеграм группы в форме "-chatId"'>
                <ProjectForm
                  type='number'
                  value={this.state.chatId}
                  inputName='chatId'
                  changeInput={this.inputChangeHandler}
                  placeholder="Id чата"
                />
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={7} label='Выберите время для отправки отчетов в группу'>
                <DatePicker
                  selected={this.state.reportsSendTime}
                  onChange={date => this.setState({reportsSendTime: date, showNotificationSwitcher: true})}
                  locale="ru"
                  showTimeSelect
                  showTimeSelectOnly
                  minTime={TIME_PICKER_MIN_TIME}
                  maxTime={TIME_PICKER_MAX_TIME}
                  timeFormat="HH:mm"
                  timeIntervals={60}
                  timeCaption="Time"
                  placeholderText="Выберите время"
                  dateFormat="HH:mm"
                  required
                />
              </FormGroupInput>
              {isEditMode && this.state.showNotificationSwitcher ? (
                <FormGroupInput labelSize={3} inputSize={7} className='align-items-center'
                                label='Уведомлять участников об изменении времени?'>
                  <Label className="switch switch-3d switch-primary switch-pill">
                    <Input type="checkbox" className="switch-input"
                           checked={this.state.isChecked}
                           onChange={() => this.setState(prevState => ({isChecked: !prevState.isChecked}))}/>
                    <span className="switch-slider"/>
                  </Label>
                </FormGroupInput>
              ) : null}
              <FormGroupInput labelSize={3} inputSize={9} label='Выберите пользователя'>
                <Row>
                  <Col sm={9}>
                    <Select
                      options={this.props.users}
                      placeholder={'Поиск...'}
                      valueKey="_id" labelKey="name"
                      isSearchable
                      name='Выберите пользователя'
                      value={this.state.selectedUser}
                      onChange={selectedUser => this.setState({selectedUser})}
                    />
                  </Col>
                  <Col sm={3}>
                    <Button className='bot-btn-st' color='primary' onClick={this.onAddUserClick}
                            disabled={!this.canAddUser()}>
                      <span className='fa fa-plus'/>{' '}Добавить
                    </Button>
                  </Col>
                </Row>
              </FormGroupInput>
              <Row>
                <Col sm={12}>
                  <AppTable
                    columns={columns}
                    data={this.props.addedUsers}
                    showRowNumbers
                    pageSize={5}
                    getTdProps={(state, rowInfo) => {
                      return {
                        onClick: () => {
                          if (rowInfo && rowInfo.original.role === 'user') {
                            this.setReportsOptional(rowInfo.original)
                          }
                        }
                      };
                    }}
                  />
                </Col>
                <Col className='py-1'>
                  <Button className='bot-btn-st' color="danger"
                          onClick={() => this.props.history.goBack()}>Назад</Button>{' '}
                  <Button className='bot-btn-st' type="submit" color="primary">Сохранить проект</Button>
                </Col>
              </Row>
            </Form>
          </CardBody>
        </Card>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    project: state.projects.projectById,
    user: state.users.user,
    users: state.users.users,
    addedUsers: state.projects.addedUsers,
    notNotification: state.projects.notNotification,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProject: id => dispatch(getProjectById(id)),
    onSetProjectMembers: (id) => dispatch(getProjectMembers(id)),
    onSetProjectMembersNotNotification: (id) => dispatch(getProjectMembersNotNotification(id)),
    addProject: data => dispatch(addProject(data)),
    updateProject: (data, id) => dispatch(editProject(data, id)),
    getNotMemberUsers: () => dispatch(getNotMemberUsers()),
    onAddUser: (user) => dispatch(addUserToTheProject(user)),
    onAddUserNotNotification: (user) => dispatch(addUserToTheProjectNotNotification(user)),
    onRemoveUser: (id) => dispatch(removeUserFromTheProject(id)),
    onRemoveUserNotNotification: (id) => dispatch(removeUserNotNotification(id)),
    resetProjectMembers: () => dispatch(resetProjectMembers()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddProject);
