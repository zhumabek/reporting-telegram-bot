import React, {Component} from 'react';
import {Button, Col, Container, Row, Spinner} from "reactstrap";
import AppTable from "../../components/AppTable";
import {getAllUsers, patchUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import PopUp from "./PopUp";


class UsersPage extends Component {
  state = {
    role: '',
    password: '',
    status: '',
    isPopUp: false,
    user: null
  };

  hideModal = () => {
    this.setState({
      role: '',
      password: '',
      status: '',
      isPopUp: false,
      user: null
    });
  };

  showModal = (user) => {
    this.setState({
      role: user.role,
      password: '',
      status: user.active === true ? 'active' : 'inactive',
      isPopUp: true,
      user: user
    })
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.hideModal();
    this.props.onUserPatch(this.state.user._id, this.state.role, this.state.password, this.state.status);
  };

  componentDidMount() {
    this.props.onFetchUsers();
  }

  render() {
    if (!this.props.users) {
      return <Spinner color="success"/>
    }

    const columns = [
      {
        Header: `Список пользователей`, columns: [
          {Header: "Имя", accessor: "name"},
          {Header: "E-mail", accessor: "email"},
          {Header: "Телефон", accessor: "phoneNumber", width: 130},
          {Header: "Роль", accessor: "role", width: 130},
          {
            Header: "Статус", width: 100, filterable: false, sortable: false,
            Cell: (row) => (
              <p>{row.original.active === true ? 'active' : 'inactive'}</p>
            )
          },
          {
            filterable: false, sortable: false, width: 142,
            Cell: (row) => (
              <Button
                sm={2}
                className='bot-btn-st'
                color={'primary'}
                disabled={(this.props.user._id === row.original._id) || (this.props.user.role === 'project manager' &&
                  (row.original.role === 'admin' || row.original.role === 'project manager'))}
                onClick={() => this.showModal(row.original)}
              >
                <i className="fa fa-lg fa-edit"/> Изменить
              </Button>
            )
          }
        ]
      }
    ];

    return (
      <div>
        <Container>
          <Row>
            <Col xs={12}>
              <AppTable columns={columns} data={this.props.users}/>
            </Col>
          </Row>
          {
            this.state.user &&
            <PopUp
              user={this.props.user}
              isOpen={this.state.isPopUp}
              toggle={this.hideModal}
              state={this.state}
              onSubmit={this.submitFormHandler}
              onChange={this.inputChangeHandler}
            />
          }
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user,
  users: state.users.allUsers
});

const mapDispatchToProps = dispatch => ({
  onFetchUsers: () => dispatch(getAllUsers()),
  onUserPatch: (id, role, password, status) => dispatch(patchUser(id, role, password, status)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);