import React from 'react';
import {
  Button,
  Col,
  CustomInput,
  Form,
  FormGroup,
  FormText,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalHeader,
  Row
} from "reactstrap";

const PopUp = ({user, isOpen, toggle, state, onSubmit, onChange}) => {
  return (
    <Modal isOpen={isOpen} toggle={toggle}>
      <ModalHeader toggle={toggle}>Назначьте статус, роль и пароль пользователю</ModalHeader>
      <ModalBody>
        <p>Пользователь: {state.user.name}</p>
        <p>Роль: {state.role}</p>
        <p>Статус: {state.status}</p>
        <Form onSubmit={onSubmit}>
          <Row form>
            <Col sm={12}>
              <FormGroup check style={{marginBottom: '16px', padding: 0}}>
                <Label check>Измените статус пользователя</Label>
                <Row>
                  <div className="RadioBox-input">
                    <CustomInput
                      required
                      sm={6}
                      type="radio"
                      id="active"
                      name="status"
                      label="Активный"
                      checked={state.status === 'active'}
                      value={'active'}
                      onChange={onChange}
                    />
                  </div>
                  <div className="RadioBox-input">
                    <CustomInput
                      required
                      sm={6}
                      type="radio"
                      id="inactive"
                      name="status"
                      label="Не активный"
                      checked={state.status === 'inactive'}
                      value={'inactive'}
                      onChange={onChange}
                    />
                  </div>
                </Row>
              </FormGroup>
            </Col>
            {user.role === 'admin' ?
              <Col sm={5}>
                <FormGroup disabled>
                  <Input
                    required
                    type="select"
                    name="role"
                    value={state.role}
                    onChange={onChange}
                  >
                    <option>admin</option>
                    <option>user</option>
                    <option>project manager</option>
                  </Input>
                  <FormText color="muted">
                    Выберите роль пользователю
                  </FormText>
                </FormGroup>
              </Col> : null}
            {user.role === 'admin' ?
              <Col sm={7}>
                <FormGroup>
                  <Input
                    type="password"
                    name="password"
                    placeholder="Назначьте пароль"
                    value={state.password}
                    onChange={onChange}
                  />
                  <FormText color="muted">
                    Назначьте пароль пользователю
                  </FormText>
                </FormGroup>
              </Col>
              : null}
            <Col>
              <Button sm={12} className='bot-btn-st' color={'primary'}>
                <i className="far fa-save"/> Сохранить
              </Button>
            </Col>
          </Row>
        </Form>
      </ModalBody>
    </Modal>
  );
};

export default PopUp;