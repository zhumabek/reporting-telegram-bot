import React, {Component, Fragment} from 'react';
import ProjectForm, {FormGroupInput} from "../../components/ProjectForm/ProjectForm";
import {connect} from "react-redux";
import {addProject, editProject, getProjectById} from "../../store/actions/projectActions";
import {Button, Card, CardBody, CardHeader, Col, Container, CustomInput, Form, Input, Label, Row} from "reactstrap";
import DatePicker from "../../components/DatePicker";
import {
  BASIC_PROJECT_ENUM,
  BASIC_PROJECT_ENUM_DESCRIPTION,
  convertFromHourToDate,
  formatTime,
  JIRA_PROJECT_ENUM,
  JIRA_PROJECT_ENUM_DESCRIPTION,
  TIME_PICKER_MAX_TIME,
  TIME_PICKER_MIN_TIME
} from "../../utils/helpers";

import '../../assets/css/Button.css';

class AddProject extends Component {
  state = {
    name: '',
    chatId: '',
    reportsSendTime: null,
    isChecked: true,
    showNotificationSwitcher: false,
    projectType: BASIC_PROJECT_ENUM,

    domainName: '',
    apiAccessEmail: '',
    apiAccessToken: '',
    projectKey: '',
  };

  async componentDidMount() {
    let projectId = this.props.match.params.id;
    if (projectId) {
      await this.props.onGetProject(projectId);

      let {data, credentials} = this.props.project;
      this.setState({
        name: data.name,
        chatId: data.chatId,
        projectType: data.pmtType,
        reportsSendTime: convertFromHourToDate(data.reportsSendTime),
        domainName: credentials && credentials.domainName,
        apiAccessEmail: credentials && credentials.apiAccessEmail,
        apiAccessToken: credentials && credentials.apiAccessToken,
        projectKey: credentials && credentials.key,
      });
    }
  }

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();
    let projectId = this.props.match.params.id;
    let reportsSendTime = formatTime(this.state.reportsSendTime, 'LT');
    let {projectType, name, chatId, isChecked, domainName, apiAccessEmail, apiAccessToken, projectKey} = this.state;

    let dataToSave = {
      name,
      chatId,
      reportsSendTime: reportsSendTime,
      isCheckedToNotify: isChecked,
      pmtType: projectType
    };

    if (projectType === JIRA_PROJECT_ENUM) {
      dataToSave.pmtType = projectType;
      dataToSave.key = projectKey;
      dataToSave.domainName = domainName;
      dataToSave.apiAccessEmail = apiAccessEmail;
      dataToSave.apiAccessToken = apiAccessToken;
    }

    if (projectId) {
      this.props.updateProject(dataToSave, projectId);
    } else {
      this.props.addProject(dataToSave);
    }
  };

  render() {
    let isEditMode = this.props.match.params.id;
    let {
      name, chatId, projectType,
      apiAccessToken, domainName,
      apiAccessEmail, projectKey
    } = this.state;

    return (
      <Container>
        <Card>
          <CardHeader>
            {isEditMode ? 'Редактирование проекта' : 'Добавление проекта'}
          </CardHeader>
          <CardBody>
            <Form onSubmit={this.submitFormHandler}>
              <FormGroupInput labelSize={3} inputSize={7} label='Наименование проекта'>
                <ProjectForm
                  type='text'
                  inputName='name'
                  value={name}
                  changeInput={this.inputChangeHandler}
                  placeholder="Введите имя проекта"
                />
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={7} label='Введите сhatId телеграм группы в форме "-chatId"'>
                <ProjectForm
                  type='number'
                  value={chatId}
                  inputName='chatId'
                  changeInput={this.inputChangeHandler}
                  placeholder="Id чата"
                />
              </FormGroupInput>
              <FormGroupInput labelSize={3} inputSize={7} label='Выберите время для отправки отчетов в группу'>
                <DatePicker
                  selected={this.state.reportsSendTime}
                  onChange={date => this.setState({reportsSendTime: date, showNotificationSwitcher: true})}
                  locale="ru"
                  showTimeSelect
                  showTimeSelectOnly
                  minTime={TIME_PICKER_MIN_TIME}
                  maxTime={TIME_PICKER_MAX_TIME}
                  timeFormat="HH:mm"
                  timeIntervals={60}
                  timeCaption="Time"
                  placeholderText="Выберите время"
                  dateFormat="HH:mm"
                  required
                />
              </FormGroupInput>
              {isEditMode && this.state.showNotificationSwitcher ? (
                <FormGroupInput labelSize={3} inputSize={7} className='align-items-center'
                                label='Уведомлять участников об изменении времени?'>
                  <Label className="switch switch-3d switch-primary switch-pill">
                    <Input type="checkbox" className="switch-input"
                           checked={this.state.isChecked}
                           onChange={() => this.setState(prevState => ({isChecked: !prevState.isChecked}))}/>
                    <span className="switch-slider"/>
                  </Label>
                </FormGroupInput>
              ) : null}
              <FormGroupInput labelSize={3} inputSize={9} label='Вид проекта' className='align-items-center'>
                <Label check className='pr-5 d-inline-flex'>
                  <CustomInput
                    id='traditional'
                    type='radio'
                    name='projectType'
                    checked={projectType === BASIC_PROJECT_ENUM}
                    value={BASIC_PROJECT_ENUM}
                    disabled={isEditMode}
                    onChange={(event) => this.setState({projectType: parseInt(event.target.value)})}
                  />
                  {BASIC_PROJECT_ENUM_DESCRIPTION}
                </Label>
                <Label check className='d-inline-flex'>
                  <CustomInput
                    id='jira'
                    name='projectType'
                    type='radio'
                    checked={projectType === JIRA_PROJECT_ENUM}
                    value={JIRA_PROJECT_ENUM}
                    disabled={isEditMode}
                    onChange={(event) => this.setState({projectType: parseInt(event.target.value)})}
                  />
                  {JIRA_PROJECT_ENUM_DESCRIPTION}
                </Label>
              </FormGroupInput>

              {projectType === JIRA_PROJECT_ENUM ?
                <Fragment>
                  <FormGroupInput labelSize={3} inputSize={7} label='Unique ключ проекта'>
                    <ProjectForm
                      type='text'
                      value={projectKey}
                      inputName='projectKey'
                      changeInput={this.inputChangeHandler}
                      placeholder="ключ проекта"
                      required
                    />
                  </FormGroupInput>
                  <FormGroupInput labelSize={3} inputSize={7} label='Доменное имя'>
                    <ProjectForm
                      type='text'
                      value={domainName}
                      inputName='domainName'
                      changeInput={this.inputChangeHandler}
                      placeholder="доменное имя"
                      required
                    />
                  </FormGroupInput>
                  <FormGroupInput labelSize={3} inputSize={7} label='E-mail'>
                    <ProjectForm
                      type='email'
                      value={apiAccessEmail}
                      inputName='apiAccessEmail'
                      changeInput={this.inputChangeHandler}
                      placeholder="email@example.com"
                      required
                    />
                  </FormGroupInput>
                  <FormGroupInput labelSize={3} inputSize={7} label='API токен'>
                    <ProjectForm
                      type='text'
                      value={apiAccessToken}
                      inputName='apiAccessToken'
                      changeInput={this.inputChangeHandler}
                      placeholder="api token"
                      required
                    />
                  </FormGroupInput>
                </Fragment> : null}

              <Row>
                <Col className='py-1'>
                  <Button className='bot-btn-st' color="danger" onClick={() => this.props.history.goBack()}>Назад</Button>{' '}
                  <Button className='bot-btn-st' type="submit" color="primary">Сохранить проект</Button>{' '}
                  {isEditMode ? (
                    <Button color="secondary"
                            className='bot-btn-st'
                            onClick={() => {
                              this.props.history.push(`/project/${this.props.match.params.id}/members`)
                            }}>
                      Добавить участников
                    </Button>
                  ) : null}
                </Col>
              </Row>
            </Form>
          </CardBody>
        </Card>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    project: state.projects.projectById
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProject: id => dispatch(getProjectById(id)),
    addProject: data => dispatch(addProject(data)),
    updateProject: (data, id) => dispatch(editProject(data, id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddProject);
