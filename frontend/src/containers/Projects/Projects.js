import React, {Component} from 'react';
import AppTable from "../../components/AppTable";
import {Col, Row, Button, Container, Spinner} from "reactstrap";
import {NavLink} from "react-router-dom";
import {connect} from "react-redux";
import * as actions from '../../store/actions/projectActions';

class Projects extends Component {
  state = {
    status: '',
  };

  componentDidMount() {
    this.props.onGetProjects('all')
  }


  render() {
    if (!this.props.projects) {
      return <Spinner color="success"/>
    }
    const columns = [
      {
        Header: "Список проектов", columns: [
          {
            Header: "№", accessor: "index", width: 50,
            Cell: (row) => (
              <p>{row.index + 1}</p>
            )
          },
          {Header: "Наименование", accessor: "name"},
          {Header: "Вид проекта", accessor: "pmtType", width: 250},
          {
            Header: "Статус", width: 100, filterable: false, sortable: false,
            Cell: (row) => (
              <p>{row.original.isClosed === false ? 'active' : 'inactive'}</p>
            )
          },
          {
            filterable: false, sortable: false, width: 172, getTdProps: true,
            Cell: (row) => (
              row.original.isClosed === false ?
              <Button
                className='bot-btn-st'
                sm={2}
                color={'danger'}
                onClick={(e) => {e.stopPropagation(); this.props.onCloseProject(row.original._id, 'inactive')}}
              >
                Деактивировать
              </Button> :
                <Button
                  className='bot-btn-st'
                  sm={2}
                  color={'primary'}
                  onClick={(e) => {e.stopPropagation(); this.props.onCloseProject(row.original._id, 'active')}}
                >
                  Активировать
                </Button>
            )
          }

        ]
      }
    ];

    return (
      <div>
        <Container>
          <Row>
            <Col xs={12}>
              <NavLink to={'/project'}>
                <Button color='primary' className='bot-btn-st' style={{marginBottom: '9px'}}>
                  Добавить проект
                </Button>
              </NavLink>
            </Col>
            <Col xs={12}>
              <AppTable
                columns={columns}
                data={this.props.projects}
                getTdProps={(state, rowInfo) => {
                  return {
                    onClick: () => {
                      if (rowInfo) {
                        this.props.history.push('/project/' + rowInfo.original._id)
                      }
                    }
                  };
                }}
              />
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    projects: state.projects.projects,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProjects: (status) => dispatch(actions.getProjects(status)),
    onCloseProject: (id, status) => dispatch(actions.closeProject(id, status)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Projects)