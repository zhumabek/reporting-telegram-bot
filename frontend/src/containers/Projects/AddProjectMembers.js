import React, {Component} from 'react';
import {FormGroupInput} from "../../components/ProjectForm/ProjectForm";
import {connect} from "react-redux";
import {
  addProjectMember,
  addProjectMemberNotNotification,
  deleteProjectMember,
  deleteProjectMemberNotNotification,
  getJiraUsers,
  getNotMemberUsers,
  getProjectById,
  getProjectMembers,
  getProjectMembersNotNotification,
} from "../../store/actions/projectActions";
import {Button, Card, CardBody, CardHeader, Col, Container, CustomInput, FormGroup, Row} from "reactstrap";
import AppTable from "../../components/AppTable";
import Select from "../../components/Select";
import AppModal from "../../components/AppModal";
import {JIRA_PROJECT_ENUM} from "../../utils/helpers";

import '../../assets/css/Button.css';

class AddProjectMembers extends Component {
  state = {
    selectedUser: null,
    showModal: false,
    selectedJiraUser: null,
  };

  async componentDidMount() {
    const projectId = this.props.match.params.id;
    await Promise.all([
      this.props.onGetProject(projectId),
      this.props.getProjectMembers(projectId),
      this.props.getNotMemberUsers(projectId),
      this.props.onSetProjectMembersNotNotification(projectId)
    ]);

    const projectPmtType = this.props.project.data.pmtType;
    if (projectPmtType === JIRA_PROJECT_ENUM) {
      await this.props.getJiraUsers(projectId);
    }
  }

  onAddUserClick = () => {
    const projectPmtType = this.props.project.data.pmtType;
    if (projectPmtType === JIRA_PROJECT_ENUM) {
      this.toggle();
    } else {
      this.addMember();
    }
  };

  jiraUserSelectHandler = () => {
    const projectId = this.props.match.params.id;
    this.addMember();
    this.props.getNotMemberUsers(projectId);
    this.toggle();
  };

  addMember = () => {
    const projectId = this.props.match.params.id;
    const {selectedUser, selectedJiraUser} = this.state;
    const data = {
      userId: selectedUser._id,
      pmtUserIdentifier: selectedJiraUser && selectedJiraUser.accountId,
    };
    this.props.onAddUser(projectId, data);
    this.setState({selectedUser: null, selectedJiraUser: null});
  };

  toggle = () => {
    this.setState(prevState => ({showModal: !prevState.showModal}));
  };

  canAddUser() {
    return this.state.selectedUser;
  }

  canConfirmJiraUserAddition() {
    return this.state.selectedJiraUser;
  }

  onRemoveUserClick = (userId) => {
    const projectId = this.props.match.params.id;
    this.props.onRemoveUser(projectId, userId);
    this.props.onRemoveUserNotNotification(userId);
  };

  setReportsOptional = (userId) => {
    const projectId = this.props.match.params.id;
    const data = {userId: userId};
    if (!this.isCheckedUserNotNotification(userId)) {
      this.props.onAddUserNotNotification(projectId, data);
    } else {
      this.props.onRemoveUserNotNotification(projectId, userId);
    }
  };

  isCheckedUserNotNotification = (id) => {
    for (const userNotNotification of this.props.notNotification) {
      const userChecked = id === userNotNotification._id;
      if (userChecked === true) return userChecked;
    }
  };

  render() {
    const columns = [
      {
        Header: "Добавленные к проекту пользователи",
        columns: [
          {Header: "Наименование", accessor: "name"},
          {Header: "E-mail", accessor: "email"},
          {
            width: 182, filterable: false, getTdProps: true, sortable: false,
            Cell: row => (
              row.original.role === 'user'
                ? <FormGroup>
                  <div>
                    <CustomInput
                      type="checkbox"
                      id="reportsOptional"
                      label="Отчет необязателен"
                      defaultChecked={this.isCheckedUserNotNotification(row.original._id)}
                    />
                  </div>
                </FormGroup>
                : null
            )
          },
          {
            width: 40, filterable: false,
            Cell: row => {
              return <Button size={'sm'}
                             color={'danger'}
                             title={'Вы действительно хотите удалить пользователя из проекта'}
                             onClick={(e) => {
                               e.stopPropagation();
                               this.onRemoveUserClick(row.original._id)
                             }}>
                <i className="fa fa-minus"/>
              </Button>;
            }
          }
        ]
      }
    ];

    let {selectedUser, showModal, selectedJiraUser} = this.state;

    return (
      <Container>
        <Card>
          <CardHeader>
            Добавление участников к проекту
          </CardHeader>
          <CardBody>
            <FormGroupInput labelSize={3} inputSize={9} label='Выберите пользователя'>
              <Row>
                <Col sm={9}>
                  <Select
                    options={this.props.notMemberUsers}
                    placeholder={'Поиск...'}
                    valueKey="_id" labelKey="name"
                    isSearchable
                    name='Выберите пользователя'
                    value={selectedUser}
                    onChange={selectedUser => this.setState({selectedUser})}
                  />
                </Col>
                <Col sm={3}>
                  <Button color='primary'
                          className='bot-btn-st'
                          name='Add user to the current project'
                          onClick={this.onAddUserClick}
                          disabled={!this.canAddUser()}>
                    <span className='fa fa-plus'/>{' '}Добавить
                  </Button>
                </Col>
              </Row>
            </FormGroupInput>
            <Row>
              <Col sm={12}>
                <AppTable
                  columns={columns}
                  data={this.props.addedUsers}
                  showRowNumbers
                  pageSize={5}
                  getTdProps={(state, rowInfo) => {
                    return {
                      onClick: () => {
                        if (rowInfo && rowInfo.original.role === 'user') {
                          this.setReportsOptional(rowInfo.original._id)
                        }
                      }
                    };
                  }}
                />
              </Col>
              <Col sm={12} className='mt-2 d-flex justify-content-lg-end'>
                <Button color='danger' className='bot-btn-st' onClick={() => this.props.history.goBack()}>
                  Назад
                </Button>
              </Col>
            </Row>
          </CardBody>
        </Card>
        {showModal ?
          <AppModal isOpen={showModal}
                    toggle={this.toggle}
                    onConfirm={this.jiraUserSelectHandler}
                    size='lg'
                    canConfirm={!this.canConfirmJiraUserAddition()}>
            <FormGroupInput labelSize={3} inputSize={9} label='Выберите пользователя'>
              <Select
                options={this.props.jiraUsers}
                placeholder='Выберите пользователя'
                valueKey="accountId" labelKey="displayName"
                isSearchable
                name='Выберите пользователя'
                value={selectedJiraUser}
                onChange={selectedJiraUser => this.setState({selectedJiraUser})}
              />
            </FormGroupInput>
          </AppModal> : null}
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    project: state.projects.projectById,
    notMemberUsers: state.projects.notMemberUsers,
    addedUsers: state.projects.addedUsers,
    jiraUsers: state.projects.jiraUsers,
    notNotification: state.projects.notNotification,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onGetProject: (id) => dispatch(getProjectById(id)),
    getProjectMembers: (id) => dispatch(getProjectMembers(id)),
    getNotMemberUsers: (id) => dispatch(getNotMemberUsers(id)),
    getJiraUsers: (id) => dispatch(getJiraUsers(id)),
    onAddUser: (id, data) => dispatch(addProjectMember(id, data)),
    onRemoveUser: (projectId, userId) => dispatch(deleteProjectMember(projectId, userId)),
    onSetProjectMembersNotNotification: (id) => dispatch(getProjectMembersNotNotification(id)),
    onAddUserNotNotification: (projectId, data) => dispatch(addProjectMemberNotNotification(projectId, data)),
    onRemoveUserNotNotification: (projectId, userId) => dispatch(deleteProjectMemberNotNotification(projectId, userId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddProjectMembers);
