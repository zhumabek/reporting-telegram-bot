import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, FormText, Input, Label} from "reactstrap";
import {loginUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";

import '../../assets/css/Button.css';

class Login extends Component {
  state = {
    someLogin: '',
    password: ''
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.loginUser({...this.state});
  };

  render() {
    return (
      <Fragment>
        <Form onSubmit={this.submitFormHandler}>
          <Col sm={{offset: 3, size: 6}}>
            <FormGroup row>
              <Label sm={2} for="login">Логин</Label>
              <Col sm={10}>
                <Input
                  type="text"
                  name="someLogin"
                  id="login"
                  placeholder="Введите логин"
                  required
                  value={this.state.someLogin}
                  onChange={this.inputChangeHandler}
                />
                <FormText color="muted">
                  (телефон в формате: +996ХХХХХХХХХ или Email: username@domain.XX)
                </FormText>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Label sm={2} for="password">Пароль</Label>
              <Col sm={10}>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Введите пароль"
                  required
                  value={this.state.password}
                  onChange={this.inputChangeHandler}
                />
              </Col>
            </FormGroup>
            <Button color={'primary'} className='bot-btn-st'>
              <i className="fa fa-fw fa-user"/>{'Войти'}
            </Button>
          </Col>
        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.loginError
});

const mapDispatchToProps = dispatch => ({
  loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);