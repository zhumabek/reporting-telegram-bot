Telegram bot for reporting (for the local software company *****)
The company's developers need to write a daily report on completed tickets during work day. All information should be collected in a table of general reports for clarity and analysis of the work of developers. All information is manually recorded in general reports, which is an inconvenience, takes time, and there is also the likelihood of errors due to the human factor. We made a bot through which developers will send their daily reports on completed tickets, then these reports will be automatically collected in tables and sent to the right place.

The application consists of the following components:
- Rest API  with database(Node.js + express.js + MongoDB)
- telegram bot(Telegraf.js)
- admin panel(React + Redux + Core UI)
- unit tests(mocha+chai)
- acceptance tests(codecept.js + Selenium WebDriver)
- CI/CD(Jenkins + Docker)
