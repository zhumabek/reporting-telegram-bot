exports.config = {
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'http://localhost:3010',
      show: !process.env.CI
    },
    LoginHelper: {
      require: './helpers/login.js',
    },
    notificationHelper: {
      require: './helpers/notification.js',
    },
  },
  include: {
    I: './steps_file.js'
  },
  mocha: {},
  bootstrap: null,
  teardown: null,
  hooks: [],
  gherkin: {
    features: './features/*.feature',
    steps: [
    './step_definitions/addJiraProject.js',
    './step_definitions/addProject.js',
    './step_definitions/addMembersToTheProject.js',
    './step_definitions/addReport.js',
    './step_definitions/editReport.js',
    './step_definitions/login.js',
    './step_definitions/editProject.js',
    './step_definitions/editJiraProject.js',
    './step_definitions/viewTickets.js',
    './step_definitions/viewReports.js',
    './step_definitions/saveReports.js',
    './step_definitions/changeRoleUser.js',
    './step_definitions/checksForLoggedInUserAndRights.js',
    './step_definitions/checkThatISeeThePasswordField.js',
    './step_definitions/saveButtonIsNotActive.js',
    './step_definitions/changeStatusUserPM.js',
    './step_definitions/checkThatISeeAssignAPassword.js',
    './step_definitions/checkThatTheTimeIsCorrect.js',
    './step_definitions/checkErrorsAuthorization.js',
    './step_definitions/viewInfoSingleTicket.js',
    './step_definitions/viewInfoSingleReport.js',
    './step_definitions/changeStatusForProject.js',
    './step_definitions/saveReportsOfDeveloper.js',
    './step_definitions/addAdditionalReport.js',
    './step_definitions/editAdditionalReport.js',
    './step_definitions/viewTicketsUser.js',
    './step_definitions/saveReportsOfDeveloperUser.js',
    './step_definitions/paginationCheck.js',
    './step_definitions/checkUsersReportsOptional.js',
    ]
  },
  plugins: {
    screenshotOnFail: {
      enabled: true
    }
  },
  tests: './*_test.js',
  name: 'tests'
};