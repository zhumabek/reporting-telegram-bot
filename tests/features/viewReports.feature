# language: ru

Функция: Просмотр списка отчетов в разделе "Отчеты"
  @viewReports
  Сценарий: Успешный просмотр списка отчетов в разделе "Отчеты"
    Допустим я авторизованный пользователь "SuperAdmin"
    И я перехожу на страницу "/reports"
    И я выбираю отчеты "По проекту"
    И я нажимаю на поисковик
    И я ввожу "Project A" в поисковике
    И я вижу пользователя "rysbai_coder" в списке отчетов
    И я вижу номер тикета "1" в списке отчетов
    И я вижу описание отчета "Report-2" в списке отчетов
    И я вижу время "2" в списке отчетов
    И я вижу статус "closed" в списке отчетов
    И я ввожу в "dateFrom" дату "11.11.2019"
    И я выбираю "dateBefore"
    И я ввожу в "dateBefore" дату "12.12.2019"
    И я чтобы посмотреть отчеты нажимаю на кнопку "Посмотреть"
    И я вижу пользователя с именем "evlmnd"
    И я вижу тикет с номером "3"
    И я вижу описание отчета "Report-5"
    И я вижу потраченное время "2"
    То я вижу тикет статус "closed"



