# language: ru


Функция: Добавление нового проекта project managers
  @addProjectPM
  Сценарий: Успешное добавление нового проекта project managers
    Допустим я авторизованный пользователь "zhumabek12"
    И я вижу кнопку "Добавить проект" и нажимаю ее
    И я вижу текст "Добавление проекта"
    И я ввожу "lolkek" в поле "Введите имя проекта"
    И я ввожу "-321043559" в поле "Id чата"
    И я вижу и нажимаю "Выберите время" для выбора времени отправки отчетов
    И я ввожу время "15:31" в поле "Выберите время"
    И нажимаю на кнопку "Сохранить проект"
    То я вижу текст "Project was successfully created"