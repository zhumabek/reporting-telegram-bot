# language: ru


Функция: Добавление нового проекта
  @addProject
  Сценарий: Успешное добавление нового проекта
    Допустим я авторизованный пользователь "SuperAdmin"
    И я вижу кнопку "Добавить проект" и нажимаю ее
    И я вижу текст "Добавление проекта"
    И я ввожу название "Test project" в поле "Введите имя проекта"
    И я ввожу "-321025269" в поле "Id чата"
    И я вижу и нажимаю "Выберите время" для выбора времени отправки отчетов
    И я ввожу время "15:21" в поле "Выберите время"
    И я выбираю вид проекта "Обычный проект"
    И нажимаю на кнопку "Сохранить проект"
    То я вижу текст "Project was successfully created"