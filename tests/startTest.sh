#!/bin/bash

cd ../server

NODE_ENV=test npm run seed

cd ../tests

npx codeceptjs run -f $1 --steps
