#!/bin/bash
set -e
CURRENT_DIR=`dirname $0`
cd "$CURRENT_DIR"
CURRENT_DIR=`pwd`

cd "$CURRENT_DIR/../server"
npm run seed:test

pm2 start "npm run start:test" --name="bot-reporting-app-server-test"

cd "$CURRENT_DIR/../frontend"

pm2 start "npm run start:test" --name="bot-reporting-app-frontend-test"

echo "$CURRENT_DIR"
cd "$CURRENT_DIR"

while ! nc -z localhost 3010; do
  sleep 0.1 # wait for 1/10 of the second before check again
done

npm start

pm2 kill
