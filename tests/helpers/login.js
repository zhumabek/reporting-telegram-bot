
const Helper = codeceptjs.helper;

const userData = {
  'SuperAdmin': {
    'login': '+996550672597',
    'password': 'admin345Aa'
  },
  'zhumabek12': {
    'login': '+996555333333',
    'password': 'pm12345'
  },
  'evlmnd': {
    'login': '+996555222222',
    'password': '123456'
  }
};



class LoginHelper extends Helper {

  async loginAsUser(name) {
    const user = userData[name];

    if (!user) throw new Error('No such user is known! Check helpers/login.js');

    const I = this.helpers['Puppeteer'];

    await I.amOnPage('/login');

    const alreadyLoggedIn = await I._locate(`//h6[contains(., 'Привет, ${name}!')]`);

    if (alreadyLoggedIn.length > 0) {
      return;
    }

    await I.fillField(`//input[@id='login']`, user.login);
    await I.fillField(`//input[@id='password']`, user.password);

    await I.click(`//button[.='Войти']`);

    await I.waitForText(`Привет, ${name}!`, 6);
  }
}

module.exports = LoginHelper;
