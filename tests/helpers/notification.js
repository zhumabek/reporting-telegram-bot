const Helper = codeceptjs.helper;

class notificationHelper extends Helper {

  async seeNotification(text) {
    const I = this.helpers['Puppeteer'];
    await I.waitForElement(`//div[@class='notification-container notification-container-empty']/span[.='${text}']`, 7);
  };

}

module.exports = notificationHelper;