const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я перехожу на страницу {string}', (fieldName) => {
  I.amOnPage(`${fieldName}`);
});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я нажимаю на поисковик', (fieldName) => {
  I.click(`//div[contains(@class,'css-1hwfws3')]`);
  I.wait(1);
});

When('я ввожу {string} в поисковике', (fieldName) => {
  I.fillField(`//div[@class='css-b8ldur-Input']`, fieldName);
  I.pressKey("Enter");
  I.wait(1);
});

When('я вижу пользователя {string} в списке отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу номер тикета {string} в списке отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу описание отчета {string} в списке отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу время {string} в списке отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу статус {string} в списке отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я ввожу в {string} дату {string}', (fieldName, text) => {
  I.fillField(`//input[@name='${fieldName}']`, text);
  I.pressKey("Enter");
});

When('я выбираю {string}', (fieldName) => {
  I.click(`//input[@name='${fieldName}']`)
});

When('я ввожу в {string} дату {string}', (fieldName, text) => {
  I.fillField(`//input[@name='${fieldName}']`, text);
  I.pressKey("Enter");
});

When('я чтобы посмотреть отчеты нажимаю на кнопку {string}', (buttonName) => {
  I.click(`//button[.='${buttonName}']`);
});

When('я вижу описание {string} в списке отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу пользователя с именем {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу тикет с номером {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу описание отчета {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу потраченное время {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

Then('я вижу тикет статус {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});