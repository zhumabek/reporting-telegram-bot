const {I} = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я вижу проект {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 3);
});

When('я выбираю проект {string}, чтобы отредактировать', (text) => {
  I.wait(3);
  I.click(`//div[contains(text(),'${text}')]`);
});

When('я ввожу  в поле {string} имя проекта {string}', (fieldName, text) => {
  I.fillField(`//input[@name='${fieldName}']`, text)
});

When('я ввожу в поле {string} новый чат Id {string}', (fieldName, text) => {
  I.fillField(`//input[@name='${fieldName}']`, text)
});

When('я выбираю {string} в поле {string}', (text, fieldName) => {
  I.waitForElement(`//div[contains(., '${fieldName}')]`, 1);
});

When('я вижу поле время и нажимаю, чтобы выбрать время отправки отчетов', () => {
  I.waitForElement(`//input[@placeholder='Выберите время']`, 1);
  I.wait(1);
});

When('я ввожу время {string} в поле {string}', (text, fieldName) => {
  I.clearField(`//input[@placeholder='${fieldName}']`);
  I.waitForElement(`//input[@placeholder='${fieldName}']`, 1);
  I.fillField(`//input[@placeholder='${fieldName}']`, text);
  I.pressKey('Enter');
});

When('нажимаю на кнопку {string}', (text) => {
  I.wait(3);
  I.click(`//button[.='${text}']`);
});

Then('я вижу текст {string}', (text) => {
  I.waitForElement(`//div[contains(text(), '${text}')]`, 5);
});