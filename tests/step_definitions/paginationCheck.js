const { I } = inject();
Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я перехожу на страницу {string}', (pageUrl) => {
  I.amOnPage(`${pageUrl}`);
});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я ввожу {string} в поисковике', (fieldName) => {
  I.click(`//div[contains(@class,'css-1hwfws3')]`);
  I.fillField(`//div[@class='css-b8ldur-Input']`, fieldName);
  I.pressKey("Enter");
});

When('я выбираю отображение 5 строк на странице', () => {
  I.click(`//span[@class='select-wrap -pageSizeOptions']//select`);
  I.waitForElement(`//span[@class='select-wrap -pageSizeOptions']//option[contains(text(), '5 строк')]`, 1);
  I.fillField(`//span[@class='select-wrap -pageSizeOptions']//option`, '5 строк');
});

When('я переключаюсь на 2 страницу', () => {
  I.click(`//button[contains(text(),'>')]`);
  I.seeElement(`//div[@class='-pageJump']//input`);
});

When('я выбераю другого пользователя {string} в поисковике', (fieldName) => {
  I.click(`//div[contains(@class,'css-1hwfws3')]`);
  I.fillField(`//div[@class='css-b8ldur-Input']`, fieldName);
  I.pressKey("Enter");
});

Then('я не вижу страницу 2', () => {
  I.seeElement(`//div[@class='-pageJump']//input`);
  I.dontSeeInField(`//div[@class='-pageJump']//input`, '2');
});
