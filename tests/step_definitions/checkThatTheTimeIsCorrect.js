const {I} = inject();

When('я выбираю проект {string}, чтобы отредактировать его', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 3);
  I.click(`//div[contains(text(), '${fieldName}')]`);
});

Then('я вижу время {string}', (text) => {
  I.waitForElement(`//input[@value='${text}']`, 5);
});