const { I } = inject();
Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я перехожу на страницу {string}', () => {
  I.amOnPage('/reports');
});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я вижу имя авторизованого пользователя', () => {
  I.waitForElement(`//div[contains(text(), 'evlmnd')]`, 1);
});

When('я для просмотра отчетов по выбранным датам нажимаю на кнопку {string}', (btnName) => {
  I.click(`//button[.='${btnName}']`);
});

Then('я чтобы сохранить отчет нажимаю на кнопку {string}', (btnName) => {
  I.click(`//button[.='${btnName}']`);
});
