const { I } = inject();

When('я нажимаю на вкладку {string}', (name) => {
  I.click(`//a[contains(., '${name}')]`);
});

When('я вижу {string}', (name) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 1);
});

When('я нахожу пользователя {string} и нажимаю на кнопку "Изменить"', (name) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 1);
  I.click(`//div[contains(text(),'${name}')]/ancestor::div[@class='rt-tr-group']/descendant::button[contains(text(), 'Изменить')]`);
});

Then('я вижу поле {string}', (text) => {
  I.waitForElement(`//input[@placeholder='${text}']`, 5);
});