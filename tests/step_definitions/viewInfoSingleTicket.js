const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я нажимаю на меню "Тикеты"', () => {
  I.click(`//li[3]//a[1]`);
  I.amOnPage(`/tickets`);
});

When('я вижу таблицу {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я нажимаю на поле {string}', (fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.wait(1);
});

When('я ввожу значение {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//div[contains(text(),'${fieldName}')]`, text);
  I.pressKey("Enter");
  I.wait(1);
});

When('я нажимаю на кнопку выбора проекта {string}', () => {
  I.click(`//div[@class='col-sm-3']//button[@class='btn btn-primary']`);
  I.wait(1);
});

When('я вижу номер тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
  I.waitForElement(`//div[contains(text(),'Create fixtures')]`, 1);
  I.waitForElement(`//div[contains(text(),'rysbai_coder')]`, 1);
  I.waitForElement(`//div[contains(text(),'closed')]`, 1);
});

When('я нажимаю на тикет {string} в таблице "Список тикетов к проекту"', (text) => {
  I.click(`//div[contains(text(),'${text}')]`);
});

Then('я вижу модальное окно {string}{string}', (text, number) => {
  I.waitForElement(`//h5[@class='modal-title' and text()='${text}' and text()='${number}']`, 1);
  I.waitForElement(`//h5[text()='Fix something']`, 1);
  I.waitForElement(`//p[text()='rysbai_coder']`, 1);
});
