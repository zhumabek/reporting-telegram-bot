const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я вижу кнопку {string} и нажимаю ее', (name) => {
  I.click(`//button[.='${name}']`);
});

When("я вижу текст {string}", (text) => {
  I.waitForElement(`//h2[contains(., '${text}')]`, 1);
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text);
  I.pressKey('Enter');
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text);
  I.pressKey('Enter');
});

When('я вижу и нажимаю {string} для выбора времени отправки отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(., '${fieldName}')]`, 1);
  I.click(`//div[@class='col-sm-7']//div[contains(@class,'css-1hwfws3')]`);
  I.pressKey('Enter');
});

When('я выбираю {string} из вариантов', (fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.pressKey('Enter');
});

When('я выбираю вид проекта {string}', (fieldName) => {
  I.click(`//label[contains(text(),'${fieldName}')]`);
});

Then('я нажимаю на кнопку {string}', (text) => {
  I.click(`//button[.='${text}']`);
  I.seeNotification('Project was successfully created');
});
