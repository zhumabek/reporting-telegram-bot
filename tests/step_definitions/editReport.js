const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я добавляю проект под наименованием {string} и к проекту добавляю пользователя {string}', (projectName, userName) => {

    I.click(`//button[contains(text(), 'Добавить проект')]`);

    I.fillField(`//input[@placeholder='Введите имя проекта']`, projectName);

    I.fillField(`//input[@placeholder='Id чата']`, "-55555555555");

    I.waitForElement(`//input[@placeholder='Выберите время']`, 1);

    I.fillField(`//input[@placeholder='Выберите время']`, "15:00");
    I.pressKey('Enter');

    I.click(`//button[.='Сохранить проект']`);
    I.waitForElement(`//div[contains(text(), 'Поиск...')]`, 3);
    I.click(`//div[contains(text(),'Поиск...')]`);
    I.fillField(`//input[@name='Выберите пользователя']`, userName);
    I.pressKey('Enter');

    I.click(`//span[@class='fa fa-plus']`);
});

When('я добавляю новый тестовый отчет по тикету {string} для проверки редактирования отчета', (ticketNumber) => {

    const data = {
      project: { field: 'Выберите проект', value: 'Проект для проверки редактирования отчета'},
      user: {field: 'Выберите пользователя', value: 'adievkadyrbek'},
      date: {field: 'Дата отчета', value: '15.01.2020'},
      ticketNumber: { field: 'Номер тикета', value: ticketNumber},
      ticketName: { field: 'Наименование тикета', value: 'Наименование тестого тикета'},
      reportDescription: { field: 'Описание отчета', value: 'Описание отчета'},
      timeSpent: { field: 'Потраченное время', value: '6'},
      status: { field: 'Выберите статус', value: 'in progress'},
    };

    I.click(`//div[contains(text(),'${data.project.field}')]`);
    I.fillField(`//input[@name='${data.project.field}']`, data.project.value);
    I.pressKey('Enter');

    I.click(`//div[contains(text(),'${data.user.field}')]`);
    I.wait(1);
    I.fillField(`//input[@name='${data.user.field}']`, data.user.value);
    I.pressKey('Enter');

    I.fillField(`//input[@placeholder='${data.date.field}']`, data.date.value);

    I.click(`//input[@placeholder='${data.ticketNumber.field}']`);
    I.fillField(`//input[@placeholder='${data.ticketNumber.field}']`, data.ticketNumber.value);

    I.click(`//input[@placeholder='${data.ticketName.field}']`);
    I.fillField(`//input[@placeholder='${data.ticketName.field}']`, data.ticketName.value);

    I.fillField(`//textarea[@placeholder='${data.reportDescription.field}']`, data.reportDescription.value);

    I.fillField(`//input[@placeholder='${data.timeSpent.field}']`, data.timeSpent.value);

    I.click(`//div[contains(text(),'${data.status.field}')]`);
    I.fillField(`//input[@name='${data.status.field}']`, data.status.value);
    I.pressKey('Enter');

    I.click(`//button[.='Сохранить отчет']`);
    I.waitForElement(`//div[contains(text(),'Список отчетов')]`, 3);


});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я выбираю проект {string} в выпадающем списке {string}', (value, fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

When('я вижу отчет под номером тикета {string} в таблице отчетов и нажимаю на отчет', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 2);
  I.click(`//div[contains(text(),'${fieldName}')]`);
});

When('я вижу модальное окно в котором есть кнопка "Отредактировать отчет" и нажимаю на него', () => {
  I.waitForElement(`//div[@class='modal-content']`, 2);
  I.click(`//div[@class='modal-footer']//button[@class='bot-btn-st btn btn-primary']`);
});

When('я ввожу новую дату отчета {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text);
  I.pressKey('Enter');
});

When('я в поле {string} ввожу новое значение {string}', (fieldName, value) => {
  I.click(`//input[@placeholder='${fieldName}']`);
  I.fillField(`//input[@placeholder='${fieldName}']`, value);
});

When('я в текстовом поле {string} ввожу новое значение {string}', (fieldName, value) => {
  I.click(`//textarea[@placeholder='${fieldName}']`);
  I.fillField(`//textarea[@placeholder='${fieldName}']`, value);
});

When('я меняю старый статус {string} новый статус тикета {string} в выпадающем списке {string}', (oldValue, value, fieldName) => {
  I.click(`//div[contains(text(),'${oldValue}')]`);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

Then('я теперь нахожусь на странице "Отчеты" и вижу таблицу {string}', (name) => {
   I.waitForElement(`//div[contains(text(),'${name}')]`, 3);
});


