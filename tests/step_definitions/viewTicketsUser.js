const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я нахожусь на странице {string}', (fieldName) => {
  I.waitInUrl(fieldName, 2);
});

When('я вижу таблицу {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я нажимаю на поле {string}', () => {
  I.click(`//div[contains(@class,'css-1hwfws3')]`);
  I.wait(1);
});

When('я ввожу значение {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@id='react-select-2-input']`, text);
  I.pressKey("Enter");
  I.wait(1);
});

When('я нажимаю на кнопку выбора проекта {string}', () => {
  I.click(`//div[@class='col-sm-3']//button[@class='bot-btn-st btn btn-primary']`);
  I.wait(1);
});

When('я не вижу пользователя тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.dontSeeElement(`//div[contains(text(),'${fieldName}')]`);
});

Then('я вижу пользователя тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.waitForElement(`//div[@class='rt-tr -odd']//div[@class='rt-td'][contains(text(),'${fieldName}')]`, 1);
});
