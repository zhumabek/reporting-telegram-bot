const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.wait(5);
  I.loginAsUser(name);
});

When('я вижу кнопку {string} и нажимаю ее', (name) => {
  I.click(`//button[contains(text(), '${name}')]`);
});

When("я вижу текст {string}", (text) => {
  I.waitForElement(`//h2[contains(text(), '${text}')]`, 1);
});

When('я ввожу название {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text)
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text)
});

When('я вижу и нажимаю {string} для выбора времени отправки отчетов', (fieldName) => {
  I.waitForElement(`//input[@placeholder='${fieldName}']`, 1);
});

When('я ввожу время {string} в поле {string}', (text, fieldName) => {
  I.waitForElement(`//input[@placeholder='${fieldName}']`, 1);
  I.fillField(`//input[@placeholder='${fieldName}']`, text);
  I.pressKey('Enter');
});

When('я выбираю вид проекта {string}', (fieldName) => {
  I.click(`//label[contains(text(),'${fieldName}')]`);
});

When('я нажимаю на кнопку {string}', (text) => {
  I.click(`//button[.='${text}']`);
});

Then('я вижу текст {string}', (text) => {
  I.waitForElement(`//div[contains(text(), '${text}')]`, 1);
});