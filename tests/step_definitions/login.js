const { I } = inject();

Given('я нохожусь на странице авторизации', () => {
  I.amOnPage('/login')
});

When('я ввожу {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text)
});

When('нажимаю на кнопку {string}', (text) => {
  I.click(`//button[.='${text}']`);
});

Then('я вижу имя пользователя {string}', (name) => {
  I.waitForElement(`//h6[contains(., 'Привет, ${name}!')]`, 10);
});