const { I } = inject();

When('я вижу заголовок {string}', (text) => {
  I.waitForElement(`//h5[contains(text(),'${text}')]`, 1);
});

Then('я вижу поле {string}', (text) => {
  I.waitForElement(`//input[@placeholder='${text}']`, 5);
});