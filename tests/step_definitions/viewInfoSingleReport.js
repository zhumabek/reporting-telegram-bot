const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я перехожу на страницу {string}', () => {
  I.amOnPage('/reports');
});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я нажимаю на поле {string}', (fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.wait(1);
});

When('я ввожу значение {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//div[contains(text(),'${fieldName}')]`, text);
  I.pressKey("Enter");
  I.wait(1);
});

When('я вижу пользователя {string} в списке отчетов', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
  I.waitForElement(`//div[contains(text(),'1')]`, 1);
  I.waitForElement(`//div[contains(text(),'Report-2')]`, 1);
  I.waitForElement(`//div[contains(text(),'2')]`, 1);
  I.waitForElement(`//div[contains(text(),'closed')]`, 1);
});

When('я нажимаю на отчет {string} в списке отчетов', (text) => {
  I.click(`//div[contains(text(),'${text}')]`);
});

Then('я вижу модальное окно {string}{string}', (text, number) => {
  I.waitForElement(`//h5[@class='modal-title' and text()='${text}' and text()='${number}']`, 1);
  I.waitForElement(`//p[text()='rysbai_coder']`, 1);
});