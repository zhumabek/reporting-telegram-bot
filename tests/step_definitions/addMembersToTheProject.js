const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.wait(5);
  I.loginAsUser(name);
});

When('я добавляю пользователя {string} в проект {string}', (userName, projectName) => {
  I.waitForElement(`//div[contains(text(),'${projectName}')]`, 3);
  I.click(`//div[contains(text(), '${projectName}')]`);
  I.waitForElement(`//button[.='Добавить участников']`, 3);
  I.click(`//button[.='Добавить участников']`);
  I.waitForElement(`//div[contains(text(), 'Поиск...')]`, 3);
  I.click(`//div[contains(text(), 'Поиск...')]`);
  I.fillField(`//input[@name='Выберите пользователя']`, userName);
  I.pressKey("Enter");

  I.click(`//button[@name='Add user to the current project']`);

  I.waitForElement(`//div[contains(text(),'${userName}')]`, 4);
  I.waitForElement(`//div[contains(text(),'A new member successfully has been added to the project!')]`, 3);
});