const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я вижу проект {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 3);
});

When('я ввожу  в поле {string} имя проекта {string}', (fieldName, text) => {
  I.fillField(`//input[@name='${fieldName}']`, text)
});

When('я ввожу в поле {string} новый чат Id {string}', (fieldName, text) => {
  I.fillField(`//input[@name='${fieldName}']`, text)
});

When('я ввожу новое значение {string} в поле {string}', (value, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, value);
});

Then('я вижу уведомление {string}', (text) => {
  I.waitForElement(`//div[contains(text(), '${text}')]`, 3);
});