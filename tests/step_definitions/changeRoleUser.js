const { I } = inject();

When('я нажимаю на вкладку {string}', (name) => {
  I.click(`//a[contains(., '${name}')]`);
});

When('я вижу {string}', (name) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 1);
});

When('я нахожу пользователя {string} и нажимаю на кнопку "Изменить"', (name) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 1);
  I.click(`//div[contains(text(),'${name}')]/ancestor::div[@class='rt-tr-group']/descendant::button[contains(text(), 'Изменить')]`);
});

When('я вижу модальное окно {string}', (name) => {
  I.waitForElement(`//h5[@class='modal-title' and contains(text(), '${name}')]`, 10);
});

When('я нажимаю на радио точку "не активный"', () => {
  I.click(`//div[@class='modal-body']//div[2]//div[1]//label[1]`);
});

When('я выбераю роль {string} вполе {string}', (role, optionName) => {
  I.selectOption(`//select[@name='${optionName}']`, role);
});

When('я ввожу в поле {string} пароль {string}', (fieldName, text) => {
  I.fillField(`//input[@name='${fieldName}']`, text);
});

When('я нажимаю кнопку чтобы сохранить', () => {
  I.click(`//i[@class='far fa-save']`);
});

When('я вижу что пользователь {string} теперь {string}', (name, role) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 1);
  I.waitForElement(`//div[contains(text(),'${name}')]/ancestor::div[@class='rt-tr-group']/descendant::div[contains(text(), '${role}')]`, 1);
});

Then('я вижу оповещение {string}', (text) => {
  I.seeNotification(text);
});