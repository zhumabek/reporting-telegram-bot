const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я добавляю новый проект под наименованием {string} и к проекту добавляю пользователя {string}', (projectName, userName) => {

    I.click(`//button[contains(text(), 'Добавить проект')]`);

    I.fillField(`//input[@placeholder='Введите имя проекта']`, projectName);

    I.fillField(`//input[@placeholder='Id чата']`, "-8898989899898");

    I.waitForElement(`//input[@placeholder='Выберите время']`, 1);

    I.fillField(`//input[@placeholder='Выберите время']`, "15:00");
    I.wait(1);

    I.click(`//button[.='Сохранить проект']`);
    I.waitForElement(`//div[contains(text(),'Поиск...')]`, 3);
    I.click(`//div[contains(text(),'Поиск...')]`);
    I.fillField(`//input[@name='Выберите пользователя']`, userName);
    I.pressKey('Enter');

    I.click(`//span[@class='fa fa-plus']`);
});

When('я нажимаю на кнопку написания отчета {string}', (btnName) => {
  I.waitForElement(`//button[.='${btnName}']`, 3);
  I.click(`//button[.='${btnName}']`);
});

When('я выбираю проект {string} в выпадающем списке {string}', (value, fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

When('я выбираю пользователя {string} в выпадающем списке {string}', (value, fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

When('я ввожу дату отчета {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text)
});

When('я выбираю поле {string}', (fieldName) => {
  I.click(`//input[@placeholder='${fieldName}']`)
});

When('я в поле {string} ввожу значение {string}', (fieldName, value) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, value);
});

When('я в текстовом поле {string} ввожу значение {string}', (fieldName, value) => {
  I.fillField(`//textarea[@placeholder='${fieldName}']`, value);
});

When('я выбираю статус тикета {string} в выпадающем списке {string}', (value, fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

When('я нажимаю на кнопку сохранения отчета {string}', (btnName) => {
  I.click(`//button[.='${btnName}']`);
});

When('я теперь нахожусь на странице "Отчеты" и вижу таблицу {string}', (name) => {
   I.waitForElement(`//div[contains(text(),'${name}')]`, 3);
});


