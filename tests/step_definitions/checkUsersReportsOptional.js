const {I} = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я нахожусь на странице {string}', (urlPage) => {
  I.seeCurrentUrlEquals(`${urlPage}`);
});

When('я выбираю проект {string}, чтобы отметить разработчика', (name) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 1);
  I.click(`//div[contains(text(),'${name}')]`);
  I.waitForElement(`//div[contains(text(), 'Редактирование проекта')]`, 1);
});

When('я выбираю {string} чтобы он не получал уведомления', (userName) => {
  I.click(`//button[.='Добавить участников']`);
  I.waitForElement(`//div[contains(text(),'${userName}')]`, 1);
  I.click(`//div[contains(text(),'${userName}')]`);
  I.seeNotification(`Разработчик не будет получать уведомления по данному проекту!`);
  I.click(`//button[.='Назад']`);
});

When('я нажимаю на кнопку {string}', (btnName) => {
  I.click(`//div[@class='py-1 col']//button[contains(text(), '${btnName}')]`);
});

Then('я вижу уведомление {string}', (text) => {
  I.seeNotification(text);
});

