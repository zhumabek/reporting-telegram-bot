const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я создаю проект под названием {string} и к проекту добавляю пользователя {string}', (projectName, userName) => {

  I.click(`//button[contains(text(), 'Добавить проект')]`);

  I.fillField(`//input[@placeholder='Введите имя проекта']`, projectName);
  I.pressKey('Enter');

  I.fillField(`//input[@placeholder='Id чата']`, "-34521453256");
  I.pressKey('Enter');

  I.waitForElement(`//input[@placeholder='Выберите время']`, 1);

  I.fillField(`//input[@placeholder='Выберите время']`, "15:00");
  I.pressKey('Enter');
  I.wait(1);

  I.click(`//button[.='Сохранить проект']`);
  I.waitForElement(`//div[contains(text(),'Поиск...')]`, 3);
  I.click(`//div[contains(text(),'Поиск...')]`);
  I.fillField(`//input[@name='Выберите пользователя']`, userName);
  I.pressKey('Enter');

  I.click(`//span[@class='fa fa-plus']`);
});

When('я нажимаю на кнопку написания отчета {string}', (btnName) => {
  I.waitForElement(`//button[.='${btnName}']`, 3);
  I.click(`//button[.='${btnName}']`);
});

When('я выбираю проект {string} в выпадающем списке {string}', (value, fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

When('я выбираю пользователя {string} в выпадающем списке {string}', (value, fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.wait(1);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

When('я ввожу дату отчета {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text);
  I.pressKey('Enter');
});

When('я вижу и переключаю флаг {string}', (fieldName) => {
  I.waitForElement(`//div[@class='align-items-center row form-group']//label[contains(text(),'${fieldName}')]`, 1);
  I.click(`//span[@class='switch-slider']`);
});

When('я вижу поле {string}', (fieldName) => {
  I.waitForElement(`//input[@placeholder='${fieldName}']`, 1)
});

When('я в поле {string} вижу его номер {string}', (fieldName, value) => {
  I.waitForValue(`//input[@placeholder='${fieldName}']`, value, 1);
});

When('я в поле {string} вижу значение {string}', (fieldName, value) => {
  I.waitForValue(`//input[@placeholder='${fieldName}']`, value, 1);
});

When('я в текстовом поле {string} ввожу значение {string}', (fieldName, value) => {
  I.fillField(`//textarea[@placeholder='${fieldName}']`, value);
  // I.pressKey('Enter');
});

When('я вижу статус тикета {string}', (value) => {
  I.waitForElement(`//div[contains(text(),'${value}')]`, 1);
});

When('я нажимаю на кнопку сохранения отчета {string}', (btnName) => {
  I.waitForElement(`//button[.='${btnName}']`, 1);
  I.click(`//button[.='${btnName}']`);
});

When('я теперь нахожусь на странице "Отчеты" и вижу таблицу {string}', (name) => {
  I.waitInUrl('/reports', 2);
  I.waitForElement(`//div[contains(text(),'${name}')]`, 7);
});


