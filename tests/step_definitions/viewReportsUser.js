const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я перехожу на страницу {string}', (fieldName) => {
  I.amOnPage(`${fieldName}`);
});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я нажимаю на поисковик', (fieldName) => {
  I.click(`//div[contains(@class,'css-1hwfws3')]`);
  I.wait(1);
});

When('я ввожу {string} в поисковике', (fieldName) => {
  I.fillField(`//div[@class='css-b8ldur-Input']`, fieldName);
  I.pressKey("Enter");
  I.wait(1);
});

When('я не вижу пользователя тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.dontSeeElement(`//div[contains(text(),'${fieldName}')]`);
});

Then('я вижу пользователя тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.waitForElement(`//div[@class='rt-tr -odd']//div[@class='rt-td'][contains(text(),'${fieldName}')]`, 1);
});