const { I } = inject();
Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я перехожу на страницу {string}', () => {
  I.amOnPage('/reports');
});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я нажимаю на поисковик', () => {
  I.click(`//div[contains(@class,'css-1hwfws3')]`);
});

When('я ввожу {string} в поисковике', (fieldName) => {
  I.fillField(`//div[@class='css-b8ldur-Input']`, fieldName);
  I.pressKey("Enter");
});

When('я для просмотра отчетов по выбранным датам нажимаю на кнопку {string}', (btnName) => {
  I.click(`//button[.='${btnName}']`);
});

Then('я чтобы сохранить отчет нажимаю на кнопку {string}', (btnName) => {
  I.click(`//button[.='${btnName}']`);
});
