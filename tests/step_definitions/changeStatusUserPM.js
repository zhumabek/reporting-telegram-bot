const { I } = inject();

When('я вижу что у пользователя {string} статус {string}', (name, status) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 1);
  I.waitForElement(`//div[contains(text(),'${name}')]/ancestor::div[@class='rt-tr-group']/descendant::p[contains(text(), '${status}')]`, 1);
});