const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я захожу на страницу {string}', (fieldName) => {
  I.amOnPage(fieldName);
});

When('я вижу таблицу {string}', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я нажимаю на поле {string}', () => {
  I.click(`//div[contains(@class,'css-1hwfws3')]`);
  I.wait(1);
});

When('я ввожу значение {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@id='react-select-2-input']`, text);
  I.pressKey("Enter");
  I.wait(1);
});

When('я нажимаю на кнопку выбора проекта {string}', () => {
  I.click(`//div[@class='col-sm-3']//button[@class='btn btn-primary']`);
  I.wait(1);
});

When('я вижу номер тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу наименование тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

When('я вижу пользователя тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});

Then('я вижу статус тикета {string} в таблице "Список тикетов к проекту"', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 1);
});
