const { I } = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я добавляю проект под названием {string} и к проекту добавляю пользователя {string}', (projectName, userName) => {

  I.click(`//button[contains(text(), 'Добавить проект')]`);

  I.fillField(`//input[@placeholder='Введите имя проекта']`, projectName);

  I.fillField(`//input[@placeholder='Id чата']`, "-55554445555");

  I.waitForElement(`//input[@placeholder='Выберите время']`, 1);

  I.fillField(`//input[@placeholder='Выберите время']`, "16:00");
  I.pressKey('Enter');

  I.click(`//button[.='Сохранить проект']`);

  I.click(`//div[contains(text(),'Поиск...')]`);
  I.fillField(`//input[@name='Выберите пользователя']`, userName);
  I.pressKey('Enter');

  I.click(`//span[@class='fa fa-plus']`);
});

When('я нажимаю на кнопку написания отчета {string}', (btnName) => {
  I.waitForElement(`//button[.='${btnName}']`, 3);
  I.click(`//button[.='${btnName}']`);
});

When('я добавляю новый тестовый {string} для проверки редактирования дополнительного отчета', (fieldName) => {

  const data = {
    project: { field: 'Выберите проект', value: 'Проект для проверки редактирования дополнительного отчета'},
    user: {field: 'Выберите пользователя', value: 'adievkadyrbek'},
    date: {field: 'Дата отчета', value: '15.01.2020'},
    ticketNumber: { field: 'Номер тикета', value: '9000'},
    ticketName: { field: 'Наименование тикета', value: fieldName},
    reportDescription: { field: 'Описание отчета', value: 'Описание отчета'},
    timeSpent: { field: 'Потраченное время', value: '6'},
    status: {value: 'closed'},
  };

  I.waitForElement(`//div[contains(text(),'${data.project.field}')]`, 1);
  I.click(`//div[contains(text(),'${data.project.field}')]`);
  I.fillField(`//input[@name='${data.project.field}']`, data.project.value);
  I.pressKey('Enter');

  I.click(`//div[contains(text(),'${data.user.field}')]`);
  I.fillField(`//input[@name='${data.user.field}']`, data.user.value);
  I.pressKey('Enter');

  I.fillField(`//input[@placeholder='${data.date.field}']`, data.date.value);
  I.pressKey('Enter');

  I.waitForElement(`//div[@class='align-items-center row form-group']//label[contains(text(),'${fieldName}')]`, 1);
  I.click(`//span[@class='switch-slider']`);

  I.waitForElement(`//input[@placeholder='${data.ticketNumber.field}']`, 1);
  I.waitForValue(`//input[@placeholder='${data.ticketNumber.field}']`, data.ticketNumber.value, 1);

  I.waitForValue(`//input[@placeholder='${data.ticketName.field}']`, data.ticketName.value, 1);

  I.fillField(`//textarea[@placeholder='${data.reportDescription.field}']`, data.reportDescription.value);

  I.fillField(`//input[@placeholder='${data.timeSpent.field}']`, data.timeSpent.value);

  I.waitForElement(`//div[contains(text(),'${data.status.value}')]`, 1);

  I.click(`//button[.='Сохранить отчет']`);
  I.waitForElement(`//div[contains(text(),'Список отчетов')]`, 3);
});

When('я выбираю отчеты {string}', (project) => {
  I.click(`//label[contains(text(),'${project}')]`);
});

When('я выбираю проект {string} в выпадающем списке {string}', (value, fieldName) => {
  I.click(`//div[contains(text(),'${fieldName}')]`);
  I.wait(1);
  I.fillField(`//input[@name='${fieldName}']`, value);
  I.pressKey('Enter');
});

When('я вижу отчет под номером тикета {string} в таблице отчетов и нажимаю на отчет', (fieldName) => {
  I.waitForElement(`//div[contains(text(),'${fieldName}')]`, 2);
  I.click(`//div[contains(text(),'${fieldName}')]`);
});

When('я вижу модальное окно в котором есть кнопка "Отредактировать отчет" и нажимаю на него', () => {
  I.waitForElement(`//div[@class='modal-content']`, 2);
  I.click(`//div[@class='modal-footer']//button[@class='bot-btn-st btn btn-primary']`);
});

When('я ввожу новую дату отчета {string} в поле {string}', (text, fieldName) => {
  I.fillField(`//input[@placeholder='${fieldName}']`, text);
  I.pressKey('Enter');
});

When('я в поле {string} вижу значение {string}', (fieldName, value) => {
  I.waitForValue(`//input[@placeholder='${fieldName}']`, value, 1);
});

When('я в текстовом поле {string} вижу значение {string}', (fieldName, value) => {
  I.waitForValue(`//input[@placeholder='${fieldName}']`, value, 1);
});

When('я вижу статус тикета {string} в выпадающем списке {string}', (value) => {
  I.waitForElement(`//div[contains(text(),'${value}')]`, 1);
});

When('я нажимаю на кнопку сохранения отчета {string}', (btnName) => {
  I.waitForElement(`//button[.='${btnName}']`, 1);
  I.click(`//button[.='${btnName}']`);
});

Then('я теперь нахожусь на странице "Отчеты" и вижу таблицу {string}', (name) => {
  I.waitForElement(`//div[contains(text(),'${name}')]`, 3);
});
