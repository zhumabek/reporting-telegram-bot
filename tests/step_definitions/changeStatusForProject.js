const {I} = inject();

Given('я авторизованный пользователь {string}', name => {
  I.loginAsUser(name);
});

When('я вижу проект {string} и его статус {string}', (project, status) => {
  I.waitForElement(`//div[contains(text(),'${project}')]/ancestor::div[@class='rt-tr -even']/descendant::p[contains(text(),'${status}')]`, 1);
});

When('я нажимаю на кнопку {string} у проекта {string}', (btn, project) => {
  I.wait(1);
  I.click(`//div[contains(text(),'${project}')]/ancestor::div[@class='rt-tr -even']/descendant::button[contains(text(),'${btn}')]`);
});

When('я вижу оповещение {string}', (text) => {
  I.wait(1);
  I.seeNotification(text);
});

Then('я вижу статус {string} у проект {string}', (status, project) => {
  I.waitForElement(`//div[contains(text(),'${project}')]`, 1);
  I.waitForElement(`//div[@class='rt-tr -odd']//p[contains(text(),'${status}')]`, 1);
});